<?php
class Model_Attendance_Management extends Model_Master {
	
	public $strHierarchy;
		
	function __construct() {
		 parent::__construct();
	}
	
	function getAttendanceRecordsPK($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $doSort = true, $filterStr = false) {
				
		$db = mysqli_connect(ATTENDANCE_DB_HOST_PK, $this->config->item('attendance_db_pk'));
		$strQuery = "SELECT U.Badgenumber as USERID, min(C.CHECKTIME) as 'IN', max(C.CHECKTIME) as 'OUT', cast(C.CHECKTIME as date) as 'DATE' FROM " . TABLE_ATTENDANCE_PK . " C left join USERINFO U ON U.USERID = C.USERID WHERE ";
		
		if($arrWhere['USERID'] != '') {
			$strQuery .= " U.Badgenumber = " . (int)$arrWhere['USERID'];
		} else if($arrWhere['USERID in '] != '') {
			$strQuery .= " U.Badgenumber in (" . $arrWhere['USERID in '] . ")";
		}
		if($arrWhere['DATE <= '] != '') {
			$strQuery .= " AND C.CHECKTIME <= '" . $arrWhere['DATE <= '] . "'";
		}
		if($arrWhere['DATE >= '] != '') {
			$strQuery .= " AND C.CHECKTIME >= '" . $arrWhere['DATE >= '] . "'";
		}
		
		$strQuery .= " GROUP BY U.Badgenumber, cast(C.CHECKTIME as date) ORDER BY U.Badgenumber, 'DATE' ASC";
		
		$objResult = mysqli_query($db, $strQuery);
		$arrResult = array();
		
		while( $arrRow = mysqli_fetch_array( $objResult, SQLSRV_FETCH_ASSOC) ) {
			$arrResult[] = $arrRow;
		}
		
		return $arrResult;
	}
	
	function getAttendanceRecordsDxb($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $doSort = true, $filterStr = false) {
				
		$db = mysqli_connect(ATTENDANCE_DB_HOST_DXB, $this->config->item('attendance_db_dxb'));
		$strQuery = "SELECT UserID AS USERID, max(Punch1) as 'IN', max(OutPunch) as 'OUT', cast(PDate as date) as 'DATE' FROM " . TABLE_ATTENDANCE_DXB . " WHERE ";
		
		if($arrWhere['USERID'] != '') {
			$strQuery .= " USERID = " . (int)$arrWhere['USERID'];
		} else if($arrWhere['USERID in '] != '') {
			$strQuery .= " USERID in (" . $arrWhere['USERID in '] . ")";
		}
		
		if($arrWhere['DATE <= '] != '') {
			$strQuery .= " and PDate <= '" . $arrWhere['DATE <= '] . "'";
		}
		if($arrWhere['DATE >= '] != '') {
			$strQuery .= " and PDate >= '" . $arrWhere['DATE >= '] . "'";
		}
		
		$strQuery .= " GROUP BY USERID, cast(PDate as date) ORDER BY USERID, 'DATE' ASC";
		
		$objResult = mysqli_query($db, $strQuery);
		$arrResult = array();
		
		while( $arrRow = mysqli_fetch_array( $objResult, SQLSRV_FETCH_ASSOC) ) {
			$arrResult[] = $arrRow;
		}
		
		return $arrResult;
	}
	
	function getClockingRecordsPK($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $doSort = true) {
		
		$db = mysqli_connect(ATTENDANCE_DB_HOST_PK, $this->config->item('attendance_db_pk'));
		$strQuery = "SELECT U.Badgenumber as USERID, C.CHECKTIME as 'DATE' FROM " . TABLE_ATTENDANCE_PK . " C left join USERINFO U ON U.USERID = C.USERID WHERE ";
		
		if($arrWhere['USERID'] != '') {
			$strQuery .= " U.Badgenumber = " . (int)$arrWhere['USERID'];
		} else if($arrWhere['USERID in '] != '') {
			$strQuery .= " U.Badgenumber in (" . $arrWhere['USERID in '] . ")";
		}
		if($arrWhere['DATE <= '] != '') {
			$strQuery .= " AND C.CHECKTIME <= '" . $arrWhere['DATE <= '] . "'";
		}
		if($arrWhere['DATE >= '] != '') {
			$strQuery .= " AND C.CHECKTIME >= '" . $arrWhere['DATE >= '] . "'";
		}
		
		$strQuery .= " ORDER BY 'DATE' ASC";
		
		$objResult = mysqli_query($db, $strQuery);
		$arrResult = array();
		
		while( $arrRow = mysqli_fetch_array( $objResult, SQLSRV_FETCH_ASSOC) ) {
			$arrResult[] = $arrRow;
		}
		
		return $arrResult;
	}
	
	function getClockingRecordsDxb($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $doSort = true) {
		
		$db = mysqli_connect(ATTENDANCE_DB_HOST_DXB, $this->config->item('attendance_db_dxb'));
		$strQuery = "SELECT UserID AS USERID, Edatetime as 'DATE' FROM " . TABLE_ATTENDANCE_CLOCKING_DXB . " WHERE ";
		
		if($arrWhere['USERID'] != '') {
			$strQuery .= " USERID = " . (int)$arrWhere['USERID'];
		} else if($arrWhere['USERID in '] != '') {
			$strQuery .= " USERID in (" . $arrWhere['USERID in '] . ")";
		}
		
		if($arrWhere['DATE <= '] != '') {
			$strQuery .= " and Edatetime <= '" . $arrWhere['DATE <= '] . "'";
		}
		if($arrWhere['DATE >= '] != '') {
			$strQuery .= " and Edatetime >= '" . $arrWhere['DATE >= '] . "'";
		}
		
		$strQuery .= " ORDER BY 'DATE' ASC";
		
		$objResult = mysqli_query($db, $strQuery);
		$arrResult = array();
		
		while( $arrRow = mysqli_fetch_array( $objResult, SQLSRV_FETCH_ASSOC) ) {
			$arrResult[] = $arrRow;
		}
		
		return $arrResult;
	}
	
	function getLeaves($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
				
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
			
			if(strpos($key, 'custom_string') !== false) {
				$this->db->where($arrWhere[$key], NULL, false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$this->db->order_by('l.leave_id', 'DESC');
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}		
		
		$objResult = $this->db->get(TABLE_ATTENDANCE_LEAVES . ' l ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTotalLeaves($arrWhere = array()) {
		
		$this->db->select(' count(*) as total_count ');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
			
			if(strpos($key, 'custom_string') !== false) {
				$this->db->where($arrWhere[$key], NULL, false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}		
		
		$objResult = $this->db->get(TABLE_ATTENDANCE_LEAVES . ' l ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return (int)$arrResult[0]['total_count'];
	}
	
	function getLeavesForApproval($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $doSort = true) {
		
		$this->db->distinct();
		$this->db->select(' e.emp_full_name, e.emp_authority_id, e.emp_annual_leaves, e.emp_sick_leaves, l.* ');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = l.emp_id', 'left');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.emp_id = e.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
			
			if(strpos($key, 'custom_string') !== false) {
				$this->db->where($arrWhere[$key], NULL, false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}		
		
		$this->db->order_by('l.leave_id', 'DESC');
		
		$objResult = $this->db->get(TABLE_ATTENDANCE_LEAVES . ' l ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTotalLeavesForApproval($arrWhere = array()) {
		
		$this->db->distinct();
		$this->db->select(' count(*) as total_count ');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = l.emp_id', 'left');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.emp_id = e.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
			
			if(strpos($key, 'custom_string') !== false) {
				$this->db->where($arrWhere[$key], NULL, false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$objResult = $this->db->get(TABLE_ATTENDANCE_LEAVES . ' l ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return (int)$arrResult[0]['total_count'];
	}
	
	function getLeavesSummary($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db_attend = $this->load->database('attendance', TRUE);
		
		if(count($arrWhere)) {
			$this->db_attend->where($arrWhere);			
		}		
		
		$this->db_attend->order_by('emp_code', 'ASC');
		
		$objResult = $this->db_attend->get(TABLE_ATTENDANCE_SUMMARY);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getLeaveDetails($leaveID) {
		
		$this->db->select(' l.*, lc.leave_category, e.emp_full_name, e.emp_designation, e.emp_annual_leaves, e.emp_sick_leaves, e.emp_flexi_leaves, e.emp_edu_leaves, e.emp_maternity_leaves, e1.emp_full_name as processed_by_name ');
		$this->db->join(TABLE_ATTENDANCE_LEAVE_CATEGORIES . ' lc ', 'lc.leave_category_id = l.leave_category', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = l.emp_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e1 ', 'e1.emp_id = l.processed_by', 'left');
		
		$this->db->where(array('leave_id' => (int)$leaveID));
		
		$objResult = $this->db->get(TABLE_ATTENDANCE_LEAVES . ' l ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult[0];
	}
}
?>