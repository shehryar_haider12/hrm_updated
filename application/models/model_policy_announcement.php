<?php
class Model_Policy_Announcement extends Model_Master {
	
	function __construct() {
		 parent::__construct();	
	}
		
	function getAnnouncements($arrWhere = array(), $rowsLimit = 0, $rowsOffset = 0) 
	{
		$this->db->select(' a.*, c.company_name ');
		
		$this->db->join(TABLE_COMPANIES . ' c ', 'c.company_id = a.ann_company_id', 'left');
		
		if((int)$arrWhere['ann_company_id']) {
			$this->db->where(' (ann_company_id = ' . (int)$arrWhere['ann_company_id'] . ' or ann_company_id = 0) ', null, false);
			unset($arrWhere['ann_company_id']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('c.company_name', 'ASC');
		$this->db->order_by('c.company_name, a.created_date', 'DESC');
		$results = $this->db->get(TABLE_ANNOUNCEMENTS . ' a ');
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;		
	}
}
?>