<?php
$baseURL	= (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on') ? 'https://' : 'http://' . $_SERVER['HTTP_HOST'];
$baseURL = $baseURL . "/HRM";
?>
<!DOCTYPE html>
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>PCE - HRMS</title>
	<link type="text/css" rel="stylesheet" href="<?php echo $baseURL; ?>css/style.css">
	<link type="text/css" rel="stylesheet" href="<?php echo $baseURL; ?>css/reset.css">
	<script type="text/javascript" src="<?php echo $baseURL; ?>js/common.js"></script>
	<style>
@font-face {
 font-family:"OSWALD-LIGHT";
 src:url(<?php echo $baseURL; ?>font/OSWALD-LIGHT.TTF) format("truetype");
}
</style>
	</head>
	<body>
    <div class="mainBody">
      <div class="mainDiv"> 
        
        <!----- Header Start ----->
        <div class="headerMain">
          <div class="headerTitle"> <a href="<?php echo $baseURL; ?>home">PCE Employee Dashboard - HRMS</a> </div>
        </div>
        <!----- Header End -----> 
        
        <!----- Mid Section Start  ----->
        <div class="midMain"> 
          <!-----	Message Box Start ----->
          <div class="accessDeniedMain">
            <div class="accessDenied"> <img src="<?php echo $baseURL; ?>/images/page-not-available.jpg" alt="Page Not Found"> <br />
              <br />
              <input type="button" onclick="history.go(-1)" value="Back" class="smallButtonMsg" />
            </div>
          </div>
          <!-----	Message Box End -----> 
        </div>
        <!----- Mid Section End -----> 
        
        <!----- Footer Start ----->
        <div class="footerMain">
          <div class="footerText"> Copyright <?php echo date('Y'); ?> PCE. All Rights Reserved. </div>
        </div>
        <!----- Footer End -----> 
        
      </div>
    </div>
</body>
</html>