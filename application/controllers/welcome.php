<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends Master_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	private $arrData = array();
	 
	function __construct() {
		
		parent::__construct();
		$this->arrData["baseURL"] = $this->baseURL;
		$this->arrData["imagePath"] = $this->imagePath;
		$this->arrData["modulesAllowed"] = $this->modulesAllowed;
		$this->arrData["dateTimeFormat"] = DATE_TIME_FORMAT;
	}
	
	public function index()
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|md5');
		$candidatewelcomeFlag = (int)$this->input->post('candidatewelcome');

		if ($this->form_validation->run() == true) {
			
			$inOffice = true;
			
			$this->load->model('Model_User_Management', 'users', true);
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$arrUser = $this->users->isValidUser($username, $password);
			
			if($arrUser) {
				
				if($arrUser['user_role_id'] == 1) {
					$this->userWelcomeName = $this->session->set_userdata('display_name', $arrUser['candidate_name']);
				} else {
					if((int)CHECK_OFFICE_IP_SECURITY) {
						if(!in_array(getIP(), $this->config->item('office_ips')) && !in_array($arrUser['emp_id'], $this->config->item('allowed_employees'))) {
							$this->arrData['error_message'] = 'Attention !!! It is not allowed for Naval Anchorage Club employees to access Dashboard outside office premises';
							$inOffice = false;
						}
					}
					$this->userWelcomeName = $this->session->set_userdata('display_name', $arrUser['emp_full_name']);
				}
				
				if($inOffice) {
					$this->session->set_userdata('user_name', $username);
					$this->session->set_userdata('user_id', $arrUser['user_id']);
					$this->session->set_userdata('employee_id', $arrUser['emp_id']);
					$this->session->set_userdata('user_role_id', $arrUser['user_role_id']);
					$this->session->set_userdata('emp_job_category_id', $arrUser['emp_job_category_id']);
					$this->session->set_userdata('emp_company_id', $arrUser['emp_company_id']);
					$this->session->set_userdata('display_name', $arrUser['employee_name']);
					$this->session->set_userdata('location_id', $arrUser['emp_location_id']);
					
					# SET SBT DISCUSSION FORUM COOKIE
					$this->cookieSetUnset(false, $arrUser['emp_id'], $username);					
					
					# SET LOG
					if((int)$arrUser['emp_id']) {
						debugLog("Log-In", "", "true", $arrUser['emp_id'], 1);
					}
					
					if($this->session->userdata('came_from') != '') {
						$strRedirectURL = $this->session->userdata('came_from');
						$this->session->unset_userdata('came_from');
						redirect($strRedirectURL);
					} else {
						$arrValues = array(
											'last_login_date' => date($this->arrData["dateTimeFormat"])
											);
						$this->users->saveValues(TABLE_USER, $arrValues, array('user_id' => $this->session->userdata('user_id')));
						redirect(base_url() . 'home');
					}
					
					exit;
				}
				
			} else {
				
				# SET LOG
				
				$this->arrData['error_message'] = 'You have entered an incorrect Username or Password';
			}
		}
		else
			$this->arrData['validation_error_message'] = validation_errors();
		if($candidatewelcomeFlag == 1)
		{
			$this->session->set_userdata('error_message', $this->arrData['error_message']);
			$this->session->set_userdata('validation_error_message', validation_errors());
			redirect(base_url() . 'career?fail='.$candidatewelcomeFlag);
		}
		
		$this->template->write_view('content', 'welcome_message', $this->arrData);
		$this->template->render();
	}
	
	public function forgot_password()	{		
		
		$this->load->model('Model_User_Management', 'user', true);
		$this->load->model('Model_Employee_Management', 'employee', true);
		$this->load->model('Model_Recruitment_Management', 'recruitment', true);
		
		#################################### FORM VALIDATION START ####################################
		
		$this->form_validation->set_rules('userName', 'User Name', 'trim|required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('userDOB', 'Date of Birth', 'trim|required|exact_length[10]|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			
			$this->arrData['strUserName'] = $this->input->post('userName');
			$this->arrData['strUserDOB'] = $this->input->post('userDOB');
										
			$arrUser = $this->user->getUsers(array('u.user_name' => $this->arrData['strUserName']));
			$arrUser = $arrUser[0];
				
			$arrUserRecord = $this->employee->getEmployees(array('e.emp_id' => $arrUser['employee_id']));
			$arrUserRecord = $arrUserRecord[0];
			
			if(count($arrUser) && $arrUserRecord['emp_dob'] == $this->arrData['strUserDOB']) {
				
				
				# SHOOT EMAIL
				# Template: retrieve_login_details_emp.html
			
				$arrValues = array(
									'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
									'[EMPLOYEE_NAME]' => $arrUserRecord['emp_full_name'],
									'[EMPLOYEE_DASHBOARD_LINK]' => $this->baseURL,
									'[EMPLOYEE_USERNAME]' => $arrUser['user_name'],
									'[EMPLOYEE_PASSWORD]' => $arrUser['plain_password'],
									'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' Naval Anchorage Club. All Rights Reserved.'
									);
									
				$emailHTML = getHTML($arrValues, 'retrieve_login_details_emp.html');
		
				$this->sendEmail(
									$arrTo = array(
													$arrUserRecord['emp_work_email']
													), 										# RECEIVER DETAILS
									'Account Details' . EMAIL_SUBJECT_SUFFIX, 				# SUBJECT
									$emailHTML												# EMAIL HTML MESSAGE
								);
								
				# SET LOG
				
				$this->session->set_flashdata('success_message', 'Your account details are sent to ' . $arrUserRecord['emp_work_email']);
				redirect($this->baseURL . '/' . $this->currentController);
			} else {
				
				# SET LOG
				
				$this->arrData['validation_error_message'] = 'Invalid Details Provided';
			}
			
		} else {
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# TEMPLATE LOADING		
		$this->template->write_view('content', 'forgot_password', $this->arrData);
		$this->template->render();
	}
	
	function logoff() {
		$this->session->sess_destroy();
				
		# SET LOG
				
		redirect(show_url() . 'welcome');	
		exit;		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */