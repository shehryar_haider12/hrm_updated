<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lunch_Management extends Master_Controller {
		
	private $arrData = array();
	private $maxLinks;
	private $limitRecords;
	private $delimiter = '-';
	
	function __construct() {
		
		parent::__construct();
		
		$this->arrData["baseURL"]				= $this->baseURL;
		$this->arrData["imagePath"]				= $this->imagePath;
		$this->arrData["screensAllowed"]		= $this->screensAllowed;
		$this->arrData["dateTimeFormat"] 		= DATE_TIME_FORMAT;
		$this->arrData["dateFormat"] 			= DATE_FORMAT;
		$this->arrData["showDateFormat"] 		= SHOW_DATE_TIME_FORMAT;
		$this->maxLinks 						= MAX_PAGING_VISIBLE;
		$this->limitRecords 					= MAX_RECORDS_LISTING;
		
		$currentActionArray = array();
		while(!empty($this->arrData["screensAllowed"]))
		{
			$temp = array_pop($this->arrData["screensAllowed"]);
			if($temp['module_name']== $this->currentAction){
				array_push($currentActionArray,$temp);
			}
		}
		$currentActionArray = $currentActionArray[0];
		$this->arrData['canWrite'] = $currentActionArray["can_write"];
		$this->arrData['canDelete'] = $currentActionArray["can_delete"];
	}
	
	public function index()
	{
		$moduleID = getValue($this->modulesAllowed, 'module_name', $this->currentController, 'module_id');
		$this->arrData['allowedSubModulesList'] = getValue($this->modulesAllowedForMenu, 'module_name', $this->currentController, 'sub_menu'); 
		$this->template->write_view('content', 'lunch_management/index', $this->arrData);
		$this->template->render();
	}
}

/* End of file user_management.php */
/* Location: ./application/controllers/user_management.php */