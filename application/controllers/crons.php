<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crons extends CI_Controller {
	 
	public $arrData = array();
	public $currentController;
	private $maxLinks;
	private $limitRecords;
	
	
	function __construct() {
		
		parent::__construct();
		
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		date_default_timezone_set("Asia/Dubai");
		$this->load->helper('common');
		$this->load->model('model_master', 'master', true);
		$this->load->model('model_system_configuration', 'configuration', true);
		
		$this->setupConfig($this->configuration->getSettings()); # SETUP ALL SYSTEM WIDE VARIABLES REQUIRED IN CODE (TABLE hrm_configuration)
		$this->setupEmail(); # SETUP EMAIL ATTRIBUTES IF SMTP DETAILS ARE AVAILABLE
	
		$this->load->model('model_employee_management', 'employee', true);
		$this->load->model('model_recruitment_management', 'recruitment', true);
		$this->load->model('model_user_management', 'user_management', true);
		$this->load->model('model_complain_management', 'complain', true);
		$this->load->model('model_attendance_management', 'attendance', true);
		$this->load->model('model_payroll_management', 'payroll', true);
		$this->load->model('model_sbt_forum', 'forum', true);
		
		$this->arrData["baseURL"] 				= $this->config->item("base_url");
		$this->arrData["imagePath"] 			= $this->config->item("image_path");
		$this->arrData["dateFormat"] 			= DATE_FORMAT;
		$this->arrData["dateTimeFormat"] 		= DATE_TIME_FORMAT;
		$this->arrData["showDateFormat"] 		= SHOW_DATE_TIME_FORMAT;
		$this->arrData["emailTemplatesFolder"]	= EMAIL_TEMPLATE_FOLDER;
	}
	
	function debugLogCron($msg, $logFilename = "dashboardcron.log", $live = "true")	{
		if($live)
		{
			set_time_limit(0);
			ini_set("error_log", CRONLOGS_PATH.$logFilename);
			error_log($msg);
		}
		else
		{
			print("<br>[$msg]");
		}
	}
	
	
	
	
	
	
	public function appraisalNotification2($byPass = 0)
	{
	
	    $cronTime = '09:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
		            # SHOOT EMAIL
					# Template: tms_notificaiton.html
					
					$strEmailTxt = 'This is a gentle reminder, if you have not submitted/completed your Online Self-Appraisal form, please take out few minutes and login to the HRM by clicking on the below link.<br/><br />';
					
					$strEmailTxt.= 'http://hrnetvs.no-ip.biz:7080/HRM/task_management/my_tasks<br/><br />';	
															
					$strEmailTxt.= 'The final last date to complete/submit the online form is extended to <b>25th July 2019.</b><br/><br />';			
										
					
					
                    $strEmailTxt.= 'If you have already submitted, kindly ignore this email.<br/><br />';			
					
                                 
											
					
					
					$arrValues = array(
							'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
							'[EMPLOYEE_NAME]' => 'Recipient',
							'[MESSAGE_BODY]' => $strEmailTxt,
							'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' Naval Anchorage Club. All 

Rights Reserved.'
							);
							
				$emailHTML = getHTML($arrValues, 'general.html');
				//die($emailHTML);
				
					
					//die($emailHTML);
					
					//$senderEmail = 'Zeeshan.Qasim@NavalAnchorageClub.com';
					//$arrTo = array('zeeshan.qasim@NavalAnchorageClub.com','Coleen.Franklin@Naval Anchorage Club.com','mehreen.nawaz@Naval Anchorage Club.com');
					
 // $arrTo = array('TeamDubai@Naval Anchorage Club.com','teamkarachi@NavalAnchorageClub.com','TeamQatar@NavalAnchorageClub.com','TeamKSA@Naval Anchorage Club.com','Julian.Marwitz@Naval Anchorage Club.com');
				//$arrTo = array('zeeshan.qasim@NavalAnchorageClub.com');
				
				 $this->sendEmail(
										$arrTo,//array('Zeeshan.Qasim@Naval Anchorage Club.com','Soheb.Sultan@Naval Anchorage Club.com'),												# RECEIVER DETAILS                    
										'Self Appraisal Reminder' . EMAIL_SUBJECT_SUFFIX	,			# SUBJECT
									    $emailHTML
										
									
										
										
																				//print_r(error_get_last())
										
										
																				# EMAIL HTML MESSAGE
									);
				
				
				//echo $fromEmail;
									
				}
				echo 'yes';
	
	}
	
	
	public function sendBdayAlert($byPass = 0) {
		
		$cronTime = '09:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
			
			set_time_limit(0);
			
			$strDate = date('Y-m-d', strtotime('+2 day'));
			$arrDate = explode('-', $strDate);
			
			$this->db->select(' e.emp_id, e.emp_full_name, e.emp_dob, l.location_name ');
			$this->db->join(TABLE_LOCATION . ' l ', 'e.emp_location_id = l.location_id', 'left');
			$this->db->where(array('e.emp_status' => 1, 'e.emp_id > ' => 1, 'e.emp_employment_status < ' => 

STATUS_EMPLOYEE_SEPARATED));
			$this->db->like('e.emp_dob', '-' . $arrDate[1] . '-' . $arrDate[2]); 
			
			$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
			$arrEmployees = $objResult->result_array();
			$objResult->free_result();
			
			$arrTo = array();
			$arrTo[] = SYSTEM_EMAIL_ADDRESS;
			
			if(count($arrEmployees) > 0) {
				
				$strEmailTxt = 'Below is the list of employee(s) whose birthday is in next 2 days.<br 

/><br />';
				
				$strEmailTxt .= '<table align="center" cellspacing="0" cellpadding="0" style="border:3px 

solid #89CFE6;color:#3E3E3E;font-family:arial;font-size:14px;line-height:24px;padding:15px;width:600px">
									<tr>
										<th width="250px" 

align="center">Employee</th>
										<th align="center">Location</th>
										<th align="center">Birth Date</th>
									</tr>
									<tr>
										<th colspan="3" width="350px" 

align="center"><hr /></th>
									</tr>';
				for($ind = 0; $ind < count($arrEmployees); $ind++) {
					
					$arrSupervisors = $this->employee->getEmpSupervisorsDetails(array('es.emp_id' => 

$arrEmployees[$ind]['emp_id']));
					foreach($arrSupervisors as $arrSupervisor) {
						$arrTo[] = $arrSupervisor['emp_work_email'];
					}
					
					$strEmailTxt .= '<tr>
											<td>' . $arrEmployees[$ind]

['emp_full_name'] . '</td>
											<td>' . $arrEmployees[$ind]

['location_name'] . '</td>
											<td>' . date

(SHOW_DATE_TIME_FORMAT, strtotime($arrEmployees[$ind]['emp_dob'])) . '</td>
										</tr>';
				}
				
				$strEmailTxt .= '</table>';
				
				# SHOOT EMAIL
				# Template: general.html
				
				$arrValues = array(
							'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
							'[EMPLOYEE_NAME]' => 'Recipient',
							'[MESSAGE_BODY]' => $strEmailTxt,
							'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' Naval Anchorage Club. All 

Rights Reserved.'
							);
							
				$emailHTML = getHTML($arrValues, 'general.html');
				//die($emailHTML);
				$emailFlag = $this->sendEmail(
									$arrTo, 						

											# RECEIVER DETAILS
									'Birthday Reminder' . EMAIL_SUBJECT_SUFFIX,	 	

							# SUBJECT
									$emailHTML						

											# EMAIL HTML MESSAGE
								);
				echo 'Alert Sent';
			}
			
			echo 'Cron Done';
			
		}
	}
	
	public function sendBdayGreetings($byPass = 0) {
		
		$cronTime = '09:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
			
			set_time_limit(0);
			
			$this->db->select(' e.emp_full_name, e.emp_work_email ');
			$this->db->where(array('e.emp_status' => 1, 'e.emp_id > ' => 0, 'e.emp_work_email != ' => '', 

'e.emp_employment_status < ' => STATUS_EMPLOYEE_SEPARATED));
			$this->db->like('e.emp_dob', '-' . date('m') . '-' . date('d')); 
			
			$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
			$arrEmployees = $objResult->result_array();
			$objResult->free_result();
			
			for($ind = 0; $ind < count($arrEmployees); $ind++) {
				
				# SHOOT EMAIL
				
				if(!empty($arrEmployees[$ind]['emp_work_email'])) {
					
					$arrValues = array(
										'[HEADER_LOGO_LINK]' 		=> EMAIL_HEADER_LOGO,
										'[EMPLOYEE_NAME]'			=> $arrEmployees[$ind]['emp_full_name'],
										'[BIRTHDAY_CARD_LINK]' 		=> $this->arrData["baseURL"] . '/images/birthday-card.png',
										'[COPYRIGHT_TEXT]' 			=> 'Copyright ' . date('Y') . ' Naval Anchorage Club. All Rights Reserved.'
										);
										
					$emailHTML = getHTML($arrValues, 'birthday_greeting.html');
					
					$emailFlag = $this->sendEmail(
										$arrTo = array(
														

$arrEmployees[$ind]['emp_work_email']				# RECEIVER EMAIL
														), 		

											# RECEIVER DETAILS
										'Birthday Greetings' . 

EMAIL_SUBJECT_SUFFIX, 						# SUBJECT
										$emailHTML					

										# EMAIL HTML MESSAGE
									);
				}
			}
			
			echo $ind . ' Records Done';
			
		}
	}
	
	public function leavesQuotaRevision($byPass = 0) {
		
		$cronTime = '09:00';
		$cronDay = '1';
		
		if((date('H:i') == $cronTime && date('j') == $cronDay) || $byPass == 13579) {
			
			set_time_limit(0);
			
			$this->db->select(' e.*, c.* ');
			$this->db->join(TABLE_COMPANIES . ' c ', 'e.emp_company_id = c.company_id', 'left');
			$this->db->where(
								array(
										'e.emp_id > ' => 1,
										'e.emp_status' => 1,
										'e.emp_employment_status < ' => 

STATUS_EMPLOYEE_SEPARATED
									)
								);
	
			$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			
			for($ind = 0; $ind < count($arrResult); $ind++) {
				
				$cronMonth = date('n');
				$sickLeaves = 0;
				$annualLeaves = 0;
				$annualProRataFactor = round(($arrResult[$ind]['company_annual_leaves'] / 12), 1);
				
				if($cronMonth == 1) {
					$sickLeaves = $arrResult[$ind]['company_sick_leaves'];
					$annualLeaves = ((((int)$arrResult[$ind]['emp_annual_leaves'] > 5) ? 5 : 

$arrResult[$ind]['emp_annual_leaves']) + $annualProRataFactor);
				} else {
					$sickLeaves = $arrResult[$ind]['emp_sick_leaves'];
					$annualLeaves = ($arrResult[$ind]['emp_annual_leaves'] + $annualProRataFactor);
				}
				
				$arrValues = array(
								   'emp_annual_leaves' 	=> $annualLeaves,
								   'emp_sick_leaves' 	=> $sickLeaves
								  );
				
				$this->employee->saveValues(TABLE_EMPLOYEE, $arrValues, array('emp_id' => $arrResult

[$ind]['emp_id'])); 
			}
		}
		
		echo (int)$ind . ' Records Processed';
		exit;
	}
	
	/*public function leavesAutoReject() {
		
		$arrWhere 					= array();
		$arrWhere['leave_status'] 	= 0;
		$arrWhere['custom_string'] 	= " (time_to_sec(timediff(now(), created_date )) / 3600) > 72 ";		

//	3 Days calculation
		$arrRecords					= $this->attendance->getLeaves($arrWhere);
		
		for($ind = 0; $ind < count($arrRecords); $ind++) {
			
			$arrValues = array(
							   'leave_status' 	=> 2,
							   'processed_by' 	=> 1,
							   'processed_date'	=> date($this->arrData["dateTimeFormat"])
							  );
			
			$this->attendance->saveValues(TABLE_ATTENDANCE_LEAVES, $arrValues, array('leave_id' => 

$arrRecords[$ind]['leave_id'])); 
			
			#	SHOOT EMAIL
			
			$arrEmployee = $this->employee->getEmployees(array('emp_id' => $arrRecords[$ind]['emp_id']));
			$arrSupervisor = $this->employee->getEmployees(array('emp_id' => $arrEmployee[0]

['emp_authority_id']));
			
			$this->arrData['arrCategories'] 	= $this->configuration->getValues

(TABLE_ATTENDANCE_LEAVE_CATEGORIES, '*', array('leave_category_id' => $arrRecords[$ind]['leave_category']));
			$this->arrData['arrTypes'] 			= $this->config->item('leave_types');
			
			$arrValues = array(
								'[HEADER_LOGO_LINK]' 		=> EMAIL_HEADER_LOGO,
								'[EMPLOYEE_NAME]' 			=> $arrEmployee

[0]['emp_full_name'],
								'[APPLICATION_STATUS]'		=> 'automatically 

<b>Rejected</b> by <b>System</b> after staying unattended for 3 days.',
								'[EMPLOYEE_CODE]'			=> $arrEmployee

[0]['emp_code'],
								'[LEAVE_TYPE]'				=> $this->arrData

['arrTypes'][$arrRecords[$ind]['leave_type']],
								'[LEAVE_CATEGORY]'			=> $this->arrData

['arrCategories'][0]['leave_category'],
								'[DATE_FROM]'				=> readableDate

($arrRecords[$ind]['leave_from'], $this->arrData["showDateFormat"]),
								'[DATE_TO]'					=> 

readableDate($arrRecords[$ind]['leave_to'], $this->arrData["showDateFormat"]),
								'[COPYRIGHT_TEXT]' 			=> 'Copyright ' . 

date('Y') . ' Naval Anchorage Club. All Rights Reserved.'
								);
			
			$arrTo = array($arrEmployee[0]['emp_work_email']);
			if($arrSupervisor[0]['emp_work_email'] != '') {
				$arrTo[] = $arrSupervisor[0]['emp_work_email'];
			}
			
			$emailHTML = getHTML($arrValues, 'leave_status_notification.html');
			$emailFlag = $this->sendEmail(
								$arrTo, 							

										# RECEIVER DETAILS
								'Leave Status Notification' . EMAIL_SUBJECT_SUFFIX,	 	

					# SUBJECT
								$emailHTML							

										# EMAIL HTML MESSAGE
							);
		}
		echo $ind . ' Records Processed';
		exit;
	}*/
	
	public function sendHolidayAlert($byPass = 0) {
		
		$cronTime = '09:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
			
			$strDate = date('Y-m-d', strtotime('+6 day'));
			
			$this->db->select(' h.*, l.location_name ');
			$this->db->join(TABLE_LOCATION . ' l ', 'h.holiday_country_id = l.location_id', 'left');
			$this->db->where(
								array(
										'h.holiday_date' => $strDate,
										'h.send_email' => 1
									)
								);
	
			$objResult = $this->db->get(TABLE_PUBLIC_HOLIDAYS . ' h ');
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			
			if(count($arrResult) > 0) {
				
				$strEmailTxt = 'Below are the public holidays due in some days.<br /><br />';
				
				$strEmailTxt .= '<table align="center" cellspacing="0" cellpadding="0" style="border:3px 

solid #89CFE6;color:#3E3E3E;font-family:arial;font-size:14px;line-height:24px;padding:15px;width:600px">
									<tr>
										<th>Date</th>
										<th>Country</th>
										<th>Public Holiday</th>
									</tr>
									<tr>
										<th colspan="3" align="center"><hr /></th>
									</tr>';
				for($ind = 0; $ind < count($arrResult); $ind++) {
					
					$strEmailTxt .= '<tr>
											<td align="center">' . date('j F, 

Y', strtotime($arrResult[$ind]['holiday_date'])) . '</td>
											<td align="center">' . $arrResult

[$ind]['location_name'] . '</td>
											<td align="center">' . $arrResult

[$ind]['holiday_name'] . '</td>
										</tr>';
				}
				
				$strEmailTxt .= '</table>';
					
				# SHOOT EMAIL
				# Template: general.html
				
				$arrValues = array(
							'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
							'[EMPLOYEE_NAME]' => 'Recipient',
							'[MESSAGE_BODY]' => $strEmailTxt,
							'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' Naval Anchorage Club. All 

Rights Reserved.'
							);
							
				$emailHTML = getHTML($arrValues, 'general.html');
				//die($emailHTML);
				$emailFlag = $this->sendEmail(
									$arrTo = array(
													

SYSTEM_EMAIL_ADDRESS
													), 			

											# RECEIVER DETAILS
									'Public Holiday Notification' . 

EMAIL_SUBJECT_SUFFIX,	 				# SUBJECT
									$emailHTML						

										# EMAIL HTML MESSAGE
								);
			}
			echo 'Cron Done';
		}
		echo 'Done';
		exit;
	}
	
	public function sendProbationEndAlert($byPass = 0) {
		
		$cronTime = '09:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
			
			$endDate = date('Y-m-d', strtotime('+7 day'));
			
			$this->db->select(' e.*, c.company_name ');
			$this->db->join(TABLE_COMPANIES . ' c ', 'e.emp_company_id = c.company_id', 'left');
			$this->db->where(
								array(
										'e.emp_probation_end_date' => $endDate,
										'e.emp_status' => 1, 
										'e.emp_employment_status < ' => 

STATUS_EMPLOYEE_CONFIRMED
									)
								);
			$this->db->order_by('e.emp_probation_end_date', 'ASC');
	
			$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			
			if(count($arrResult) > 0) {
				for($ind = 0; $ind < count($arrResult); $ind++) {
				
					$strEmailTxt = 'Below is the list of employees whose probation period is going to 

end in next 7 days.<br /><br />';
					
					$strEmailTxt .= '<table align="center" cellspacing="0" cellpadding="0" 

style="border:3px solid #89CFE6;color:#3E3E3E;font-family:arial;font-size:14px;line-height:24px;padding:15px;width:600px">
										<tr>
											<th width="250px" 

align="center">Employee</th>
											<th align="center">Company</th>
											<th align="center">End Date</th>
										</tr>
										<tr>
											<th colspan="3" width="350px" 

align="center"><hr /></th>
										</tr>';
										
					$strEmailTxt .= '<tr>
											<td>' . $arrResult[$ind]

['emp_full_name'] . '</td>
											<td>' . $arrResult[$ind]

['company_name'] . '</td>
											<td>' . date

(SHOW_DATE_TIME_FORMAT, strtotime($arrResult[$ind]['emp_probation_end_date'])) . '</td>
										</tr>';
					$strEmailTxt .= '</table>';
					
					# SHOOT EMAIL
					# Template: general.html
					
					$arrTo = array(SYSTEM_EMAIL_ADDRESS);
					$arrSupervisors = $this->employee->getEmpSupervisorsDetails(array('es.emp_id' => 

$arrResult[$ind]['emp_id']));
					foreach($arrSupervisors as $arrSupervisor) {
						$arrTo[] = $arrSupervisor['emp_work_email'];
					}
					
					$arrValues = array(
								'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
								'[EMPLOYEE_NAME]' => 'Recipient',
								'[MESSAGE_BODY]' => $strEmailTxt,
								'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' 

Naval Anchorage Club. All Rights Reserved.'
								);
								
					$emailHTML = getHTML($arrValues, 'general.html');
					//die($emailHTML);
					$emailFlag = $this->sendEmail(
										$arrTo, 					

												# RECEIVER DETAILS
										'Probation End Notification' . 

EMAIL_SUBJECT_SUFFIX,	 					# SUBJECT
										$emailHTML					

												# EMAIL HTML MESSAGE
									);
				}
				
					
				echo 'Alert Sent';
			}
		}
		echo 'Done';
		exit;
	}
	
	public function sendProbationEndReminder($byPass = 0) {
		
		$cronTime = '09:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
			
			$endDate = date('Y-m-d');			
			$this->db->select(' e.*, c.company_name ');
			$this->db->join(TABLE_COMPANIES . ' c ', 'e.emp_company_id = c.company_id', 'left');
			$this->db->where(								array(
										'e.emp_probation_end_date < ' => $endDate,
										'e.emp_status' => 1, 
										'e.emp_employment_status < ' => 

STATUS_EMPLOYEE_CONFIRMED
									)
								);
			$this->db->order_by('e.emp_probation_end_date', 'ASC');
	
			$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			
			if(count($arrResult) > 0) {
				for($ind = 0; $ind < count($arrResult); $ind++) {
				
					$strEmailTxt = 'Below is the list of employees whose probation period has ended 

and the assessment is due.<br /><br />';
					
					$strEmailTxt .= '<table align="center" cellspacing="0" cellpadding="0" 

style="border:3px solid #89CFE6;color:#3E3E3E;font-family:arial;font-size:14px;line-height:24px;padding:15px;width:600px">
										<tr>
											<th width="250px" 

align="center">Employee</th>
											<th align="center">Company</th>
											<th align="center">End Date</th>
										</tr>
										<tr>
											<th colspan="3" width="350px" 

align="center"><hr /></th>
										</tr>';
										
					$strEmailTxt .= '<tr>
											<td>' . $arrResult[$ind]

['emp_full_name'] . '</td>
											<td>' . $arrResult[$ind]

['company_name'] . '</td>
											<td>' . date

(SHOW_DATE_TIME_FORMAT, strtotime($arrResult[$ind]['emp_probation_end_date'])) . '</td>
										</tr>';
					$strEmailTxt .= '</table>';
					
					# SHOOT EMAIL
					# Template: general.html
					
					$arrTo = array(SYSTEM_EMAIL_ADDRESS);
					$arrSupervisors = $this->employee->getEmpSupervisorsDetails(array('es.emp_id' => 

$arrResult[$ind]['emp_id']));
					foreach($arrSupervisors as $arrSupervisor) {
						$arrTo[] = $arrSupervisor['emp_work_email'];
					}
					
					$arrValues = array(
								'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
								'[EMPLOYEE_NAME]' => 'Recipient',
								'[MESSAGE_BODY]' => $strEmailTxt,
								'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' 

Naval Anchorage Club. All Rights Reserved.'
								);
								
					$emailHTML = getHTML($arrValues, 'general.html');
					//die($emailHTML);
					$emailFlag = $this->sendEmail(
										$arrTo, 					

												# RECEIVER DETAILS
										'Probation Assessment Reminder' . 

EMAIL_SUBJECT_SUFFIX,	 					# SUBJECT
										$emailHTML					

												# EMAIL HTML MESSAGE
									);
				}
				
					
				echo 'Alert Sent';
			}
		}
		echo 'Done';
		exit;
	}
	
	public function sendTradeLicenseExpiryAlert($byPass = 0) {
		
		$cronTime = '09:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
							
			$dateExpiry = date('Y-m-d', strtotime('+10 day'));
			
			$this->db->select(' c.* ');
			$this->db->where(array('trade_license_expiry_date' => $dateExpiry));
	
			$objResult = $this->db->get(TABLE_COMPANIES . ' c ');
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			
			if(count($arrResult) > 0) {
				
				$strEmailTxt = 'Below is the list of companies whose Trade License is going to expire in 10 days.<br /><br />';
				
				$strEmailTxt .= '<table align="center" cellspacing="0" cellpadding="0" style="border:3px 

solid #89CFE6;color:#3E3E3E;font-family:arial;font-size:14px;line-height:24px;padding:15px;width:600px">
									<tr>
										<th width="250px">Company</th>
										<th>Expiry Date</th>
									</tr>
									<tr>
										<th colspan="3" width="350px" 

align="center"><hr /></th>
									</tr>';
				for($ind = 0; $ind < count($arrResult); $ind++) {
					$strEmailTxt .= '<tr>
											<td align="center">' . $arrResult[$ind]['company_name'] . '</td>
											<td align="center">' . date(SHOW_DATE_TIME_FORMAT, strtotime($arrResult[$ind]['trade_license_expiry_date'])) . '</td>
										</tr>';
				}
				
				$strEmailTxt .= '</table>';
					
				# SHOOT EMAIL
				# Template: general.html
				
				$arrValues = array(
							'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
							'[EMPLOYEE_NAME]' => 'Recipient',
							'[MESSAGE_BODY]' => $strEmailTxt,
							'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' Naval Anchorage Club. All 

Rights Reserved.'
							);
							
				$emailHTML = getHTML($arrValues, 'general.html');
				
				$emailFlag = $this->sendEmail(
									$arrTo = array(	
													SYSTEM_EMAIL_ADDRESS
													), 																# RECEIVER DETAILS
									'Trade License Expiry Notification' . EMAIL_SUBJECT_SUFFIX,	 					# SUBJECT
									$emailHTML																		# EMAIL HTML MESSAGE
								);
				echo 'Alert Sent';
			}
		}
		echo 'Done';
		exit;
	}
	
	
	public function sendEstablishmentCardExpiryAlert($byPass = 0) {
		
		$cronTime = '09:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
							
			$dateExpiry = date('Y-m-d', strtotime('+10 day'));
			
			$this->db->select(' c.* ');
			$this->db->where(array('establishment_card_expiry_date' => $dateExpiry));
	
			$objResult = $this->db->get(TABLE_COMPANIES . ' c ');
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			
			if(count($arrResult) > 0) {
				
				$strEmailTxt = 'Below is the list of companies whose Establishment Card is going to expire in 10 days.<br /><br />';
				
				$strEmailTxt .= '<table align="center" cellspacing="0" cellpadding="0" style="border:3px 

solid #89CFE6;color:#3E3E3E;font-family:arial;font-size:14px;line-height:24px;padding:15px;width:600px">
									<tr>
										<th width="250px">Company</th>
										<th>Expiry Date</th>
									</tr>
									<tr>
										<th colspan="3" width="350px" 

align="center"><hr /></th>
									</tr>';
				for($ind = 0; $ind < count($arrResult); $ind++) {
					$strEmailTxt .= '<tr>
											<td align="center">' . $arrResult[$ind]['company_name'] . '</td>
											<td align="center">' . date(SHOW_DATE_TIME_FORMAT, strtotime($arrResult[$ind]['trade_license_expiry_date'])) . '</td>
										</tr>';
				}
				
				$strEmailTxt .= '</table>';
					
				# SHOOT EMAIL
				# Template: general.html
				
				$arrValues = array(
							'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
							'[EMPLOYEE_NAME]' => 'Recipient',
							'[MESSAGE_BODY]' => $strEmailTxt,
							'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' Naval Anchorage Club. All 

Rights Reserved.'
							);
							
				$emailHTML = getHTML($arrValues, 'general.html');
				
				$emailFlag = $this->sendEmail(
									$arrTo = array(	
													SYSTEM_EMAIL_ADDRESS
													), 																# RECEIVER DETAILS
									'Establishment Card Expiry Notification' . EMAIL_SUBJECT_SUFFIX,	 					# SUBJECT
									$emailHTML																		# EMAIL HTML MESSAGE
								);
				echo 'Alert Sent';
			}
		}
		echo 'Done';
		exit;
	}
	
	public function sendDubaiChamberExpiryAlert($byPass = 0) {
		
		$cronTime = '09:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
							
			$dateExpiry = date('Y-m-d', strtotime('+10 day'));
			
			$this->db->select(' c.* ');
			$this->db->where(array('dubaichamber_expiry_date' => $dateExpiry));
	
			$objResult = $this->db->get(TABLE_COMPANIES . ' c ');
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			
			if(count($arrResult) > 0) {
				
				$strEmailTxt = 'Below is the list of companies whose Dubai Chamber Of Commerce is going to expire in 10 days.<br /><br />';
				
				$strEmailTxt .= '<table align="center" cellspacing="0" cellpadding="0" style="border:3px 

solid #89CFE6;color:#3E3E3E;font-family:arial;font-size:14px;line-height:24px;padding:15px;width:600px">
									<tr>
										<th width="250px">Company</th>
										<th>Expiry Date</th>
									</tr>
									<tr>
										<th colspan="3" width="350px" 

align="center"><hr /></th>
									</tr>';
				for($ind = 0; $ind < count($arrResult); $ind++) {
					$strEmailTxt .= '<tr>
											<td align="center">' . $arrResult[$ind]['company_name'] . '</td>
											<td align="center">' . date(SHOW_DATE_TIME_FORMAT, strtotime($arrResult[$ind]['trade_license_expiry_date'])) . '</td>
										</tr>';
				}
				
				$strEmailTxt .= '</table>';
					
				# SHOOT EMAIL
				# Template: general.html
				
				$arrValues = array(
							'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
							'[EMPLOYEE_NAME]' => 'Recipient',
							'[MESSAGE_BODY]' => $strEmailTxt,
							'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' Naval Anchorage Club. All 

Rights Reserved.'
							);
							
				$emailHTML = getHTML($arrValues, 'general.html');
				
				$emailFlag = $this->sendEmail(
									$arrTo = array(	
													SYSTEM_EMAIL_ADDRESS
													), 																# RECEIVER DETAILS
									'Dubai Chamber Of Commerce Expiry Notification' . EMAIL_SUBJECT_SUFFIX,	 					# SUBJECT
									$emailHTML																		# EMAIL HTML MESSAGE
								);
				echo 'Alert Sent';
			}
		}
		echo 'Done';
		exit;
	}
	
	
	public function sendPICoverExpiryAlert($byPass = 0) {
		
		$cronTime = '09:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
							
			$dateExpiry = date('Y-m-d', strtotime('+10 day'));
			
			$this->db->select(' c.* ');
			$this->db->where(array('picover_expiry_date' => $dateExpiry));
	
			$objResult = $this->db->get(TABLE_COMPANIES . ' c ');
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			
			if(count($arrResult) > 0) {
				
				$strEmailTxt = 'Below is the list of companies whose PI Cover is going to expire in 10 days.<br /><br />';
				
				$strEmailTxt .= '<table align="center" cellspacing="0" cellpadding="0" style="border:3px 

solid #89CFE6;color:#3E3E3E;font-family:arial;font-size:14px;line-height:24px;padding:15px;width:600px">
									<tr>
										<th width="250px">Company</th>
										<th>Expiry Date</th>
									</tr>
									<tr>
										<th colspan="3" width="350px" 

align="center"><hr /></th>
									</tr>';
				for($ind = 0; $ind < count($arrResult); $ind++) {
					$strEmailTxt .= '<tr>
											<td align="center">' . $arrResult[$ind]['company_name'] . '</td>
											<td align="center">' . date(SHOW_DATE_TIME_FORMAT, strtotime($arrResult[$ind]['trade_license_expiry_date'])) . '</td>
										</tr>';
				}
				
				$strEmailTxt .= '</table>';
					
				# SHOOT EMAIL
				# Template: general.html
				
				$arrValues = array(
							'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
							'[EMPLOYEE_NAME]' => 'Recipient',
							'[MESSAGE_BODY]' => $strEmailTxt,
							'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' Naval Anchorage Club. All 

Rights Reserved.'
							);
							
				$emailHTML = getHTML($arrValues, 'general.html');
				
				$emailFlag = $this->sendEmail(
									$arrTo = array(	
													SYSTEM_EMAIL_ADDRESS
													), 																# RECEIVER DETAILS
									'PI Cover Expiry Notification' . EMAIL_SUBJECT_SUFFIX,	 					# SUBJECT
									$emailHTML																		# EMAIL HTML MESSAGE
								);
				echo 'Alert Sent';
			}
		}
		echo 'Done';
		exit;
	}
	
	public function sendTenancyContractExpiryAlert($byPass = 0) {
		
		$cronTime = '09:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
							
			$dateExpiry = date('Y-m-d', strtotime('+10 day'));
			
			$this->db->select(' c.* ');
			$this->db->where(array('tenancycontract_expiry_date' => $dateExpiry));
	
			$objResult = $this->db->get(TABLE_COMPANIES . ' c ');
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			
			if(count($arrResult) > 0) {
				
				$strEmailTxt = 'Below is the list of companies whose Tenancy Contract is going to expire in 10 days.<br /><br />';
				
				$strEmailTxt .= '<table align="center" cellspacing="0" cellpadding="0" style="border:3px 

solid #89CFE6;color:#3E3E3E;font-family:arial;font-size:14px;line-height:24px;padding:15px;width:600px">
									<tr>
										<th width="250px">Company</th>
										<th>Expiry Date</th>
									</tr>
									<tr>
										<th colspan="3" width="350px" 

align="center"><hr /></th>
									</tr>';
				for($ind = 0; $ind < count($arrResult); $ind++) {
					$strEmailTxt .= '<tr>
											<td align="center">' . $arrResult[$ind]['company_name'] . '</td>
											<td align="center">' . date(SHOW_DATE_TIME_FORMAT, strtotime($arrResult[$ind]['trade_license_expiry_date'])) . '</td>
										</tr>';
				}
				
				$strEmailTxt .= '</table>';
					
				# SHOOT EMAIL
				# Template: general.html
				
				$arrValues = array(
							'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
							'[EMPLOYEE_NAME]' => 'Recipient',
							'[MESSAGE_BODY]' => $strEmailTxt,
							'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' Naval Anchorage Club. All 

Rights Reserved.'
							);
							
				$emailHTML = getHTML($arrValues, 'general.html');
				
				$emailFlag = $this->sendEmail(
									$arrTo = array(	
													SYSTEM_EMAIL_ADDRESS
													), 																# RECEIVER DETAILS
									'Tenancy Contract Expiry Notification' . EMAIL_SUBJECT_SUFFIX,	 					# SUBJECT
									$emailHTML																		# EMAIL HTML MESSAGE
								);
				echo 'Alert Sent';
			}
		}
		echo 'Done';
		exit;
	}
	
	
	
	public function sendMaintenanceContractExpiryAlert($byPass = 0) {
		
		$cronTime = '09:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
							
			$dateExpiry = date('Y-m-d', strtotime('+10 day'));
			
			$this->db->select(' c.* ');
			$this->db->where(array('maintenance_contract_expiry_date' => $dateExpiry));
	
			$objResult = $this->db->get(TABLE_COMPANIES . ' c ');
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			
			if(count($arrResult) > 0) {
				
				$strEmailTxt = 'Below is the list of companies whose Maintenance Contract is going to expire in 10 days.<br /><br />';
				
				$strEmailTxt .= '<table align="center" cellspacing="0" cellpadding="0" style="border:3px 

solid #89CFE6;color:#3E3E3E;font-family:arial;font-size:14px;line-height:24px;padding:15px;width:600px">
									<tr>
										<th width="250px">Company</th>
										<th>Expiry Date</th>
									</tr>
									<tr>
										<th colspan="3" width="350px" 

align="center"><hr /></th>
									</tr>';
				for($ind = 0; $ind < count($arrResult); $ind++) {
					$strEmailTxt .= '<tr>
											<td align="center">' . $arrResult[$ind]['company_name'] . '</td>
											<td align="center">' . date(SHOW_DATE_TIME_FORMAT, strtotime($arrResult[$ind]['trade_license_expiry_date'])) . '</td>
										</tr>';
				}
				
				$strEmailTxt .= '</table>';
					

				# SHOOT EMAIL
				# Template: general.html
				
				$arrValues = array(
							'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
							'[EMPLOYEE_NAME]' => 'Recipient',
							'[MESSAGE_BODY]' => $strEmailTxt,
							'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' Naval Anchorage Club. All 

Rights Reserved.'
							);
							
				$emailHTML = getHTML($arrValues, 'general.html');
				
				$emailFlag = $this->sendEmail(
									$arrTo = array(	
													SYSTEM_EMAIL_ADDRESS
													), 																# RECEIVER DETAILS
									'Maintenance Contract Expiry Notification' . EMAIL_SUBJECT_SUFFIX,	 					# SUBJECT
									$emailHTML																		# EMAIL HTML MESSAGE
								);
				echo 'Alert Sent';
			}
		}
		echo 'Done';
		exit;
	}
	
	
	public function sendDcdFireExpiryAlert($byPass = 0) {
		
		$cronTime = '09:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
							
			$dateExpiry = date('Y-m-d', strtotime('+10 day'));
			
			$this->db->select(' c.* ');
			$this->db->where(array('dcdfire_expiry_date' => $dateExpiry));
	
			$objResult = $this->db->get(TABLE_COMPANIES . ' c ');
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			
			if(count($arrResult) > 0) {
				
				$strEmailTxt = 'Below is the list of companies whose DCD Fire is going to expire in 10 days.<br /><br />';
				
				$strEmailTxt .= '<table align="center" cellspacing="0" cellpadding="0" style="border:3px 

solid #89CFE6;color:#3E3E3E;font-family:arial;font-size:14px;line-height:24px;padding:15px;width:600px">
									<tr>
										<th width="250px">Company</th>
										<th>Expiry Date</th>
									</tr>
									<tr>
										<th colspan="3" width="350px" 

align="center"><hr /></th>
									</tr>';
				for($ind = 0; $ind < count($arrResult); $ind++) {
					$strEmailTxt .= '<tr>
											<td align="center">' . $arrResult[$ind]['company_name'] . '</td>
											<td align="center">' . date(SHOW_DATE_TIME_FORMAT, strtotime($arrResult[$ind]['trade_license_expiry_date'])) . '</td>
										</tr>';
				}
				
				$strEmailTxt .= '</table>';
					
				# SHOOT EMAIL
				# Template: general.html
				
				$arrValues = array(
							'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
							'[EMPLOYEE_NAME]' => 'Recipient',
							'[MESSAGE_BODY]' => $strEmailTxt,
							'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' Naval Anchorage Club. All 

Rights Reserved.'
							);
							
				$emailHTML = getHTML($arrValues, 'general.html');
				
				$emailFlag = $this->sendEmail(
									$arrTo = array(	
													SYSTEM_EMAIL_ADDRESS
													), 																# RECEIVER DETAILS
									'Dcd Fire Expiry Notification' . EMAIL_SUBJECT_SUFFIX,	 					# SUBJECT
									$emailHTML																		# EMAIL HTML MESSAGE
								);
				echo 'Alert Sent';
			}
		}
		echo 'Done';
		exit;
	}
	
	public function sendPoBoxExpiryAlert($byPass = 0) {
		
		$cronTime = '09:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
							
			$dateExpiry = date('Y-m-d', strtotime('+10 day'));
			
			$this->db->select(' c.* ');
			$this->db->where(array('pobox_renewal_expiry_date' => $dateExpiry));
	
			$objResult = $this->db->get(TABLE_COMPANIES . ' c ');
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			
			if(count($arrResult) > 0) {
				
				$strEmailTxt = 'Below is the list of companies whose PO Box is going to expire in 10 days.<br /><br />';
				
				$strEmailTxt .= '<table align="center" cellspacing="0" cellpadding="0" style="border:3px 

solid #89CFE6;color:#3E3E3E;font-family:arial;font-size:14px;line-height:24px;padding:15px;width:600px">
									<tr>
										<th width="250px">Company</th>
										<th>Expiry Date</th>
									</tr>
									<tr>
										<th colspan="3" width="350px" 

align="center"><hr /></th>
									</tr>';
				for($ind = 0; $ind < count($arrResult); $ind++) {
					$strEmailTxt .= '<tr>
											<td align="center">' . $arrResult[$ind]['company_name'] . '</td>
											<td align="center">' . date(SHOW_DATE_TIME_FORMAT, strtotime($arrResult[$ind]['trade_license_expiry_date'])) . '</td>
										</tr>';
				}
				
				$strEmailTxt .= '</table>';
					
				# SHOOT EMAIL
				# Template: general.html
				
				$arrValues = array(
							'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
							'[EMPLOYEE_NAME]' => 'Recipient',
							'[MESSAGE_BODY]' => $strEmailTxt,
							'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' Naval Anchorage Club. All 

Rights Reserved.'
							);
							
				$emailHTML = getHTML($arrValues, 'general.html');
				
				$emailFlag = $this->sendEmail(
									$arrTo = array(	
													SYSTEM_EMAIL_ADDRESS
													), 																# RECEIVER DETAILS
									'PO Box Expiry Notification' . EMAIL_SUBJECT_SUFFIX,	 					# SUBJECT
									$emailHTML																		# EMAIL HTML MESSAGE
								);
				echo 'Alert Sent';
			}
		}
		echo 'Done';
		exit;
	}
	
	
	public function sendBusinessOperationExpiryAlert($byPass = 0) {
		
		$cronTime = '09:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
							
			$dateExpiry = date('Y-m-d', strtotime('+10 day'));
			
			$this->db->select(' c.* ');
			$this->db->where(array('business_operation_expiry_date' => $dateExpiry));
	
			$objResult = $this->db->get(TABLE_COMPANIES . ' c ');
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			
			if(count($arrResult) > 0) {
				
				$strEmailTxt = 'Below is the list of companies whose Business Operations is going to expire in 10 days.<br /><br />';
				
				$strEmailTxt .= '<table align="center" cellspacing="0" cellpadding="0" style="border:3px 

solid #89CFE6;color:#3E3E3E;font-family:arial;font-size:14px;line-height:24px;padding:15px;width:600px">
									<tr>
										<th width="250px">Company</th>
										<th>Expiry Date</th>
									</tr>
									<tr>
										<th colspan="3" width="350px" 

align="center"><hr /></th>
									</tr>';
				for($ind = 0; $ind < count($arrResult); $ind++) {
					$strEmailTxt .= '<tr>
											<td align="center">' . $arrResult[$ind]['company_name'] . '</td>
											<td align="center">' . date(SHOW_DATE_TIME_FORMAT, strtotime($arrResult[$ind]['trade_license_expiry_date'])) . '</td>
										</tr>';
				}
				
				$strEmailTxt .= '</table>';
					
				# SHOOT EMAIL
				# Template: general.html
				
				$arrValues = array(
							'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
							'[EMPLOYEE_NAME]' => 'Recipient',
							'[MESSAGE_BODY]' => $strEmailTxt,
							'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' Naval Anchorage Club. All 

Rights Reserved.'
							);
							
				$emailHTML = getHTML($arrValues, 'general.html');
				
				$emailFlag = $this->sendEmail(
									$arrTo = array(	
													SYSTEM_EMAIL_ADDRESS
													), 																# RECEIVER DETAILS
									'Business Operations Expiry Notification' . EMAIL_SUBJECT_SUFFIX,	 					# SUBJECT
									$emailHTML																		# EMAIL HTML MESSAGE
								);
				echo 'Alert Sent';
			}
		}
		echo 'Done';
		exit;
	}
	
	public function sendRERAExpiryAlert($byPass = 0) {
		
		$cronTime = '09:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
							
			$dateExpiry = date('Y-m-d', strtotime('+10 day'));
			
			$this->db->select(' c.* ');
			$this->db->where(array('rera_expiry_date' => $dateExpiry));
	
			$objResult = $this->db->get(TABLE_COMPANIES . ' c ');
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			
			if(count($arrResult) > 0) {
				
				$strEmailTxt = 'Below is the list of companies whose RERA is going to expire in 10 days.<br /><br />';
				
				$strEmailTxt .= '<table align="center" cellspacing="0" cellpadding="0" style="border:3px 

solid #89CFE6;color:#3E3E3E;font-family:arial;font-size:14px;line-height:24px;padding:15px;width:600px">
									<tr>
										<th width="250px">Company</th>
										<th>Expiry Date</th>
									</tr>
									<tr>
										<th colspan="3" width="350px" 

align="center"><hr /></th>
									</tr>';
				for($ind = 0; $ind < count($arrResult); $ind++) {
					$strEmailTxt .= '<tr>
											<td align="center">' . $arrResult[$ind]['company_name'] . '</td>
											<td align="center">' . date(SHOW_DATE_TIME_FORMAT, strtotime($arrResult[$ind]['trade_license_expiry_date'])) . '</td>
										</tr>';
				}
				
				$strEmailTxt .= '</table>';
					
				# SHOOT EMAIL
				# Template: general.html
				
				$arrValues = array(
							'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
							'[EMPLOYEE_NAME]' => 'Recipient',
							'[MESSAGE_BODY]' => $strEmailTxt,
							'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' Naval Anchorage Club. All 

Rights Reserved.'
							);
							
				$emailHTML = getHTML($arrValues, 'general.html');
				
				$emailFlag = $this->sendEmail(
									$arrTo = array(	
													SYSTEM_EMAIL_ADDRESS
													), 																# RECEIVER DETAILS
									' RERA Expiry Notification' . EMAIL_SUBJECT_SUFFIX,	 					# SUBJECT
									$emailHTML																		# EMAIL HTML MESSAGE
								);
				echo 'Alert Sent';
			}
		}
		echo 'Done';
		exit;
	}
	
	
	
	
	
	public function sendVisaExpiryAlert($byPass = 0) {
		
		$cronTime = '09:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
							
			$dateExpiry1 = date('Y-m-d', strtotime('+15 day'));
			$dateExpiry2 = date('Y-m-d', strtotime('+30 day'));
			
			$this->db->select(' e.*, c.company_name ');
			$this->db->join(TABLE_COMPANIES . ' c ', 'e.emp_company_id = c.company_id', 'left');
			$this->db->where(" (e.emp_visa_expiry_date = '" . $dateExpiry1 . "' OR e.emp_visa_expiry_date = '" 

. $dateExpiry2 . "') AND e.emp_status = 1 AND e.emp_employment_status < " . STATUS_EMPLOYEE_SEPARATED, null, false);
			$this->db->order_by('e.emp_visa_expiry_date', 'ASC');
	
			$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			
			if(count($arrResult) > 0) {
				
				$strEmailTxt = 'Below is the list of employees whose visa is going to expire after a 

month.<br /><br />';
				
				$strEmailTxt .= '<table align="center" cellspacing="0" cellpadding="0" style="border:3px 

solid #89CFE6;color:#3E3E3E;font-family:arial;font-size:14px;line-height:24px;padding:15px;width:600px">
									<tr>
										<th width="250px" 

align="center">Employee</th>
										<th align="center">Company</th>
										<th align="center">Expiry Date</th>
									</tr>
									<tr>
										<th colspan="3" width="350px" 

align="center"><hr /></th>
									</tr>';
				for($ind = 0; $ind < count($arrResult); $ind++) {
					$strEmailTxt .= '<tr>
											<td>' . $arrResult[$ind]

['emp_full_name'] . '</td>
											<td>' . $arrResult[$ind]

['company_name'] . '</td>
											<td>' . date

(SHOW_DATE_TIME_FORMAT, strtotime($arrResult[$ind]['emp_visa_expiry_date'])) . '</td>
										</tr>';
				}
				
				$strEmailTxt .= '</table>';
					
				# SHOOT EMAIL
				# Template: general.html
				
				$arrValues = array(
							'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
							'[EMPLOYEE_NAME]' => 'Recipient',
							'[MESSAGE_BODY]' => $strEmailTxt,
							'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' Naval Anchorage Club. All 

Rights Reserved.'
							);
							
				$emailHTML = getHTML($arrValues, 'general.html');
				
				$emailFlag = $this->sendEmail(
									$arrTo = array(
													

SYSTEM_EMAIL_ADDRESS
													), 			

												# RECEIVER DETAILS
									'Visa Expiry Notification' . EMAIL_SUBJECT_SUFFIX,	

 						# SUBJECT
									$emailHTML						

											# EMAIL HTML MESSAGE
								);
				echo 'Alert Sent';
			}
		}
		echo 'Done';
		exit;
	}
	
	public function sendPassportExpiryAlert($byPass = 0) {
		
		$cronTime = '09:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
							
			$dateExpiry1 = date('Y-m-d', strtotime('+15 day'));
			$dateExpiry2 = date('Y-m-d', strtotime('+30 day'));
			
			$this->db->select(' e.*, c.company_name ');
			$this->db->join(TABLE_COMPANIES . ' c ', 'e.emp_company_id = c.company_id', 'left');
			$this->db->where(" (e.emp_passport_expiry = '" . $dateExpiry1 . "' OR e.emp_passport_expiry = '" . 

$dateExpiry2 . "') AND e.emp_status = 1 AND e.emp_employment_status < " . STATUS_EMPLOYEE_SEPARATED, null, false);
			$this->db->order_by('e.emp_passport_expiry', 'ASC');
	
			$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			
			if(count($arrResult) > 0) {
				
				$strEmailTxt = 'Below is the list of employees whose passport is going to expire after a 

month.<br /><br />';
				
				$strEmailTxt .= '<table align="center" cellspacing="0" cellpadding="0" style="border:3px 

solid #89CFE6;color:#3E3E3E;font-family:arial;font-size:14px;line-height:24px;padding:15px;width:600px">
									<tr>
										<th width="250px" 

align="center">Employee</th>
										<th align="center">Company</th>
										<th align="center">Expiry Date</th>
									</tr>
									<tr>
										<th colspan="3" width="350px" 

align="center"><hr /></th>
									</tr>';
				for($ind = 0; $ind < count($arrResult); $ind++) {
					$strEmailTxt .= '<tr>
											<td>' . $arrResult[$ind]

['emp_full_name'] . '</td>
											<td>' . $arrResult[$ind]

['company_name'] . '</td>
											<td>' . date

(SHOW_DATE_TIME_FORMAT, strtotime($arrResult[$ind]['emp_passport_expiry'])) . '</td>
										</tr>';
				}
				
				$strEmailTxt .= '</table>';
					
				# SHOOT EMAIL
				# Template: general.html
				
				$arrValues = array(
							'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
							'[EMPLOYEE_NAME]' => 'Recipient',
							'[MESSAGE_BODY]' => $strEmailTxt,
							'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' Naval Anchorage Club. All 

Rights Reserved.'
							);
							
				$emailHTML = getHTML($arrValues, 'general.html');
				//die($emailHTML);
				$emailFlag = $this->sendEmail(
									$arrTo = array(
													

SYSTEM_EMAIL_ADDRESS
													), 			

												# RECEIVER DETAILS
									'Passport Expiry Notification' . 

EMAIL_SUBJECT_SUFFIX,	 					# SUBJECT
									$emailHTML						

											# EMAIL HTML MESSAGE
								);
				echo 'Alert Sent';
			}
		}
		echo 'Done';
		exit;
	}
	
	public function sendNICExpiryAlert($byPass = 0) {
		
		$cronTime = '09:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
							
			$dateExpiry1 = date('Y-m-d', strtotime('+15 day'));
			$dateExpiry2 = date('Y-m-d', strtotime('+30 day'));
			
			$this->db->select(' e.*, c.company_name ');
			$this->db->join(TABLE_COMPANIES . ' c ', 'e.emp_company_id = c.company_id', 'left');
			$this->db->where(" (e.emp_nic_expiry_date = '" . $dateExpiry1 . "' OR e.emp_nic_expiry_date = '" . 

$dateExpiry2 . "') AND e.emp_status = 1 AND e.emp_employment_status < " . STATUS_EMPLOYEE_SEPARATED, null, false);
			$this->db->order_by('e.emp_nic_expiry_date', 'ASC');
	
			$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			
			if(count($arrResult) > 0) {
				
				$strEmailTxt = 'Below is the list of employees whose Emirates ID/CNIC is going to expire 

after a month.<br /><br />';
				
				$strEmailTxt .= '<table align="center" cellspacing="0" cellpadding="0" style="border:3px 

solid #89CFE6;color:#3E3E3E;font-family:arial;font-size:14px;line-height:24px;padding:15px;width:600px">
									<tr>
										<th width="250px" 

align="center">Employee</th>
										<th align="center">Company</th>
										<th align="center">Expiry Date</th>
									</tr>
									<tr>
										<th colspan="3" width="350px" 

align="center"><hr /></th>
									</tr>';
				for($ind = 0; $ind < count($arrResult); $ind++) {
					$strEmailTxt .= '<tr>
											<td>' . $arrResult[$ind]

['emp_full_name'] . '</td>
											<td>' . $arrResult[$ind]

['company_name'] . '</td>
											<td>' . date

(SHOW_DATE_TIME_FORMAT, strtotime($arrResult[$ind]['emp_nic_expiry_date'])) . '</td>
										</tr>';
				}
				
				$strEmailTxt .= '</table>';
					
				# SHOOT EMAIL
				# Template: general.html
				
				$arrValues = array(
							'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
							'[EMPLOYEE_NAME]' => 'Recipient',
							'[MESSAGE_BODY]' => $strEmailTxt,
							'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' Naval Anchorage Club. All 

Rights Reserved.'
							);
							
				$emailHTML = getHTML($arrValues, 'general.html');
				//die($emailHTML);
				$emailFlag = $this->sendEmail(
									$arrTo = array(
													

SYSTEM_EMAIL_ADDRESS
													), 			

												# RECEIVER DETAILS
									'EmiratesID/CNIC Expiry Notification' . 

EMAIL_SUBJECT_SUFFIX,	 						# SUBJECT
									$emailHTML						

											# EMAIL HTML MESSAGE
								);
				echo 'Alert Sent';
			}
		}
		echo 'Done';
		exit;
	}
	
	public function sendLabourExpiryAlert($byPass = 0) {
		
		$cronTime = '09:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
			
			$dateExpiry1 = date('Y-m-d', strtotime('+15 day'));
			$dateExpiry2 = date('Y-m-d', strtotime('+30 day'));
			
			$this->db->select(' e.*, c.company_name ');
			$this->db->join(TABLE_COMPANIES . ' c ', 'e.emp_company_id = c.company_id', 'left');
			$this->db->where(" (e.emp_labour_card_expiry_date = '" . $dateExpiry1 . "' OR 

e.emp_labour_card_expiry_date = '" . $dateExpiry2 . "') AND e.emp_status = 1 AND e.emp_employment_status < " . 

STATUS_EMPLOYEE_SEPARATED, null, false);
			$this->db->order_by('e.emp_labour_card_expiry_date', 'ASC');
	
			$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			
			if(count($arrResult) > 0) {
				
				$strEmailTxt = 'Below is the list of employees whose Labour Card is going to expire after 

a month.<br /><br />';
				
				$strEmailTxt .= '<table align="center" cellspacing="0" cellpadding="0" style="border:3px 

solid #89CFE6;color:#3E3E3E;font-family:arial;font-size:14px;line-height:24px;padding:15px;width:600px">
									<tr>
										<th width="250px" 

align="center">Employee</th>
										<th align="center">Company</th>
										<th align="center">Expiry Date</th>
									</tr>
									<tr>
										<th colspan="3" width="350px" 

align="center"><hr /></th>
									</tr>';
				for($ind = 0; $ind < count($arrResult); $ind++) {
					$strEmailTxt .= '<tr>
											<td>' . $arrResult[$ind]

['emp_full_name'] . '</td>
											<td>' . $arrResult[$ind]

['company_name'] . '</td>
											<td>' . date

(SHOW_DATE_TIME_FORMAT, strtotime($arrResult[$ind]['emp_labour_card_expiry_date'])) . '</td>
										</tr>';
				}
				
				$strEmailTxt .= '</table>';
					
				# SHOOT EMAIL
				# Template: general.html
				
				$arrValues = array(
							'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
							'[EMPLOYEE_NAME]' => 'Recipient',
							'[MESSAGE_BODY]' => $strEmailTxt,
							'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' Naval Anchorage Club. All 

Rights Reserved.'
							);
							
				$emailHTML = getHTML($arrValues, 'general.html');
				//die($emailHTML);
				$emailFlag = $this->sendEmail(
									$arrTo = array(
													

SYSTEM_EMAIL_ADDRESS
													), 			

												# RECEIVER DETAILS
									'Labour Card Expiry Notification' . 

EMAIL_SUBJECT_SUFFIX,	 						# SUBJECT
									$emailHTML						

											# EMAIL HTML MESSAGE
								);
				echo 'Alert Sent';
			}
		}
		echo 'Done';
		exit;
	}
	
	public function sendAbsenceAlert($byPass = 0) {
		
		$cronTime = '09:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
			
			$strDate = date('Y-m-d', strtotime('-1 day'));
			
			$this->db->select(' e.*, l.*, c.company_name ');
			$this->db->join(TABLE_COMPANIES . ' c ', 'e.emp_company_id = c.company_id', 'left');
			$this->db->join(TABLE_ATTENDANCE_LEAVES . ' l ', 'e.emp_id = l.emp_id', 'left');
			$this->db->where(
								array(
										'l.leave_return' => $strDate,
										'l.leave_status' => 1	// APPROVED
									)
								);
	
			$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			
			$totalEmployees = 0;
			
			if(count($arrResult) > 0) {
				
				$strEmailTxt = 'Below is the list of employees whose return from leave date was yesterday 

but they did not come.<br /><br />';
				
				$strEmailTxt .= '<table align="center" cellspacing="0" cellpadding="0" style="border:3px 

solid #89CFE6;color:#3E3E3E;font-family:arial;font-size:14px;line-height:24px;padding:15px;width:600px">
									<tr>
										<th width="250px">Employee</th>
										<th>Company</th>
									</tr>
									<tr>
										<th colspan="2" width="350px" 

align="center"><hr /></th>
									</tr>';
				for($ind = 0; $ind < count($arrResult); $ind++) {
					
					$checkAtt = true;
					
					if($arrResult[$ind]['emp_company_id'] == 1) {
						$arrAttData = $this->attendance->getAttendanceRecordsPK(
																

					array(
																

						'USERID' => $arrResult[$ind]['emp_code'],
																

						'DATE >= ' => $strDate,
																

						'DATE <= ' => date('Y-m-d', strtotime($strDate . ' +1 day'))
																

					)
																

				);
					} else if($arrResult[$ind]['emp_company_id'] == 2 || $arrResult[$ind]

['emp_company_id'] == 3) {
						$arrAttData = $this->attendance->getAttendanceRecordsDxb(
																

					array(
																

						'USERID' => $arrResult[$ind]['emp_code'],
																

						'DATE >= ' => $strDate,
																

						'DATE <= ' => date('Y-m-d', strtotime($strDate . ' +1 day'))
																

					)
																

				);
					} else {
						$checkAtt = false;
					}
					
					if((count($arrAttData) <= 0 || ($arrAttData[0]['IN'] == '' && $arrAttData[0]

['OUT'] == '')) && $checkAtt) {
						$totalEmployees += 1;
						$strEmailTxt .= '<tr>
												<td>' . $arrResult[$ind]

['emp_full_name'] . '</td>
												<td>' . $arrResult[$ind]

['company_name'] . '</td>
											</tr>';
					}
				}
				
				$strEmailTxt .= '</table>';
					
				# SHOOT EMAIL
				# Template: general.html
				
				if($totalEmployees) {
					$arrValues = array(
								'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
								'[EMPLOYEE_NAME]' => 'Recipient',
								'[MESSAGE_BODY]' => $strEmailTxt,
								'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' 

Naval Anchorage Club. All Rights Reserved.'
								);
								
					$emailHTML = getHTML($arrValues, 'general.html');
					//die($emailHTML);
					$emailFlag = $this->sendEmail(
										$arrTo = array(
														

SYSTEM_EMAIL_ADDRESS
														), 		

												# RECEIVER DETAILS
										'Employee Absence Notification' . 

EMAIL_SUBJECT_SUFFIX,	 				# SUBJECT
										$emailHTML					

											# EMAIL HTML MESSAGE
									);
					echo 'Alert Sent';
				}
			}
			echo 'Cron Done';
		}
		exit;
	}
	
	public function sendMissingInfoAlert($byPass = 0) {
		
		$cronTime = '09:00';
		
		if(date('H:i') == $cronTime || $byPass == 13579) {
			
			set_time_limit(0);
			
			$objResult = $this->db->get(TABLE_EMPLOYEE_DOCUMENT_TYPES);
			$arrDocumentTypes = $objResult->result_array();
			$objResult->free_result();
			
			$emailHTML = '';
			
			for($ind = 0; $ind < count($arrDocumentTypes); $ind++) {
				
				if($arrDocumentTypes[$ind]['doc_is_mandatory'] == 1) {
					
					$this->db->distinct();
					$this->db->select(' e.emp_full_name, (SELECT COUNT(*) FROM ' . 

TABLE_EMPLOYEE_DOCUMENTS . ' WHERE doc_type_id = ' . $arrDocumentTypes[$ind]['doc_type_id'] . ' AND emp_id = e.emp_id) AS 

total_found ');
					$this->db->join(TABLE_EMPLOYEE_DOCUMENTS . ' d ', 'e.emp_id = d.emp_id', 'left');
					$this->db->where(array('e.emp_status' => 1, 'e.emp_id > ' => 1, 

'e.emp_employment_status < ' => STATUS_EMPLOYEE_SEPARATED));
					if($arrDocumentTypes[$ind]['doc_is_mandatory_for_companies'] != '') {
						$this->db->where_in('e.emp_company_id', explode(',', $arrDocumentTypes

[$ind]['doc_is_mandatory_for_companies']));
					}
					$this->db->having('total_found', 0); 
			
					$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
					$arrEmployees = $objResult->result_array();
					$objResult->free_result();
					
					for($jnd = 0; $jnd < count($arrEmployees); $jnd++) {
						if($jnd == 0) {
							$emailHTML .= '
							<tr>
							<td>&nbsp</td>
							</tr><tr>
							<td><h3>' . $arrDocumentTypes[$ind]['doc_type'] . '</h3></td>
							</tr>
							<tr>
							<td>&nbsp</td>
							</tr>';
						}
						$emailHTML .= '<tr>
							<td>' . $arrEmployees[$jnd]['emp_full_name'] . '</td>
							</tr>';
					}
				}
			}
			
			if($emailHTML != '') {
				
				$emailHTML = '<tr>
							<td>Following documents are missing for below employees. Please do 

the needful.</td>
							</tr>
							<tr>
							<td>&nbsp</td>
							</tr>
							' . $emailHTML; 
				
				# SHOOT EMAIL
				# Template: general.html
				
				$arrValues = array(
							'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
							'[EMPLOYEE_NAME]' => 'Recipient',
							'[MESSAGE_BODY]' => $emailHTML,
							'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' Naval Anchorage Club. All 

Rights Reserved.'
							);
							
				$emailHTML = getHTML($arrValues, 'general.html');
				//die($emailHTML);
				$emailFlag = $this->sendEmail(
									$arrTo = array(
													

SYSTEM_EMAIL_ADDRESS
												), 				

												# RECEIVER DETAILS
									'Missing Document Alert' . EMAIL_SUBJECT_SUFFIX,	

 						# SUBJECT
									$emailHTML						

											# EMAIL HTML MESSAGE
								);
								
				echo 'Alert Sent';
			}
		}
		echo 'Done';	
		exit;
	}
	
	
	
	
	
	
	
	
	
	public function autoComplainStatus($byPass = 0)
	{
	
	   set_time_limit(0);
	     $cronTime = '09:00';
	
	       if(date('H:i') == $cronTime || $byPass == 13579) {
							
			$dateExpiry = date('Y-m-d', strtotime('-3 day'));
			
			//echo $dateExpiry;
			
			$this->db->select(' c.* ');
			$this->db->where(array('created_date' => $dateExpiry));
	
			$objResult = $this->db->get(TABLE_EMPLOYEE_COMPLAINS . ' c ');
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			
			for($ind = 0; $ind < count($arrResult); $ind++) {
				
				$arrValues = array(
										'complain_id' => $arrResult[0]['complain_id'],
										'emp_id' => $arrResult[0]['emp_id'],
										'created_date' =>  $arrResult[0]['created_date'],										
										'complain_status' => 3
										);
				
					
					
					
				}
				$this->db->where('complain_id', $arrResult[0]['complain_id']);
                 $this->db->update(TABLE_EMPLOYEE_COMPLAINS,$arrValues);
				
				//echo $this->db->last_query();
				//var_dump($arrValues);
				
				
				
			
	}
	
				
	
	
	
	
	
		
		
		
		
		
		}

	

	
	
	
	
	
	
	
	
	
	public function executePayroll($byPass = 0) {
					
		set_time_limit(0);
		
		$cronTime = '01:00';
		
		if((date('H:i') == $cronTime && date('j') == 20) || $byPass == 13579) {
			
			$arrEmployee = $this->employee->getEmployees(array('e.emp_status' => 1, 'e.emp_employment_status < 

' => STATUS_EMPLOYEE_SEPARATED, 'e.emp_payroll_execution' => 1));
				
			for($ind = 0; $ind < count($arrEmployee); $ind++) {
					
				$arrSalary = $this->payroll->getPayrolls(array('ep.emp_id' => $arrEmployee[$ind]

['emp_id']), 1, 0);
				
				if(count($arrSalary[0])) {
					$arrValues = array(
										'emp_id' => $arrEmployee[$ind]['emp_id'],
										'payroll_month' => date("n", strtotime("now")),
										'payroll_year' => date("Y", strtotime("now")),
										'payroll_earning_basic' => $arrSalary[0]['payroll_earning_basic'],
										'payroll_earning_housing' => $arrSalary[0]['payroll_earning_housing'],
										'payroll_earning_transport' => $arrSalary[0]['payroll_earning_transport'],
										'payroll_earning_utility' => $arrSalary[0]['payroll_earning_utility'],
										'payroll_deduction_tax' => $arrSalary[0]['payroll_deduction_tax'],
										'payroll_deduction_pf' => $arrSalary[0]['payroll_deduction_pf'],
										'payroll_deduction_eobi' => $arrSalary[0]['payroll_deduction_eobi']
										);
										
					if($arrEmployee[$ind]['emp_company_id'] == 1) { // PAKISTAN
						$arrValues['payroll_earning_misc'] = $arrSalary[0]['payroll_earning_misc'];
					}
										
					$this->employee->saveValues(TABLE_EMPLOYEE_PAYROLL, $arrValues);
				}
			}
		}
		
		exit;
	}
	
	public function sendAttCheckAlert($byPass = 0) {
		
		$cronTime = '09:00';
		
		if((date('H:i') == $cronTime && date('j') == 1) || $byPass == 13579) {
			
			$arrWhere = array(
								'u.user_role_id in ' => ' (2,4,11,12,15) ',
								'e.emp_status' => 1,
								'e.emp_employment_status < ' => STATUS_EMPLOYEE_SEPARATED,
								'e.emp_location_id in ' => ' (91,132) '
							);
	
			$arrResult = $this->employee->getEmployees($arrWhere);
			
			$arrTo = array();
			
			if(count($arrResult) > 0) {
				for($ind = 0; $ind < count($arrResult); $ind++) {
					$arrIndex = count($arrTo);
					$arrTo[$arrIndex]['emailAdd'] = $arrResult[$ind]['emp_work_email'];
					$arrTo[$arrIndex]['isBCC'] = true;
				}
			}
			
			$strEmailTxt = 'Kindly review your team\'s monthly attendance through HRM System and send a 

confirmation email to HR.<br /><br />';
				
			# SHOOT EMAIL
			# Template: general.html
			
			if(count($arrTo)) {
				$arrValues = array(
							'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
							'[EMPLOYEE_NAME]' => 'Employee',
							'[MESSAGE_BODY]' => $strEmailTxt,
							'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' Naval Anchorage Club. All 

Rights Reserved.'
							);
							
				$emailHTML = getHTML($arrValues, 'general.html');
				//die($emailHTML);
				
				$emailFlag = $this->sendMultiEmail(
									$arrTo, 						

								# RECEIVER DETAILS
									date('F Y', strtotime(date('F'). " last month")) . 

' Attendance' . EMAIL_SUBJECT_SUFFIX,	 				# SUBJECT
									$emailHTML						

								# EMAIL HTML MESSAGE
								);
			}
			
			echo 'Cron Done';
		}
		exit;
	}
	
	public function sendLoginDetails__($authCode = 0) {
		
		if($authCode == 13579) {
			$arrUserRecord = $this->employee->getEmployees(array('e.emp_id > ' => 1, 'e.emp_status' => 1, 

'e.emp_employment_status < ' => 6, 'e.emp_work_email !=' => '', 'e.email_sent' => 0));
			diee($arrUserRecord, true);
			for($ind = 0; $ind < count($arrUserRecord); $ind++) {
				
				$arrUser = $this->user_management->getUsers(array('u.employee_id' => $arrUserRecord[$ind]

['emp_id']));diee($arrUser, true);
				$arrUser = $arrUser[0];
				
				if(count($arrUser)) {
					
					
					# SHOOT EMAIL
					# Template: retrieve_login_details_emp.html
				
					$arrValues = array(
										'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
										'[EMPLOYEE_NAME]' => $arrUserRecord[$ind]['emp_full_name'],
										'[EMPLOYEE_DASHBOARD_LINK]' => $this->arrData["baseURL"],
										'[EMPLOYEE_USERNAME]' => $arrUser['user_name'],
										'[EMPLOYEE_PASSWORD]' => $arrUser['plain_password'],
										'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' Naval Anchorage Club. All Rights Reserved.'
										);
										
					$emailHTML = getHTML($arrValues, 'retrieve_login_details_emp.html');
					//die($emailHTML);
					
					if($this->sendEmail(
										$arrTo = array(
														

$arrUserRecord[$ind]['emp_work_email']
														), 		

								# RECEIVER DETAILS
										'HRMS Account Details' . 

EMAIL_SUBJECT_SUFFIX, 				# SUBJECT
										$emailHTML					

							# EMAIL HTML MESSAGE
									)) {
																

			
										$this->employee->saveValues

(TABLE_EMPLOYEE, array('email_sent' => 1), array('emp_id' => $arrUserRecord[$ind]['emp_id']));
									}
				}
			}
		}		
	}
	
	public function setSupervisors($byPass = 0) {
					
		set_time_limit(0);
		
		$cronTime = '00:00';
		
		if((date('H:i') == $cronTime && date('j') == 1) || $byPass == 13579) {
			
			$arrEmployee = $this->employee->getEmployees();
				
			for($ind = 0; $ind < count($arrEmployee); $ind++) {
				
				$this->master->deleteValue(TABLE_EMPLOYEE_SUPERVISORS, null, array('emp_id' => 

$arrEmployee[$ind]['emp_id']));
				
				if((int)$arrEmployee[$ind]['emp_authority_id'] <= 0) {
					$arrEmployee[$ind]['emp_authority_id'] = 245;
				}
				
				$arrValues = array(
									'emp_id' => $arrEmployee[$ind]['emp_id'],
									'supervisor_emp_id' => $arrEmployee[$ind]

['emp_authority_id'],
									'created_by' => 1,
									'created_date' => date($this->arrData

["dateTimeFormat"])		
									);
									
				$this->employee->saveValues(TABLE_EMPLOYEE_SUPERVISORS, $arrValues);
			}
		}
		
		exit;
	}
	
	private function setupConfig($arrConfig) {
		for($ind = 0; $ind < count($arrConfig); $ind++) {
			if(!defined($arrConfig[$ind]['config_key'])) {
				define($arrConfig[$ind]['config_key'], $arrConfig[$ind]['config_value']);
			}
		}
	}
	
	private function setupEmail() {
		
		if(SMTP_HOST != '') {
					
			$this->config->set_item('protocol', 'smtp');
			$this->config->set_item('smtp_host', SMTP_HOST);
			$this->config->set_item('smtp_user', SMTP_USER);
			$this->config->set_item('smtp_pass', SMTP_PASSWORD);
			$this->config->set_item('smtp_port', SMTP_PORT);
			
		} else {
			$this->config->set_item('protocol', 'sendmail');
		}
	}
	
	public function sendEmail($arrTo = array(), $strSubject, $strMessage, $fromEmail = SYSTEM_EMAIL_ADDRESS, $fromName 

= SYSTEM_SENDER_NAME, $arrAttachment = array(), $boolSend = true, $logFileName = "") {
		
		//if((int)SYSTEM_SEND_EMAILS) {
			
			require_once(APPPATH . 'libraries/PHPMailerAutoload.php');
			$objMail = new PHPMailer();
			
			if(SMTP_HOST != '') { 			
				$objMail->isSMTP();
				//$objMail->SMTPDebug = 2;
				$objMail->SMTPSecure = 'tls';
				$objMail->SMTPAuth = true;
				$objMail->Host = SMTP_HOST;
				$objMail->Port = SMTP_PORT;				
				$objMail->Username = SMTP_USER;
				$objMail->Password = SMTP_PASSWORD;							
			} else {
				$objMail->isSendmail();
			}
			
			$objMail->setFrom($fromEmail, $fromName);
			
			for($ind = 0; $ind < count($arrTo); $ind++) {
				$objMail->addAddress($arrTo[$ind]);
			}
			
			$objMail->Subject = $strSubject;
			
			$objMail->msgHTML($strMessage);
			
			if(count($arrAttachment)) {
				for($ind = 0; $ind < count($arrAttachment); $ind++) {
					$objMail->AddAttachment($arrAttachment[$ind]);
				}
			}
			
			if (!$objMail->send()) {
				if(!empty($logFileName))
					$this->debugLogCron("============ MAILER ERROR ============ ::: " . $objMail->ErrorInfo, $logFileName);
				//	echo "Mailer Error: " . $objMail->ErrorInfo; die();
				return false;
			}
		//}
		return true;
	}
	
	public function sendMultiEmail($arrTo = array(), $strSubject, $strMessage, $fromEmail = SYSTEM_EMAIL_ADDRESS, 

$fromName = SYSTEM_SENDER_NAME, $arrAttachment = array(), $boolSend = true) {
		
		if((int)SYSTEM_SEND_EMAILS) {
			
			require_once(APPPATH . 'libraries/PHPMailerAutoload.php');
			$objMail = new PHPMailer();
			
			if(SMTP_HOST != '') { 			
				$objMail->isSMTP();
				//$objMail->SMTPDebug = 2;
				$objMail->SMTPSecure = 'tls';
				$objMail->SMTPAuth = true;
				$objMail->Host = SMTP_HOST;
				$objMail->Port = SMTP_PORT;				
				$objMail->Username = SMTP_USER;
				$objMail->Password = SMTP_PASSWORD;							
			} else {
				$objMail->isSendmail();
			}
			
			$objMail->setFrom($fromEmail, $fromName);
			
			for($ind = 0; $ind < count($arrTo); $ind++) {
				if($arrTo[$ind]['isBCC']) {
					$objMail->addBCC($arrTo[$ind]['emailAdd']);
				} else if($arrTo[$ind]['isCC']) {
					$objMail->addCC($arrTo[$ind]['emailAdd']);
				} else {
					$objMail->addAddress($arrTo[$ind]['emailAdd']);
				}
			}
			
			$objMail->Subject = $strSubject;
			
			$objMail->msgHTML($strMessage);
			
			if(count($arrAttachment)) {
				for($ind = 0; $ind < count($arrAttachment); $ind++) {
					$objMail->AddAttachment($arrAttachment[$ind]);
				}
			}
			
			if (!$objMail->send()) {
				echo "Mailer Error: " . $objMail->ErrorInfo; die();
				return false;
			}
		}
		return true;
	}
}

/* End of file crons.php */
/* Location: ./application/controllers/crons.php */