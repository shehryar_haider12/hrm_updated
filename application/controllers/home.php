<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Master_Controller {
	 
	private $arrData = array();
	public $arrRoleIDs = array();
	
	function __construct() {
		
		parent::__construct();
		
		$this->arrRoleIDs = array(HR_ADMIN_ROLE_ID, WEB_ADMIN_ROLE_ID, HR_EMPLOYEE_ROLE_ID, HR_MANAGER_ROLE_ID);
		$this->arrData["baseURL"] = $this->baseURL;
		$this->arrData["imagePath"] = $this->imagePath;
		$this->arrData["skipModule"] = array('home');
		
	}
	
	public function index()
	{
		$this->load->model('model_task_management', 'task', true);
		$this->load->model('model_policy_announcement', 'policy', true);
		
		$arrWhere = array('ann_status' => 1);
		
		if(!isAdmin($this->userRoleID)) {
			$arrWhere['ann_company_id'] = $this->userCompanyID;
		}
		
		$this->arrData['arrAnnouncements'] = $this->policy->getAnnouncements($arrWhere, 4);
		
		$strDate = date('Y-m-d');
		
		$arrWhere = array();
		$arrWhere['emp_id'] = $this->userEmpNum;
		$arrWhere['assessment_month'] = PERFORMANCE_DUE_MONTH;
		$arrWhere['assessment_year'] = PERFORMANCE_DUE_YEAR;
		$arrWhere['assessment_status_id > '] = STATUS_ENTERING_TASKS;
		$arrTask = $this->task->getTasks($arrWhere);
		
		if($strDate >= PERFORMANCE_START_DATE && $strDate <= PERFORMANCE_END_DATE && count($arrTask) <= 0) {
			$this->arrData['performanceMessage'] = 'Your self-appraisal submission is due and the last date of submission is ' . date('jS F, Y', strtotime(PERFORMANCE_END_DATE) . '.');
	}
		
		$this->template->write_view('content', 'home', $this->arrData);
		$this->template->render();

	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */