<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends Master_Controller {
	
	private $arrData = array();
	public $arrRoleIDs = array();
	private $maxLinks;
	private $limitRecords;
	private $candidateID = 0;
	
	function __construct() {
		
		parent::__construct();
		
		$this->load->model('model_employee_management', 'employee', true);
		$this->load->model('model_profile', 'my_profile', true);
		$this->load->model('model_recruitment_management', 'recruitment', true);
		
		$arrWhere = array(
						'u.user_id' => $this->userID
						);
						
		$this->candidateID = $this->my_profile->getCandidateID($arrWhere);	
		
		$this->arrRoleIDs = array(HR_ADMIN_ROLE_ID, SUPER_ADMIN_ROLE_ID, WEB_ADMIN_ROLE_ID);
		
		if(!isAdmin($this->userRoleID) && $this->userRoleID > 1) {
			redirect(base_url());
			exit;
		}
		
		$this->arrData["baseURL"] 				= $this->baseURL;
		$this->arrData["imagePath"] 			= $this->imagePath;
		$this->arrData["screensAllowed"] 		= $this->screensAllowed;
		$this->arrData["currentController"] 	= $this->currentController;
		$this->arrData["dateFormat"] 			= DATE_FORMAT;
		$this->arrData["dateTimeFormat"] 		= DATE_TIME_FORMAT;
		$this->arrData["showDateFormat"] 		= SHOW_DATE_TIME_FORMAT;
		$this->arrData["resumeFolder"]			= RESUME_FOLDER;
		$this->arrData["resumeFolderDownload"]	= str_replace('./', '', RESUME_FOLDER);
		$this->arrData["emailTemplatesFolder"]	= EMAIL_TEMPLATE_FOLDER;
		$this->maxLinks 						= MAX_PAGING_VISIBLE;
		$this->limitRecords 					= MAX_RECORDS_LISTING;
		
		$currentActionArray = array();
		while(!empty($this->arrData["screensAllowed"]))
		{
			$temp = array_pop($this->arrData["screensAllowed"]);
			if($temp['module_name']== $this->currentAction){
				array_push($currentActionArray,$temp);
			}
		}
		$currentActionArray = $currentActionArray[0];
		$this->arrData['canWrite'] = $currentActionArray["can_write"];
		$this->arrData['canDelete'] = $currentActionArray["can_delete"];
	}
	
	public function index()
	{
		$moduleID = getValue($this->modulesAllowed, 'module_name', $this->currentController, 'module_id');
		$this->arrData['allowedSubModulesList'] = $this->privilege->getPrivilege($this->userRoleID, $moduleID, true);
		
		# CODE FOR CURRENT CANDIDATE RECORD
		$arrWhere = array(
						'c.candidate_id' => $this->candidateID
						);
						
		$this->arrData['arrCandidate'] = $this->my_profile->getCandidateDetail($arrWhere);
		
		$this->template->write_view('content', 'profile/index', $this->arrData);
		$this->template->render();
	}
	
	public function update()
	{
		$tblName = TABLE_CANDIDATE;
		$tblNameUsers = TABLE_USER;	
		$tblNameLocations = TABLE_LOCATION;		
		
		$this->load->model('model_user_management', 'user_management', true);
		
		$arrWhere = array();
		$this->arrData['record'] = array();
		$arrWhere = array(
						'c.candidate_id' => $this->candidateID
						);
		
		$changePassWord = false;
		
		#################################### FORM VALIDATION START ####################################
			
		if($this->input->post('changePW') == 1) {
			
			$changePassWord = true;
			$this->form_validation->set_rules('newPassWord', 'New Password', 'trim|required|min_length[8]|matches[confirmPassWord]|xss_clean');
			$this->form_validation->set_rules('confirmPassWord', 'Confirm Password', 'trim|required|min_length[8]|xss_clean');
			$this->form_validation->set_rules('currentPassWord', 'Current Password', 'trim|required|min_length[8]|xss_clean');
			$this->form_validation->set_rules('oldPassWord', 'Old Password', 'trim|required|min_length[8]|md5|callback_checkOldPassword[currentPassWord]|xss_clean');
			
		} else {
			
			$this->form_validation->set_rules('firstName', 'First Name', 'trim|required|alpha_dash_space|min_length[3]|xss_clean');
			$this->form_validation->set_rules('lastName', 'Last Name', 'trim|required|alpha_dash_space|min_length[3]|xss_clean');
			$this->form_validation->set_rules('eMail', 'Email', 'trim|required|valid_email|xss_clean');
			$this->form_validation->set_rules('nicNo', 'NIC Number', 'trim|required|numeric|min_length[13]|max_length[13]|xss_clean');
			$this->form_validation->set_rules('fatherNICNo', 'Father\'s NIC Number', 'trim|numeric|exact_length[13]|xss_clean');
			$this->form_validation->set_rules('Gender', 'Gender', 'trim|required|xss_clean');
			$this->form_validation->set_rules('maritalStatus', 'Marital Status', 'trim|required|xss_clean');
			$this->form_validation->set_rules('dateOfBirth', 'Date of Birth', 'trim|required|xss_clean');
			$this->form_validation->set_rules('Country', 'Country', 'trim|required|numeric|xss_clean');
			$this->form_validation->set_rules('City', 'City', 'trim|required|numeric|xss_clean');
			$this->form_validation->set_rules('contactNo', 'Contact Number', 'trim|xss_clean');
			$this->form_validation->set_rules('homePhoneNo', 'Home Phone Number', 'trim|xss_clean');
			$this->form_validation->set_rules('candidateAddress', 'Address', 'trim|required|xss_clean');
			$this->form_validation->set_rules('Comments', 'Comments', 'trim|xss_clean');
		
			if(isset($_POST['Area'])){	
				$this->form_validation->set_rules('Area', 'Area', 'trim|required|numeric|xss_clean');
			}			
		}
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			if($changePassWord) {
				
				$arrValues = array(
									'plain_password' => $this->input->post("newPassWord"),
									'password' => md5($this->input->post("newPassWord")),
									'modified_date' => date($this->arrData["dateTimeFormat"])
									);
											
				$this->user_management->saveValues($tblNameUsers, $arrValues, array('user_id' => $this->userID));
				$this->session->set_flashdata('success_message', 'Your Password is modified Successfully');
				
			} else {
				if(
					$this->my_profile->notExistingCandidate('email', $this->input->post('eMail'), $this->candidateID) &&
					$this->my_profile->notExistingCandidate('nic_number', $this->input->post("nicNo"), $this->candidateID)
				) {
					$uploadConfig['upload_path'] = $this->arrData["resumeFolder"];
					$uploadConfig['allowed_types'] = 'doc|docx|pdf';
					$uploadConfig['max_size']	= '1024';
					$uploadConfig['max_filename']	= '100';
					$uploadConfig['file_name']	= strtolower($this->input->post("firstName") . '_' . $this->input->post("lastName")) . '_' . date('dmYHis');
		
					$this->load->library('upload', $uploadConfig);
					
					if(!$this->upload->do_upload('Resume')) {
						$error = array('error' => $this->upload->display_errors());		
						$this->arrData['error_message'] = $error['error'];
					} else {		
						$dataUpload = $this->upload->data();
						$fileName = basename($dataUpload['file_name']);
						if($fileName != $this->input->post("cvFileName")) {
							unlink($this->arrData["resumeFolder"] . $this->input->post("cvFileName")); # DELETE PREVIOUS RESUME FILE
						}
					}
					
					$statusID = (int)$this->input->post("Status"); 
					if($statusID == -1) {
						$statusID = 0;
					}
					$arrValues = array(
										'first_name' => $this->input->post("firstName"),
										'last_name' => $this->input->post("lastName"),
										'contact_number' => $this->input->post("contactNo"),
										'home_contact_number' => $this->input->post("homePhoneNo"),
										'candidate_address' => $this->input->post("candidateAddress"),
										'email' => $this->input->post("eMail"),
										'nic_number' => $this->input->post("nicNo"),
										'father_nic_number' => $this->input->post("fatherNICNo"),
										'gender' => $this->input->post("Gender"),
										'marital_status' => $this->input->post("maritalStatus"),
										'dob' => $this->input->post("dateOfBirth"),
										'city' => $this->input->post("City"),
										'area' => $this->input->post("Area"),
										'country' => $this->input->post("Country"),
										'comment' => $this->input->post("Comments")		
										);
					
					$arrValues['modified_by'] = $this->userEmpNum;
					$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
										
					if(trim($fileName) != '') {
						$arrValues['cv_file_name'] = $fileName;
					}
					
					$candidateID = $this->my_profile->saveValues($tblName, $arrValues, array('candidate_id' => $this->candidateID));					
				
					# SET LOG
				
				} else {
					$this->arrData['validation_error_message'] = '<p>' . $this->input->post("eMail") . ', This E-Mail/NIC is already registered</p>';
				}
				$this->session->set_flashdata('success_message', 'Your Profile is updated Successfully');
			}
				
			//..	$this->session->set_flashdata('success_message', 'Profile Updated Successfully');
			redirect(base_url() . $this->currentController . '/');
			exit;
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT CANDIDATE RECORD
		$this->arrData['record'] = $this->my_profile->getCandidateDetail($arrWhere);
		
		# CODE FOR PAGE CONTENT
		$this->arrData['Countries'] = $this->configuration->getLocations(array('location_type_id' => '2'));
		if((int)$this->arrData['record']['city']) {
			$countryID = $this->configuration->getLocationParent(array('location_id' => (int)$this->arrData['record']['city']));
			$this->arrData['countryID'] = (int)$countryID['location_parent_id'];
			$this->arrData['Cities'] = $this->configuration->getLocations(array('location_parent_id' => $this->arrData['countryID']));			
		}
		$this->arrData['Genders'] = $this->config->item('genders');
		$this->arrData['maritalStatuses'] = $this->config->item('marital_status');
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'profile/update', $this->arrData);
		$this->template->render();
	}
	
	public function application_form($appID = 0) {
		
		$tblName = TABLE_CANDIDATE_APPLICATION;
		$appCandidateID = $this->candidateID;
		
		if(!$appCandidateID) {
			redirect(base_url() . $this->currentController);
			exit;
		}
		
		$this->load->model('model_user_management', 'user_management', true);
		
		$arrWhere = array();
		$this->arrData['record'] = array();
		$arrWhere = array('a.candidate_id' => $this->candidateID);
		
		#################################### FORM VALIDATION START ####################################
		
		if($this->input->post())
		{
		$this->form_validation->set_rules('currentSalary', 'Current Salary', 'trim|required|xss_clean|numeric|max_length[7]');
		$this->form_validation->set_rules('expectedSalary', 'Expected Salary', 'trim|required|xss_clean|numeric|max_length[7]');
		$this->form_validation->set_rules('isWorkedBefore', 'isWorkedBefore', 'trim|required|xss_clean');
		if($this->input->post("isWorkedBefore") == 1) {
			$this->form_validation->set_rules('previousPosition', 'Previous Position', 'trim|required|xss_clean');
		}
		
		$this->form_validation->set_rules('firstReferenceFullName', 'First Reference Person Name', 'trim|required|alpha_dash_space|min_length[3]|xss_clean');
		$this->form_validation->set_rules('firstReferenceEmail', 'First Reference Person Email', 'trim|required|valid_email|xss_clean');
		$this->form_validation->set_rules('firstReferenceContactNo', 'First Reference Person Number', 'trim|required|xss_clean|callback_strValidate[firstReferenceContactNo]');
		$this->form_validation->set_rules('firstReferenceAddress', 'First Reference Person Address', 'trim|required|xss_clean');
		
		$this->form_validation->set_rules('secondReferenceFullName', 'Second Reference Person Name', 'trim|required|alpha_dash_space|min_length[3]|xss_clean');
		$this->form_validation->set_rules('secondReferenceEmail', 'Second Reference Person Email', 'trim|required|valid_email|xss_clean');
		$this->form_validation->set_rules('secondReferenceContactNo', 'Second Reference Person Number', 'trim|required|xss_clean|callback_strValidate[secondReferenceContactNo]');
		$this->form_validation->set_rules('secondReferenceAddress', 'Second Reference Person Address', 'trim|required|xss_clean');
		}
		
		#################################### FORM VALIDATION END ####################################
		
		# CODE FOR CURRENT CANDIDATE RECORD
		$this->arrData['record'] = $this->my_profile->getCandidateApplicationDetail($arrWhere);
		//..	print("<PRE>"); print_r($this->arrData['record']);
		
		$previousPosition = ($this->input->post("isWorkedBefore") == 1) 	? 	$this->input->post("previousPosition")	:	"";
		
		if ($this->form_validation->run() == true) {
			# CODE FOR INSERT CANDIDATE RECORD
			$arrValues = array(
								'candidate_id' => $this->candidateID,
								'current_salary' => $this->input->post("currentSalary"),
								'expected_salary' => $this->input->post("expectedSalary"),
								'is_worked_before' => $this->input->post("isWorkedBefore"),
								'previous_position_id' => $previousPosition,
								'first_reference_name' => $this->input->post("firstReferenceFullName"),
								'first_reference_email' => $this->input->post("firstReferenceEmail"),
								'first_reference_contact_number' => $this->input->post("firstReferenceContactNo"),
								'first_reference_address' => $this->input->post("firstReferenceAddress"),
								'second_reference_name' => $this->input->post("secondReferenceFullName"),
								'second_reference_email' => $this->input->post("secondReferenceEmail"),
								'second_reference_contact_number' => $this->input->post("secondReferenceContactNo"),
								'second_reference_address' => $this->input->post("secondReferenceAddress"),
								);
			
			if(isset($this->arrData['record']['candidate_id'])) {
				$arrValues['modified_by'] = $this->userID;
				$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
			} else {
				$arrValues['created_by'] = $this->userID;
				$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
			}
			
			if(isset($this->arrData['record']['candidate_id']))
				$this->my_profile->saveValues($tblName, $arrValues, array('candidate_id' => $this->arrData['record']['candidate_id']));
			else
				$this->my_profile->saveValues($tblName, $arrValues);
			
			$this->session->set_flashdata('success_message', 'Your Application is updated Successfully');
			redirect(base_url() . $this->currentController . '/');
			exit;
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR PAGE CONTENT
		$this->arrData['jobPositions'] = $this->employee->populateJobTitles();
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'profile/application_form', $this->arrData);
		$this->template->render();
	}
	
	public function strValidate($strValue, $strField) {
		
		if($strField == 'firstReferenceContactNo') { 
			$this->form_validation->set_message('strValidate', 'First Reference Person Number field must only contain numbers.');
			return (preg_match("/^[0-9+-]*$/u", $strValue)) ? true : false;    
		} else if($strField == 'secondReferenceContactNo') { 
			$this->form_validation->set_message('strValidate', 'Second Reference Person Number field must only contain numbers.');
			return (preg_match("/^[0-9+-]*$/u", $strValue)) ? true : false;    
		}

		return false;
		
 	}
	
	public function download_resume($fileName = '') {
		
		if($this->candidateID && file_exists($this->arrData["resumeFolder"] . $fileName)) {
			
			header('Content-Type: text/doc');
			header('Content-Disposition: attachment;filename="'.$fileName.'"');
			header('Cache-Control: max-age=0');
			readfile('./' . $this->arrData["resumeFolder"] . '/' . $fileName);			
			exit;
		
		} else {
			redirect(base_url() . $this->currentController);
			exit;
		}
		
	}
	
	public function apply_vacancy($vacancyID = 0) {		
		$tblName = TABLE_CANDIDATE_VACANCY;
		$tblNameHistory = TABLE_CANDIDATE_HISTORY;
		
		if(!$vacancyID) {
			redirect(base_url() . $this->currentController);
			exit;
		}
		
		$candidateID = $this->candidateID;
		$arrValues = array(
							'candidate_id' => $this->candidateID,
							'vacancy_id' => $vacancyID,
							'status' => 1,
							'applied_date' => date($this->arrData["dateTimeFormat"])
							);
							
		$this->my_profile->saveValues($tblName, $arrValues);			
		
		$arrValuesHistory = array(
							'candidate_id' => $this->candidateID,
							'vacancy_id' => $vacancyID,
							'performed_by' => $this->userEmpNum,
							'performed_date' => date($this->arrData["dateTimeFormat"]),
							'status_id' => 1,
							'note' => 'Applied for vacancy by candidate'
							);								
		
		$this->my_profile->saveValues($tblNameHistory, $arrValuesHistory);
		
		# SHOOT EMAIL
		# Template: apply_vacancy.html
		
		$arrCandidate = $this->my_profile->getCandidateDetail(array('c.candidate_id' => $this->candidateID));
		$arrVacancy = $this->my_profile->getVacancies(array('v.vacancy_id' => $vacancyID));
		$arrVacancy = $arrVacancy[0];
	
		$arrValues = array(
							'[HEADER_LOGO_LINK]' => EMAIL_HEADER_LOGO,
							'[CANDIDATE_NAME]' => $arrCandidate['first_name'] . ' ' . $arrCandidate['last_name'],
							'[VACANCY_NAME]' => $arrVacancy['vacancy_name'],
							'[COPYRIGHT_TEXT]' => 'Copyright ' . date('Y') . ' Naval Anchorage Club. All Rights Reserved.'
							);
							
		$emailHTML = getHTML($arrValues, 'apply_vacancy.html');

		$this->sendEmail(
							$arrTo = array(
											$arrCandidate['email']
											), 				# RECEIVER DETAILS
							'Application Received', 		# SUBJECT
							$emailHTML						# EMAIL HTML MESSAGE
						);
								
		# SET LOG
					
		$this->session->set_flashdata('success_message', 'You Have Successfully Applied To Vacancy');
		redirect(base_url() . $this->currentController . '/applied_vacancies/');
		exit;
	}
	
	public function education_history($eduID = 0)
	{		
		$tblName = TABLE_CANDIDATE_EDUCATION;
		$eduCandidateID = $this->candidateID;
		
		if(!$eduCandidateID) {
			redirect(base_url() . $this->currentController);
			exit;
		}
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == 1) {				
				if(!$this->my_profile->deleteValue($tblName, array('edu_id' => (int)$this->input->post("record_id")))) {
					echo "0"; exit;
				} else {
					echo "1"; exit;
				}								
			}
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('eduCandidateID', 'Candidate', 'trim|required|numeric');
		$this->form_validation->set_rules('eduLevel', 'Qualification Level', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('eduInstitute', 'Educational Institute', 'trim|required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('eduMajors', 'Majors', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('eduGPA', 'GPA', 'trim|xss_clean');
		$this->form_validation->set_rules('eduEnded', 'Passing Year', 'trim|required|min_length[4]|xss_clean');
		#$this->form_validation->set_rules('eduStatus', 'Education Status', 'trim|required|xss_clean');
		#$this->form_validation->set_rules('eduTo', 'Education Ending Date', 'trim|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
				
		$eduID = (int)$eduID;
		$arrWhere['e.edu_candidate_id'] = $eduCandidateID;
		$this->arrData['record'] = array();
		
		if($eduID) {			
			$arrWhere['e.edu_id'] = $eduID;
		}
		
		if ($this->form_validation->run() == true) {
							
			$arrValues = array(
								'edu_candidate_id' => $this->input->post("eduCandidateID"),
								'edu_level_id' => $this->input->post("eduLevel"),
								'edu_institute' => $this->input->post("eduInstitute"),
								'edu_major_id' => $this->input->post("eduMajors"),
								'edu_gpa' => ($this->input->post("eduGPA") != FALSE) ? $this->input->post("eduGPA") : NULL,
								'edu_ended' => $this->input->post("eduEnded"),
								#'edu_status' => $strEduStatus
								);	
								
			if($eduID) {
				$arrValues['modified_by'] = $this->userEmpNum;
				$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
			} else {
				$arrValues['created_by'] = $this->userEmpNum;
				$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
			}
								
			if($eduID) {
				$this->my_profile->saveValues($tblName, $arrValues, $arrWhere);
			} else {
				$this->my_profile->saveValues($tblName, $arrValues);
			}
			
			$arrValues = array();
			$arrValues['modified_by'] = $this->userEmpNum;
			$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
			
			$tblNameCandidate = TABLE_CANDIDATE;
			$this->my_profile->saveValues($tblNameCandidate, $arrValues, array('candidate_id' => $eduCandidateID));
			
			$this->session->set_flashdata('success_message', 'Educational history saved successfully');
			redirect(base_url() . $this->currentController . '/' . $this->currentAction);
			exit;
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT EDUCATION RECORD
		if($eduID) {
			$this->arrData['record'] = $this->recruitment->getEducationalHistory($arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# CODE FOR PAGE CONTENT
		$this->arrData['eduCandidateID'] = $eduCandidateID;
		$this->arrData['eduLevels'] = $this->configuration->getValues(TABLE_EDUCATION_LEVELS, 'edu_level_id, edu_level_name', array('edu_level_status' => 1));
		$this->arrData['eduMajors'] = $this->configuration->getValues(TABLE_EDUCATION_MAJORS, 'edu_major_id, edu_major_name', array('edu_major_status' => 1));
		$this->arrData['arrCandidate'] = $this->my_profile->getCandidateDetail(array('c.candidate_id' => $eduCandidateID));
		$this->arrData['arrRecords'] = $this->recruitment->getEducationalHistory(array('e.edu_candidate_id' => $eduCandidateID));
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'profile/education_history', $this->arrData);
		$this->template->render();
	}
	
	public function work_history($workID = 0)
	{		
		$tblName = TABLE_CANDIDATE_WORK_EXPERIENCE;
		$workCandidateID = $this->candidateID;
		
		if(!$workCandidateID) {
			redirect(base_url() . $this->currentController);
			exit;
		}
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == 1) {				
				if(!$this->my_profile->deleteValue($tblName, array('work_id' => (int)$this->input->post("record_id")))) {
					echo "0"; exit;
				} else {
					echo "1"; exit;
				}								
			}
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('workCandidateID', 'Candidate', 'trim|required|numeric');
		$this->form_validation->set_rules('workCompany', 'Company Name', 'trim|required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('workJobTitle', 'Job Title', 'trim|required|alpha_dash_space|min_length[5]|xss_clean');
		$this->form_validation->set_rules('workFrom', 'Starting Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('workTo', 'Work Ending Date', 'trim|xss_clean');
		$this->form_validation->set_rules('workDescription', 'Work Description', 'trim|max_length[1000]|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
				
		$workID = (int)$workID;
		$arrWhere['work_candidate_id'] = $workCandidateID;
		$this->arrData['record'] = array();
		
		if($workID) {			
			$arrWhere['work_id'] = $workID;
		}
		
		if ($this->form_validation->run() == true) {
			$arrValues = array(
								'work_candidate_id' => $this->input->post("workCandidateID"),
								'work_company' => $this->input->post("workCompany"),
								'work_job_title' => $this->input->post("workJobTitle"),
								'work_from' => $this->input->post("workFrom"),
								'work_to' => $this->input->post("workTo"),
								'work_description' => $this->input->post("workDescription")
								);		
								
			if($workID) {
				$arrValues['modified_by'] = $this->userEmpNum;
				$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
			} else {
				$arrValues['created_by'] = $this->userEmpNum;
				$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
			}							
								
			if($workID) {
				$this->my_profile->saveValues($tblName, $arrValues, $arrWhere);
			} else {
				$this->my_profile->saveValues($tblName, $arrValues);
			}			
				
			$arrValues = array();
			$arrValues['modified_by'] = $this->userEmpNum;
			$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
			
			$tblNameCandidate = TABLE_CANDIDATE;
			$this->my_profile->saveValues($tblNameCandidate, $arrValues, array('candidate_id' => $workCandidateID));
			
			$this->session->set_flashdata('success_message', 'Work history saved successfully');
			redirect(base_url() . $this->currentController . '/' . $this->currentAction);
			exit;
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT WORK RECORD
		if($workID) {
			$this->arrData['record'] = $this->recruitment->getWorkHistory($arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# CODE FOR PAGE LOADING
		$this->arrData['workCandidateID'] = $workCandidateID;
		$this->arrData['arrCandidate'] = $this->my_profile->getCandidateDetail(array('c.candidate_id' => $workCandidateID));
		$this->arrData['arrRecords'] = $this->recruitment->getWorkHistory(array('work_candidate_id' => $workCandidateID));
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'profile/work_history', $this->arrData);
		$this->template->render();
	}
	
	public function applied_vacancies($recordID = 0)
	{		
		$tblName = TABLE_CANDIDATE_VACANCY;
		$tblNameCandidate = TABLE_CANDIDATE;
		$tblNameHistory = TABLE_CANDIDATE_HISTORY;
		$tblNameEmp = TABLE_EMPLOYEE;
		$tblNameEmpEdu = 'hrm_employee_education';
		$tblNameEmpWork = 'hrm_employee_work_experience';
		
		$vacCandidateID = $this->candidateID;
		
		if(!$vacCandidateID) {
			redirect(base_url() . $this->currentController);
			exit;
		}
		
		# CODE FOR PAGE LOADING
		$this->arrData['vacCandidateID'] = $vacCandidateID;
		$this->arrData['arrCandidate'] = $this->my_profile->getCandidateDetail(array('c.candidate_id' => $vacCandidateID));
		$this->arrData['arrCanStatuses'] = $this->my_profile->getCandidateStatuses();	
		$this->arrData['arrVacancies'] = $this->my_profile->getVacancies();
		$this->arrData['arrRecords'] = $this->my_profile->getCandidateVacancies(array('candidate_id' => $vacCandidateID));
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'profile/applied_vacancies', $this->arrData);
		$this->template->render();
	}
	
	public function open_vacancies($pageNum = 1) {
				
		$tblName = TABLE_VACANCY;
		$this->my_profile->saveValues($tblName, array('vacancy_status' => 3), array('vacancy_to < ' => date('Y-m-d')));
		
		# CODE FOR PAGING		
		$arrWhere = array(
							'cv.candidate_id' => $this->candidateID
							);
		
		$totalCount = $this->my_profile->getOpenVacancies($arrWhere);
		$totalCount = count($totalCount);		
		$this->arrData['totalRecordsCount'] = (int)$totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->my_profile->getOpenVacancies($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($totalCount / $this->limitRecords);				
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListJobVacancy');		
		
		# CODE FOR PAGE LOADING
		$this->arrData['candidateID'] = $this->candidateID;
		$this->arrData['jobTitles'] = $this->configuration->getJobTitles(array('job_title_status' => 1));	
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'profile/open_vacancies', $this->arrData);
		$this->template->render();
	}
	
	public function checkOldPassword($strCurrentPassword, $strOldPassword) {
		if($strCurrentPassword == $this->input->post($strOldPassword)) {
			return true;
		}
		$this->form_validation->set_message('checkOldPassword', 'Incorrect Old Password');
		return false;
	}
}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */