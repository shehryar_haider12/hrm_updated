<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forum extends Master_Controller {
	
	private $arrData = array();
	public $arrRoleIDs = array();
	private $maxLinks;
	private $limitRecords;
	private $employeeID = 0;
	public $maxSMSToSend = 100;
	
	function __construct() {
		
		parent::__construct();
		
		$this->load->model('model_sbt_forum', 'forum', true);
		$this->load->model('model_employee_management', 'employee', true);
		$this->load->model('model_task_management', 'task', true);
		
		$this->arrRoleIDs       				= array(HR_ADMIN_ROLE_ID, WEB_ADMIN_ROLE_ID, SUPER_ADMIN_ROLE_ID, HR_MANAGER_ROLE_ID, HR_EMPLOYEE_ROLE_ID);
		$this->arrData["baseURL"] 				= $this->baseURL . '/';
		$this->arrData["imagePath"] 			= $this->imagePath;
		$this->arrData["screensAllowed"] 		= $this->screensAllowed;
		$this->arrData["currentController"] 	= $this->currentController;
		$this->arrData["dateFormat"] 			= DATE_FORMAT;
		$this->arrData["dateTimeFormat"] 		= DATE_TIME_FORMAT;
		$this->arrData["showDateFormat"] 		= SHOW_DATE_TIME_FORMAT;
		$this->arrData["smsFileFolder"]			= str_replace('./', '', BULK_SMS_FILE_FOLDER);
		$this->arrData["pictureFolder"]			= PROFILE_PICTURE_FOLDER;
		$this->arrData["pictureFolderShow"]		= str_replace('./', '', PROFILE_PICTURE_FOLDER);
		$this->arrData["emailTemplatesFolder"]	= EMAIL_TEMPLATE_FOLDER;
		$this->maxLinks 						= MAX_PAGING_VISIBLE;
		$this->limitRecords 					= MAX_RECORDS_LISTING;
		$this->arrData["pollHeadersFolder"]		= POLL_HEADERS_FILE_FOLDER;
		$this->arrData["smsAPI"]				= $this->config->item('sms_api');
		$this->arrData["forcedAccessRoles"]		= $this->config->item('forced_access_roles');
		$this->arrData['strHierarchy'] 			= $this->employee->getHierarchy($this->userEmpNum);
		
		$currentActionArray = array();
		while(!empty($this->arrData["screensAllowed"]))
		{
			$temp = array_pop($this->arrData["screensAllowed"]);
			if($temp['module_name']== $this->currentAction){
				array_push($currentActionArray,$temp);
			}
		}
		$currentActionArray = $currentActionArray[0];
		$this->arrData['canWrite'] = $currentActionArray["can_write"];
		$this->arrData['canDelete'] = $currentActionArray["can_delete"];
		
	}
	
	public function index() 
	{		
		$moduleID = getValue($this->modulesAllowed, 'module_name', $this->currentController, 'module_id');
		$this->arrData['allowedSubModulesList'] = getValue($this->modulesAllowedForMenu, 'module_name', $this->currentController, 'sub_menu'); 
		$this->template->write_view('content', 'forum/index', $this->arrData);
		$this->template->render();	
	}
	
	public function birthday_calendar()
	{
		if((int)$_POST['selMonth']) {
			$cMonth = $_POST['selMonth'];
		} else {
			$cMonth = date("m");
		}
		
		$arrWhere = array(
				'emp_dob like' => '%-'.$cMonth.'-%',
				'emp_employment_status < ' => STATUS_EMPLOYEE_SEPARATED,
				'emp_status' => STATUS_ACTIVE
				
			);
			
		$arrBirthdayDetails = $this->forum->getValues(TABLE_EMPLOYEE, 'emp_dob', $arrWhere);
		for($ind = 0; $ind < count($arrBirthdayDetails);  $ind++)
		{
			$arrDOB[] = explode('-',$arrBirthdayDetails[$ind]['emp_dob']);
			$arrBirthdayDOB[] = ltrim($arrDOB[$ind][2], "0");
		}
		
		sort($arrBirthdayDOB);
		$this->arrData['birthdays'] = array_count_values(array_values($arrBirthdayDOB));
			
		# TEMPLATE LOADING
		$this->template->write_view('content', 'forum/birthdays/birthday_calendar', $this->arrData);
		$this->template->render();
	}
	
	public function birthdays_detail($month, $day)
	{
		$cDay = sprintf('%02d', $day);
		
		$arrWhere = array(
				'e.emp_dob like ' => '%-'.$month.'-'.$cDay,
				'e.emp_employment_status < ' => STATUS_EMPLOYEE_SEPARATED,
				'emp_status' => STATUS_ACTIVE
			);
		
		$this->arrData['arrBirthdayDetails'] = $this->forum->getBirthdayDetails($arrWhere);
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'forum/birthdays/birthday_details', $this->arrData);
		$this->template->render();
	}
	
	public function list_events($pageNum = 1) {
		
		if((int)$pageNum <= 0) {
			$pageNum = 1;
		}	
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {				
				if($this->task->deleteValue(TABLE_FORUM_EVENTS, null, array('event_id' => (int)$this->input->post("record_id")))) {
					$this->task->deleteValue(TABLE_FORUM_EVENTS_IMAGES, null, array('event_id' => (int)$this->input->post("record_id")));
					echo "1"; exit;
				} else {				
					# SET LOG
					echo "0"; exit;
				}		
			}
		}
		
		$arrWhere = array();
		
		if ($this->input->post()) {
			
			if($this->input->post("companyID")) {
				$arrWhere['e.company_id'] = $this->input->post("companyID");
				$this->arrData['companyID'] = $this->input->post("companyID");
			}
			
			if($this->input->post("dateFrom")) {
				$arrWhere['e.event_date >= '] = $this->input->post("dateFrom");
				$this->arrData['dateFrom'] = $this->input->post("dateFrom");
			}
			
			if($this->input->post("dateTo")) {
				$arrWhere['e.event_date <= '] = $this->input->post("dateTo");
				$this->arrData['dateTo'] = $this->input->post("dateTo");
			}
		}
		
		# CODE FOR DISPLAYING DATA OF SELECTED BOOKING
		
		$this->arrData['totalRecordsCount'] = $this->forum->getTotalEvents($arrWhere);
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->forum->getEvents($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#/', 'frmListEvents');
		
		$this->arrData['arrCompanies'] = $this->configuration->getCompanies();
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'forum/list_events', $this->arrData);
		$this->template->render();
	}
	
	public function save_event($eventID = 0) {
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {				
				if($this->forum->deleteValue(TABLE_FORUM_EVENTS_IMAGES, null, array('image_id' => (int)$this->input->post("record_id")))) {
					echo "1"; exit;
				} else {				
					# SET LOG
					echo "0"; exit;
				}		
			}
		}
		
		#################################### FORM VALIDATION START ####################################		
				
		$this->form_validation->set_rules('eventTitle', 'Title', 'trim|required|xss_clean');
		$this->form_validation->set_rules('eventDesc', 'Description', 'trim|xss_clean');
		$this->form_validation->set_rules('eventDate', 'Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('companyID', 'Company', 'trim|required|numeric|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			
			$boolFileUp = true;
			$strFileName = null;
			
			if(!empty($_FILES['eventImages']['name'][0])) {
				
				$this->load->library('upload');
				
				$uploadDocConfig['upload_path'] 	= EVENTS_IMAGES_FOLDER;
				$uploadDocConfig['allowed_types'] 	= 'jpg|jpeg|png|bmp';
				$uploadDocConfig['max_size']		= '1024';
				$uploadDocConfig['max_filename']	= '100';
				$uploadDocConfig['encrypt_name']	= true;
					
				$this->upload->initialize($uploadDocConfig);
				
				foreach($_FILES['eventImages'] as $key => $val)
				{
					$i = 1;
					foreach($val as $v)
					{
						$field_name = "file_".$i;
						$_FILES[$field_name][$key] = $v;
						$i++;   
					}
				}
				
				unset($_FILES['eventImages']);
				
				$arrFileNames = array();
				
				foreach($_FILES as $field_name => $file) {				   	
					if(!$this->upload->do_upload($field_name)) {
						$error = array('error' => $this->upload->display_errors());	
						$this->arrData['error_message'] = $error['error'];	
						$boolFileUp = false;			
					} else {			
						$dataUpload = $this->upload->data();
						$arrFileNames[] = basename($dataUpload['file_name']);
					}
				}
			}
			
			if($boolFileUp) {
			
				$arrValues = array(
									'company_id' => $this->input->post("companyID"),
									'event_title' => $this->input->post("eventTitle"),
									'event_description' => $this->input->post("eventDesc"),
									'event_date' => $this->input->post("eventDate"),
									'created_by' => $this->userEmpNum
								);
				
				if((int)$eventID) {
					$arrValues['modified_by'] = $this->userEmpNum;
					$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
					$this->forum->saveValues(TABLE_FORUM_EVENTS, $arrValues, array('event_id' => $eventID));
				} else {
					$arrValues['created_by'] = $this->userEmpNum;
					$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
					$eventID = $this->forum->saveValues(TABLE_FORUM_EVENTS, $arrValues);
				}
				
				for($ind = 0; $ind < count($arrFileNames); $ind++) {
					$arrValues = array(
									'event_id' => $eventID,
									'image_name' => $arrFileNames[$ind],
									'created_by' => $this->userEmpNum,
									'created_date' => date($this->arrData["dateTimeFormat"])
								);
					$this->forum->saveValues(TABLE_FORUM_EVENTS_IMAGES, $arrValues);
				}
				
				$this->session->set_flashdata('success_message', 'Event saved successfully');
				redirect($this->baseURL . '/' . $this->currentController . '/list_events');
				exit;
				
			} else {
				$this->arrData['validation_error_message'] = validation_errors();
			}
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT EMPLOYEE RECORD
		if($eventID) {
			$this->arrData['arrRecord'] = $this->forum->getEventDetails($eventID);
			$this->arrData['arrRecordImages'] = $this->arrData['arrRecord'][1];
			$this->arrData['arrRecord'] = $this->arrData['arrRecord'][0][0];
		}
		
		$this->arrData['arrCompanies'] = $this->configuration->getCompanies();
		
		$this->template->write_view('content', 'forum/save_event', $this->arrData);
		$this->template->render();
	}
	
	public function event_details($eventID = 0) {
		
		if(!(int)$eventID) {
			redirect(base_url() . 'message/not_found');
			exit;
		} else {
			$this->arrData['arrRecord'] = $this->forum->getEventDetails($eventID);
			$this->arrData['arrRecordImages'] = $this->arrData['arrRecord'][1];
			$this->arrData['arrRecord'] = $this->arrData['arrRecord'][0][0];
		}
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'forum/event_details', $this->arrData);
		$this->template->render();
	}
	
	public function checkTimeDifference() {
		$reserveFrom = strtotime($this->input->post("dateReservedFrom"));
		$reserveTo = strtotime($this->input->post("dateReservedTo"));				
		$hourDiff = round((strtotime($this->input->post("dateReservedTo")) - strtotime($this->input->post("dateReservedFrom")))/3600, 1);
		
		if($reserveTo < $reserveFrom) {
			$this->form_validation->set_message('checkTimeDifference', 'Reserve To time should not be less than Reserved From time. Correct Time Slot.');
			return FALSE;
		} else if($hourDiff < 1) {
			$this->form_validation->set_message('checkTimeDifference', 'Reservation Time Slot should be atleast an hour. Correct Time Slot.');
			return FALSE;
		}	
	}
}