<?php
$empTitle 				= $_POST['empTitle'];
$empFirstName 			= $_POST['empFirstName'];
$empSecondName 			= $_POST['empSecondName'];
$empLastName 			= $_POST['empLastName'];
$empFamilyName 			= $_POST['empFamilyName'];
$empRPTName 			= $_POST['empRPTName'];
$empPassportName 		= $_POST['empPassportName'];
$empPassportNo 			= $_POST['empPassportNo'];
$empPassportExpiry 		= $_POST['empPassportExpiry'] ? $_POST['empPassportExpiry'] : 'NILL';
$empFatherName 			= $_POST['empFatherName'];
$empMotherName 			= $_POST['empMotherName'];
$empSpouseName 			= $_POST['empSpouseName'];
$empEmail 				= $_POST['empEmail'];
$empPassord 			= $_POST['passWord'];
$empCode 				= $_POST['empCode'];
$empIP 					= $_POST['empIP'];
$empWorkEmail 			= $_POST['empWorkEmail'];
$empNICNo 				= $_POST['empNICNo'];
$empGender 				= $_POST['empGender'];
$empBloodGroup			= $_POST['empBloodGroup'];
$empMaritalStatus 		= $_POST['empMaritalStatus'];
$empReligion 			= $_POST['empReligion'];
$empDOB					= $_POST['empDOB'];
$empAddress				= $_POST['empAddress'];
$empPermAddress			= $_POST['empPermAddress'];
$empCountryID 			= $_POST['Country'];
$empNationalityID 		= $_POST['Nationality'];
$empHomePhone			= $_POST['empHomePhone'];
$empCellPhone			= $_POST['empCellPhone'];
$empRoleID 				= $_POST['empRoleID'];
$empCompany				=	$_POST['empCompany'];
$empID					=	$_POST['empID'];
$empIPNum				=	$_POST['empIPNum'];
$empDepartment			=	$_POST['empDepartment'];
$empWorkEmail			=	$_POST['empWorkEmail'];
$empJobLocation			=	$_POST['empJobLocation'];
$empOT					=	$_POST['empOT'];
$empDesignation			=	$_POST['empDesignation'] ? $_POST['empDesignation'] : 'NILL';
$empSupervisor			=	$_POST['empSupervisor'];
$empCurrency			=	$_POST['empCurrency'];
$empSponsor				=	$_POST['empSponsor'] ? $_POST['empSponsor'] : 'NILL';
$empEmploymentType		=	$_POST['empEmploymentType'];
$empEmploymentStatus	=	$_POST['empEmploymentStatus'];
$empJoiningDate			=	$_POST['empJoiningDate'] ? $_POST['empJoiningDate'] : 'NILL';
$empProbationEndDate	=	$_POST['empProbationEndDate'] ? $_POST['empProbationEndDate'] : 'NILL';
$empConfirmationDate	=	$_POST['empConfirmationDate'] ? $_POST['empConfirmationDate'] : 'NILL';
$empEndDate				=	$_POST['empEndDate'] ? $_POST['empEndDate'] : '0';
$empVisaCompany			=	$_POST['empVisaCompany'];
$empVisaIssueDate		=	$_POST['empVisaIssueDate'] ? $_POST['empVisaIssueDate'] : '0';
$empVisaExpiryDate		=	$_POST['empVisaExpiryDate'] ? $_POST['empVisaExpiryDate'] : '0';
$empNICIssueDate		=	$_POST['empNICIssueDate'] ? $_POST['empNICIssueDate'] : '0';
$empNICExpiryDate		=	$_POST['empNICExpiryDate'] ? $_POST['empNICExpiryDate'] : '0';
$empLabourIssueDate		=	$_POST['empLabourIssueDate'] ? $_POST['empLabourIssueDate'] : '0';
$empLabourExpiryDate	=	$_POST['empLabourExpiryDate'] ? $_POST['empLabourExpiryDate'] : '0';
$empAnnualLeaves		=	$_POST['empAnnualLeaves'] ? $_POST['empAnnualLeaves'] : '0';
$empSickLeaves			=	$_POST['empSickLeaves'] ? $_POST['empSickLeaves'] : '0';

?>
<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( "#empDOB" ).datepicker( "setDate", "<?php echo $empDOB; ?>" );
	$( "#empDOB" ).datepicker( "option", "maxDate", '<?php echo date('Y-m-d', strtotime('18 years ago')); ?>' );
	<?php if($empPassportExpiry != '0000-00-00') { ?>
		$( "#empPassportExpiry" ).datepicker( "setDate", "<?php echo $empPassportExpiry; ?>" );
		$( "#empPassportExpiry" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	<?php if($empJoiningDate != '0000-00-00') { ?>
		$( "#empJoiningDate" ).datepicker( "setDate", "<?php echo $empJoiningDate; ?>" );
	<?php } ?>
	<?php if($empProbationEndDate != '0000-00-00') { ?>
		$( "#empProbationEndDate" ).datepicker( "setDate", "<?php echo $empProbationEndDate; ?>" );
	<?php } ?>
	<?php if($empConfirmationDate != '0000-00-00') { ?>
		$( "#empConfirmationDate" ).datepicker( "setDate", "<?php echo $empConfirmationDate; ?>" );
	<?php } ?>
	<?php if($empEndDate != '0000-00-00') { ?>
		$( "#empEndDate" ).datepicker( "setDate", "<?php echo $empEndDate; ?>" );
	<?php } ?>
	<?php if($empVisaIssueDate != '0000-00-00') { ?>
		$( "#empVisaIssueDate" ).datepicker( "setDate", "<?php echo $empVisaIssueDate; ?>" );
	<?php } ?>
	<?php if($empVisaExpiryDate != '0000-00-00') { ?>
		$( "#empVisaExpiryDate" ).datepicker( "setDate", "<?php echo $empVisaExpiryDate; ?>" );
		$( "#empVisaExpiryDate" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	<?php if($empNICIssueDate != '0000-00-00') { ?>
		$( "#empNICIssueDate" ).datepicker( "setDate", "<?php echo $empNICIssueDate; ?>" );
	<?php } ?>
	<?php if($empNICExpiryDate != '0000-00-00') { ?>
		$( "#empNICExpiryDate" ).datepicker( "setDate", "<?php echo $empNICExpiryDate; ?>" );
		$( "#empNICExpiryDate" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	<?php if($empLabourIssueDate != '0000-00-00') { ?>
		$( "#empLabourIssueDate" ).datepicker( "setDate", "<?php echo $empLabourIssueDate; ?>" );
	<?php } ?>
	<?php if($empLabourExpiryDate != '0000-00-00') { ?>
		$( "#empLabourExpiryDate" ).datepicker( "setDate", "<?php echo $empLabourExpiryDate; ?>" );
		$( "#empLabourExpiryDate" ).datepicker( "option", "minDate", "0" );
	<?php } ?>
	
});
</script>
<form name="frmAddEmployee" id="frmAddEmployee" enctype="multipart/form-data" method="post">
  <div class="listPageMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
        <tr>
            <td class="formHeaderRow" colspan="6">
            	Add Employee
            </td>
        </tr>
        <tr>
            <td class="formHeaderRow" colspan="2" align="center" width="30%">Personal Details</td>
            <td class="formHeaderRow" colspan="2" align="center" width="30%">Contact Details</td>
            <td class="formHeaderRow" colspan="2" align="center" width="40%">Contract Details</td>
        </tr>
		<tr>
            <td class="formLabelContainer">Title:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
                <select id="empTitle" name="empTitle" class="dropDown" tabindex="1">
                  <option value="">Select Title</option>
                  <?php
                  if (count($Salutations)) {
                      foreach($Salutations as $salutationVal) {
                          $selected = '';
                          if($empTitle == $salutationVal) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $salutationVal; ?>" <?php echo $selected; ?>><?php echo $salutationVal; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>
            </td>
            <td class="formLabelContainer">Local Address:</td>
            <td class="formTextBoxContainer">
        		<textarea rows="5" cols="30" name="empAddress" id="empAddress" class="textArea" tabindex="20"><?php echo $empAddress; ?></textarea>
            </td>
            <td class="formLabelContainer">Company:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<select id="empCompany" name="empCompany" class="dropDown" tabindex="26">
                  <option value="">Select Company</option>
                  <?php
                  if (count($empCompanies)) {
                      foreach($empCompanies as $arrCompany) {
                          $selected = '';
                          if($empCompany == $arrCompany['company_id']) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $arrCompany['company_id']; ?>" <?php echo $selected; ?>><?php echo $arrCompany['company_name']; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>
            </td>
        </tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">First Name:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
                <input type="text" name="empFirstName" maxlength="50" id="empFirstName" class="textBox" value="<?php echo $empFirstName; ?>" tabindex="2">
            </td>
            <td class="formLabelContainer">Permanent Address:</td>
            <td class="formTextBoxContainer">
        		<textarea rows="5" cols="30" name="empPermAddress" id="Address" class="textArea" tabindex="21"><?php echo $empPermAddress; ?></textarea>
            </td>
            <td class="formLabelContainer">Employee ID:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empID" id="empID" class="textBox" value="<?php echo $empID; ?>" tabindex="27">
            </td>
        </tr>
		<tr>
            <td class="formLabelContainer">Second Name:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empSecondName" maxlength="50" id="empSecondName" class="textBox" value="<?php echo $empSecondName; ?>" tabindex="3">
            </td>
            <td class="formLabelContainer">Country:</td>
            <td class="formTextBoxContainer" id="tdlocation">
        		<select id="Country" name="Country" class="dropDown" tabindex="22">
                  	<option value="">Select Country</option>
                    <?php 
                    if (count($Countries)) {
                        foreach($Countries as $dataCountry) {
							$strSelected = '';
							if($empCountryID == $dataCountry['location_id']) {
								$strSelected = 'selected="selected"';
							}
                    ?>
                        <option value="<?php echo $dataCountry['location_id']; ?>" <?php echo $strSelected; ?>><?php echo $dataCountry['location_name']; ?></option>
                    <?php
                        }
                    }
                    ?>
                </select>
            </td>
            <td class="formLabelContainer">IP Extension:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empIPNum" maxlength="4" id="empIPNum" class="textBox" value="<?php echo $empIPNum; ?>" tabindex="28">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Last Name:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empLastName" maxlength="50" id="empLastName" class="textBox" value="<?php echo $empLastName; ?>" tabindex="4">
            </td>
            <td class="formLabelContainer">Nationality:</td>
            <td class="formTextBoxContainer" id="tdlocation">
        		<select id="Nationality" name="Nationality" class="dropDown" tabindex="23">
                  	<option value="">Select Nationality</option>
                    <?php 
                    if (count($Countries)) {
                        foreach($Countries as $dataCountry) {
							$strSelected = '';
							if($empNationalityID == $dataCountry['location_id']) {
								$strSelected = 'selected="selected"';
							}
                    ?>
                        <option value="<?php echo $dataCountry['location_id']; ?>" <?php echo $strSelected; ?>><?php echo $dataCountry['location_name']; ?></option>
                    <?php
                        }
                    }
                    ?>
                </select>
            </td>
            <td class="formLabelContainer">Work Email:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empWorkEmail" maxlength="100" id="empWorkEmail" class="textBox" value="<?php echo $empWorkEmail; ?>" tabindex="29">
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Family Name:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empFamilyName" maxlength="50" id="empFamilyName" class="textBox" value="<?php echo $empFamilyName; ?>" tabindex="5">
            </td>
            <td class="formLabelContainer">Home Phone Number:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empHomePhone" id="empHomePhone" class="textBox" value="<?php echo $empHomePhone; ?>" tabindex="24">
            </td>
            <td class="formLabelContainer">Department:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<select id="empDepartment" name="empDepartment" class="dropDown" tabindex="30">
                  <option value="">Select Department</option>
                  <?php
                  if (count($empDepartments)) {
                      foreach($empDepartments as $departmentVal) {
                          $selected = '';
                          if($empDepartment == $departmentVal['job_category_id']) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $departmentVal['job_category_id']; ?>" <?php echo $selected; ?>><?php echo $departmentVal['job_category_name']; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select> 
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Reporting Person (RPT) Name:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empRPTName" maxlength="50" id="empRPTName" class="textBox" value="<?php echo $empRPTName; ?>" tabindex="6">
            </td>
            <td class="formLabelContainer">Cell Phone Number:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empCellPhone" id="empCellPhone" class="textBox" value="<?php echo $empCellPhone; ?>" tabindex="25">
            </td>
            <td class="formLabelContainer">Designation:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empDesignation" id="empDesignation" class="textBox" value="<?php echo $empDesignation; ?>" tabindex="31">
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Passport Name:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empPassportName" maxlength="50" id="empPassportName" class="textBox" value="<?php echo $empPassportName; ?>" tabindex="7">
            </td>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer">Job Location:</td>
            <td class="formTextBoxContainer">
        		<select id="empJobLocation" name="empJobLocation" class="dropDown" tabindex="32">
                  <option value="">Select Location</option>
                  <?php 
                    if (count($Countries)) {
                        foreach($Countries as $dataCountry) {
							$strSelected = '';
							if($empJobLocation == $dataCountry['location_id']) {
								$strSelected = 'selected="selected"';
							}
                  ?>
                        <option value="<?php echo $dataCountry['location_id']; ?>" <?php echo $strSelected; ?>><?php echo $dataCountry['location_name']; ?></option>
                  <?php
                        }
                    }
                  ?>
              </select> 
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Passport Number:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empPassportNo" maxlength="50" id="empPassportNo" class="textBox" value="<?php echo $empPassportNo; ?>" tabindex="8">
            </td>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer">Supervisor:</td>
            <td class="formTextBoxContainer">
        		<select id="empSupervisor" name="empSupervisor[]" class="dropDown" tabindex="33" multiple="multiple" style="height:100px">
                  <option value="">Select Supervisor</option>
                  <?php
                  if (count($empSupervisors)) {
                      foreach($empSupervisors as $supervisorVal) {
                          $selected = '';
                          if(in_array($supervisorVal['emp_id'], $empSupervisor)) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $supervisorVal['emp_id']; ?>" <?php echo $selected; ?>><?php echo $supervisorVal['emp_full_name']; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Passport Expiry Date:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empPassportExpiry" id="empPassportExpiry" class="textBox datePicker" tabindex="8">
            </td>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer">Over Time (OT) Eligibility:</td>
            <td class="formTextBoxContainer">
        		<select id="empOT" name="empOT" class="dropDown" tabindex="34">
                  <option value="">Select OT Eligibility</option>
                  <option value="Yes">Yes</option>
                  <option value="No">No</option>
              </select>  
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Father Name:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empFatherName" maxlength="50" id="empFatherName" class="textBox" value="<?php echo $empFatherName; ?>" tabindex="8">
            </td>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer">Currency:</td>
            <td class="formTextBoxContainer">
        		<select id="empCurrency" name="empCurrency" class="dropDown" tabindex="35">
                  <option value="">Select Currency</option>
                  <?php 
                    if (count($Countries)) {
                        foreach($Countries as $dataCountry) {
							$strSelected = '';
							if($empCurrency == $dataCountry['location_id']) {
								$strSelected = 'selected="selected"';
							}
                  ?>
                        <option value="<?php echo $dataCountry['location_id']; ?>" <?php echo $strSelected; ?>><?php echo $dataCountry['location_name'] . ' - ' . $dataCountry['location_currency']; ?></option>
                  <?php
                        }
                    }
                  ?>
              </select>  
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Mother Name:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empMotherName" maxlength="50" id="empMotherName" class="textBox" value="<?php echo $empMotherName; ?>" tabindex="9">
            </td>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer">Employment Status:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<select id="empEmploymentStatus" name="empEmploymentStatus" class="dropDown" tabindex="36">
                  <option value="">Select Employment Status</option>
                  <?php
                  if (count($empEmploymentStatuses)) {
                      foreach($empEmploymentStatuses as $employmentStatusVal) {
                          $selected = '';
                          if($empEmploymentStatus == $employmentStatusVal['employment_status_id']) {
                              $selected = 'selected="selected"';
                          }
						  
						  /*if($employmentStatusVal['employment_status_id'] >= 6) {
								  $selected .= ' disabled="disabled"';
							  }*/
                  ?>
                      <option value="<?php echo $employmentStatusVal['employment_status_id']; ?>" <?php echo $selected; ?>><?php echo $employmentStatusVal['employment_status_name']; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select> 
              <script>$('#lblEmpEmploymentStatus').html($('#empEmploymentStatus').find(":selected").text());</script>
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Spouse Name:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empSpouseName" maxlength="50" id="empSpouseName" class="textBox" value="<?php echo $empSpouseName; ?>" tabindex="10">
            </td>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer">Joining Date:<!--<span class="mandatoryStar"> *</span>--></td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empJoiningDate" maxlength="30" id="empJoiningDate" class="textBox datePicker" tabindex="37">
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Personal Email:</td>
            <td class="formTextBoxContainer" id="tdEmail">
        		<input type="text" name="empEmail" id="empEmail" class="textBox" value="<?php echo $empEmail; ?>" tabindex="11">
            </td>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer">Probation End Date:<!--<span class="mandatoryStar"> *</span>--></td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empProbationEndDate" maxlength="30" id="empProbationEndDate" class="textBox datePicker" tabindex="38">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Date of Birth:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empDOB" maxlength="30" id="empDOB" class="textBox datePicker" tabindex="12">
            </td>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer">Confirmation Date:</td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empConfirmationDate" maxlength="30" id="empConfirmationDate" class="textBox datePicker" tabindex="39">
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Gender:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<select id="empGender" name="empGender" class="dropDown" tabindex="13">
                  <option value="">Select Gender</option>
                  <?php
                  if (count($Genders)) {
                      foreach($Genders as $genderVal) {
                          $selected = '';
                          if($empGender == $genderVal) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $genderVal; ?>" <?php echo $selected; ?>><?php echo $genderVal; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select> 
            </td>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer">Employment End Date:</td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empEndDate" maxlength="30" id="empEndDate" class="textBox datePicker" tabindex="39">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Marital Status:<!--<span class="mandatoryStar"> *</span>--></td>
            <td class="formTextBoxContainer">
        		<select id="empMaritalStatus" name="empMaritalStatus" class="dropDown" tabindex="14">
                  <option value="">Select Marital Status</option>
                  <?php
                  if (count($maritalStatuses)) {
                      foreach($maritalStatuses as $maritalVal) {
                          $selected = '';
                          if($empMaritalStatus == $maritalVal) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $maritalVal; ?>" <?php echo $selected; ?>><?php echo $maritalVal; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>  
            </td>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer">Remaining Annual Leaves:</td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empAnnualLeaves" maxlength="3" id="empAnnualLeaves" class="textBox" value="<?php echo $empAnnualLeaves; ?>" tabindex="40">
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Identification Number:<!--<span class="mandatoryStar"> *</span>--></td>
            <td class="formTextBoxContainer" id="tdNIC">
        		<input type="text" name="empNICNo" maxlength="20" id="empNICNo" class="textBox" value="<?php echo $empNICNo; ?>" tabindex="15">
            </td>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer">Remaining Sick Leaves:</td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empSickLeaves" maxlength="3" id="empSickLeaves" class="textBox" value="<?php echo $empSickLeaves; ?>" tabindex="41">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Blood Group:</td>
            <td class="formTextBoxContainer">
            	<select id="empBloodGroup" name="empBloodGroup" class="dropDown" tabindex="16">
                  <option value="">Select Blood Group</option>
                  <?php
                  if (count($bloodGroups)) {
                      foreach($bloodGroups as $bloodGroup) {
                          $selected = '';
                          if($empBloodGroup == $bloodGroup) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $bloodGroup; ?>" <?php echo $selected; ?>><?php echo $bloodGroup; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>
            </td>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer">Visa Company:</td>
            <td class="formTextBoxContainer">
        		 <select id="empVisaCompany" name="empVisaCompany" class="dropDown" tabindex="42">
                  <option value="">Select Visa Company</option>
                  <?php
                  if (count($empCompanies)) {
                      foreach($empCompanies as $arrCompany) {
                          $selected = '';
                          if($empVisaCompany == $arrCompany['company_id']) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $arrCompany['company_id']; ?>" <?php echo $selected; ?>><?php echo $arrCompany['company_name']; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Religion:<!--<span class="mandatoryStar"> *</span>--></td>
            <td class="formTextBoxContainer">
            	<select id="empReligion" name="empReligion" class="dropDown" tabindex="17">
                  <option value="">Select Religion</option>
                  <?php
                  if (count($Religions)) {
                      foreach($Religions as $arrReligion) {
                          $selected = '';
                          if($empReligion == $arrReligion['religion_id']) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $arrReligion['religion_id']; ?>" <?php echo $selected; ?>><?php echo $arrReligion['religion_name']; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>
            </td>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer">Visa Issue Date:</td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empVisaIssueDate" maxlength="30" id="empVisaIssueDate" class="textBox datePicker" tabindex="43">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Profile Picture:</td>
            <td class="formTextBoxContainer">
        		<input type="file" name="empPicture" id="empPicture" tabindex="18" />&nbsp;
				<?php
                if(file_exists($pictureFolder . $empPicture)) {
				?>
                	<input type="hidden" name="picFileName" id="picFileName" value="<?php echo $empPicture; ?>" />
                <?php
				}
				?>
            </td>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer">Visa Expiry Date:</td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empVisaExpiryDate" maxlength="30" id="empVisaExpiryDate" class="textBox datePicker" tabindex="44">
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Role:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
            	<select class="dropDown" name="empRoleID" id="empRoleID" tabindex="19">
					<option value="">Select Role</option>
                    <?php 
                    if (count($userRoles)) {
                        foreach($userRoles as $userRole) {
							$strSelected = '';
							if($empRoleID == $userRole['user_role_id']) {
								$strSelected = 'selected="selected"';
							}
                    ?>
                        <option value="<?php echo $userRole['user_role_id']; ?>" <?php echo $strSelected; ?>><?php echo $userRole['user_role_name']; ?></option>
                    <?php
                        }
                    }
                    ?>
                </select>
      		</td>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer">Sponsor:</td>
            <td class="formTextBoxContainer">
        		 <select id="empSponsor" name="empSponsor" class="dropDown" tabindex="45">
                  <option value="">Select Sponsor</option>
                  <?php
                  if (count($empSponsors)) {
                      foreach($empSponsors as $arrSponsor) {
                          $selected = '';
                          if($empSponsor == $arrSponsor['sponsor_id']) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $arrSponsor['sponsor_id']; ?>" <?php echo $selected; ?>><?php echo $arrSponsor['sponsor_type']; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer">Identification Document Issue Date (EID/CNIC):</td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empNICIssueDate" maxlength="30" id="empNICIssueDate" class="textBox datePicker" tabindex="43">
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer">Identification Document Expiry Date (EID/CNIC):</td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empNICExpiryDate" maxlength="30" id="empNICExpiryDate" class="textBox datePicker" tabindex="43">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer">Labour Card Issue Date:</td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empLabourIssueDate" maxlength="30" id="empLabourIssueDate" class="textBox datePicker" tabindex="43">
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer">Labour Card Expiry Date:</td>
            <td class="formTextBoxContainer">
        		 <input type="text" name="empLabourExpiryDate" maxlength="30" id="empLabourExpiryDate" class="textBox datePicker" tabindex="43">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer"></td>
            <td class="formLabelContainer">Contract Type:</td>
            <td class="formTextBoxContainer">
        		 <select id="empEmploymentType" name="empEmploymentType" class="dropDown" tabindex="46">
                  <option value="">Select Contract Type</option>
                  <?php
                  if (count($employmentTypes)) {
                      foreach($employmentTypes as $employmentType) {
                          $selected = '';
                          if($employmentType == $empEmploymentType) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $employmentType; ?>" <?php echo $selected; ?>><?php echo $employmentType; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>
            </td>
      	</tr>
		<tr>
            <td class="formTextBoxContainer" colspan="6" align="center">
      			<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save" tabindex="46">&nbsp;
        		<input type="button" class="smallButton" value="Back" onclick="history.go(-1)">      
    		</td>
        </tr>
  </table>
  </div>
</form>
<script>
	<?php if($canWrite == NO) { ?>
	$("#frmAddEmployee :input").attr("disabled", true);
	<?php } ?>
</script>