<?php
$payrollMonth 	= 	$_POST['payrollMonth'];
$payrollYear 	= 	$_POST['payrollYear'];
?>
<form name="frmAddDocument" id="frmAddDocument" method="post" enctype="multipart/form-data">
  <div class="employeeFormMain">
  <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
  	<tr>
        <td class="formHeaderRow" colspan="2">Salary Details</td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Month:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
          <select name="payrollMonth" id="payrollMonth" class="dropDown">
              <option value="">Select Month</option>
              <?php
              if (count($arrMonths)) {
                  foreach($arrMonths as $strKey => $strValue) {
              ?>
              <option value="<?php echo $strKey; ?>"><?php echo $strValue; ?></option>
              <?php
                  }
              }
              ?>
          </select>
      </td>
    </tr>
    <tr>
      <td class="formLabelContainer">Year:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
      	  <select name="payrollYear" id="payrollYear" class="dropDown">
              <option value="">Select Year</option>
              <?php for($ind = $this->salaryYearStarted; $ind <= date('Y'); $ind++) { ?>
              <option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
              <?php } ?>
          </select>
      </td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer"></td>
      <td class="formTextBoxContainer">
      	<input type="submit" class="smallButton" name="btnSearch" id="btnSearch" value="Search" onClick="$('#txtExport').val('0')">
        <?php if(count($record)) { ?>
        <input type="hidden" id="txtExport" name="txtExport" value="<?php echo (int)$_POST['txtExport']; ?>">
        <input type="submit" class="smallButton" name="btnExport" id="btnExport" value="Export PDF" onClick="$('#txtExport').val('1')">
        <?php } ?>
      </td>
    </tr>
    <tr>
    	<td colspan="2" height="25px">&nbsp;</td>
    </tr>
  </table>
  </div>
</form>
<br  />
<?php
if(count($record)) {
	$currencyCode = getValue($arrLocations, 'location_id', $record['company_currency_id'], 'location_currency_code') . ' ';
	$totalEarning = (int)$record['payroll_earning_basic'] + 
					(int)$record['payroll_earning_housing'] +
					(int)$record['payroll_earning_transport'] +
					(int)$record['payroll_earning_utility'] +
					(int)$record['payroll_earning_travel'] +
					(int)$record['payroll_earning_health'] +
					(int)$record['payroll_earning_fuel'] +
					(int)$record['payroll_earning_mobile'] +
					(int)$record['payroll_earning_medical_relief'] +
					(int)$record['payroll_earning_bonus'] +
					(int)$record['payroll_earning_annual_leave_encashment'] +
					(int)$record['payroll_earning_claims'] +
					(int)$record['payroll_earning_commission'] +
					(int)$record['payroll_earning_annual_ticket'] +
					(int)$record['payroll_earning_gratuity'] +
					(int)$record['payroll_earning_survey_expense'] +
					(int)$record['payroll_earning_settlement'] +
					(int)$record['payroll_earning_misc'];
	
	$arrEarning = array(
						'payroll_earning_basic' => 'Basic Salary',
						'payroll_earning_housing' => 'Housing Allowance',
						'payroll_earning_transport' => 'Transport Allowance',
						'payroll_earning_utility' => 'Utility Allowance',
						'payroll_earning_travel' => 'Travel Expenses',
						'payroll_earning_survey_expense' => 'Travel/Survey Expense',
						'payroll_earning_commission' => 'Commission',
						'payroll_earning_health' => 'Health Allowance',
						'payroll_earning_fuel' => 'Fuel Allowance',
						'payroll_earning_mobile' => 'Mobile/Telephone Allowance',
						'payroll_earning_medical_relief' => 'Medical Relief',
						'payroll_earning_bonus' => 'Bonus',
						'payroll_earning_annual_ticket' => 'Annual Ticket',
						'payroll_earning_claims' => 'Claims',
						'payroll_earning_annual_leave_encashment' => 'Annual Leave Encashment',
						'payroll_earning_gratuity' => 'Gratuity',
						'payroll_earning_settlement' => 'Final Settlement',
						'payroll_earning_misc' => 'Others'
						);
					
	$totalDeduction = (int)$record['payroll_deduction_tax'] + 
					  (int)$record['payroll_deduction_pf'] + 
					  (int)$record['payroll_deduction_loan'] + 
					  (int)$record['payroll_deduction_eobi'] + 
					  (int)$record['payroll_deduction_telephone'] + 
					  (int)$record['payroll_deduction_misc'];
	
	$arrDeduction = array(
						'payroll_deduction_tax' => 'Income Tax',
						'payroll_deduction_pf' => 'Provident Fund',
						'payroll_deduction_loan' => 'Salary Advance/Loan Recovery',
						'payroll_deduction_eobi' => 'EOBI',
						'payroll_deduction_telephone' => 'Telephone Expense',
						'payroll_deduction_misc' => 'Others'
						);
?>
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain dottedBorder">
	<tr class="listHeader">
    	<td class="listHeaderCol" width="50%" align="center">Earning</th>
    	<td class="listHeaderCol" width="50%" align="center">Deduction</th>
    </tr>
    <tr>
    	<td style="vertical-align:top">
        	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
            	<?php
				$ind = 0;
				$cssClassE = '';
                foreach($arrEarning as $colName => $txtLabel) {
					if($record[$colName] > 0) {
						$ind++;
				?>
            	<tr<?php echo $cssClassE; ?>>
                	<td class="formLabelContainer"><?php echo $txtLabel; ?></td>
                    <td class="formTextBoxContainer"><?php echo $currencyCode . number_format($record[$colName], 2); ?></td>
                </tr>
                <?php
						if($cssClassE == '') {
							$cssClassE = ' class="formAlternateRow"';
						} else {
							$cssClassE = '';
						}
					}
				}
				?>                
            	<tr<?php echo $cssClassE; ?>>
                	<td class="formLabelContainer"><b>Total</b></td>
                    <td class="formTextBoxContainer"><b><?php echo $currencyCode . number_format($totalEarning, 2); ?></b></td>
                </tr>
            </table>
        </td>
        <td style="vertical-align:top">
        	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
                <?php
				$jnd = 0;
				$cssClassD = '';
                foreach($arrDeduction as $colName => $txtLabel) {
					if($record[$colName] > 0) {
						$jnd++;
				?>
            	<tr<?php echo $cssClassD; ?>>
                	<td class="formLabelContainer"><?php echo $txtLabel; ?></td>
                    <td class="formTextBoxContainer"><?php echo $currencyCode . number_format($record[$colName], 2); ?></td>
                </tr>
                <?php
						if($cssClassD == '') {
							$cssClassD = ' class="formAlternateRow"';
						} else {
							$cssClassD = '';
						}
					}
				}
				?>
				
				<?php
                for($knd = $jnd; $knd < $ind; $knd++) {
				?>
            	<tr<?php echo $cssClassD; ?>>
                	<td class="formLabelContainer"></td>
                    <td class="formTextBoxContainer"></td>
                </tr>
                <?php
					if($cssClassD == '') {
						$cssClassD = ' class="formAlternateRow"';
					} else {
						$cssClassD = '';
					}
                }
				?>
            	<tr<?php echo $cssClassD; ?>>
                	<td class="formLabelContainer"><b>Total</b></td>
                    <td class="formTextBoxContainer"><b><?php echo $currencyCode . number_format($totalDeduction, 2); ?></b></td>
                </tr>
                <?php
				if($cssClassD == '') {
					$cssClassD = ' class="formAlternateRow"';
				} else {
					$cssClassD = '';
				}
				?>
            </table>
        </td>
    </tr>
    <tr<?php echo $cssClassD; ?>>
    	<td class="formLabelContainer"><b>Net Salary:</b></td>
		<td class="formTextBoxContainer"><b><?php echo $currencyCode . number_format((int)($totalEarning - $totalDeduction), 2); ?></b></td>
    </tr>
	<?php
    if($cssClassD == '') {
        $cssClassD = ' class="formAlternateRow"';
    } else {
        $cssClassD = '';
    }
    ?>
    
</table>
<?php
} else {
	if(count($_POST)) {
?>
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
	<tr class="listContentAlternate">
		<td align="center" class="listContentCol">No Record Found</td>
	</tr>
</table>
<?php } 
}
?>
<script>
  $('#payrollMonth').val('<?php echo $payrollMonth; ?>');
  $('#payrollYear').val('<?php echo $payrollYear; ?>');
</script>