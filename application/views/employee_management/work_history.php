<?php
$workCompany 		= (isset($_POST['workCompany'])) 			? $_POST['workCompany'] 		: $record['work_company'];
$workJobTitle 		= (isset($_POST['workJobTitle'])) 			? $_POST['workJobTitle'] 		: $record['work_job_title'];
$workFrom 			= (isset($_POST['workFrom'])) 				? $_POST['workFrom'] 			: $record['work_from'];
$workTo 			= (isset($_POST['workTo'])) 				? $_POST['workTo'] 				: $record['work_to'];
$workDescription 	= (isset($_POST['workDescription'])) 		? $_POST['workDescription'] 	: $record['work_description'];
?>
<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( ".datePicker" ).datepicker( "option", "maxDate", 0 );
	$( "#workFrom" ).datepicker( "setDate", "<?php echo $workFrom; ?>" );
	<?php if($workTo != '0000-00-00') { ?>
	$( "#workTo" ).datepicker( "setDate", "<?php echo $workTo; ?>" );
	<?php } ?>
});
</script>
<?php if($canWrite == YES) { ?>
<form name="frmWorkExp" id="frmWorkExp" method="post">
  <div class="employeeFormMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
    <tr>
    	<td class="formHeaderRow" colspan="2">Add/Edit Working Experience</td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Company Name:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer"><input type="text" name="workCompany" maxlength="100" id="workCompany" class="textBox" value="<?php echo $workCompany; ?>"></td>
    </tr>
    <tr>
      <td class="formLabelContainer">Job Title:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer"><input type="text" name="workJobTitle" maxlength="100" id="workJobTitle" class="textBox" value="<?php echo $workJobTitle; ?>"></td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Starting Date:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer"><input type="text" name="workFrom" maxlength="10" id="workFrom" class="textBox datePicker" value="<?php echo $workFrom; ?>"></td>
    </tr>
    <tr>
      <td class="formLabelContainer">Ending Date:</td>
      <td class="formTextBoxContainer"><input type="text" name="workTo" maxlength="10" id="workTo" class="textBox datePicker" value="<?php echo $workTo; ?>"></td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Job Description:</td>
      <td class="formTextBoxContainer"><textarea rows="6" cols="30" name="workDescription" id="workDescription" class="textArea"><?php echo $workDescription; ?></textarea></td>
    </tr>
    <tr>
      <td class="formLabelContainer"><input type="hidden" name="workEmployeeID" id="workEmployeeID" value="<?php echo $workEmployeeID; ?>"></td>
      <td class="formTextBoxContainer">
      	<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save">&nbsp;
        <input type="button" class="smallButton" value="Back" onclick="history.go(-1)">
      </td>
    </tr>
  </table>
  </div>
</form>
<br  />
<?php } ?>
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain dottedBorder">
	<tr class="listHeader">
    	<td class="listHeaderCol">Company</td>
    	<td class="listHeaderCol">Job Title</td>
    	<td class="listHeaderCol">Duration</td>
    	<td class="listHeaderCol">Description</td>
        <?php if($canWrite == YES) { ?>
    	<td class="listHeaderColLast">Action</td>
		<?php } ?>
    </tr>
    <?php
    for($ind = 0; $ind < count($arrRecords); $ind++) {
	?>
    <tr class="listContent">
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['work_company']; ?></td>
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['work_job_title']; ?></td>
    	<td class="listContentCol">
			<?php 
			if($arrRecords[$ind]['work_to'] != '0000-00-00') {
				echo dateDifference($arrRecords[$ind]['work_from'], $arrRecords[$ind]['work_to']);
			} else {
				echo 'Since ' . readableDate($arrRecords[$ind]['work_from'], $showDateFormat);
			}
			?>
        </td>
    	<td class="listContentCol"><?php echo (!empty($arrRecords[$ind]['work_description'])) ? $arrRecords[$ind]['work_description'] : ' - '; ?></td>
        <?php if($canWrite == YES) { ?>
    	<td class="listContentColLast">
        	<div class="empColButtonContainer">
			<?php if($canWrite == YES) { ?>
        	<input type="button" class="smallButton" value="View/Edit" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $workEmployeeID . '/' . $arrRecords[$ind]['work_id']; ?>';" />
            <?php } if($canDelete == YES) { ?>
            <input type="button" class="smallButton" value="Delete" onclick="deleteRecord('/<?php echo $this->currentController . '/' . $this->currentAction; ?>/<?php echo $arrEmployee['emp_id']; ?>', '<?php echo $arrRecords[$ind]['work_id']; ?>');" />
            <?php } ?>
			</div>
        </td>
        <?php } ?>
    </tr>
    <?php
	}
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="5" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
<br />
<?php if($canWrite == NO) { ?>
<script>$("#frmWorkExp :input").attr("disabled", true);</script>
<?php } ?>