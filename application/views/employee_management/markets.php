<?php
$market	=	(isset($_POST['market']))	?	$_POST['market']	:	$record['location_id'];
?>

<?php //if($canWrite == YES) { ?>
<form name="frmMarkets" id="frmMarkets" method="post">
  <div class="employeeFormMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
    <tr>
    	<td class="formHeaderRow" colspan="2">Add/Edit Markets</td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Market:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
      	<select id="market" name="market" class="dropDown">
            <option value="">All</option>
            <?php
            if (count($arrMarkets)) {
                foreach($arrMarkets as $arrMarket) {
            ?>
                <option value="<?php echo $arrMarket['location_id']; ?>"><?php echo $arrMarket['location_name']; ?></option>
            <?php
                }
            }
            ?>
        </select>
      </td>
    </tr>    
    <tr>
      <td class="formLabelContainer"><input type="hidden" name="employeeID" id="employeeID" value="<?php echo $arrEmployee['emp_id']; ?>"></td>
      <td class="formTextBoxContainer">
      	<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save">&nbsp;
        <input type="button" class="smallButton" value="Back" onclick="history.go(-1)">
      </td>
    </tr>
  </table>
  </div>
</form>
<br  />
<?php //} ?>

<script>
	$('#market').val('<?php echo $market; ?>');
</script>

<table border="0" cellspacing="0" cellpadding="0" class="listTableMain dottedBorder">
	<tr class="listHeader">
    	<td class="listHeaderCol">Market</td>
        <?php //if($canWrite == YES) { ?>
    	<td class="listHeaderColLast">Action</td>
		<?php //} ?>
    </tr>
    <?php
    for($ind = 0; $ind < count($arrRecords); $ind++) {
	?>
    <tr class="listContent">
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['location_name']; ?></td>
        <?php //if(($canWrite == YES) || ($canDelete == YES)) { ?>
    	<td class="listContentColLast">
        	<div class="empColButtonContainer">
			<?php //if($canWrite == YES) { ?>
        	<input type="button" class="smallButton" value="View/Edit" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $arrEmployee['emp_id'] . '/' . $arrRecords[$ind]['emp_market_id']; ?>';" />
            <?php //} if($canDelete == YES) { ?>
            <input type="button" class="smallButton" value="Delete" onclick="deleteRecord('/<?php echo $this->currentController . '/' . $this->currentAction; ?>/<?php echo $arrEmployee['emp_id']; ?>', '<?php echo $arrRecords[$ind]['emp_market_id']; ?>');" />
            <?php //} ?>
			</div>
        </td>
        <?php //} ?>
    </tr>
    <?php
	}
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="5" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
<?php if($canWrite == NO) { ?>
<!-- <script>$("#frmMarkets :input").attr("disabled", true);</script> -->
<?php } ?>

<div class="internalMessageBox"> Only HR department is authorised to make any change in Market Details. If any modification is required in this profile, please request HR Department.</div>