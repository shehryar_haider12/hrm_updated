<?php
$txtSortField 	= (isset($_POST['sort_field'])) 	? $_POST['sort_field'] 		: '';
$txtSortOrder 	= (isset($_POST['sort_order'])) 	? $_POST['sort_order'] 		: '';
?>
<form name="frmListEmployees" id="frmListEmployees" method="post" action="<?php echo $frmActionURL; ?>">
  <div class="searchBoxMain">
    <div class="searchHeader">Search Criteria</div>
    <div class="searchcontentmain">
      <div class="searchCol">
        <div class="labelContainer">Employee Code:</div>
        <div class="textBoxContainer">
			<input type="text" id="empCode" name="empCode" maxlength="4" class="textBox" value="<?php echo $empCode; ?>">
        </div>
        <div class="labelContainer">IP Extension:</div>
        <div class="textBoxContainer">
          <input type="text" id="empIP" name="empIP" maxlength="4" class="textBox" value="<?php echo $empIP; ?>">
        </div>              
        <div class="labelContainer">Supervisor:</div>
        <div class="textBoxContainer">
        	<select name="empSupervisor" id="empSupervisor" class="dropDown">
            	<option value="">All</option>
                <?php
                if (count($arrSupervisors)) {
                    foreach($arrSupervisors as $key => $arrSupervisor) {
                ?>
                    <optgroup label="<?php echo $key; ?>">
                        <?php for($i=0;$i<count($arrSupervisor);$i++) { ?>					
                            <option value="<?php echo $arrSupervisor[$i]['emp_id']; ?>"><?php echo $arrSupervisor[$i]['emp_full_name']; ?></option>
                        <?php } ?>
                    </optgroup>
                <?php	}
                }
                ?>
            </select>
        </div>
      </div>
      <div class="searchCol">
        <div class="labelContainer">Company:</div>
        <div class="textBoxContainer">
        	<select id="empCompany" name="empCompany" class="dropDown">
                <option value="">All</option>
                <?php
                  for($ind = 0; $ind < count($arrCompanies); $ind++) {
                      $selected = '';
                      if($arrCompanies[$ind]['company_id'] == $empCompany) {
                          $selected = 'selected="selected"';
                      }
                ?>
                 <option value="<?php echo $arrCompanies[$ind]['company_id']; ?>" <?php echo $selected; ?>><?php echo $arrCompanies[$ind]['company_name']; ?></option>
                <?php
                  }
                ?>
              </select>
        </div>
        <div class="labelContainer">Bank:</div>
        <div class="textBoxContainer">
        	<select id="empBank" name="empBank" class="dropDown">
                <option value="">All</option>
                <?php
                  for($ind = 0; $ind < count($arrBanks); $ind++) {
                      $selected = '';
                      if($arrBanks[$ind]['bank_id'] == $empBank) {
                          $selected = 'selected="selected"';
                      }
                ?>
                 <option value="<?php echo $arrBanks[$ind]['bank_id']; ?>" <?php echo $selected; ?>><?php echo $arrBanks[$ind]['bank_name']; ?></option>
                <?php
                  }
                ?>
              </select>
        </div>
        <div class="labelContainer">Department:</div>
        <div class="textBoxContainer">
          <select id="empDepartment" name="empDepartment" class="dropDown">
            <option value="">All</option>
            <?php
			  for($ind = 0; $ind < count($empDepartments); $ind++) {
				  $selected = '';
				  if($empDepartments[$ind]['job_category_id'] == $empDepartment) {
					  $selected = 'selected="selected"';
				  }
            ?>
             <option value="<?php echo $empDepartments[$ind]['job_category_id']; ?>" <?php echo $selected; ?>><?php echo $empDepartments[$ind]['job_category_name']; ?></option>
            <?php
              }
            ?>
          </select>
        </div>
      </div>
      <div class="buttonContainer">
      	<input type="hidden" name="sort_field" id="sort_field" value="<?php echo $txtSortField; ?>" />
      	<input type="hidden" name="sort_order" id="sort_order" value="<?php echo $txtSortOrder; ?>" />
        <input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Search">
      </div>
    </div>
  </div> 
  <script>
  	$('#empCode').val('<?php echo $empCode; ?>');
  	$('#empIP').val('<?php echo $empIP; ?>');
  	$('#empDesignation').val('<?php echo $empDesignation; ?>');
	$('#empDepartment').val('<?php echo $empDepartment; ?>');
  	$('#empSupervisor').val('<?php echo $empSupervisor; ?>');
  	$('#empCompany').val('<?php echo $empCompany; ?>');
  	$('#empBank').val('<?php echo $empBank; ?>');
  </script>
</form>

<div class="centerElementsContainer">
	<div class="recordCountContainer">
		<?php echo "Total Records Count: " . $totalRecordsCount; ?>
    </div>
	
	<?php
	if($pageLinks) {
	?>
	<div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
	<?php 	}	?>
</div>

	<div class="listContentMain">
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
  <tr class="listHeader">
    <td class="listHeaderCol">Employee Name</td>
    <td class="listHeaderCol">Code</td>
    <td class="listHeaderCol">Bank</td>
    <td class="listHeaderCol">Branch</td>
    <td class="listHeaderCol">Account Number</td>
    <td class="listHeaderCol">Pay Mode</td>
    <td class="listHeaderCol">Payroll Execution</td>
    <td class="listHeaderColLast">Action</td>
  </tr>
  <?php
    for($ind = 0; $ind < count($arrRecords); $ind++) {
		$trCSSClass = '';
		if($ind % 2 == 0) {
			$trCSSClass = ' class="listContentAlternate"';
		}
	?>
  <tr<?php echo $trCSSClass; ?> id="tr<?php echo $ind; ?>">
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_full_name']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_code']; ?></td>
    <td class="listContentCol"><?php echo getValue($arrBanks, 'bank_id', $arrRecords[$ind]['emp_salary_bank_id'], 'bank_name'); ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_salary_bank_branch']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_salary_bank_account_number']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_pay_mode']; ?></td>
    <td class="listContentCol"><?php echo ((int)$arrRecords[$ind]['emp_payroll_execution']) ? 'Yes' : 'No'; ?></td>
    <td class="listContentColLast">
      <div class="empColButtonContainer">
        <?php if($canWrite == YES) { ?>
      	<img title="Details" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/display.png';?>" onclick="showPopup('<?php echo $this->baseURL . '/' . $this->currentController . '/employee_detail/' . $arrRecords[$ind]['emp_id']; ?>', 650, 500)" />
      	<img title="View/Edit" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/view.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/save_account/' . $arrRecords[$ind]['emp_id']; ?>';">
      	<?php } ?>
	  </div>
	  </td>
  </tr>
  <?php
	} 
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="8" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
</div>
<div style="clear:both">&nbsp;<div>