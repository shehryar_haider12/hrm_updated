<div class="listContentMain">
    <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
	<tr class="listHeader">
    	<td class="listHeaderCol">Vacancy</td>
    	<td class="listHeaderCol">Applied Date</td>
    	<td class="listHeaderCol">Status</td>
    </tr>
    <?php
    for($ind = 0; $ind < count($arrRecords); $ind++) {
	?>
    <tr class="listContent">
    	<td class="listContentCol"><?php echo getValue($arrVacancies, 'vacancy_id', $arrRecords[$ind]['vacancy_id'], 'vacancy_name'); ?></td>
    	<td class="listContentCol"><?php echo readableDate($arrRecords[$ind]['applied_date'], $showDateFormat); ?></td>
    	<td class="listContentCol"><?php echo getValue($arrCanStatuses, 'status_id', $arrRecords[$ind]['status'], 'status_title'); ?></td>    	
    </tr>
    <?php
	}
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="3" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
</div>