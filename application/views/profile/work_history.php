<?php
$workCompany = (set_value('workCompany') != '') ? set_value('workCompany') : $record['work_company'];
$workJobTitle = (set_value('workJobTitle') != '') ? set_value('workJobTitle') : $record['work_job_title'];
$workFrom = (set_value('workFrom') != '') ? set_value('workFrom') : $record['work_from'];
$workTo = (set_value('workTo') != '') ? set_value('workTo') : $record['work_to'];
$workDescription = (set_value('workDescription') != '') ? set_value('workDescription') : $record['work_description'];
?>
<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( ".datePicker" ).datepicker( "option", "maxDate", 0 );
	$( "#workFrom" ).datepicker( "setDate", "<?php echo $workFrom; ?>" );
	<?php if($workTo != '0000-00-00') { ?>
	$( "#workTo" ).datepicker( "setDate", "<?php echo $workTo; ?>" );
	<?php } ?>
});
</script>
<form name="frmWorkExp" id="frmWorkExp" method="post">
<div class="listPageMain">
  <div class="formMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
    <tr>
    	<td class="formHeaderRow" colspan="2">Add/Edit Working Experience</td>
    </tr>
    <tr>
      <td class="formLabelContainer">Candidate Name:</td>
      <td class="formTextBoxContainer"><div><?php echo $arrCandidate['first_name'] . ' ' . $arrCandidate['last_name']; ?></div></td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Company Name:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer"><input type="text" name="workCompany" maxlength="100" id="workCompany" class="textBox" value="<?php echo $workCompany; ?>"></td>
    </tr>
    <tr>
      <td class="formLabelContainer">Job Title:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer"><input type="text" name="workJobTitle" maxlength="100" id="workJobTitle" class="textBox" value="<?php echo $workJobTitle; ?>"></td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Starting Date: <span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer"><input type="text" name="workFrom" maxlength="10" id="workFrom" class="textBox datePicker" value="<?php echo $workFrom; ?>"></td>
    </tr>
    <tr>
      <td class="formLabelContainer">Ending Date:</td>
      <td class="formTextBoxContainer"><input type="text" name="workTo" maxlength="10" id="workTo" class="textBox datePicker" value="<?php echo $workTo; ?>"></td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Comments:</td>
      <td class="formTextBoxContainer"><textarea rows="9" cols="30" name="workDescription" id="workDescription" class="textArea"><?php echo $workDescription; ?></textarea></td>
    </tr>
    <tr>
      <td class="formLabelContainer"><input type="hidden" name="workCandidateID" id="workCandidateID" value="<?php echo $workCandidateID; ?>"></td>
      <td class="formTextBoxContainer">
      	<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save">&nbsp;
        <input type="button" class="smallButton" value="Back" onclick="history.go(-1)">
      </td>
    </tr>
  </table>
  </div>
  
  <div class="internalMessageBox"> You can enter multiple records in your experience list.</div>
  
  </div>
</form>
<br  />
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
	<tr class="listHeader">
    	<td class="listHeaderCol">Company Name</td>
    	<td class="listHeaderCol">Job Title</td>
    	<td class="listHeaderCol">Duration</td>
    	<td class="listHeaderCol">Description</td>
    	<td class="listHeaderColLast">Action</td>
    </tr>
    <?php
    for($ind = 0; $ind < count($arrRecords); $ind++) {
	?>
    <tr class="listContent">
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['work_company']; ?></td>
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['work_job_title']; ?></td>
    	<td class="listContentCol">
			<?php 
			if($arrRecords[$ind]['work_to'] != '0000-00-00') {
				echo dateDifference($arrRecords[$ind]['work_from'], $arrRecords[$ind]['work_to']);
			} else {
				echo 'Since ' . readableDate($arrRecords[$ind]['work_from'], $showDateFormat);
			}
			?>
        </td>
    	<td class="listContentCol"><?php echo (!empty($arrRecords[$ind]['work_description'])) ? $arrRecords[$ind]['work_description'] : ' - '; ?></td>
    	<td class="listContentColLast">
        	<div class="empColButtonContainer">
			<?php if($canWrite == 1) { ?>
        	<input type="button" class="smallButton" value="View/Edit" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/' . $this->currentAction . '/' . $arrRecords[$ind]['work_id']; ?>';" />
            <?php } if($canDelete == 1) { ?>
            <input type="button" class="smallButton" value="Delete" onclick="deleteRecord('/<?php echo $this->currentController . '/' . $this->currentAction; ?>/<?php echo $arrCandidate['candidate_id']; ?>', '<?php echo $arrRecords[$ind]['work_id']; ?>');" />
            <?php } ?>
			</div>
        </td>
    </tr>
    <?php
	}
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="5" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
<?php //echo buildGrid($arrRecords, array('work_id', 'work_candidate_id'), $currentController . '/work_history/', array('work_candidate_id', 'work_id')); ?>
<br />
<?php if($canWrite == 0) { ?>
<script>$("#frmWorkExp :input").attr("disabled", true);</script>
<?php } ?>