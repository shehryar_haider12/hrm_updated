<?php
$css_path = $this->config->item("css_path");
$style_css_path = $this->config->item("style_css_path");
$script_path = $this->config->item("script_path");
$this->baseURL = $this->config->item('base_url');
?>
<!DOCTYPE html>
<html>
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Naval Anchorage Club - HRMS</title>
	
	<link type="text/css" rel="stylesheet" href="<?php echo $css_path; ?>jquery-ui.css">
	<link type="text/css" rel="stylesheet" href="<?php echo $style_css_path; ?>">
	<link type="text/css" rel="stylesheet" href="<?php echo $css_path; ?>reset.css">
	<script type="text/javascript" src="<?php echo $script_path; ?>/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="<?php echo $script_path; ?>/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo $script_path; ?>/common.js"></script>	
	<?php echo $font; ?>
  </head>
  <body>

	<div class="mainBody">
		<div class="mainDiv">
			<!----- Header Start ----->
			<div class="headerMain">
				<?php echo $header; ?>
			</div>
			<!-----	Header End ----->
			
				<?php 
				if(trim($this->session->flashdata('success_message')) != '')
					$success_message = trim($this->session->flashdata('success_message'));
				if ($success_message != '' || $error_message != '' || $validation_error_message != '') { ?>
					<div class="messageBox">
						<?php if($success_message != '') {?>
						<div class="successMessage">
							<?php echo $success_message; ?>
						</div>
						<?php } ?>

						<?php if($error_message != '') {?>
						<div class="errorMessage">
							<?php echo $error_message; ?>
						</div>
						<?php } ?>
						
						<?php if($validation_error_message != '') { ?>
						<div class="errorMessage">
							<?php echo $validation_error_message; ?>
						</div>
						<?php } ?>
					</div>
				<?php } ?>
				
			<!----- Mid Section Start  ----->
			<div class="midMain">
				<?php echo $content; ?>
			</div>
			<!----- Mid Section End ----->
			
			<!----- Footer Start ----->
			<div class="footerMain">
				<?php echo $footer; ?>
			</div>
			<!----- Footer End ----->
		</div>
	</div>
  </body>
</html>