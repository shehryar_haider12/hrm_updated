<?php
$empID 		= (isset($_POST['empID'])) 					? $_POST['empID'] 				: '';
$ddlMonth 	= (isset($_POST['ddlMonth'])) 				? $_POST['ddlMonth'] 			: '';
$ddlYear 	= (isset($_POST['ddlYear'])) 				? $_POST['ddlYear'] 			: '';
$setTeam 	= (isset($_POST['setTeam'])) 				? $_POST['setTeam'] 			: '';
?>
<?php if($canWrite == 1) { ?>
<form name="frmWeekendSchedule" id="frmWeekendSchedule" method="post">
<div class="listPageMain">
  <div class="formMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
    <tr>
    	<td class="formHeaderRow" colspan="2">Add/Edit Weekend Schedule</td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Employee:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
      	<select name="empID" id="empID" class="dropDown">
            <option value="">Select Employee</option>
            <?php for($ind = 0; $ind < count($allEmployees); $ind++) { ?>
            <option value="<?php echo $allEmployees[$ind]['emp_id']; ?>"><?php echo $allEmployees[$ind]['emp_code'] . ' - ' . $allEmployees[$ind]['emp_full_name']; ?></option>
            <?php } ?>
        </select>
      </td>
    </tr>
    <tr>
      <td class="formLabelContainer">Set For Team:</td>
      <td class="formTextBoxContainer">
      	<input type="checkbox" name="setTeam" id="setTeam" value="1" />
      </td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Month:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
      	<select name="ddlMonth" id="ddlMonth" class="dropDown">
        	<option value="">Select Month</option>
        	<option value="01">Jan</option>
        	<option value="02">Feb</option>
        	<option value="03">Mar</option>
        	<option value="04">Apr</option>
        	<option value="05">May</option>
        	<option value="06">Jun</option>
        	<option value="07">Jul</option>
        	<option value="08">Aug</option>
        	<option value="09">Sep</option>
        	<option value="10">Oct</option>
        	<option value="11">Nov</option>
        	<option value="12">Dec</option>
        </select>
      </td>
    </tr>
    <tr>
      <td class="formLabelContainer">Year:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">      
      	<select name="ddlYear" id="ddlYear" class="dropDown">
        	<option value="">Select Year</option>
            <?php for($ind = date('Y'); $ind < date('Y') + 2; $ind++) { ?>
        	<option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
            <?php } ?>
        </select>
      </td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Saturdays:</td>
      <td class="formTextBoxContainer">
      	<input type="checkbox" name="sat1" id="sat1" value="1" /><label for="sat1">1st Saturday</label><br /><br />
      	<input type="checkbox" name="sat2" id="sat2" value="1" /><label for="sat2">2nd Saturday</label><br /><br />
      	<input type="checkbox" name="sat3" id="sat3" value="1" /><label for="sat3">3rd Saturday</label><br /><br />
      	<input type="checkbox" name="sat4" id="sat4" value="1" /><label for="sat4">4th Saturday</label><br /><br />
      	<input type="checkbox" name="sat5" id="sat5" value="1" /><label for="sat5">5th Saturday</label>
      </td>
    </tr>
    <tr>
      <td class="formLabelContainer"></td>
      <td class="formTextBoxContainer">
      	<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save">&nbsp;
        <input type="button" class="smallButton" value="Back" onclick="history.go(-1)">
      </td>
    </tr>
  </table>
  </div>
  </div>
</form>
<script>
$('#empID').val('<?php echo $empID; ?>');
$('#ddlMonth').val('<?php echo $ddlMonth; ?>');
$('#ddlYear').val('<?php echo $ddlYear; ?>');
if(<?php echo (int)$setTeam; ?> == 1) {
	$('#setTeam').attr('checked', true);
}
</script>
<br  />
<?php } ?>