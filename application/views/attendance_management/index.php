<?php
if(count($monthSummary) && (int)$typeSum) {
?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() {
	
	var data = google.visualization.arrayToDataTable([
	  ['Type', 'Count'],
	  <?php
	  	$totalDays = 0;
	  	foreach($monthSummary as $strType => $intCount) {
	  ?>
	  ['<?php echo $strType; ?>',  <?php echo (int)$intCount; ?>],
	  <?php
	  	$totalDays += (int)$intCount;
	  }
	  ?>
	]);

	var options = {
	  title: 'My Attendance (<?php echo readableDate($this->dateFrom, 'd-M-y') . ' - ' . readableDate($this->dateTo, 'd-M-y'); ?>) - (<?php echo (int)$totalDays . ' Days'; ?>)',
	  is3D: true,
      pieHole: 0.4,
      pieSliceText: 'label',
	  legend: { position: 'bottom' },
      sliceVisibilityThreshold: 0,
  	  colors: ['#006500', '#FCD208', '#F38374', '#C70808']
	};

	var chart = new google.visualization.PieChart(document.getElementById('divChartSource'));
	chart.draw(data, options);
	
  }
</script>
<?php
}
?>
<div class="midSection">
    <div class="dashboardTitle">
        Welcome To Attendance Management System
    </div>
    
	<div class="dashboardBoxesMain">
    <?php
	foreach($allowedSubModulesList as $modules => $moduleValue)
	{
		if(strpos(strtolower($moduleValue['module_name']), 'leave_requests') !== false) 
		{
			$arrWhere = array(
				'e.emp_status' => 1,
				'l.leave_status' => 0,
				'es.supervisor_emp_id' => $this->userEmpNum
			);
									
			$leavesUnChecked = getLeavesUnchecked($arrWhere);
			
			if((int)$leavesUnChecked)
			{ $tipsyID = 'id="tipsyID"'; }
		}

		if($moduleValue['module_name'] == 'weekend_schedule') {
	?>
    	<div class="boxMain">			
				<div class="boxImageMain">
                	<img src="<?php echo $this->imagePath . '/' . $moduleValue['module_name']; ?>.jpg" alt="" style="cursor:pointer" onclick="showPopup('<?php echo $this->baseURL . '/' . $this->currentController . '/' . $moduleValue['module_name'] . '/' . $empCode; ?>', 700, 600)">
                </div>
				<div class="boxTitle">
					<a href="javascript:showPopup('<?php echo $this->baseURL . '/' . $this->currentController . '/' . $moduleValue['module_name'] . '/' . $employeeCode; ?>', 700, 600)">
                    	<?php echo $moduleValue['display_name']; ?>
                    </a>
                </div>
		</div>    
    <?php
		} else {
	?>
		<div class="boxMain" <?php echo $tipsyID; ?> original-title="<?php echo $leavesUnChecked; ?> leave request(s) awaiting response">			
				<div class="boxImageMain">
                	<a href="<?php echo $this->baseURL . '/' . $this->currentController. '/' . $moduleValue['module_name']; ?>">
                		<img src="<?php echo $this->imagePath . '/modules/' . $moduleValue['module_name']; ?>.png" alt="">
                    </a>
                </div>
				<!--<div class="boxTitle">
                	<a href="<?php echo $this->baseURL . '/' . $this->currentController. '/' . $moduleValue['module_name']; ?>">
						<?php echo $moduleValue['display_name']; ?>
                    </a>
                </div>-->
		</div>
	<?php
		}
	}
	?>		
	</div>
</div>
<script>
	$("#tipsyID").tipsy({gravity: "s", title: "original-title", trigger: "manual"});
	$("#tipsyID").tipsy("show");
</script>