<div class="popupNotificationMain">
	<div class="popupNotificationBox">
		<table align="center" cellspacing="0" cellpadding="0" border="0" style="width:605px">
			<tr>
				<td>
					<?php for($ind = 0; $ind < count($arrBirthdayDetails); $ind++) 
					{				
						if(!file_exists($pictureFolder . $arrBirthdayDetails[$ind]['emp_photo_name']) || empty($arrBirthdayDetails[$ind]['emp_photo_name']))
						{
							if(empty($arrBirthdayDetails[$ind]['emp_gender'])) {
								$arrBirthdayDetails[$ind]['emp_gender'] = 'male';
							}
							$arrBirthdayDetails[$ind]['emp_photo_name'] = 'no_image_' . strtolower($arrBirthdayDetails[$ind]['emp_gender']) . '.jpg';
						}
						
						$empFullName = $arrBirthdayDetails[$ind]['emp_full_name'];
						
					?>
					<div class="cardWrapper">  	
						<div class="cardPicture" style="background-image:url('<?php echo $this->baseURL . '/' . $pictureFolderShow . $arrBirthdayDetails[$ind]['emp_photo_name']; ?>');"></div>
						<div class="cardDetailContainer">
							<h1><?php echo $empFullName; ?></h1>
							<span><?php echo $arrBirthdayDetails[$ind]['job_category_name']; ?></span>                    
						</div>
					</div>
					<?php } ?>
				</td>
			</tr>
		</table>
	</div>
	<div style="text-align:center">
		<input  class="smallButton" name="close" type="button" value="Close" onclick="self.close()">
	</div>
</div>
