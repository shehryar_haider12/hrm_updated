<?php if($canWrite == 1)  { ?>
<div class="centerButtonContainer">
	<input class="addButton" type="button" value="Create New Poll" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/manage_poll' ?>';" />
</div>
<?php } ?>
<form name="frmListPolls" id="frmListPolls" method="post" action="<?php echo $frmActionURL; ?>">
<div class="centerElementsContainer">
    <div class="recordCountContainer"><?php echo "Total Records Count: ".$totalRecordsCount; ?>
    </div>
    <?php
    if($pageLinks) {
    ?>
        <div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
    <?php 	}	?>
</div>

<table border="0" cellspacing="0" cellpadding="0" class="listTableMain dottedBorder">
	<tr class="listHeader">
    	<?php if($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID) { ?>
        <td class="listHeaderCol">ID</td>
        <td class="listHeaderCol">Created By</td>
        <?php } ?>
        <td class="listHeaderCol" width="400px">Poll Topic</td>
        <td class="listHeaderCol">Start Date</td>
        <td class="listHeaderCol">End Date</td>       
        <td class="listHeaderCol">Poll Status</td>
        <?php if($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID) { ?>
        <td class="listHeaderCol">Visibility Status</td>
        <?php } ?>
    	<td class="listHeaderColLast">Action</td>
    </tr>
    <?php for($ind = 0; $ind < count($arrRecords); $ind++) 
	{
		foreach($arrPollStatus as $key => $value) {
			if($key == $arrRecords[$ind]['poll_status'])
			{ $status = $value; }
		}
		
		foreach($arrPollVisibilityStatus as $key => $value) {
			if($key == $arrRecords[$ind]['poll_visibility_status'])
			{ $visibilityStatus = $value; }
		}
	?>
	<tr class="listContent">
    	<?php if($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID) { ?>
        	<td class="listContentCol"><?php echo $arrRecords[$ind]['poll_id'] ?></td>
            <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_full_name']; ?></td>
        <?php } ?>
        <td class="listContentCol"><?php echo nl2br($arrRecords[$ind]['poll_topic']); ?></td>
        <td class="listContentCol"><?php echo readableDate($arrRecords[$ind]['poll_start_date'], 'M j, Y'); ?></td>
        <td class="listContentCol"><?php echo readableDate($arrRecords[$ind]['poll_end_date'], 'M j, Y'); ?></td>
        <td class="listContentCol"><?php echo $status; ?></td>
        <?php if($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID) { ?>
        <td class="listContentCol"><?php echo $visibilityStatus; ?></td>
        <?php } ?>
        <td class="listContentColLast" align="left">
        	<?php if((!($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID)) && ($arrRecords[$ind]['created_by'] != $this->userEmpNum) && ($arrRecords[$ind]['already_participated'] <= 0 || $arrRecords[$ind]['poll_results_display'] == 1)) { ?>
            	<input type="button" class="smallButton" value="Participate / View Results" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/answer_poll/' . $arrRecords[$ind]['poll_id']; ?>';" />
            <?php } else if((!($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID)) && ($arrRecords[$ind]['created_by'] != $this->userEmpNum) && ($arrRecords[$ind]['already_participated'] > 0 && $arrRecords[$ind]['poll_results_display'] == 0)) { ?>
            	<span>Already Participated</span>
			<?php } else if(($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID) || ($arrRecords[$ind]['created_by'] == $this->userEmpNum)) { ?>
                <input type="button" class="smallButton" value="Manage Poll" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/manage_poll/' . $arrRecords[$ind]['poll_id']; ?>';" />
                <input type="button" class="smallButton" value="View" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/answer_poll/' . $arrRecords[$ind]['poll_id']; ?>';" />
                <?php if($canDelete == 1) { ?>
            		<input type="button" class="smallButton" value="Delete" onclick="deleteRecord('/<?php echo $this->currentController . '/' . $this->currentAction; ?>', '<?php echo $arrRecords[$ind]['poll_id']; ?>');" />
            	<?php } ?>
            <?php } ?>            
        </td>
    </tr>
    <?php
	}
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="8" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
</form>