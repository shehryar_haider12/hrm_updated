<!-- SEARCH PANEL - START -->
<div class="listPageMain">
<form name="frmListSubmissionDetails" id="frmListSubmissionDetails" method="post" action="<?php echo $frmActionURL; ?>">
	<div class="searchBoxMain">
		<div class="searchHeader">Search Criteria</div>
		<div class="searchcontentmain">
			<div class="searchCol">
				<div class="labelContainer">Month:</div>
				<div class="textBoxContainer">
                <select class="dropDownSmall" id="month" name="month">
					<option value="">Month</option>
					<?php
                    	foreach($months as $key => $value) {
						$selected = '';
						if($key == $month)
							$selected = 'selected="selected"';
                    ?>
                    	<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                    <?php
					}
                    ?>
                </select>
              	</div>
            </div>
            
            <div class="searchCol">
				<div class="labelContainer">Year:</div>
                <div class="textBoxContainer">
                <select class="dropDownSmall" id="year" name="year">
                    <option value="">Year</option>
                    <?php for($ind = $this->HRMYearStarted; $ind <= date('Y'); $ind++) { ?>
                    	<option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
                    <?php } ?>
                </select>
                </div>
            </div>
            
            <div class="formButtonContainerWide">
				<input type="submit" class="searchButton" name="btnSearchTasks" id="btnSearchTasks" value="Search">
            </div>
		</div>
<script>
	$('#month').val('<?php echo $month; ?>');
	$('#year').val('<?php echo $year; ?>');
</script> 
    </div>
<input type="hidden" name="sendEmailTo" id="sendEmailTo" />
</form>
</div>
<!-- SEARCH PANEL - END -->

<div class="centerElementsContainer">
    <div class="recordCountContainer"><?php echo "Total Records Count: ".(int)$totalRecordsCount; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<b>Legends:</b>
        &nbsp;&nbsp;&nbsp;<span style="background-color:#FFFFFF;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="mandatoryStar"> Not Submitted Performance Evaluation </span>
        &nbsp;&nbsp;&nbsp;<span style="background-color:#f9f084;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="mandatoryStar"> Pending by Supervisor </span>
        &nbsp;&nbsp;&nbsp;<span style="background-color:#E5E5E5;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="mandatoryStar"> Pending for Employee Comments </span>
        &nbsp;&nbsp;&nbsp;<span style="background-color:#f38374;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="mandatoryStar"> Pending by Manager </span>
		&nbsp;&nbsp;&nbsp;<span style="background-color:#CCE5A6;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="mandatoryStar"> Complete Performance Evaluation </span>
	</div>
    <?php
    if($pageLinks) {
    ?>
        <div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
    <?php 	}	?>
</div>

<script>
function showResults(strType) {
	
	$('.switchWhite').hide();
	$('.switchYellow').hide();
	$('.switchGrey').hide();
	$('.switchRed').hide();
	$('.switchGreen').hide();
	
	if(strType == 'switchWhite') {
		$('.switchWhite').show();
	}
	else if(strType == 'switchYellow') {
		$('.switchYellow').show();
	}
	else if(strType == 'switchGrey') {
		$('.switchGrey').show();
	}
	else if(strType == 'switchRed') {
		$('.switchRed').show();
	}
	else if(strType == 'switchGreen') {
		$('.switchGreen').show();
	}
	else if(strType != '') {
		$('.' + strType).show();
	} else {
		$('.switchWhite').show();
		$('.switchYellow').show();
		$('.switchGrey').show();
		$('.switchRed').show();
		$('.switchGreen').show();
	}
	
	var intIndex = 1;
	
	$('.rowIndex:visible').each(function() {
	  	$(this).html(intIndex);
		intIndex++;
	});
}
function setIDs(switchID)
{
	$('#sendEmailTo').val(switchID);			
	$('#frmListSubmissionDetails').submit();
}

</script>
<div class="listContentMain">
<form name="frmResponse" id="frmResponse" method="post" action="<?php echo $this->baseURL . '/' . $this->currentController . '/my_team_performance'; ?>">
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain dottedBorder">
	<?php
	if(!$totalRecordsCount) {
	?>
	<tr class="listContentAlternate">
		<td colspan="7" align="center" class="listContentColLast bold">No Record Found</td>
	</tr>
	<?php
	}
	else
	{
	?>
	<tr class="center"><td class="formHeaderRow" colspan="7"><?php echo "[	".$months[$month]."-".$year."	]	-	".$heading; ?></td></tr>
	<tr class="listHeader">
        <td class="listHeaderCol">Employee Name</td>
        <td class="listHeaderCol">Employee Code</td>
        <td class="listHeaderCol">IP Extension</td>
        <td class="listHeaderCol">Employee Email</td>
        <td class="listHeaderCol">Department</td>
        <td class="listHeaderCol">Month & Year</td>
        <td class="listHeaderCol">Action</td>
    </tr>
    <input type="hidden" name="empCodePending" id="empCodePending" />
    <input type="hidden" id="monthPending" name="monthPending" />
    <input type="hidden" id="yearPending" name="yearPending" />
    <?php 
	$numWhite=0;
	$numGrey=0;
	$numYellow=0;
	$numRed=0;
	$numGreen=0;
	foreach($totalRecords as $key => $val) {
		if (!isset($val["pe_id"]))
		{ $totalCSSStyle = "style='background-color:#FFFFFF;'"; $numWhite++; $switchClass="switchWhite"; $whiteRecords[] = $val['emp_work_email'];}
		else if (isset($val["pe_id"]) && ($val["pe_status"] == STATUS_SUBMIT_TASKS_FOR_REVIEW || $val["pe_status"] == STATUS_REVIEWING_TASK_BY_SUPERVISOR))
		{ $totalCSSStyle = "style='background-color:#f9f084;'"; $numYellow++; $switchClass="switchYellow"; $yellowRecords[] = $val['emp_work_email'];}
		else if (isset($val["pe_id"]) && $val["pe_status"] == STATUS_SUBMIT_REVIEW_TO_EMPLOYEE)
		{ $totalCSSStyle = "style='background-color:#E5E5E5;'"; $numGrey++; $switchClass="switchGrey"; $greyRecords[] = $val['emp_work_email'];}
		else if (isset($val["pe_id"]) && ($val["pe_status"] == STATUS_EMPLOYEE_COMMENTS_PROVIDED || $val["pe_status"] == STATUS_REVIEWING_TASK_BY_MANAGER))
		{ $totalCSSStyle = "style='background-color:#f38374;'"; $numRed++; $switchClass="switchRed"; $redRecords[] = $val['emp_work_email'];}
		else if (isset($val["pe_id"]) && $val["pe_status"] == STATUS_SUBMIT_REVIEW_TO_HR)
		{ $totalCSSStyle = "style='background-color:#CCE5A6;'"; $numGreen++; $switchClass="switchGreen"; $greenRecords[] = $val['emp_work_email'];}
	?>
    <tr class="listContent <?php echo $switchClass; ?>" <?php echo $totalCSSStyle; ?>>
        <td class="listContentCol">
            <?php echo $val['emp_full_name']; ?>
        </td>
        <td class="listContentCol">
            <?php echo $val['emp_code']; ?>
        </td>
        <td class="listContentCol">
            <?php echo $val['emp_ip_num']; ?>
        </td>
        <td class="listContentCol">
            <?php echo $val['emp_work_email']; ?>
        </td>
        <td class="listContentCol">
        	<?php echo $val['job_category_name']; ?>
        </td>
		<td class="listContentCol">
			<?php echo $months[$month]."-".$year; ?>
        </td>
    	<td class="listContentCol" align="center">
            <?php if ((isset($val["pe_id"]) && $val["pe_status"] == STATUS_SUBMIT_REVIEW_TO_HR) || (!isset($val["pe_id"])))	{	?>
			View Not available
			<?php }	else {	?>
			<input type="button" class="smallButton" name="btnViewPendingReviews" id="btnViewPendingReviews" value="View Report" onclick="fillValues('<?php echo $val['emp_id']; ?>','<?php echo $month; ?>','<?php echo $year; ?>')">
			<?php } ?>
        </td>
    </tr>
	<?php
		}
	?>
    <script>
		function fillValues(empCode,month,year)
		{
			$('#empCodePending').val(empCode);
			$('#monthPending').val(month);
			$('#yearPending').val(year);			
			$('#frmResponse').submit();
		}
	</script>
	<?php
	}
	?>
</table>
</form>
</div>

<div class="clear">&nbsp;</div>
<div class="notificationWideBox">
<table class="listTableMain">
    <tr>
    	<td class="listContentCol" colspan="4" height="2px"></td>
    </tr>
    <tr class="listContent">
    	<td class="listContentCol center" colspan="4" width="180px" style="font-size:18px;background-color:#dbf3fb;"><?php echo "[	".$months[$month]."-".$year."	]	-	".$heading; ?></td>
    </tr>
    <tr class="listContent">
    	<td width="30%" class="listContentCol" style="font-size:18px">Not Submitted Performance Evaluation </td>
    	<td width="10%" class="listContentCol" style="font-size:18px; color:#F00"><?php echo $numWhite; ?></td>
		<td width="30%" class="listContentCol normalLink" style="font-size:18px; cursor:pointer" onclick="showResults('switchWhite')">Show Records</td>
		<td width="30%" class="listContentCol"><input type="submit" class="smallButton" name="sendEmail" id="sendEmail" value="Send Email Notification" onclick="setIDs('<?php echo STATUS_ENTERING_TASKS; ?>')"></td>
    </tr>
    <tr class="listContent">
    	<td class="listContentCol" style="font-size:18px;">Pending by Supervisor</td>
    	<td class="listContentCol" style="font-size:18px; color:#F00"><?php echo $numYellow; ?></td>
		<td class="listContentCol normalLink" style="font-size:18px; cursor:pointer" onclick="showResults('switchYellow')">Show Records</td>
		<td class="listContentCol"><input type="submit" class="smallButton" name="sendEmail" id="sendEmail" value="Send Email Notification" onclick="setIDs('<?php echo STATUS_REVIEWING_TASK_BY_SUPERVISOR; ?>')"></td>
    </tr>
    <tr class="listContent">
    	<td class="listContentCol" style="font-size:18px;">Pending for Employee Comments</td>
    	<td class="listContentCol" style="font-size:18px; color:#F00"><?php echo $numGrey; ?></td>
		<td class="listContentCol normalLink" style="font-size:18px; cursor:pointer" onclick="showResults('switchGrey')">Show Records</td>
		<td class="listContentCol"><input type="submit" class="smallButton" name="sendEmail" id="sendEmail" value="Send Email Notification" onclick="setIDs('<?php echo STATUS_SUBMIT_REVIEW_TO_EMPLOYEE; ?>')"></td>
    </tr>
    <tr class="listContent">
    	<td class="listContentCol" style="font-size:18px;">Pending by Manager</td>
    	<td class="listContentCol" style="font-size:18px; color:#F00"><?php echo $numRed; ?></td>
		<td class="listContentCol normalLink" style="font-size:18px; cursor:pointer" onclick="showResults('switchRed')">Show Records</td>
		<td class="listContentCol"><input type="submit" class="smallButton" name="sendEmail" id="sendEmail" value="Send Email Notification" onclick="setIDs('<?php echo STATUS_REVIEWING_TASK_BY_MANAGER; ?>')"></td>
    </tr>
    <tr class="listContent">
    	<td class="listContentCol" style="font-size:18px;">Complete Performance Evaluation</td>
    	<td class="listContentCol" style="font-size:18px; color:#F00"><?php echo $numGreen; ?></td>
		<td class="listContentCol normalLink" style="font-size:18px; cursor:pointer" onclick="showResults('switchGreen')">Show Records</td>
		<td class="listContentCol">-</td>
    </tr>

    <tr class="listContent">
    	<td class="listContentCol" width="180px" style="font-size:18px;background-color:#dbf3fb;">Total Records</td>
    	<td class="listContentCol" width="180px" style="font-size:18px;background-color:#dbf3fb; color:#F00"><?php echo $totalRecordsCount; ?></td>
		<td class="listContentCol normalLink" style="font-size:18px; cursor:pointer" onclick="showResults('')">Show All Records</td>
		<td class="listContentCol">-</td>
    </tr>
</table>
</div>