<?php
$fromYear 	= ($fromYear != '') 	? $fromYear 	: ($this->input->post('fromYear'))  ? ($this->input->post('fromYear')) : (int)date('Y');
$toYear 	= ($toYear != '') 		? $toYear 		: ($this->input->post('toYear'))  ? ($this->input->post('toYear')) : (int)date('Y');
$empCode 	= ($empCode != '') 		? $empCode 		: ($this->input->post('empCode'));

if(count($arrRecords) > 0)
{

$yearDifference = $toYear - $fromYear;
$arrResult = array();

if($yearDifference == 0)
{
	$arrayCount = 1;
	array_push($arrResult,$arrRecords);
}
else
{
	$arrayCount = $yearDifference + 1;
	$years[] = $fromYear;
	
	for($ind = 1; $ind < $arrayCount; $ind++)
	{
		$years[] = $fromYear + $ind;
	}
		
	for($knd = 0; $knd < count($years); $knd++)
	{
		for($jnd = 0; $jnd < count($arrRecords); $jnd++)
		{
			if($years[$knd] == $arrRecords[$jnd]['year'])
			{
				$arrResult[$years[$knd]][] = $arrRecords[$jnd];
			}			
		}
	}
	
}

?>

<?php if($arrayCount > 1)
{
	for($znd = $fromYear; $znd <= $arrResult[$znd]; $znd++)
	{
		if(count($arrResult[$znd]) > 0)
		{
?>            
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script type="text/javascript">
            google.load("visualization", "1", {packages:["corechart"]});
            google.setOnLoadCallback(drawChart);
            function drawChart() {
            
            var data = google.visualization.arrayToDataTable([
                    ['Month', 'Performance', { role: 'style' } ],		
                    <?php for($jnd = 0; $jnd < count($arrResult[$znd]); $jnd++) {
                        $totalRecords = count($arrResult[$znd]);
                    ?>
                    [
                    '<?php echo date("F", mktime(0, 0, 0, $arrResult[$znd][$jnd]['month'], 1)); ?>',
                    <?php echo $arrResult[$znd][$jnd]['total_weighted_score']; ?>,
                    '<?php if($arrResult[$znd][$jnd]['total_weighted_score'] < 70) { echo "silver"; } 
                       else if(($arrResult[$znd][$jnd]['total_weighted_score'] >= 70 && $arrResult[$znd][$jnd]['total_weighted_score'] < 80)) { echo "#76A7FA"; }
					   else if(($arrResult[$znd][$jnd]['total_weighted_score'] >= 80 && $arrResult[$znd][$jnd]['total_weighted_score'] < 90)) { echo "#00418C"; }
					   else if(($arrResult[$znd][$jnd]['total_weighted_score'] >= 90 && $arrResult[$znd][$jnd]['total_weighted_score'] < 100)) { echo "#DE2126"; }
                    ?>',
                    ]<?php if(!($ind == ($totalRecords - 1))) echo ","; ?>
                    <?php } ?>           
            ]);
            
            var view = new google.visualization.DataView(data);
                  view.setColumns([0, 1,
                           { calc: function (dt, row) {
										var ann = dt.getValue(row, 1);
										var ann = ann + "%";
										return ann;
									},
                             sourceColumn: 1,
                             type: "string",
                             role: "annotation"
							},
                           2]);
            
            var options = {
                    title: "Year <?php echo $znd; ?> Performance Report",
					hAxis: {title: "Month(s)"},
                    legend: { position: 'none' },
					<?php if(count($arrResult[$znd]) < 6) { ?> bar: { groupWidth: '25%' }, <?php } else { ?> bar: { groupWidth: '65%' }, <?php } ?>                    
					vAxis: {title: "Performance", viewWindow: {min: 0,max: 101}},
                  };
            
            
            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div<?php echo $znd; ?>'));
            
            chart.draw(view, options);
            
            }
        </script>           
<?php 	
		}
	} 
	 
} 
else 
{
	for($ind = 0; $ind < $arrayCount; $ind++) 
	{
		if(count($arrResult[$ind]) > 0)
		{
?>    
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
    
    var data = google.visualization.arrayToDataTable([
            ['Month', 'Performance', { role: 'style' } ],		
            <?php for($jnd = 0; $jnd < count($arrResult[$ind]); $jnd++) {
                $totalRecords = count($arrResult[$ind]);
            ?>
            [
            '<?php echo date("F", mktime(0, 0, 0, $arrResult[$ind][$jnd]['month'], 1)); ?>',
            <?php echo $arrResult[$ind][$jnd]['total_weighted_score']; ?>,
            '<?php if($arrResult[$ind][$jnd]['total_weighted_score'] < 70) { echo "silver"; } 
                else if(($arrResult[$ind][$jnd]['total_weighted_score'] >= 70 && $arrResult[$ind][$jnd]['total_weighted_score'] < 80)) { echo "#76A7FA"; }
                else if(($arrResult[$ind][$jnd]['total_weighted_score'] >= 80 && $arrResult[$ind][$jnd]['total_weighted_score'] < 90)) { echo "#00418C"; }
				else if(($arrResult[$ind][$jnd]['total_weighted_score'] >= 90 && $arrResult[$ind][$jnd]['total_weighted_score'] < 100)) { echo "#DE2126"; }
            ?>',
            ]<?php if(!($ind == ($totalRecords - 1))) echo ","; ?>
            <?php } ?>    
    ]);
    
    var view = new google.visualization.DataView(data);
          view.setColumns([0, 1,
                           { calc: function (dt, row) {
										var ann = dt.getValue(row, 1);
										var ann = ann + "%";
										return ann;
									},
                             sourceColumn: 1,
                             type: "string",
                             role: "annotation"
							},
                           2]);
    
    var options = {
            title: "Year <?php echo $arrRecords[$ind]['year']; ?> Performance Report",
			hAxis: {title: "Month(s)"},
            legend: { position: 'none' },
            <?php if(count($arrResult[$ind]) < 6) { ?> bar: { groupWidth: '25%' }, <?php } else { ?> bar: { groupWidth: '85%' }, <?php } ?>			
			vAxis: {title: "Performance", viewWindow: {min: 0,max: 101}},
          };
    
    
    var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
    
    chart.draw(view, options);
    
    }
    </script>
<?php 
		}
	} 
}

}
?>

<div class="listPageMain">
  <form name="frmSearchTasks" id="frmSearchTasks" method="post" action="<?php echo $frmActionURL; ?>">
    <div class="searchBoxMain">
      <div class="searchHeader">INDIVIDUAL REPORTS</div>
      <div class="searchcontentmain">
        <div class="searchCol">
        	<?php
			if(count($arrEmployees) > 0) {
			?>
			<div class="labelContainer">Employee:</div>
			<div class="textBoxContainer">
			  <select name="empCode" id="empCode" class="dropDown">
				  <option value="">Select Employee</option>
                  <?php
					if (count($arrEmployees)) {
						foreach($arrEmployees as $key => $arrEmployee) {
					?>
						<optgroup label="<?php echo $key; ?>">
							<?php for($i=0;$i<count($arrEmployee);$i++) { ?>					
								<option value="<?php echo $arrEmployee[$i]['emp_id']; ?>"><?php echo $arrEmployee[$i]['emp_code']." - ".$arrEmployee[$i]['emp_full_name']; ?></option>
							<?php } ?>
						</optgroup>
					<?php	}
					}
					?>
			  </select>
			</div>
			<?php
			}
			?>
        </div>
        <div class="searchCol">
        	<div class="labelContainer">From Year:</div>
          	<div class="textBoxContainer">
                <select id="fromYear" name="fromYear" class="dropDown" style="width:85px; margin-left:5px">
                  <option value="">Year</option>
                  <?php for($ind = $this->HRMYearStarted; $ind <= date('Y'); $ind++) { ?>
                  <option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
                  <?php } ?>
                </select>
             </div>
        	<div class="labelContainer">To Year:</div>
          	<div class="textBoxContainer">
                <select id="toYear" name="toYear" class="dropDown" style="width:85px; margin-left:5px">
                  <option value="">Year</option>
                  <?php for($ind = $this->HRMYearStarted; $ind <= date('Y'); $ind++) { ?>
                  <option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
                  <?php } ?>
                </select>
              </div>
        </div>
        
        <div class="formButtonContainerWide">
          <input type="submit" class="searchButton" name="btnSearchTasks" id="btnSearchTasks" value="Search">
          <input class="searchButton" name="btnBack" id="btnBack" type="button" value="Back" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction; ?>';">
        </div>
      </div>
      <script>
  	$('#empCode').val('<?php echo $empCode; ?>');
  	$('#fromYear').val('<?php echo $fromYear; ?>');
  	$('#toYear').val('<?php echo $toYear; ?>');
  </script> 
    </div>
  </form>
</div>
<div class="listPageMain">
	<?php if($arrayCount > 1)
	{
		for($znd = $fromYear; $znd <= $arrResult[$znd]; $znd++)
		{
	?>
    			<div id="chart_div<?php echo $znd; ?>" style="float:left; width: 100%; height: 500px;"></div>
	<?php
		}		 
	} 
	else if($arrayCount == 1) { ?>
		<div id="chart_div" style="float:left; width: 100%; height: 500px;"></div>
    <?php } else if($message) { ?>
		<div class="listContentAlternate bold" align="center">
        	<?php echo $message; ?>
        </div>
	<?php } else { ?>
    	<div class="listContentAlternate bold" align="center">
        	<?php echo "Select Employee, From Year and To Year field and then click Search"; ?>
        </div>
    <?php } ?>
</div>