<div class="midSection">	
	<div class="notificationWideMain" style="min-height:0px">
    	<div class="notificationWideBox">
        	<h1>PMS Reporting</h1>
            <div style="clear:both">&nbsp;</div>
            <div style="font-size:12px; font-family:Arial, Helvetica, sans-serif; float:left; width:500px">
				<ul>
					<li class="boldBlueHeading individual" style="padding-right:20px; list-style:circle; margin-left:20px"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/individual'; ?>" class="normalLink">Individual Reports</a></li>
					<li class="boldBlueHeading team" style="padding-right:20px; list-style:circle; margin-left:20px"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/team'; ?>" class="normalLink">Department Reports</a></li>
					<li class="boldBlueHeading submissionDetail" style="padding-right:20px; list-style:circle; margin-left:20px"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/submissionDetail/S'; ?>" class="normalLink">Performance Evaluation Submission Details - Sales Staff</a></li>
					<li class="boldBlueHeading submissionDetail" style="padding-right:20px; list-style:circle; margin-left:20px"><a href="<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/submissionDetail/G'; ?>" class="normalLink">Performance Evaluation Submission Details - General Staff</a></li>
				</ul>
            </div>
		</div>
	</div>
</div>
<script type="text/javascript">
<?php if(!in_array($this->userRoleID, $forcedAccessRoles)) { ?>
$('.boldBlueHeading').hide();
<?php
	for($ind = 0; $ind < count($arrFullReportAccess); $ind++) {
		if(!empty($arrFullReportAccess[$ind])) {
?>
$('.<?php echo $arrFullReportAccess[$ind]; ?>').show();
<?php
		}
	}
?>
<?php } ?>
</script>