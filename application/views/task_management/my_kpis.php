<style>
.formLabelContainer { padding: 0 10px 0 0 !important; width:50px !important; }
</style>
<?php
$path 				= '/'.$this->currentController.'/'.$this->currentAction;
$currentMonth		= 6;
$currentYear		= date('Y');
$month 				= ($month) 		? $month 	: ($this->input->post('month') 	? $this->input->post('month') 	: (($this->input->post('selectedMonth')) 	? $this->input->post('selectedMonth') 	: 6));
$year 				= ($year) 		? $year		: ($this->input->post('year') 	? $this->input->post('year') 	: (($this->input->post('selectedYear')) 	? $this->input->post('selectedYear') 	: (int)date('Y')));

if(count($arrTasks)) {
	$txt0			= json_decode($arrTasks[0]['review_kpis']);
	$jobKPIScore	= $arrTasks[0]['review_kpis_score'];
	$vsKPIScore		= $arrTasks[0]['review_vs_kpis_score'];
	$arrAssessment 	= json_decode($arrTasks[0]['review_vs_kpis']);
	$arrRadVS[]		= $arrAssessment[0];
	$arrRadVS[]		= $arrAssessment[1];
	$arrRadVS[]		= $arrAssessment[2];
	$arrRadVS[]		= $arrAssessment[3];
	$arrRadVS[]		= $arrAssessment[4];
	$arrRadVS[]		= $arrAssessment[5];
	$arrRadVS[]		= $arrAssessment[6];
	$arrRadVS[]		= $arrAssessment[7];
	$arrRadVS[]		= $arrAssessment[8];
	$arrRadVS[]		= $arrAssessment[9];
	$empRemarks		= $arrTasks[0]['review_emp_remarks'];
}
 
?>

<div class="listPageMain">
<form name="frmSearchTasks" id="frmSearchTasks" method="post" action="<?php echo $frmActionURL; ?>">
<div class="searchBoxMain">
    <div class="searchHeader">MY KPIs - <?php echo date('F', mktime(0, 0, 0, $month, 10)) . ' ' .$year; ?></div>
    <div class="searchcontentmain">
        <div class="searchCol">
        	<div class="labelContainer">Month:</div>
            <div class="textBoxContainer">
                <select class="dropDown" id="month" name="month" style="width:85px; margin-left:5px">
                    <option value="">Month</option>
                    <option value="6">June</option>
					<option value="12">December</option>
                    <option value="1">Jan</option>
                    <option value="2">Feb</option>
                </select>
            </div>
            <div class="labelContainer">Year:</div>
            <div class="textBoxContainer">
                <select id="year" name="year" class="dropDown" style="width:85px; margin-left:5px">
                    <option value="">Year</option>
                    <?php for($ind = $this->HRMYearStarted; $ind <= (date('Y') + 1); $ind++) { ?>
                    <option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="formButtonContainerWide">
            <input type="submit" class="searchButton" name="btnSearchTasks" id="btnSearchTasks" value="Search">
            <?php if(count($arrTasks)) { ?>
            <input type="button" class="searchButton" name="btnExport" id="btnExport" value="Export to PDF" onClick="window.location.href = '<?php echo $frmActionURL; ?>/<?php echo $arrTasks[0]['review_id']; ?>'">
            <?php } ?>
        </div>
    </div>
    <script>
  	$('#month').val('<?php echo $month; ?>');
  	$('#year').val('<?php echo $year; ?>');
  </script>
</div>
</form>
</div>

<?php if(count($arrTasks)) { ?>

<div class="listPageMain">
    <div class="searchBoxMain">
        <div class="yellow" style="padding-left:20px"><br />
            <b>GUIDELINES FOR THIS FORM:</b><br /><br />
            <ul style="list-style:circle; padding-left:30px">
				<li>Unsatisfactory (U)</li>
				<li>Needs Improvement (NI)</li>
				<li>Meets Expectations (ME)</li>
				<li>Exceeds Expectations (EE)</li>
				<li>Excellent (E)</li>
			</ul><br />
        </div>
    </div>
</div>

<div class="listContentMain" style="height:auto">
<form name="frmAddTasks" id="frmAddTasks" method="post" action="<?php echo $frmActionURL; ?>">
	<input type="hidden" id="selMonth" name="selMonth" value="<?php echo $month; ?>" />
    <input type="hidden" id="selYear" name="selYear" value="<?php echo $year; ?>" />
    <table border="0" cellspacing="0" cellpadding="0" class="listTableMain" style="font-size:14px; padding:20px 170px;">
      <!-- POST RECORDS START -->
        <tr>
        	<td>
                <table border="1" cellspacing="0" cellpadding="0" class="listTableMain" style="font-size:14px; padding:20px">
                    <tr class="listHeader">
                        <td class="listHeaderCol center" colspan="6">
                            KPI PERFORMANCE REVIEW FORM
                        </td>
                    </tr>
                    <tr class="listContent">
                        <td class="listContentCol" width="25%">Employee</td>
                        <td class="listContentCol" width="25%"><?php echo $arrEmployee['emp_full_name']; ?></td>
                        <td class="listContentCol" width="25%">Line Manager</td>
                        <td class="listContentCol" width="25%"><?php echo getSupervisorName($arrEmployee['emp_id']); ?></td>
                    </tr>
                    <tr class="listContent">
                        <td class="listContentCol">Designation</td>
                        <td class="listContentCol"><?php echo $arrEmployee['emp_designation']; ?></td>
                        <td class="listContentCol">Department</td>
                        <td class="listContentCol"><?php echo $arrEmployee['job_category_name']; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
        	<td>
            	<table border="1" cellspacing="0" cellpadding="0" class="listTableMain" style="font-size:14px; padding:20px" id="table0">
                    <tr class="listHeader">
                        <td width="95%" colspan="7" align="center">Job Specific KPIs (60%)</td>
                    </tr>
                    <tr class="listHeader">
                        <td align="center" width="5%">No.</td>
                        <td align="center" width="70%">Key Performance Indicators</td>
                        <td align="center" width="5%">U</td>
                        <td align="center" width="5%">NI</td>
                        <td align="center" width="5%">ME</td>
                        <td align="center" width="5%">EE</td>
                        <td align="center" width="5%">E</td>
                    </tr>
                    <tr>
                        <td colspan="7">&nbsp;</td>
                    </tr>
                <?php
                $totalRecords = count($txt0);
				if(!$totalRecords) {
					$totalRecords = 4;
				}
				
				for($ind = 0; $ind < $totalRecords; $ind++) {
				?>
                	<tr class="listContent">
                        <td width="5%"><?php echo ($ind + 1); ?>.</td>
                        <td width="70%"><?php echo $txt0[$ind][0]; ?></td>
                        <td align="center" width="5%"><input type="radio" name="rad<?php echo $ind; ?>" id="rad<?php echo $ind; ?>_1" value="1"></td>
                        <td align="center" width="5%"><input type="radio" name="rad<?php echo $ind; ?>" id="rad<?php echo $ind; ?>_2" value="2"></td>
                        <td align="center" width="5%"><input type="radio" name="rad<?php echo $ind; ?>" id="rad<?php echo $ind; ?>_3" value="3"></td>
                        <td align="center" width="5%"><input type="radio" name="rad<?php echo $ind; ?>" id="rad<?php echo $ind; ?>_4" value="4"></td>
                        <td align="center" width="5%"><input type="radio" name="rad<?php echo $ind; ?>" id="rad<?php echo $ind; ?>_5" value="5"></td>
                    </tr>
                    <script>$('input:radio[name="rad<?php echo $ind; ?>"][value="<?php echo $txt0[$ind][1]; ?>"]').attr('checked',true);</script>
                <?php
				}
				?>
                </table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;
                
            </td>
        </tr>
        <tr>
            <td>
                <hr />
            </td>
        </tr>
        <tr>
            <td>&nbsp;
                
            </td>
        </tr>
        <tr>
        	<td>
            	<table border="1" cellspacing="0" cellpadding="0" class="listTableMain" style="font-size:14px; padding:20px">
                    <tr class="listHeader">
                        <td colspan="7" align="center">Naval Anchorage Club KPIs (40%)</td>
                    </tr>
                    <tr class="listHeader">
                        <td align="center" width="5%">No.</td>
                        <td align="center" width="70%">Key Performance Indicators</td>
                        <td align="center" width="5%">U</td>
                        <td align="center" width="5%">NI</td>
                        <td align="center" width="5%">ME</td>
                        <td align="center" width="5%">EE</td>
                        <td align="center" width="5%">E</td>
                    </tr>
                    <tr class="listContent">
                        <td align="center" width="5%">1</td>
                        <td align="center" width="70%">Client interaction and satisfaction</td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_0" id="rad_vs_0_1" value="1"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_0" id="rad_vs_0_2" value="2"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_0" id="rad_vs_0_3" value="3"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_0" id="rad_vs_0_4" value="4"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_0" id="rad_vs_0_5" value="5"></td>
                    </tr>
                    <tr class="listContentAlternate">
                        <td align="center" width="5%">2</td>
                        <td align="center" width="70%">Team interaction and teamwork</td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_1" id="rad_vs_1_1" value="1"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_1" id="rad_vs_1_2" value="2"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_1" id="rad_vs_1_3" value="3"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_1" id="rad_vs_1_4" value="4"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_1" id="rad_vs_1_5" value="5"></td>
                    </tr>
                    <tr class="listContent">
                        <td align="center" width="5%">3</td>
                        <td align="center" width="70%">Initiative for process improvement</td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_2" id="rad_vs_2_1" value="1"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_2" id="rad_vs_2_2" value="2"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_2" id="rad_vs_2_3" value="3"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_2" id="rad_vs_2_4" value="4"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_2" id="rad_vs_2_5" value="5"></td>
                    </tr>
                    <tr class="listContentAlternate">
                        <td align="center" width="5%">4</td>
                        <td align="center" width="70%">On time delivery of work</td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_3" id="rad_vs_3_1" value="1"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_3" id="rad_vs_3_2" value="2"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_3" id="rad_vs_3_3" value="3"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_3" id="rad_vs_3_4" value="4"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_3" id="rad_vs_3_5" value="5"></td>
                    </tr>
                    <tr class="listContent">
                        <td align="center" width="5%">5</td>
                        <td align="center" width="70%">Accuracy of work delivered</td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_4" id="rad_vs_4_1" value="1"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_4" id="rad_vs_4_2" value="2"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_4" id="rad_vs_4_3" value="3"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_4" id="rad_vs_4_4" value="4"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_4" id="rad_vs_4_5" value="5"></td>
                    </tr>
                    <tr class="listContentAlternate">
                        <td align="center" width="5%">6</td>
                        <td align="center" width="70%">Punctuality</td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_5" id="rad_vs_5_1" value="1"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_5" id="rad_vs_5_2" value="2"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_5" id="rad_vs_5_3" value="3"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_5" id="rad_vs_5_4" value="4"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_5" id="rad_vs_5_5" value="5"></td>
                    </tr>
                    <tr class="listContent">
                        <td align="center" width="5%">7</td>
                        <td align="center" width="70%">Dependability</td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_6" id="rad_vs_6_1" value="1"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_6" id="rad_vs_6_2" value="2"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_6" id="rad_vs_6_3" value="3"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_6" id="rad_vs_6_4" value="4"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_6" id="rad_vs_6_5" value="5"></td>
                    </tr>
                    <tr class="listContentAlternate">
                        <td align="center" width="5%">8</td>
                        <td align="center" width="70%">Interaction with manager</td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_7" id="rad_vs_7_1" value="1"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_7" id="rad_vs_7_2" value="2"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_7" id="rad_vs_7_3" value="3"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_7" id="rad_vs_7_4" value="4"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_7" id="rad_vs_7_5" value="5"></td>
                    </tr>
                    <tr class="listContent">
                        <td align="center" width="5%">9</td>
                        <td align="center" width="70%">General Conduct / Presentable / Professionalism</td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_8" id="rad_vs_8_1" value="1"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_8" id="rad_vs_8_2" value="2"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_8" id="rad_vs_8_3" value="3"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_8" id="rad_vs_8_4" value="4"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_8" id="rad_vs_8_5" value="5"></td>
                    </tr>
                    <tr class="listContentAlternate">
                        <td align="center" width="5%">10</td>
                        <td align="center" width="70%">Adaptability to Change</td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_9" id="rad_vs_9_1" value="1"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_9" id="rad_vs_9_2" value="2"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_9" id="rad_vs_9_3" value="3"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_9" id="rad_vs_9_4" value="4"></td>
                        <td align="center" width="5%"><input type="radio" name="rad_vs_9" id="rad_vs_9_5" value="5"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <script>
		<?php for($ind = 0; $ind < count($arrRadVS); $ind++) { ?>
			$('input:radio[name="rad_vs_<?php echo $ind; ?>"][value="<?php echo $arrRadVS[$ind]; ?>"]').attr('checked',true);
		<?php } ?>
		</script>
        <tr>
        	<td>
            	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain" style="font-size:14px; padding:20px">
                    <tr>
                        <td width="10%"><b>Job KPIs</b></td>
                        <td width="10%" align="center">60%</td>
                        <td width="80%" align="left"><?php echo $jobKPIScore; ?></td>
                    </tr>
                    <tr>
                        <td><b>Naval Anchorage Club KPIs</b></td>
                        <td align="center">40%</td>
                        <td align="left"><?php echo $vsKPIScore; ?></td>
                    </tr>
                    <tr>
                        <td><b>Total</b></td>
                        <td align="center">100%</td>
                        <td align="left"><?php echo ($jobKPIScore + $vsKPIScore) . '%'; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <hr />
            </td>
        </tr>
        <tr>
            <td>&nbsp;
                
            </td>
        </tr>
        <!-- POST RECORDS END -->
		<?php
			if($arrTasks[0]['review_status_id'] >= STATUS_SUPERVISOR_ENTERING_KPIS && $arrTasks[0]['review_remarks'] != '') {
		?>
        <tr>
            <td>
            <table border="0" cellspacing="0" cellpadding="0" class="listTableMain" style="font-size:14px; padding:20px">
            <tr>
            <td>
        <div class="intervieweeMain">
            <div class="interviewee">
                <?php echo nl2br("<b>Supervisor's Comments: </b><br />".$arrTasks[0]['review_remarks']); ?>
            </div>
            <div class="intervieweeIcon">
                <div class="intervieweeIconImage">
                <?php
                if(!file_exists($pictureFolder . $arrReviewerDetails['emp_photo_name']) || empty($arrReviewerDetails['emp_photo_name']))
                    {
                        if(empty($arrReviewerDetails['emp_gender'])) {
                            $arrReviewerDetails['emp_gender'] = 'male';
                        }
                        $arrReviewerDetails['emp_photo_name'] = 'no_image_' . strtolower($arrReviewerDetails['emp_gender']) . '.jpg';
                    }
                ?>
                <img src="<?php echo $this->baseURL . '/' . $pictureFolderShow . $arrReviewerDetails['emp_photo_name']; ?>" alt="" width="60" />
                </div>
                <div class="intervieweeIconName">
                    <b><?php echo $arrReviewerDetails['emp_full_name']; ?></b>
                    <br /><?php echo date($showDateFormat . ' g:i A', strtotime($arrTasks[0]['review_remarks_date'])); ?>
                </div>
            </div>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
    <?php } ?>
    
	<?php if($arrTasks[0]['review_status_id'] > STATUS_EMPLOYEE_ENTERING_KPIS_REMARKS) { ?>
        <tr>
            <td>
            	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain" style="font-size:14px; padding:20px">
                	<tr>
                    	<td>
        <div class="intervieweeMain">
            <div class="interviewee">
                <?php echo nl2br("<b>Employee's Comments: </b><br />".$arrTasks[0]['review_emp_remarks']); ?>
            </div>
            <div class="intervieweeIcon">
                <div class="intervieweeIconImage">
                <?php
                if(!file_exists($pictureFolder . $arrEmployee['emp_photo_name']) || empty($arrEmployee['emp_photo_name']))
                    {
                        if(empty($arrEmployee['emp_gender'])) {
                            $arrEmployee['emp_gender'] = 'male';
                        }
                        $arrEmployee['emp_photo_name'] = 'no_image_' . strtolower($arrEmployee['emp_gender']) . '.jpg';
                    }
                ?>
                <img src="<?php echo $this->baseURL . '/' . $pictureFolderShow . $arrEmployee['emp_photo_name']; ?>" alt="" width="60" />
                </div>
                <div class="intervieweeIconName">
                    <b><?php echo $arrEmployee['emp_full_name']; ?></b>
                    <br /><?php echo date($showDateFormat . ' g:i A', strtotime($arrTasks[0]['review_emp_remarks_date'])); ?>
                </div>
            </div>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
    <?php } else if($arrTasks[0]['review_status_id'] == STATUS_EMPLOYEE_ENTERING_KPIS_REMARKS) { 
	?>
    <tr>
        	<td>
            	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain" style="font-size:14px; padding:20px">
                    <tr class="listHeader">
                        <td align="center">Employee Comments</td>
                    </tr>
                    <tr>
                       <td>&nbsp;</td>
                    </tr>
                    <tr>
                       <td><textarea name="empRemarks" id="empRemarks" rows="10" style="width:99%"><?php echo $empRemarks; ?></textarea></td>
                    </tr>
                    <tr>
                    	<td>
                        	<?php if((int)$arrTasks[0]['review_status_id'] == STATUS_EMPLOYEE_ENTERING_KPIS_REMARKS && (isset($_POST['month']) && isset($_POST['year'])) || (isset($_POST['selMonth']) && isset($_POST['selYear']))) { ?>        
                            <input type="submit" class="smallButton" name="btnSubmitTasks" id="btnSubmitTasks" value="Submit" onClick="confirmation()">
    <?php } ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    <?php
	}	
	?>
	
    	<script>
			$("#frmAddTasks :input").attr("disabled", true).css({"background-color": "#ddd"});
			<?php if((int)$arrTasks[0]['review_status_id'] == STATUS_EMPLOYEE_ENTERING_KPIS_REMARKS) { ?>
			$('#btnSubmitTasks').attr("disabled", false).css({"background-color": "#00769C"});
			$('#empRemarks').attr("disabled", false).css({"background-color": "#fff"});
			$('#selMonth').attr("disabled", false).css({"background-color": "#fff"});
			$('#selYear').attr("disabled", false).css({"background-color": "#fff"});
			<?php } ?>
        </script>
		<?php if($arrTasks[0]['review_status_id'] == STATUS_EMPLOYEE_ENTERING_KPIS_REMARKS) { ?>
        <tr class="listContentAlternate">
            <td colspan="4" align="center" class="listContentCol bold">
                KPI assessment has been submitted for employee's remarks.
            </td>
        </tr>
        <?php } else if($arrTasks[0]['review_status_id'] == STATUS_SUPERVISOR_ENTERING_KPIS_REMARKS) { ?>
        <tr class="listContentAlternate">
            <td colspan="4" align="center" class="listContentCol bold">
                KPI assessment has been submitted for supervisor's remarks.
            </td>
        </tr>
        <?php } else if($arrTasks[0]['review_status_id'] == STATUS_KPIS_SUBMITTED_TO_HR) { ?>
        <tr class="listContentAlternate">
            <td colspan="4" align="center" class="listContentCol bold">
                KPI assessment has been submitted to HR.
            </td>
        </tr>
        <?php } ?>
    </table>
</form>
</div>
<?php } else { ?>
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
	<tr class="listContentAlternate">
		<td align="center" class="listContentCol">No Record Found</td>
	</tr>
</table>
<?php } ?>

<script>
function confirmation()
{
	if (confirm("Are you sure you want to submit KPI form remarks to your Supervisor/Manager?")) {
		return true;
	} else {
		return false;
	}
}
</script>