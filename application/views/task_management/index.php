<div class="midSection">
    <div class="dashboardTitle">
        Welcome To Performance Management System
    </div>
    
	<div class="dashboardBoxesMain">
	<?php
	foreach($allowedSubModulesList as $modules => $moduleValue)
	{
		$tipsyID = '';
		
		if($moduleValue['module_name'] == 'my_team_tasks')
		{
			$reviewUnChecked = getReviewsUnChecked($this->userEmpNum);
			
			if((int)$reviewUnChecked)
			{
				$tipsyID = ' tipsyID';
			}
		}
	?>
		<div class="boxMain<?php echo $tipsyID; ?>" original-title="<?php echo $reviewUnChecked; ?> review(s) not submitted">			
				<div class="boxImageMain">
                	<a href="<?php echo $this->baseURL . '/' . $this->currentController. '/' . $moduleValue['module_name']; ?>">
                		<img src="<?php echo $this->imagePath . '/modules/' . $moduleValue['module_name']; ?>.png" alt="">
                    </a>
                </div>
				<!--<div class="boxTitle">
                	<a href="<?php echo $this->baseURL . '/' . $this->currentController. '/' . $moduleValue['module_name']; ?>">
						<?php echo $moduleValue['display_name']; ?>
                    </a>
                </div>-->
		</div>
	<?php
	}
	?>
	</div>
</div>

<script>	
$(".tipsyID").each(function() {
	$( this ).tipsy({gravity: "s", title: "original-title", trigger: "manual"});
	$( this ).tipsy("show");
});
</script>