<?php
$empID 				= (isset($_POST['empID'])) 	? 	$_POST['empID']		:	$arrRecord['emp_id'];

if(count($_POST)) {
	
	$rad1			= $_POST['rad1'];
	$rad2			= $_POST['rad2'];
	$rad3			= $_POST['rad3'];
	$rad4			= $_POST['rad4'];
	$rad5			= $_POST['rad5'];
	$rad6			= $_POST['rad6'];
	$rad7			= $_POST['rad7'];
	$rad8			= $_POST['rad8'];
	$rad9			= $_POST['rad9'];
	$rad10			= $_POST['rad10'];
	$rad11			= $_POST['rad11'];
	$rad12			= $_POST['rad12'];
	$rad13			= $_POST['rad13'];
	$txt14			= $_POST['txt14'];
	$txt15			= $_POST['txt15'];
	$txt16			= $_POST['txt16'];
	$txt17			= $_POST['txt17'];
	$rad18			= $_POST['rad18'];
	$txt18a			= $_POST['txt18a'];
	$txt18b			= $_POST['txt18b'];
	
} else if(count($arrRecord)) {
	
	$arrAssessment 	= json_decode($arrRecord['assessment_detail']);
	$rad1			= $arrAssessment->rad1;
	$rad2			= $arrAssessment->rad2;
	$rad3			= $arrAssessment->rad3;
	$rad4			= $arrAssessment->rad4;
	$rad5			= $arrAssessment->rad5;
	$rad6			= $arrAssessment->rad6;
	$rad7			= $arrAssessment->rad7;
	$rad8			= $arrAssessment->rad8;
	$rad9			= $arrAssessment->rad9;
	$rad10			= $arrAssessment->rad10;
	$rad11			= $arrAssessment->rad11;
	$rad12			= $arrAssessment->rad12;
	$rad13			= $arrAssessment->rad13;
	$txt14			= $arrAssessment->txt14;
	$txt15			= $arrAssessment->txt15;
	$txt16			= $arrAssessment->txt16;
	$supBy			= $arrAssessment->sup_by;
	$supDateTime	= $arrAssessment->sup_date_time;
	$txt17			= $arrAssessment->txt17;
	$rad18			= $arrAssessment->rad18;
	$txt18a			= $arrAssessment->txt18a;
	$txt18b			= $arrAssessment->txt18b;
	$empDateTime	= $arrAssessment->emp_date_time;	
}
?>

<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( "#txt18a" ).datepicker( "setDate", "<?php echo $txt18a; ?>" );
	$( "#txt18b" ).datepicker( "setDate", "<?php echo $txt18b; ?>" );
	
});
</script>

<!-- GUIDELINE PANEL - START -->

<div class="listPageMain">
    <div class="searchBoxMain">
        <div class="yellow" style="padding-left:20px"><br />
            <b>RATING GUIDELINES:</b><br /><br />
            <ul style="list-style:circle; padding-left:30px">
				<li>(1) Marginal - Does not meet job requirements</li>
				<li>(2) Needs Improvement</li>
				<li>(3) Full Standard - Meets job requirements</li>
				<li>(4) Above Standard - Often exceeds job requirements</li>
				<li>(5) Outstanding - Consistently exceeds job requirements</li>
			</ul><br />
        </div>
    </div>
</div>

<div class="listContentMain" style="height:auto">
<form name="frmAddTasks" id="frmAddTasks" method="post">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
        <tr class="listHeader">
    		<td class="listHeaderCol center" colspan="6">
                PROBATIONARY ASSESSMENT FORM
            </td>
        </tr>
        <tr class="listContent">
        	<td class="listContentCol" width="25%">Employee</td>
            <td class="listContentCol" width="25%">
            <?php
            if(count($arrRecord)) {
				echo getEmployeeName($arrRecord['emp_id']);
			?>
            	<input type="hidden" id="empID" name="empID" value="<?php echo $arrRecord['emp_id']; ?>">
                <script type="text/javascript">
					getEmpDetails(<?php echo (int)$arrRecord['emp_id']; ?>);
					getEmpSupervisor(<?php echo (int)$arrRecord['emp_id']; ?>);
                </script>
            <?php } else { ?>
            	<select name="empID" id="empID" class="dropDown" onchange="getEmpDetails(this.value); getEmpSupervisor(this.value)">
				  <option value="">Select Employee</option>
                  <?php
					if (count($arrEmployees)) {
						foreach($arrEmployees as $key => $arrEmployee) {
					?>
                    <optgroup label="<?php echo $key; ?>">
                        <?php for($ind = 0; $ind < count($arrEmployee); $ind++) { ?>					
                            <option value="<?php echo $arrEmployee[$ind]['emp_id']; ?>"><?php echo $arrEmployee[$ind]['emp_full_name']; ?></option>
                        <?php } ?>
                    </optgroup>
					<?php	}
					}
					?>
			  </select>
              <?php } ?>
            </td>
        	<td class="listContentCol" width="25%">Date of Joining</td>
        	<td class="listContentCol" width="25%" id="empDoJ">-</td>
        </tr>
        <tr class="listContent">
        	<td class="listContentCol">Job Title</td>
        	<td class="listContentCol" id="empJT">-</td>
        	<td class="listContentCol">End of Probation</td>
        	<td class="listContentCol" id="empEoP">-</td>
        </tr>
        <tr class="listContent">
        	<td class="listContentCol">Name of Supervisor</td>
        	<td class="listContentCol" id="empNoS">-</td>
        	<td class="listContentCol">Review Date</td>
        	<td class="listContentCol" id="empRD">
            <?php
                if($arrRecord['created_date'] != '') {
					echo date('d M Y', strtotime($arrRecord['created_date']));
				} else {
					echo date('d M Y', strtotime('now'));
				}
			?>
            </td>
        </tr>
    </table>
    <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
                
        <!-- POST RECORDS START -->
        <tr class="listHeader">
    		<td class="listHeaderCol center" colspan="6">
                QUALITY OF WORK PERFORMANCE
            </td>
        </tr>
        <tr class="listContentAlternate">
            <td class="listContentCol">Description</td>
            <td class="listContentCol">1</td>
            <td class="listContentCol">2</td>
            <td class="listContentCol">3</td>
            <td class="listContentCol">4</td>
            <td class="listContentCol">5</td>
        </tr>
        <tr class="listContent">
            <td class="listContentCol">Accuracy & timeliness: Are work products completed on time, with accuracy, without consistent supervision?</td>
            <td class="listContentCol"><input type="radio" name="rad1" id="rad1_1" value="1"></td>
            <td class="listContentCol"><input type="radio" name="rad1" id="rad1_2" value="2"></td>
            <td class="listContentCol"><input type="radio" name="rad1" id="rad1_3" value="3"></td>
            <td class="listContentCol"><input type="radio" name="rad1" id="rad1_4" value="4"></td>
            <td class="listContentCol"><input type="radio" name="rad1" id="rad1_5" value="5"></td>
        </tr>
        <tr class="listContent">
            <td class="listContentCol">Planning & independence: Does employee effectively plan workload to establish priorities, taking a long-range view of the work?</td>
            <td class="listContentCol"><input type="radio" name="rad2" id="rad2_1" value="1"></td>
            <td class="listContentCol"><input type="radio" name="rad2" id="rad2_2" value="2"></td>
            <td class="listContentCol"><input type="radio" name="rad2" id="rad2_3" value="3"></td>
            <td class="listContentCol"><input type="radio" name="rad2" id="rad2_4" value="4"></td>
            <td class="listContentCol"><input type="radio" name="rad2" id="rad2_5" value="5"></td>
        </tr>
        <tr class="listContent">
            <td class="listContentCol">Adaptability: Can employee adjust to changes / handle pressure?</td>
            <td class="listContentCol"><input type="radio" name="rad3" id="rad3_1" value="1"></td>
            <td class="listContentCol"><input type="radio" name="rad3" id="rad3_2" value="2"></td>
            <td class="listContentCol"><input type="radio" name="rad3" id="rad3_3" value="3"></td>
            <td class="listContentCol"><input type="radio" name="rad3" id="rad3_4" value="4"></td>
            <td class="listContentCol"><input type="radio" name="rad3" id="rad3_5" value="5"></td>
        </tr>
        <tr class="listContent">
            <td class="listContentCol">Creativity & initiative: Does employee use creativity and take initiative in finding new ways to complete the assigned work?</td>
            <td class="listContentCol"><input type="radio" name="rad4" id="rad4_1" value="1"></td>
            <td class="listContentCol"><input type="radio" name="rad4" id="rad4_2" value="2"></td>
            <td class="listContentCol"><input type="radio" name="rad4" id="rad4_3" value="3"></td>
            <td class="listContentCol"><input type="radio" name="rad4" id="rad4_4" value="4"></td>
            <td class="listContentCol"><input type="radio" name="rad4" id="rad4_5" value="5"></td>
        </tr>
        <tr class="listContent">
            <td class="listContentCol">Communication skills: Does employee effectively express herself verbally (e.g. telephone), in person, and in writing?</td>
            <td class="listContentCol"><input type="radio" name="rad5" id="rad5_1" value="1"></td>
            <td class="listContentCol"><input type="radio" name="rad5" id="rad5_2" value="2"></td>
            <td class="listContentCol"><input type="radio" name="rad5" id="rad5_3" value="3"></td>
            <td class="listContentCol"><input type="radio" name="rad5" id="rad5_4" value="4"></td>
            <td class="listContentCol"><input type="radio" name="rad5" id="rad5_5" value="5"></td>
        </tr>
        <tr class="listContent">
            <td class="listContentCol">Technology: Does employee have proficiency using computer, software, etc.?</td>
            <td class="listContentCol"><input type="radio" name="rad6" id="rad6_1" value="1"></td>
            <td class="listContentCol"><input type="radio" name="rad6" id="rad6_2" value="2"></td>
            <td class="listContentCol"><input type="radio" name="rad6" id="rad6_3" value="3"></td>
            <td class="listContentCol"><input type="radio" name="rad6" id="rad6_4" value="4"></td>
            <td class="listContentCol"><input type="radio" name="rad6" id="rad6_5" value="5"></td>
        </tr>
        <tr class="listContent">
            <td class="listContentCol">Organization: Does employee maintain organized systems, files, phone messages, computer, etc.?</td>
            <td class="listContentCol"><input type="radio" name="rad7" id="rad7_1" value="1"></td>
            <td class="listContentCol"><input type="radio" name="rad7" id="rad7_2" value="2"></td>
            <td class="listContentCol"><input type="radio" name="rad7" id="rad7_3" value="3"></td>
            <td class="listContentCol"><input type="radio" name="rad7" id="rad7_4" value="4"></td>
            <td class="listContentCol"><input type="radio" name="rad7" id="rad7_5" value="5"></td>
        </tr>
        <tr>
            <td colspan="6">&nbsp;
                
            </td>
        </tr>
        <tr class="listHeader">
    		<td class="listHeaderCol center" colspan="6">
                RELATIONSHIPS
            </td>
        </tr>
        <tr class="listContentAlternate">
            <td class="listContentCol">Description</td>
            <td class="listContentCol">1</td>
            <td class="listContentCol">2</td>
            <td class="listContentCol">3</td>
            <td class="listContentCol">4</td>
            <td class="listContentCol">5</td>
        </tr>
        <tr class="listContent">
            <td class="listContentCol">Attitude towards work & organization: Does employee have a positive attitude about the job, understand our mission, and represent the organization well?</td>
            <td class="listContentCol"><input type="radio" name="rad8" id="rad8_1" value="1"></td>
            <td class="listContentCol"><input type="radio" name="rad8" id="rad8_2" value="2"></td>
            <td class="listContentCol"><input type="radio" name="rad8" id="rad8_3" value="3"></td>
            <td class="listContentCol"><input type="radio" name="rad8" id="rad8_4" value="4"></td>
            <td class="listContentCol"><input type="radio" name="rad8" id="rad8_5" value="5"></td>
        </tr>
        <tr class="listContent">
            <td class="listContentCol">Relationships with co-workers: Does employee work cooperatively with co-workers, maintain good relationships, and exert a positive influence in the office?</td>
            <td class="listContentCol"><input type="radio" name="rad9" id="rad9_1" value="1"></td>
            <td class="listContentCol"><input type="radio" name="rad9" id="rad9_2" value="2"></td>
            <td class="listContentCol"><input type="radio" name="rad9" id="rad9_3" value="3"></td>
            <td class="listContentCol"><input type="radio" name="rad9" id="rad9_4" value="4"></td>
            <td class="listContentCol"><input type="radio" name="rad9" id="rad9_5" value="5"></td>
        </tr>
        <tr class="listContent">
            <td class="listContentCol">Relationship with supervisor: Does employee accept supervision and constructive criticism?</td>
            <td class="listContentCol"><input type="radio" name="rad10" id="rad10_1" value="1"></td>
            <td class="listContentCol"><input type="radio" name="rad10" id="rad10_2" value="2"></td>
            <td class="listContentCol"><input type="radio" name="rad10" id="rad10_3" value="3"></td>
            <td class="listContentCol"><input type="radio" name="rad10" id="rad10_4" value="4"></td>
            <td class="listContentCol"><input type="radio" name="rad10" id="rad10_5" value="5"></td>
        </tr>
        <tr>
            <td colspan="6">&nbsp;
                
            </td>
        </tr>
        <tr class="listHeader">
    		<td class="listHeaderCol center" colspan="6">
                DEPENDABILITY AND JUDGEMENT
            </td>
        </tr>
        <tr class="listContentAlternate">
            <td class="listContentCol">Description</td>
            <td class="listContentCol">1</td>
            <td class="listContentCol">2</td>
            <td class="listContentCol">3</td>
            <td class="listContentCol">4</td>
            <td class="listContentCol">5</td>
        </tr>
        <tr class="listContent">
            <td class="listContentCol">Attendance: Does employee report to work regularly and on-time, follow office procedures regarding absences, insure that responsibilities are covered?</td>
            <td class="listContentCol"><input type="radio" name="rad11" id="rad11_1" value="1"></td>
            <td class="listContentCol"><input type="radio" name="rad11" id="rad11_2" value="2"></td>
            <td class="listContentCol"><input type="radio" name="rad11" id="rad11_3" value="3"></td>
            <td class="listContentCol"><input type="radio" name="rad11" id="rad11_4" value="4"></td>
            <td class="listContentCol"><input type="radio" name="rad11" id="rad11_5" value="5"></td>
        </tr>
        <tr class="listContent">
            <td class="listContentCol">Dependability & follow-through: Can employee be counted on to complete assigned responsibilities, to follow through on tasks, and to ask for clarity where it is needed?</td>
            <td class="listContentCol"><input type="radio" name="rad12" id="rad12_1" value="1"></td>
            <td class="listContentCol"><input type="radio" name="rad12" id="rad12_2" value="2"></td>
            <td class="listContentCol"><input type="radio" name="rad12" id="rad12_3" value="3"></td>
            <td class="listContentCol"><input type="radio" name="rad12" id="rad12_4" value="4"></td>
            <td class="listContentCol"><input type="radio" name="rad12" id="rad12_5" value="5"></td>
        </tr>
        <tr class="listContent">
            <td class="listContentCol">Judgment and problem solving: Does employee show good judgment and the ability to act independently (and appropriately) when faced with a problem?</td>
            <td class="listContentCol"><input type="radio" name="rad13" id="rad13_1" value="1"></td>
            <td class="listContentCol"><input type="radio" name="rad13" id="rad13_2" value="2"></td>
            <td class="listContentCol"><input type="radio" name="rad13" id="rad13_3" value="3"></td>
            <td class="listContentCol"><input type="radio" name="rad13" id="rad13_4" value="4"></td>
            <td class="listContentCol"><input type="radio" name="rad13" id="rad13_5" value="5"></td>
        </tr>
        <tr>
            <td colspan="6">&nbsp;
                
            </td>
        </tr>
        <tr class="listHeader">
    		<td class="listHeaderCol center" colspan="6">
                Employee's greatest strength
            </td>
        </tr>
        <tr class="listContent">
            <td class="listContentCol" colspan="6">
            	<textarea class="textArea" name="txt14" id="txt14" style="width:99%; height:100px"><?php echo $txt14; ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="6">&nbsp;
                
            </td>
        </tr>
        <tr class="listHeader">
    		<td class="listHeaderCol center" colspan="6">
                Employee's areas of improvement and recommendations to improve this aspect
            </td>
        </tr>
        <tr class="listContent">
            <td class="listContentCol" colspan="6">
            	<textarea class="textArea" name="txt15" id="txt15" style="width:99%; height:100px"><?php echo $txt15; ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="6">&nbsp;
                
            </td>
        </tr>
        <tr class="listHeader">
    		<td class="listHeaderCol center" colspan="6">
                Supervisor's additional comments
            </td>
        </tr>
        <tr class="listContent">
            <td class="listContentCol" colspan="6">
            	<textarea class="textArea" name="txt16" id="txt16" style="width:99%; height:100px"><?php echo $txt16; ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="6">&nbsp;
                
            </td>
        </tr>
        <tr class="listHeader">
    		<td class="listHeaderCol center" colspan="6">
                Supervisor's Decision
            </td>
        </tr>
        <tr class="listContent">
        	<td class="listContentCol" colspan="6">
            	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
                	<tr class="listContent">
                        <td class="listContentCol" colspan="2">
                            <input type="radio" name="rad18" id="rad18_1" value="Confirmed">&nbsp;
                            <label for="rad18_1">Confirmed</label>
                            <input type="text" name="txt18a" id="txt18a" class="textBox datePicker" style="float:none" placeholder="Confirmation Date">
                        </td>
                        <td class="listContentCol" colspan="2">
                            <input type="radio" name="rad18" id="rad18_2" value="Extend">&nbsp;
                            <label for="rad18_2">Extend Probation Upto</label>&nbsp;
                            <input type="text" name="txt18b" id="txt18b" class="textBox datePicker" style="float:none" placeholder="Probation End Date">
                        </td>
                        <td class="listContentCol" colspan="2">
                            <input type="radio" name="rad18" id="rad18_3" value="Terminate">&nbsp;
                            <label for="rad18_3">Terminate</label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <?php if((int)$supBy) { ?>
        <tr>
            <td colspan="6">&nbsp;
                
            </td>
        </tr>
        <tr>
            <td colspan="6" style="padding-left:15px">
                <b><?php echo getEmployeeName((int)$supBy); ?></b><br />
                <?php echo date('F j, Y, g:i A', strtotime($supDateTime)); ?>
            </td>
        </tr>
        <?php } ?>
        <tr>
            <td colspan="6">&nbsp;
                
            </td>
        </tr>
        <?php if($arrRecord['assessment_status'] > STATUS_SUPERVISOR_ENTERING_DETAILS) { ?>
        <tr class="listHeader">
    		<td class="listHeaderCol" colspan="6">
                Employee's Comments regarding Supervisor's Assessment
            </td>
        </tr>
        <tr class="listContent">
            <td class="listContentCol" colspan="6">
            	<textarea class="textArea" name="txt17" id="txt17" style="width:99%; height:100px"><?php echo $txt17; ?></textarea>
            </td>
        </tr>
        <?php if($empDateTime != '') { ?>
        <tr>
            <td colspan="6">&nbsp;
                
            </td>
        </tr>
        <tr>
            <td colspan="6" style="padding-left:15px">
                <b><?php echo getEmployeeName((int)$arrRecord['emp_id']); ?></b><br />
                <?php echo date('F j, Y, g:i A', strtotime($empDateTime)); ?>
            </td>
        </tr>
        <?php } ?>
        <tr>
            <td colspan="6">&nbsp;
                
            </td>
        </tr>
        <?php } ?>
        <!-- POST RECORDS END -->
    
	<?php if(((int)$arrRecord['assessment_status'] == STATUS_SUPERVISOR_ENTERING_DETAILS && $this->userRoleID > EMPLOYEE_ROLE_ID) ||
				($arrRecord['assessment_status'] == STATUS_EMPLOYEE_ENTERING_REMARKS && $arrRecord['emp_id'] == $this->userEmpNum)) { ?>
        <tr class="headerRow">
            <td colspan="6" align="right">
                <select class="dropDown" id="selStatus" name="selStatus" onchange="setButtonName(this.value);" style="display:none">
                    <option value="">Select Status</option>
                    <?php for($ind = 0; $ind < 3; $ind++) { ?>
                        <option value="<?php echo $ind; ?>"><?php echo $assessmentStatus[$ind]; ?></option>
                    <?php } ?>
                </select>&nbsp;
                <input type="submit" class="smallButton" name="btnAddAssessment" id="btnAddAssessment" value="Save" onclick="return confirmation('Save')">
                <input type="submit" class="smallButton" name="btnSubmitAssessment" id="btnSubmitAssessment" value="Submit" onclick="return confirmation('Submit')">
            </td>
        </tr>
    	<script>$('#selStatus').val('<?php echo $selStatus; ?>'); </script>
        
    <?php }
	if($arrRecord['assessment_status'] == STATUS_EMPLOYEE_ENTERING_REMARKS) { ?>
    	<script>
			$("#frmAddTasks :input").attr("disabled", true).css({"background-color": "#ddd"});
			$("#empID").attr("disabled", false).css({"background-color": "#fff"});
			$("#txt17").attr("disabled", false).css({"background-color": "#fff"});
			$("#selStatus").attr("disabled", false).css({"background-color": "#fff"});
			$("#btnAddAssessment").attr("disabled", false).css({"background-color": "#00769C"});
			$("#btnSubmitAssessment").attr("disabled", false).css({"background-color": "#00769C"});
        </script>
    	<tr class="listContentAlternate">
            <td align="center" class="listContentCol bold" colspan="6">
                Probationary assessment is submitted to employee for employee's comments
            </td>
        </tr>
    <?php
    	} else if($arrRecord['assessment_status'] == STATUS_SUBMITTED_TO_HR) { ?>
    	<script>$("#frmAddTasks :input").attr("disabled", true).css({"background-color": "#ddd"});</script>
    	<tr class="listContentAlternate">
            <td align="center" class="listContentCol bold" colspan="6">
                Probationary assessment is submitted to HR
            </td>
        </tr>
    <?php
    	}
	?>
    </table>
</form>
</div>

<script>

$('#empID').val('<?php echo $empID; ?>');
$('input:radio[name="rad1"][value="<?php echo $rad1; ?>"]').attr('checked',true);
$('input:radio[name="rad2"][value="<?php echo $rad2; ?>"]').attr('checked',true);
$('input:radio[name="rad3"][value="<?php echo $rad3; ?>"]').attr('checked',true);
$('input:radio[name="rad4"][value="<?php echo $rad4; ?>"]').attr('checked',true);
$('input:radio[name="rad5"][value="<?php echo $rad5; ?>"]').attr('checked',true);
$('input:radio[name="rad6"][value="<?php echo $rad6; ?>"]').attr('checked',true);
$('input:radio[name="rad7"][value="<?php echo $rad7; ?>"]').attr('checked',true);
$('input:radio[name="rad8"][value="<?php echo $rad8; ?>"]').attr('checked',true);
$('input:radio[name="rad9"][value="<?php echo $rad9; ?>"]').attr('checked',true);
$('input:radio[name="rad10"][value="<?php echo $rad10; ?>"]').attr('checked',true);
$('input:radio[name="rad11"][value="<?php echo $rad11; ?>"]').attr('checked',true);
$('input:radio[name="rad12"][value="<?php echo $rad12; ?>"]').attr('checked',true);
$('input:radio[name="rad13"][value="<?php echo $rad13; ?>"]').attr('checked',true);
$('input:radio[name="rad18"][value="<?php echo $rad18; ?>"]').attr('checked',true);

function confirmation(val)
{
	if(val == 'Save')
	{
		if('<?php echo $this->userEmpNum; ?>' == '<?php echo $arrRecord['emp_id']; ?>') {
			$('#selStatus').val(<?php echo STATUS_EMPLOYEE_ENTERING_REMARKS; ?>);
			alert("You are going to save this assessment.\nPlease note that you may add or modify the details until you submit it for review.");
			return true;
		} else {
			$('#selStatus').val(<?php echo STATUS_SUPERVISOR_ENTERING_DETAILS; ?>);
			alert("You are going to save this assessment.\nPlease note that you may add or modify the details until you submit it for review.");
			return true;
		}
	}
	else if(val == 'Submit')
	{
		if('<?php echo $this->userEmpNum; ?>' == '<?php echo $arrRecord['emp_id']; ?>') {
			$('#selStatus').val(<?php echo STATUS_SUBMIT_REVIEW_TO_HR; ?>);
		} else {
			$('#selStatus').val(<?php echo STATUS_EMPLOYEE_ENTERING_REMARKS; ?>);
		}
		alert("Are you sure you want to submit this probationary assessment?");
		return true;
	}
	return false;
}
</script>