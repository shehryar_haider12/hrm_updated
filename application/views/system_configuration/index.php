<!--<br />
<ul>
	<li><a href="<?php echo base_url() . $currentController . '/save_module'; ?>">Save Module</a></li>
	<li><a href="<?php echo base_url() . $currentController . '/list_module'; ?>">List Module</a></li>
	<li><a href="<?php echo base_url() . $currentController . '/save_job_detail'; ?>">Save Job Title</a></li>
	<li><a href="<?php echo base_url() . $currentController . '/'; ?>">List Job Title</a></li>
	<li><a href="<?php echo base_url() . $currentController . '/save_qualification_detail'; ?>">Save Qualification Detail</a></li>
	<li><a href="<?php echo base_url() . $currentController . '/'; ?>">List Qualification Detail</a></li>
	<li><a href="<?php echo base_url() . $currentController . '/save_email_setting'; ?>">Save Email Setting</a></li>
	<li><a href="<?php echo base_url() . $currentController . '/'; ?>">List Email Setting</a></li>
</ul>-->
<div class="midSection">
    <div class="dashboardTitle">
        Welcome To System Configuration Module
    </div>
    
	<div class="dashboardBoxesMain">
		<?php
		foreach($allowedSubModulesList as $modules => $moduleValue)
		{
		?>
			<div class="boxMain">
				<div class="boxImageMain">
					<a href="<?php echo $baseURL . '/' . $this->currentController . '/' . $moduleValue['module_name']; ?>">
						<img src="<?php echo $this->imagePath . '/modules/' . $moduleValue['module_name']; ?>.png" alt="">
					</a>
				</div>
				<!--<div class="boxTitle">
					<a href="<?php echo $baseURL . '/' . $this->currentController . '/' . $moduleValue['module_name']; ?>">
						<?php echo $moduleValue['display_name']; ?>
					</a>
				</div>-->
			</div>
		<?php
		}
		?>
	</div>
</div>