<?php
$docType 		=   (isset($_POST['docType'])) 			?	$_POST['docType']			:	$arrRecords['doc_type_id'];
$companyID 		= 	(isset($_POST['companies'])) 		? 	$_POST['companies']			:	$arrRecords['company_id'];
$docFile 		= 	($arrRecords['doc_file'] != '') 	? 	$arrRecords['doc_file'] 	: 	'';
?>
<?php if($canWrite == YES) { ?>
<form name="frmAddDocument" id="frmAddDocument" method="post" enctype="multipart/form-data">
  <div class="employeeFormMain">
  <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
  	<tr>
        <td class="formHeaderRow" colspan="2">Add Document</td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Document Type:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
          <select id="docType" name="docType" class="dropDown">
              <option value="">Select Document Type</option>
              <?php
              if (count($arrDocTypes)) {
                  foreach($arrDocTypes as $arrDocType) {
                      $selected = '';
                      if($docType == $arrDocType['doc_type_id']) {
                          $selected = 'selected="selected"';
                      }
              ?>
           <option value="<?php echo $arrDocType['doc_type_id']; ?>" <?php echo $selected; ?>><?php echo $arrDocType['doc_type']; ?></option>
              <?php
                  }
              }
              ?>
          </select>
      </td>
    </tr>
      <tr class="formAlternateRow">
      <td class="formLabelContainer">Company Name:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
          
      <select id="companyID" name="companyID" class="dropDown">
              <option value="">Select Companies</option>
              <?php
              if (count($getCompanies)) {
                  foreach($getCompanies as $getCompany) {
                      $selected = '';
                     
              ?>
                  <option value="<?php echo $getCompany['company_id']; ?>" ><?php echo $getCompany['company_name']; ?></option>
              <?php
                  }
              }
              ?>
          </select>
      
      </td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer">Document:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
      	  <input type="file" name="docFile" id="docFile" class="textBox">&nbsp;&nbsp;<a href="<?php echo $baseURL . str_replace('.', '', $companyDocFolder) . $docFile; ?>" target="_blank">View File</a>
      </td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer"></td>
      <td class="formTextBoxContainer">
      	<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save">&nbsp;
        	
        
        <input type="button" class="smallButton" value="Back" onclick="history.go(-1)"> 
        
             
      </td>
    </tr>
    <tr class="formAlternateRow">
    	<td colspan="2" height="25px">&nbsp;</td>
    </tr>
  </table>
  </div>
</form>
<script>
  	$('#docType').val('<?php echo $docType; ?>');
  	$('#companyID').val('<?php echo $companyID; ?>');
  </script>
<?php
	}
	?>