<?php
$path = '/'.$this->currentController.'/'.$this->currentAction;
$module = ($module != '') ? $module : $this->input->post('module');
$layout = ($layout != '') ? $layout : $this->input->post('layout');
$status = ($status != '') ? $status : $this->input->post('status');
?>

<form name="frmListModules" id="frmListModules" method="post" action="<?php echo $frmActionURL; ?>">
<div class="listPageMain">
	<div class="searchBoxMain">
    	<div class="searchHeader">Search Criteria</div>
        
        <div class="searchcontentmain">
			<div class="searchCol">
				<div class="labelContainer">Module Parent:</div>
				<div class="textBoxContainer">					
                    <select id="module" name="module" class="dropDown">
                        <option value="">All</option>
                        <?php
                        if (count($modules)) {
                            foreach($modules as $arrModule) {
                        ?>
                            <option value="<?php echo $arrModule['module_id']; ?>"><?php echo $arrModule['display_name']; ?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
				</div>
                <div class="labelContainer">Layout:</div>
				<div class="textBoxContainer">
                    <select id="layout" name="layout" class="dropDown">
                        <option value="">All</option>
                        <?php
                        if (count($layouts)) {
                            foreach($layouts as $arrLayout) {
                        ?>
                            <option value="<?php echo $arrLayout['layout_id']; ?>"><?php echo $arrLayout['layout_display_name']; ?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
				</div>
			</div>
			
			<div class="searchCol">
				<div class="labelContainer">Status:</div>
				<div class="textBoxContainer">
					<?php echo statusCombo('status',$this->userRoleID, 'All', 'dropDown'); ?>
				</div>
                
                <div class="labelContainer">Module Skip Listing:</div>
				<div class="textBoxContainer">
					<input value="1" type="checkbox" id="moduleSkipListing" name="moduleSkipListing" <?php echo ($moduleSkipListing == 1)? 'checked="checked"' : ''; ?>>
				</div>
			</div>
			
			<div class="formButtonContainerWide">
				<input type="submit" class="searchButton" name="btnSearchModule" id="btnSearchModule" value="Search">
			</div>
		</div>
	</div>

  <script>
  	$('#module').val('<?php echo $module; ?>');
  	$('#layout').val('<?php echo $layout; ?>');
  	$('#status').val('<?php echo $status; ?>');
  </script>
</form>

	<div class="centerButtonContainer">
		<input class="addButton" type="button" value="Add Module" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/save_module/' ?>';" />
	</div>

	<div class="centerElementsContainer">
		<div class="recordCountContainer"><?php echo "Total Records Count: ".$totalRecordsCount; ?></div>
		<?php
        if($pageLinks) {
        ?>
            <div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
        <?php 	}	?>
	</div>
    
    <div class="listContentMain">
    <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
        <tr class="listHeader">
            <td class="listHeaderCol" style="width:150px">Module Name</td>
            <td class="listHeaderCol" style="width:150px">Module Parent</td>
            <td class="listHeaderCol" style="width:150px">Status</td>
            <td class="listHeaderCol" style="width:150px">Layout</td>
            <td class="listHeaderCol" style="width:150px">Skip Listing</td>
            <td class="listHeaderCol" style="width:150px">Sort Order</td>
            <?php if(($canWrite == 1) || ($canDelete == 1) || ($arrRecords[$ind]['module_status'] != STATUS_DELETED)) { ?>
            <td class="listHeaderColLast" style="width:150px">Action</td>
            <?php } ?>
        </tr>
        <?php
        for($ind = 0; $ind < count($arrRecords); $ind++) 
		{
			($arrRecords[$ind]['module_status'] == STATUS_ACTIVE) ? $classListingData = "listContent" : $classListingData = "listContentAlternate" ;
        ?>
        <tr class="<?php echo $classListingData; ?>">
            <td class="listContentCol"><?php echo $arrRecords[$ind]['display_name']; ?></td>
            <td class="listContentCol"><?php if($arrRecords[$ind]['parent_module_name']) echo $arrRecords[$ind]['parent_module_name']; else echo "-"; ?></td>
            <td class="listContentCol"><?php if ($arrRecords[$ind]['module_status'] == STATUS_ACTIVE) echo "Active"; else if ($arrRecords[$ind]['module_status'] == STATUS_INACTIVE_VIEW) echo "InActive"; else if ($arrRecords[$ind]['module_status'] == STATUS_DELETED) echo "Deleted"; ?></td>
            <td class="listContentCol"><?php echo $arrRecords[$ind]['layout_name']; ?></td>
            <td class="listContentCol"><?php if ($arrRecords[$ind]['module_skip_display'] == 1) echo "Yes"; else echo "No"; ?></td> 
            <td class="listContentCol"><?php echo $arrRecords[$ind]['module_sort_order']; ?></td>
            <td class="listContentColLast">
            	<div class="colButtonContainer">
                <?php if($canWrite == 1) { ?>
                    <input class="smallButton" type="button" value="View/Edit" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/save_module/' . $arrRecords[$ind]['module_id']; ?>';" />
                <?php } ?>
                <?php if(($canDelete == 1) && ($arrRecords[$ind]['module_status'] != STATUS_DELETED)){ ?>
                    <input class="smallButton" type="button" value="Delete" onclick="deleteRecord('<?php echo $path; ?>','<?php echo $arrRecords[$ind]['module_id']; ?>');" />
                <?php } ?>
                </div>
            </td>
        </tr>
        <?php
       }
       ?>
    </table>
    </div>
</div>