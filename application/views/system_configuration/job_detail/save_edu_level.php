<?php
$educationLevel = (isset($_POST['educationLevel']))		?	$_POST['educationLevel']	:	$record['edu_level_name'];
if ($record['edu_level_status'] == STATUS_INACTIVE_VIEW) { $recordStatus = -1;} else if ($record['edu_level_status'] == STATUS_ACTIVE) {$recordStatus = 1;} else if ($record['edu_level_status'] == STATUS_DELETED) {$recordStatus = 2;}
$status 		= (isset($_POST['status'])) 			?	$_POST['status']			:	$recordStatus;
?>

<form name="frmAddEduLevel" id="frmAddEduLevel" method="post">
<div class="listPageMain">
	<div class="formMain">
        <table border="0" cellspacing="0" cellpadding="0" style="width:100%">
            <tr>
            	<?php if($record['edu_level_id']) { ?>
					<td class="formHeaderRow" colspan="2">Update Education Level</td>
                <?php } else { ?>
                	<td class="formHeaderRow" colspan="2">Add Education Level</td>
                <?php } ?>
			</tr>
            <tr>
            	<td class="formLabelContainer">Education Level:</td>
                <td class="formTextBoxContainer">
                	<input type="text" id="educationLevel" name="educationLevel" maxlength="100" class="textBox" value="<?php echo $educationLevel; ?>">
                </td>
            </tr>
            <tr>
                <td class="formLabelContainer">Status:</td>
                <td class="formTextBoxContainer">
					<?php echo statusCombo('status',$this->userRoleID, 'Select Status', 'dropDown'); ?>
                </td>
            </tr>
            <tr class="formAlternateRow">
                <td class="formLabelContainer"></td>
                <td class="formTextBoxContainer">
                    <input class="smallButton" name="addEduLevel" type="submit" value="Save">
                    <?php if(strpos($_SERVER["REQUEST_URI"],$record['edu_level_id']) != false) { ?>
                        <input class="smallButton" type="button" value="Cancel" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/list_edu_level' ?>';">
                    <?php } ?>
                </td>
            </tr>
        </table>
	</div>
</div>
</form>

<script>
	$('#status').val('<?php echo $status; ?>');
</script>