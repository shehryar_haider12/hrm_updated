<?php
$path = '/'.$this->currentController.'/'.$this->currentAction;
$status = ($status != '') ? $status : $this->input->post('status');
$jobCategory = ($jobCategory != '') ? $jobCategory : $this->input->post('jobCategory');
?>

<form name="frmListJobTitles" id="frmListJobTitles" method="post" action="<?php echo $frmActionURL; ?>">
<div class="listPageMain">
	<div class="searchBoxMain">
    	<div class="searchHeader">Search Criteria</div>
        
        <div class="searchcontentmain">
			<div class="searchCol">
            	<div class="labelContainer">Job Category:</div>
                <div class="textBoxContainer">					
                    <select id="jobCategory" name="jobCategory" class="dropDown">
                        <option value="">All</option>
                        <?php
                        if (count($jobCategories)) {
                            foreach($jobCategories as $arrJobCategory) {
                        ?>
                            <option value="<?php echo $arrJobCategory['job_category_id']; ?>"><?php echo $arrJobCategory['job_category_name']; ?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </div>
				<div class="labelContainer">Status:</div>
				<div class="textBoxContainer">
					<?php echo statusCombo('status',$this->userRoleID, 'All', 'dropDown'); ?>
				</div>
			</div>
			
			<div class="formButtonContainerWide">
				<input type="submit" class="searchButton" name="btnSearchJobTitle" id="btnSearchJobTitle" value="Search">
			</div>
		</div>
	</div>
      
  <script>
  	$('#status').val('<?php echo $status; ?>');
	$('#jobCategory').val('<?php echo $jobCategory; ?>');
  </script>
</form>

	<?php if($canWrite == 1) { ?>
	<div class="centerButtonContainer">
		<input class="addButton" type="button" value="Add Job Title" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/save_job_title/' ?>';" />
	</div>
	<?php }	?>
	
	<div class="centerElementsContainer">
		<div class="recordCountContainer"><?php echo "Total Records Count: ".$totalRecordsCount; ?></div>
		<?php
        if($pageLinks) {
        ?>
            <div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
        <?php 	}	?>
	</div>

	<div class="listContentMain">
    <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
        <tr class="listHeader">
            <td class="listHeaderCol" style="width:150px">Job Title</th>
            <td class="listHeaderCol" style="width:150px">Job Category</th>
            <td class="listHeaderCol" style="width:150px">Job Description</th>
            <td class="listHeaderCol" style="width:150px">Status</th>
            <?php if(($canWrite == 1) || (($canDelete == 1) && ($arrRecords[$ind]['job_title_status'] != STATUS_DELETED))) { ?>
            <td class="listHeaderColLast" style="width:150px">Action</th>
            <?php } ?>
        </tr>
        <?php
        for($ind = 0; $ind < count($arrRecords); $ind++) 
		{
			($arrRecords[$ind]['job_title_status'] == STATUS_ACTIVE) ? $classListingData = "listContent" : $classListingData = "listContentAlternate" ;
        ?>
        <tr class="<?php echo $classListingData; ?>">
            <td class="listContentCol"><?php echo $arrRecords[$ind]['job_title_name']; ?></td>
            <td class="listContentCol"><?php if($arrRecords[$ind]['job_category_name']) echo $arrRecords[$ind]['job_category_name']; else echo "-"; ?></td>
            <td class="listContentCol"><?php if($arrRecords[$ind]['job_description']) echo $arrRecords[$ind]['job_description']; else echo "-"; ?></td>
            <td class="listContentCol"><?php if ($arrRecords[$ind]['job_title_status'] == STATUS_ACTIVE) echo "Active"; else if ($arrRecords[$ind]['job_title_status'] == STATUS_INACTIVE_VIEW) echo "InActive"; else if ($arrRecords[$ind]['job_title_status'] == STATUS_DELETED) echo "Deleted"; ?></td>
            <?php if(($canWrite == 1) || (($canDelete == 1) && ($arrRecords[$ind]['job_title_status'] != STATUS_DELETED))) { ?>
			<td class="listContentColLast">
            	<div class="colButtonContainer">
                <?php if($canWrite == 1) { ?>
                    <input class="smallButton" type="button" value="View/Edit" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/save_job_title/' . $arrRecords[$ind]['job_title_id']; ?>';" />
                <?php } ?>
                <?php if(($canDelete == 1) && ($arrRecords[$ind]['job_title_status'] != STATUS_DELETED)){ ?>
                    <input class="smallButton" type="button" value="Delete" onclick="deleteRecord('<?php echo $path; ?>','<?php echo $arrRecords[$ind]['job_title_id']; ?>');" />
                <?php } ?>
                </div>
            </td>
			<?php } ?>
        </tr>
        <?php
       }
       ?>
    </table>
    </div>
</div>