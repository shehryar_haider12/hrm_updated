<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends Master_Controller {

	function populateModuleScreens()
	{
		$role_id = $this->input->post("role_id",TRUE);		
		$module_id = $this->input->post("module_id",TRUE);
		
		$this->load->model('model_user_management', 'model_user_management', true);
		$moduleScreens = $this->model_user_management->populateModuleScreens($role_id,$module_id);
		
		if(count($moduleScreens) > 0) {
			for($i=0; $i < count($moduleScreens); $i++) {
				if(($i % 7) == 0)
					$screens .= '<tr class="formAlternateRow">';
						$screens .= '<td>';
							$screens .= '<table>';
							$screens .= '<tr>';
								$screens .= '<td id="sub_modules['.$moduleScreens[$i]['module_id'].']" name="sub_modules['.$moduleScreens[$i]['module_id'].']" class="formSubHeaderRow" align="left">
												<span class="bold ">'.$moduleScreens[$i]['display_name'].'</span>
												<span><input type="hidden" name="sub_modules['.$moduleScreens[$i]['module_id'].']" value="1"></span>
											</td>';
							$screens .= '</tr>';
							$screens .= '<tr>';
								$screens .= '<td class="formLabelText" align="center">';
								$screens .= '<span>Status:</span>';
								$screens .= '<select name="status['.$moduleScreens[$i]['module_id'].']" id="status['.$moduleScreens[$i]['module_id'].']" class="dropDownSmall">
												<option value="1">Allow</option>
												<option value="0">DisAllow</option>
											</select>';
								$screens .= '</td>';
							$screens .= '</tr>';
							$screens .= '<tr>';
								$screens .= '<td class="formLabelText" align="left">';
								$screens .= '<span><input type="checkbox" name="can_read['.$moduleScreens[$i]['module_id'].']" value="1"> Can Read</span>';
								$screens .= '<br><br><span><input type="checkbox" name="can_write['.$moduleScreens[$i]['module_id'].']" value="1"> Can Write</span>';
								$screens .= '<br><br><span><input type="checkbox" name="can_delete['.$moduleScreens[$i]['module_id'].']" value="1"> Can Delete</span>';
								$screens .= '</td>';
							$screens .= '</tr>';
							$screens .= '<tr>';
							$screens .= '<td>&nbsp;</td>';
							$screens .= '</tr>';
							$screens .= '</table>';
						$screens .= '</td>';
				if(($i % 7) == 6)
					$screens .= '</tr>';	
			}
			
		} else {
			$screens = '<tr><td class="errorMessage">Privileges for the selected User Role and Module already added.</td></tr>';
		}
		
		echo $screens;
		exit;
	}
	
	function populateRoleModuleScreens()
	{
		$role_id = $this->input->post("role_id",TRUE);
		$role_name = $this->input->post("role_name",TRUE);
		$module_id = $this->input->post("module_id",TRUE);
		$screen_id = $this->input->post("screen_id",TRUE);
		
		$this->load->model('model_user_management', 'model_user_management', true);
		$moduleScreens = $this->model_user_management->populateRoleModuleScreens($role_id,$module_id);		
		if(count($moduleScreens) > 0) {
			if($screen_id == 1) {
				$screens .= '<tr><td class="articleText bold">'.$role_name.'</td></tr>';
				for($i=0; $i < count($moduleScreens); $i++) {
					$statusEnable = '';
					$statusDisable = '';
					$checkedRead = '';
					$checkedWrite = '';
					$checkedDelete = '';
					
					if($moduleScreens[$i]['status'] == 1)
						{$statusEnable = " selected='true'"; } 
					if($moduleScreens[$i]['status'] == 0)
						{$statusDisable = " selected='true'"; }
					if($moduleScreens[$i]['can_read'] == 1)
						{$checkedRead = " checked='true'";}
					if($moduleScreens[$i]['can_write'] == 1)
						{$checkedWrite = " checked='true'";}
					if($moduleScreens[$i]['can_delete'] == 1)
						{$checkedDelete = " checked='true'";}
					
					if(($i % 7) == 0)					
						$screens .= '<tr>';
							$screens .= '<td>';
								$screens .= '<table>';
								$screens .= '<tr>';
									$screens .= '<td id="sub_modules['.$moduleScreens[$i]['module_id'].']" name="sub_modules['.$moduleScreens[$i]['module_id'].']" class="formSubHeaderRow" align="left">
													<span class="bold ">'.$moduleScreens[$i]['display_name'].'</span>
													<span><input type="hidden" name="sub_modules['.$moduleScreens[$i]['module_id'].']" value="1"></span>
												</td>';
								$screens .= '</tr>';
								$screens .= '<tr>';
									$screens .= '<td class="formLabelText" align="center">';
									$screens .= '<span>Status:</span>';
									$screens .= '<select name="status['.$moduleScreens[$i]['module_id'].']" id="status'.$moduleScreens[$i]['module_id'].'" class="dropDownSmall" onchange="changeValues('.$moduleScreens[$i]['module_id'].','.$moduleScreens[$i]['can_read'].','.$moduleScreens[$i]['can_write'].','.$moduleScreens[$i]['can_delete'].')">
													<option value="1" '.$statusEnable.'>Allow</option>
													<option value="0" '.$statusDisable.'>DisAllow</option>
												</select>';
									$screens .= '</td>';
								$screens .= '</tr>';
								$screens .= '<tr>';
									$screens .= '<td class="formLabelText" align="left">';
									$screens .= '<span><input type="checkbox" id="can_read'.$moduleScreens[$i]['module_id'].'" name="can_read['.$moduleScreens[$i]['module_id'].']" value="1" '.$checkedRead.'> Can Read</span>';
									$screens .= '<br><br><span><input type="checkbox" id="can_write'.$moduleScreens[$i]['module_id'].'" name="can_write['.$moduleScreens[$i]['module_id'].']" value="1" '.$checkedWrite.'> Can Write</span>';
									$screens .= '<br><br><span><input type="checkbox" id="can_delete'.$moduleScreens[$i]['module_id'].'" name="can_delete['.$moduleScreens[$i]['module_id'].']" value="1" '.$checkedDelete.'> Can Delete</span>';
									$screens .= '</td>';
								$screens .= '</tr>';
								$screens .= '<tr>';
								$screens .= '<td>&nbsp;</td>';
								$screens .= '</tr>';
								$screens .= '</table>';
							$screens .= '</td>';
					if(($i % 7) == 6)
						$screens .= '</tr>';
				}
			} else if($screen_id == 2) {
				$screens .= '<tr><td class="articleText bold">'.$role_name.'</td></tr>';
				for($i=0; $i < count($moduleScreens); $i++) {
					$status = '';
					$checkedRead = '';
					$checkedWrite = '';
					$checkedDelete = '';
					
					if($moduleScreens[$i]['status'] == 1)
						{$status = "Allow"; } 
					if($moduleScreens[$i]['status'] == 0)
						{$status = "DisAllow"; }
					if($moduleScreens[$i]['can_read'] == 1)
						{$checkedRead = " checked='true'";}
					if($moduleScreens[$i]['can_write'] == 1)
						{$checkedWrite = " checked='true'";}
					if($moduleScreens[$i]['can_delete'] == 1)
						{$checkedDelete = " checked='true'";}
					
					if(($i % 7) == 0)					
						$screens .= '<tr>';
							$screens .= '<td>';
								$screens .= '<table>';
								$screens .= '<tr>';
									$screens .= '<td class="formSubHeaderRow bold" align="left">'.$moduleScreens[$i]['display_name'].'</td>';
								$screens .= '</tr>';
								$screens .= '<tr>';
									$screens .= '<td class="formLabelText" align="center">';
									$screens .= '<span>Status:</span>';
									$screens .= '<span>'.$status.'</span>';
									$screens .= '</td>';
								$screens .= '</tr>';
								$screens .= '<tr>';
									$screens .= '<td class="formLabelText" align="left">';
									$screens .= '<span><input type="checkbox" '.$checkedRead.'> Can Read</span>';
									$screens .= '<br><br><span><input type="checkbox" '.$checkedWrite.'> Can Write</span>';
									$screens .= '<br><br><span><input type="checkbox" '.$checkedDelete.'> Can Delete</span>';
									$screens .= '</td>';
								$screens .= '</tr>';
								$screens .= '<tr>';
								$screens .= '<td>&nbsp;</td>';
								$screens .= '</tr>';
								$screens .= '</table>';
							$screens .= '</td>';
					if(($i % 7) == 6)
						$screens .= '</tr>';
				}
			}
					
		} else {
			$screens = '<tr><td class="errorMessage">No Modules for the selected User Role.</td></tr>';
		}
		
		echo $screens;
		exit;		
	}	
	
	function populateModuleParticularScreen()
	{
		$role_id = $this->input->post("role_id",TRUE);
		$module_id = $this->input->post("module_id",TRUE);
		$sub_module_id = $this->input->post("sub_module_id",TRUE);
		
		$this->load->model('model_user_management', 'model_user_management', true);
		$moduleScreens = $this->model_user_management->populateModuleParticularScreen($role_id,$module_id,$sub_module_id);
		
		$screens .= '<tr class="formAlternateRow">';
		foreach($moduleScreens as $moduleScreen)
		{
			$screens .= '<td id="sub_modules['.$moduleScreen['module_id'].']" name="sub_modules['.$moduleScreen['module_id'].']" class="formSubHeaderRow" align="left">
						<span>'.$moduleScreen['display_name'].'</span>
						<span><input type="hidden" name="sub_modules['.$moduleScreen['module_id'].']" value="1"></span>
						';
		}
		$screens .= '</tr>';
		
		$screens .= '<tr>';
		foreach($moduleScreens as $moduleScreen)
		{
			$statusEnable = '';
			$statusDisable = '';
			if($moduleScreen['status'] == 1)
				{$statusEnable = " selected='true'"; } 
			if($moduleScreen['status'] == 0)
				{$statusDisable = " selected='true'"; } 
			
			$screens .= '<td class="formLabelText" align="center">';
			$screens .= '<span>Status:</span>';
			$screens .= '
						<select name="status['.$moduleScreen['module_id'].']" id="status['.$moduleScreen['module_id'].']" class="dropDownSmall">
							<option value="1" '.$statusEnable.'>Allow</option>
							<option value="0" '.$statusDisable.'>DisAllow</option>
						</select>
						';
			$screens .= '</td>';
		}
		$screens .= '</tr>';
		
		$screens .= '<tr>';
		foreach($moduleScreens as $moduleScreen)
		{
			$checkedRead = '';
			$checkedWrite = '';
			$checkedDelete = '';
			if($moduleScreen['can_read'] == 1)
				{$checkedRead = " checked='true'";}
			if($moduleScreen['can_write'] == 1)
				{$checkedWrite = " checked='true'";}
			if($moduleScreen['can_delete'] == 1)
				{$checkedDelete = " checked='true'";}
			
			$screens .= '<td class="formLabelText" align="left">';
			$screens .= '<span><input type="checkbox" name="can_read['.$moduleScreen['module_id'].']" value="1" '.$checkedRead.'>Can Read</span>';
			$screens .= '<br><span><input type="checkbox" name="can_write['.$moduleScreen['module_id'].']" value="1" '.$checkedWrite.'>Can Write</span>';
			$screens .= '<br><span><input type="checkbox" name="can_delete['.$moduleScreen['module_id'].']" value="1" '.$checkedDelete.'>Can Delete</span>';
			$screens .= '</td>';
		}
		$screens .= '</tr>';
		
		echo $screens;
		exit;
	}
	
	function populateLocations($elmID, $locID, $locType) {
		
		$locID = (int)$locID;
		$locType = (int)$locType;
		
		if($locType == 1) {
			$strType = 'Region';
		} else if($locType == 2) {
			$strType = 'Country';
		} else if($locType == 3) {
			$strType = 'City';
		}
		
		$this->load->model('model_system_configuration', 'configuration', true);
		$arrLocations = $this->configuration->getLocations(array('location_parent_id' => $locID, 'location_type_id' => $locType));
		if($locType < 3) {
			$strSelect = '<select name="'.$strType.'" id="'.$strType.'" class="dropDown" onchange="populateLocation(\''.$elmID.'\', this.value, \''.$locType.'\')" style="clear:both">';
		} else {
			$strSelect = '<select name="'.$strType.'" id="'.$strType.'" class="dropDown" onchange="populateAreas(\''.$elmID.'\', this.value, \''.($locType + 1).'\')" style="clear:both">';
		}
		$strSelect .= '<option value="">Select '.$strType.'</option>';
		
		for($ind = 0; $ind < count($arrLocations); $ind++) {
			$strSelect .= '<option value="'.$arrLocations[$ind]['location_id'].'">'.$arrLocations[$ind]['location_name'].'</option>';
		}
		$strSelect .= '</select>';
		
		echo $strSelect;
		exit;
	}
	
	function populateAreas($elmID, $locID, $locType) {
		
		$locID = (int)$locID;
		$locType = (int)$locType;
		$strType = 'Area';
		$strSelect = '';
		
		$this->load->model('model_system_configuration', 'configuration', true);
		$arrLocations = $this->configuration->getLocations(array('location_parent_id' => $locID, 'location_type_id' => $locType));
		
		if(count($arrLocations)) {
			
			$strSelect = '<select name="'.$strType.'" id="'.$strType.'" class="dropDown" style="clear:both">';
			$strSelect .= '<option value="">Select '.$strType.'</option>';
			
			for($ind = 0; $ind < count($arrLocations); $ind++) {
				$strSelect .= '<option value="'.$arrLocations[$ind]['location_id'].'">'.$arrLocations[$ind]['location_name'].'</option>';
			}
			$strSelect .= '</select>';
			
		}
		echo $strSelect;
		exit;
	}
	
	function populateLocationsSystem($elmID, $locID, $locType, $onChange = 1) {
		
		$locID = (int)$locID;
		$locType = (int)$locType;
		
		if($locType == 1) {
			$strType = 'Region';
		} else if($locType == 2) {
			$strType = 'Country';
		} else if($locType == 3) {
			$strType = 'City';
		}
		
		$this->load->model('model_system_configuration', 'configuration', true);
		$arrLocations = $this->configuration->getLocations(array('location_parent_id' => $locID, 'location_type_id' => $locType));
		if($locType == 1) {
			$elmID = 'tdCountry';
			$strSelect = '<select name="'.$strType.'" id="'.$strType.'" class="dropDown"';
			if((int)$onChange) {
				 $strSelect .= 'onchange="populateLocation(\''.$elmID.'\', this.value, \''.$locType.'\')"';
			}
			$strSelect .= ' style="clear:both">';
		} else {
			$strSelect = '<select name="'.$strType.'" id="'.$strType.'" class="dropDown" style="clear:both">';
		}
		$strSelect .= '<option value="">Select '.$strType.'</option>';
		
		for($ind = 0; $ind < count($arrLocations); $ind++) {
			$strSelect .= '<option value="'.$arrLocations[$ind]['location_id'].'">'.$arrLocations[$ind]['location_name'].'</option>';
		}
		$strSelect .= '</select>';
		
		echo $strSelect;
		exit;
	}
	
	function populateRegion($field,$locTypeID,$onChange = 1,$selectedID = 0)
	{
		$this->load->model('model_system_configuration', 'configuration', true);
		$arrLocations = $this->configuration->getLocations(array('location_type_id' => $locTypeID));
		
		$strSelect = '<select name="Region" id="Region" class="dropDown"';
		if((int)$onChange) {
				 $strSelect .= 'onchange="populateCountry(\'tdCountry\', this.value)"';
			}
		$strSelect .= ' style="clear:both">';
		$strSelect .= '<option value="">Select Region</option>';
		
		for($ind = 0; $ind < count($arrLocations); $ind++) {
			$txtSelected = '';
			if($selectedID == $arrLocations[$ind]['location_id']) {				
				$txtSelected = ' selected="selected" ';
			}
			$strSelect .= '<option value="'.$arrLocations[$ind]['location_id'].'" '.$txtSelected.'>'.$arrLocations[$ind]['location_name'].'</option>';
		}
		$strSelect .= '</select>';
		
		echo $strSelect;
		exit;
	}
	
	function populateCountry($field,$regionID,$selectedID = 0)
	{
		$this->load->model('model_system_configuration', 'configuration', true);
		$arrLocations = $this->configuration->getLocations(array('location_parent_id' => $regionID));
		
		$strSelect = '<select name="Country" id="Country" class="dropDown"';
		if((int)$onChange) {
				 $strSelect .= 'onchange="populateCountry(\'tdCountry\', this.value)"';
			}
		$strSelect .= ' style="clear:both">';
		$strSelect .= '<option value="">Select Country</option>';
		
		for($ind = 0; $ind < count($arrLocations); $ind++) {
			$txtSelected = '';
			if($selectedID == $arrLocations[$ind]['location_id']) {				
				$txtSelected = ' selected="selected" ';
			}
			$strSelect .= '<option value="'.$arrLocations[$ind]['location_id'].'" '.$txtSelected.'>'.$arrLocations[$ind]['location_name'].'</option>';
		}
		$strSelect .= '</select>';
		
		echo $strSelect;
		exit;
	}
	
	function checkCandidate() {
		
		$strVal = $this->input->post('strVal');
		$strCol = $this->input->post('strCol');
		$strID = (int)$this->input->post('strID');
		
		if(!empty($strCol) && !empty($strVal)) {
			$this->load->model('model_recruitment_management', 'recruitment', true);
			echo ($this->recruitment->notExistingCandidate($strCol, $strVal, $strID))
				 ? '<span id="spanMsg" style="color:#92AE2B;font-weight:bold;font-size:12px;padding-left:10px">Available</span>' 
				 : '<span id="spanMsg" style="color:#f00;font-weight:bold;font-size:12px;padding-left:10px">Already Registered</span>';
			exit;
		}
		
	}
	
	function callCandidateOld($numCall = '') {		
		
		$numCall = '03333834143';
		$this->load->model('model_employee_management', 'employee', true);
		$arrEmployee = $this->employee->getEmployeeDetail(array('e.emp_id' => (int)$this->userEmpNum));
		$ipExt = '9447'; //$arrEmployee['emp_ip_num'];
		
		$numCall = str_replace('-', '', $numCall);
		
		if(strlen($numCall) <= 11) {
			$numFiltered = (substr($numCall, 0, 1) == 0) ? substr($numCall, 1) : $numCall;
			$numCall = '+92' . $numFiltered;
		}
		
		if(!(int)$ipExt) {
			$ipExt = 9447;
		}
		
		if(!empty($numCall) && strlen($numCall) <= 13 && (int)$ipExt) {
			
			require_once APPPATH . 'libraries/AsteriskManager.php';
			$arrParams = array(
								'conDetails' => array(
														'server' => '192.168.24.249', 
														'port' => '5038'
													),
								'userID' => 'admin',
								'passWord' => 'abc123',
								);

			#	INITIATE ASTERISK OBJECT
			$objAst = new Net_AsteriskManager($arrParams['conDetails']);
						
			#	CONNECT TO SERVER AND LOG IN
			try {
				
				//if (strpos($numCall, '+82') === true) {
                //	$callingContext = "pstn-gateway";
				//} else {
                $callingContext = "pstn-gateway";
				//}
				
				$objAst->connect();
				$objAst->login($arrParams['userID'], $arrParams['passWord']);
				
				$objAst->originateCall(
										$extension	= '9430', 
										//$channel	= 'SIP/999',
										$channel	= 'IAX2/pstn-gateway:sbtr4d1ng@192.168.24.249/9447',
									   	$context	= 'to-pstn', 
									   	$cid 		= 'Call Using HRMS', 
									   	$priority 	= 1, 
									   	$timeout 	= 30000, 
									   	$variables 	= array(
															'cLRec' 	=> $ipExt . '_' . date('dmY_His') . '.wav',
															'RecFlag' 	=> 1,
															'CDRFlag' 	=> 1
															)
									  );
				echo '';
				
			} catch (PEAR_Exception $e) {
				
				echo $e;
				
			}
			
			exit;
		}		
	}
	
	function callCandidate($candID = 0, $numCall = '') {
		
		# TO GET CURRENT USER IP EXTENSION
		if(ENABLE_CALLS_FROM_RMS) {
			
			$this->load->model('model_employee_management', 'employee', true);
			
			$arrEmployee = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->userEmpNum), false);
			$ipExt = (int)$arrEmployee['emp_ip_num'];
			$strDateTime = date('dmY_His');
			
			$curlRequest = curl_init();
			// Set some options - we are passing in a useragent too here
			curl_setopt_array($curlRequest, array(
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_URL => ASTERISK_CALL_WEBSERVICE_URL . '?number=' . $numCall . '&ext=' . $ipExt,
				CURLOPT_HTTPHEADER => array(
											'IS_FROM_HRMS: 1',
											'DATETIME_FROM_HRMS: ' . $strDateTime
											),
				CURLOPT_CONNECTTIMEOUT => 30
			));
			// Send the request & save response to $resp
			$curlResponse = curl_exec($curlRequest);
			// Close request to clear up some resources
			curl_close($curlRequest);
			
			if($curlResponse != '') {
				$arrValues = array(
									'caller_emp_id' => (int)$arrEmployee['emp_id'],
									'caller_ext' => $ipExt,
									'called_candidate_id' => $candID,
									'called_ext' => $numCall,
									'call_file_name' => 'hrm_' . $ipExt . '_' . $numCall . '_' . $strDateTime . '.wav',
									'call_datetime' => date(DATE_TIME_FORMAT),
								);
				$callID = $this->employee->saveValues(TABLE_CANDIDATE_CALLS, $arrValues);
				echo $callID;
			}
		
		}
		
	}
	
	function callEmployee($empID = 0, $numCall = '') {
		
		# TO GET CURRENT USER IP EXTENSION
			
		$this->load->model('model_employee_management', 'employee', true);
		
		$arrEmployee = $this->employee->getEmployeeDetail(array('e.emp_id' => $this->userEmpNum), false);
		$ipExt = (int)$arrEmployee['emp_ip_num'];
		$strDateTime = date('dmY_His');
		
		$curlRequest = curl_init();
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curlRequest, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => ASTERISK_CALL_WEBSERVICE_URL . '?number=' . $numCall . '&ext=' . $ipExt,
			CURLOPT_HTTPHEADER => array(
										'IS_FROM_HRMS: 1',
										'DATETIME_FROM_HRMS: ' . $strDateTime
										),
			CURLOPT_CONNECTTIMEOUT => 30
		));
		// Send the request & save response to $resp
		$curlResponse = curl_exec($curlRequest);
		// Close request to clear up some resources
		curl_close($curlRequest);
		
		if($curlResponse != '') {
			$arrValues = array(
								'caller_emp_id' => (int)$arrEmployee['emp_id'],
								'caller_ext' => $ipExt,
								'called_emp_id' => $empID,
								'called_ext' => $numCall,
								'call_file_name' => 'hrm_' . $ipExt . '_' . $numCall . '_' . $strDateTime . '.wav',
								'call_datetime' => date(DATE_TIME_FORMAT),
							);
			$callID = $this->employee->saveValues(TABLE_EMPLOYEE_CALLS, $arrValues);
			echo $callID;
		}
		
	}
	
	function checkEmployee() {
		
		$strVal = $this->input->post('strVal');
		$strCol = $this->input->post('strCol');
		$strID = (int)$this->input->post('strID');
		
		if(!empty($strCol) && !empty($strVal)) {
			$this->load->model('model_employee_management', 'employee', true);
			echo ($this->employee->notExistingEmployee($strCol, $strVal, $strID))
				 ? '<span id="spanMsg" style="color:#92AE2B;font-weight:bold;font-size:12px;padding-left:10px">Available</span>' 
				 : '<span id="spanMsg" style="color:#f00;font-weight:bold;font-size:12px;padding-left:10px">Already Registered</span>';
			exit;
		}
		
	}
	
	function getEmployee() {
		
		$empID = $this->input->post('empID');
		
		if((int)$empID) {
			$this->load->model('model_employee_management', 'employee', true);
			echo json_encode($this->employee->getEmployees(array('e.emp_id' => $empID)));
			exit;
		}
		
	}
	
	function getEmployeeSupervisors() {
		
		$empID = $this->input->post('empID');
		
		if((int)$empID) {
			echo getSupervisorName($empID);
			exit;
		}
		
	}
	
	function saveAttNotes() {
		
		$empID = (int)$this->input->post("emp_id", TRUE);
		$strDate = $this->input->post("att_date", TRUE);
		$strNotes = $this->input->post("att_notes", TRUE);
					
		$this->load->model('model_attendance_management', 'attendance', true);
		$this->attendance->deleteValue(TABLE_ATTENDANCE_NOTES, null, array('emp_id' => $empID, 'att_date' => $strDate));
		$this->attendance->saveValues(TABLE_ATTENDANCE_NOTES, array('emp_id' => $empID, 'att_date' => $strDate, 'att_notes' => $strNotes, 'created_by' => $this->userEmpNum, 'created_date' => date(DATE_TIME_FORMAT)));
		echo '1';
		exit;
		
	}
	
}
?>