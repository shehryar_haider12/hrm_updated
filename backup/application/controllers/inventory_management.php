<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventory_Management extends Master_Controller {
	
	private $arrData = array();
	public $arrRoleIDs = array();
	private $maxLinks;
	private $limitRecords;
	private $employeeID = 0;
	
	function __construct() {
		
		parent::__construct();
		
		$this->load->model('model_inventory_management', 'model_inventory', true);
				
		$this->arrRoleIDs       				= array(WEB_ADMIN_ROLE_ID, SUPER_ADMIN_ROLE_ID);
		
		$arrEmployee = $this->model_inventory->getValues(TABLE_EMPLOYEE, 'emp_job_category_id', array('emp_id' => $this->userEmpNum));
		
		if(($arrEmployee[0]['emp_job_category_id'] != ADMIN_JOB_CATEGORY_ID) && (!isAdmin($this->userRoleID))) {
			redirect(base_url() . 'message/access_denied');
			exit;
		}

		$this->arrData["baseURL"] 				= $this->baseURL . '/';
		$this->arrData["imagePath"] 			= $this->imagePath;
		$this->arrData["screensAllowed"] 		= $this->screensAllowed;
		$this->arrData["currentController"] 	= $this->currentController;
		$this->arrData["dateFormat"] 			= DATE_FORMAT;
		$this->arrData["dateTimeFormat"] 		= DATE_TIME_FORMAT;
		$this->arrData["showDateFormat"] 		= SHOW_DATE_TIME_FORMAT;
		$this->arrData["pictureFolder"]			= PROFILE_PICTURE_FOLDER;
		$this->arrData["pictureFolderShow"]		= str_replace('./', '', PROFILE_PICTURE_FOLDER);
		$this->arrData["emailTemplatesFolder"]	= EMAIL_TEMPLATE_FOLDER;
		$this->maxLinks 						= MAX_PAGING_VISIBLE;
		$this->limitRecords 					= MAX_RECORDS_LISTING;
		$this->arrData['gstFolder']				= GST_CERTIFICATE_FOLDER;
		$this->arrData["gstFolderDownload"]	= str_replace('./', '', GST_CERTIFICATE_FOLDER);
		
		$currentActionArray = array();
		while(!empty($this->arrData["screensAllowed"]))
		{
			$temp = array_pop($this->arrData["screensAllowed"]);
			if($temp['module_name']== $this->currentAction){
				array_push($currentActionArray,$temp);
			}
		}
		$currentActionArray = $currentActionArray[0];
		$this->arrData['canWrite'] = $currentActionArray["can_write"];
		$this->arrData['canDelete'] = $currentActionArray["can_delete"];
		
	}
	
	public function index() {
		
		$moduleID = getValue($this->modulesAllowed, 'module_name', $this->currentController, 'module_id');
		$this->arrData['allowedSubModulesList'] = getValue($this->modulesAllowedForMenu, 'module_name', $this->currentController, 'sub_menu'); 
		$this->template->write_view('content', 'inventory_management/index', $this->arrData);
		$this->template->render();
	}
			
	public function product_types($productTypeID = 0)
	{	
		$productTypeID = (int)$productTypeID;
						
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) 
		{
			if($this->arrData['canDelete'] == 1)
			{
				$arrDeleteWhere 	= array('product_type_id' => (int)$this->input->post("record_id"));
				$arrDeleteValues 	= array('product_type_status' => 2, 'deleted_by' => $this->userEmpNum, 'deleted_date' => date($this->arrData["dateTimeFormat"]));
				
				if(!$this->model_inventory->deleteValue(TABLE_INVENTORY_PRODUCT_TYPES,$arrDeleteValues,$arrDeleteWhere)) {
					echo "0"; exit;
				} else {
					
					# SET LOG
				
					echo "1"; exit;
				}
			}
		}		
				
		if($productTypeID) {			
			$arrWhere['product_type_id'] = $productTypeID;
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('productType', 'Product Type', 'trim|required|min_length[3]|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			if($this->input->post("status")) {
				$status = $this->input->post("status");
				if($status == -1) {
					$status = 0;
				}
			}
			
			$arrValues = array(
								'product_type' 			=> $this->input->post("productType"),
								'product_type_status' 	=> (int)$status
								);	
								
			if($productTypeID) {
				$arrValues['modified_by'] = $this->userEmpNum;
				$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
			} else {
				$arrValues['created_by'] = $this->userEmpNum;
				$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
			}
						
			if($productTypeID) {
				$result = $this->model_inventory->saveValues(TABLE_INVENTORY_PRODUCT_TYPES, $arrValues, $arrWhere);
			} else {
				$result = $this->model_inventory->saveValues(TABLE_INVENTORY_PRODUCT_TYPES, $arrValues);
			}
			
			if($result)
			{
				$this->session->set_flashdata('success_message', 'Product Type saved successfully');
				redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction);
				exit;
			} else {
				$this->arrData['error_message'] = 'Data not saved, try again';
			}
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}		
		
		# CODE FOR POPULATING CURRENT RECORD
		if($productTypeID) {
			$this->arrData['record'] = $this->model_inventory->getProductTypes($arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData['arrRecords'] = $this->model_inventory->getProductTypes();
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'inventory_management/product_types', $this->arrData);
		$this->template->render();
	}
	
	public function product_frequency($productFrequencyID = 0)
	{	
		$productFrequencyID = (int)$productFrequencyID;
						
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) 
		{
			if($this->arrData['canDelete'] == 1)
			{
				$arrDeleteWhere 	= array('product_frequency_id' => (int)$this->input->post("record_id"));
				$arrDeleteValues 	= array('product_frequency_status' => 2, 'deleted_by' => $this->userEmpNum, 'deleted_date' => date($this->arrData["dateTimeFormat"]));
				
				if(!$this->model_inventory->deleteValue(TABLE_INVENTORY_PRODUCT_FREQUENCY,$arrDeleteValues,$arrDeleteWhere)) {
					echo "0"; exit;
				} else {
					
					# SET LOG
				
					echo "1"; exit;
				}
			}
		}		
				
		if($productFrequencyID) {			
			$arrWhere['product_frequency_id'] = $productFrequencyID;
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('productFrequency', 'Product Frequency', 'trim|required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('numberOfDays', 'Product Frequency', 'trim|required|numeric|min_length[1]|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			if($this->input->post("status")) {
				$status = $this->input->post("status");
				if($status == -1) {
					$status = 0;
				}
			}
			
			$arrValues = array(
								'product_frequency' 		=> $this->input->post("productFrequency"),
								'number_of_days' 			=> $this->input->post("numberOfDays"),
								'product_frequency_status' 	=> (int)$status
								);	
								
			if($productFrequencyID) {
				$arrValues['modified_by'] = $this->userEmpNum;
				$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
			} else {
				$arrValues['created_by'] = $this->userEmpNum;
				$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
			}
						
			if($productFrequencyID) {
				$result = $this->model_inventory->saveValues(TABLE_INVENTORY_PRODUCT_FREQUENCY, $arrValues, $arrWhere);
			} else {
				$result = $this->model_inventory->saveValues(TABLE_INVENTORY_PRODUCT_FREQUENCY, $arrValues);
			}
			
			if($result)
			{
				$this->session->set_flashdata('success_message', 'Product Frequency saved successfully');
				redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction);
				exit;
			} else {
				$this->arrData['error_message'] = 'Data not saved, try again';
			}
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}		
		
		# CODE FOR POPULATING CURRENT RECORD
		if($productFrequencyID) {
			$this->arrData['record'] = $this->model_inventory->getProductFrequency($arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData['arrRecords'] = $this->model_inventory->getProductFrequency();
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'inventory_management/product_frequency', $this->arrData);
		$this->template->render();
	}
	
	public function product_metrics($productMetricID = 0)
	{	
		$productMetricID = (int)$productMetricID;
						
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) 
		{
			if($this->arrData['canDelete'] == 1)
			{
				$arrDeleteWhere 	= array('product_metric_id' => (int)$this->input->post("record_id"));
				$arrDeleteValues 	= array('product_metric_status' => 2, 'deleted_by' => $this->userEmpNum, 'deleted_date' => date($this->arrData["dateTimeFormat"]));
				
				if(!$this->model_inventory->deleteValue(TABLE_INVENTORY_PRODUCT_METRICS,$arrDeleteValues,$arrDeleteWhere)) {
					echo "0"; exit;
				} else {
					
					# SET LOG
				
					echo "1"; exit;
				}
			}
		}		
				
		if($productMetricID) {			
			$arrWhere['product_metric_id'] = $productMetricID;
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('productMetric', 'Product Metric', 'trim|required|min_length[2]|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			if($this->input->post("status")) {
				$status = $this->input->post("status");
				if($status == -1) {
					$status = 0;
				}
			}
			
			$arrValues = array(
								'product_metric' 			=> $this->input->post("productMetric"),
								'product_metric_status' 	=> (int)$status
								);	
								
			if($productMetricID) {
				$arrValues['modified_by'] = $this->userEmpNum;
				$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
			} else {
				$arrValues['created_by'] = $this->userEmpNum;
				$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
			}
						
			if($productMetricID) {
				$result = $this->model_inventory->saveValues(TABLE_INVENTORY_PRODUCT_METRICS, $arrValues, $arrWhere);
			} else {
				$result = $this->model_inventory->saveValues(TABLE_INVENTORY_PRODUCT_METRICS, $arrValues);
			}
			
			if($result)
			{
				$this->session->set_flashdata('success_message', 'Product Metric saved successfully');
				redirect($this->baseURL . '/' . $this->currentController . '/' . $this->currentAction);
				exit;
			} else {
				$this->arrData['error_message'] = 'Data not saved, try again';
			}
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}		
		
		# CODE FOR POPULATING CURRENT RECORD
		if($productMetricID) {
			$this->arrData['record'] = $this->model_inventory->getProductMetrics($arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData['arrRecords'] = $this->model_inventory->getProductMetrics();
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'inventory_management/product_metrics', $this->arrData);
		$this->template->render();
	}
	
	public function save_product($productID)
	{	
		$arrWhere = array();
		$this->arrData['record'] = array();
		
		$productID = (int)$productID;
		if($productID) {				
			$arrWhere = array(
				'product_id' => $productID
				);
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('product', 'Product', 'trim|required|callback_validateFields[product]|min_length[3]|xss_clean');
		$this->form_validation->set_rules('productType', 'Product Type', 'trim|required|xss_clean');
		$this->form_validation->set_rules('productFrequency', 'Product Frequency', 'trim|required|xss_clean');
		$this->form_validation->set_rules('productMetric', 'Product Metric', 'trim|required|xss_clean');
		$this->form_validation->set_rules('productReorderQuantity', 'Product ReOrder Quantity', 'trim|required|numeric|min_length[1]|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true)
		{			
			if($this->input->post())
			{				
				$table = TABLE_INVENTORY_PRODUCTS;
				$column = 'product_id';
				$Where = array(
							'product_name' => $this->input->post("product")
							);
				$checkResult = $this->model_inventory->checkValues($table, $Where, $column, $productID);
				if($checkResult == 1)
				{		
					if($productID)
					{
						$status = $this->input->post("status");
						if($status == -1) {
							$status = 0;
						}
						
						$arrValues = array(
							'product_name' => ucwords($this->input->post("product")),
							'product_type_id' => $this->input->post("productType"),
							'product_frequency_id' => $this->input->post("productFrequency"),
							'product_metric_id' => $this->input->post("productMetric"),
							'product_reorder_quantity' => $this->input->post("productReorderQuantity"),
							'product_status' => (int)$status,
							'modified_by' => (int)$this->userEmpNum,
							'modified_date' => date($this->arrData["dateTimeFormat"])
							);
					}
					else 
					{				
						$status = $this->input->post("status");
						if($status == -1) {
							$status = 0;
						}
						
						$arrValues = array(
								'product_name' => ucwords($this->input->post("product")),
								'product_type_id' => $this->input->post("productType"),
								'product_frequency_id' => $this->input->post("productFrequency"),
								'product_metric_id' => $this->input->post("productMetric"),
								'product_reorder_quantity' => $this->input->post("productReorderQuantity"),
								'product_status' => (int)$status,
								'created_by' => (int)$this->userEmpNum,
								'created_date' => date($this->arrData["dateTimeFormat"])
								);
					}
					
					if($productID) {
					$result = $this->model_inventory->saveValues(TABLE_INVENTORY_PRODUCTS, $arrValues, $arrWhere);
					} else {
						$result = $this->model_inventory->saveValues(TABLE_INVENTORY_PRODUCTS, $arrValues);
					}
					
					if($result)
					{
						$this->session->set_flashdata('success_message', 'Product saved successfully');
						redirect(base_url() . $this->currentController . '/list_products');
						exit;
					} else {
						$this->arrData['error_message'] = 'Data not saved, try again';
					}
				}
				else
				{
					$this->arrData['error_message'] = 'Product you entering already exists.';
				}
			}
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}		
		
		# CODE FOR POPULATING CURRENT RECORD
		if($productID) {
			$this->arrData['record'] = $this->model_inventory->getValues(TABLE_INVENTORY_PRODUCTS, '*', $arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData['arrProductTypes']		= $this->model_inventory->getValues(TABLE_INVENTORY_PRODUCT_TYPES, 'product_type_id, product_type', array('product_type_status' => 1, 'order_by' => 'product_type'));
		$this->arrData['arrProductFrequencies']	= $this->model_inventory->getValues(TABLE_INVENTORY_PRODUCT_FREQUENCY, 'product_frequency_id, product_frequency', array('product_frequency_status' => 1, 'order_by' => 'product_frequency'));
		$this->arrData['arrProductMetrics'] 	= $this->model_inventory->getValues(TABLE_INVENTORY_PRODUCT_METRICS, 'product_metric_id, product_metric', array('product_metric_status' => 1, 'order_by' => 'product_metric'));
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'inventory_management/save_product', $this->arrData);
		$this->template->render();
	}
	
	public function list_products($pageNum = 1)
	{
		if((int)$pageNum < 1) $pageNum = 1;
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id"))
		{
			if($this->arrData['canDelete'] == 1)
			{
				# Check this Product in Active Vendor Products
				$tableName = TABLE_INVENTORY_VENDORS_PRODUCTS;
				
				$Where = array(
					'product_id' => $this->input->post("record_id"),
					'vendor_product_status' => '1'
					);
				
				$resultModule = $this->model_inventory->checkValues($tableName, $Where);
				if($resultModule == 1)
				{
					$tblName = TABLE_INVENTORY_PRODUCTS;
					
					$arrDeleteWhere = array(
						'product_id' => $this->input->post("record_id")
						);
						
					$arrDeleteValues = array(
						'product_status' => 2,
						'deleted_by' => (int)$this->userEmpNum,
						'deleted_date' => date($this->arrData["dateTimeFormat"])
						);
					
					if(!$this->model_inventory->deleteValue($tblName,$arrDeleteValues,$arrDeleteWhere)) {
						echo "0"; exit;
					} else {
						echo "1"; exit;
					}
				}
				else { echo "2"; exit; }
			}
		}
		
		
		$arrWhere = array();
		
		if ($this->input->post()) {
			if($this->input->post("product")) {				
				$arrWhere['ip.product_name'] = $this->input->post("product");
				$this->arrData['product'] = $this->input->post("product");
			}
			if($this->input->post("productTypes")) {				
				$arrWhere['ip.product_type_id'] = $this->input->post("productTypes");
				$this->arrData['productTypes'] = $this->input->post("productTypes");
			}
			if($this->input->post("productFrequency")) {
				$arrWhere['ip.product_frequency_id'] = $this->input->post("productFrequency");
				$this->arrData['productFrequency'] = $this->input->post("productFrequency");
			}
			if($this->input->post("productMetrics")) {
				$arrWhere['ip.product_metric_id'] = $this->input->post("productMetrics");
				$this->arrData['productMetrics'] = $this->input->post("productMetrics");
			}
			if($this->input->post("status")) {
				$status = $this->input->post("status");
				if($status == -1) {
					$status = 0;
				}
				$arrWhere['ip.product_status'] = $status;
				$this->arrData['status'] = $this->input->post("status");
			}
		}
		
		# CODE FOR APPLYING PAGING ON DATA AND DISPLAYING DATA
		$totalCount = $this->model_inventory->getTotalProducts($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = $totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_inventory->getProducts($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($totalCount / $this->limitRecords);
		$this->arrData['pageLinks']	= displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListProducts');
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData['arrProductTypes']		= $this->model_inventory->getValues(TABLE_INVENTORY_PRODUCT_TYPES, 'product_type_id, product_type', array('product_type_status' => 1, 'order_by' => 'product_type'));
		$this->arrData['arrProductFrequencies']	= $this->model_inventory->getValues(TABLE_INVENTORY_PRODUCT_FREQUENCY, 'product_frequency_id, product_frequency', array('product_frequency_status' => 1, 'order_by' => 'product_frequency'));
		$this->arrData['arrProductMetrics'] 	= $this->model_inventory->getValues(TABLE_INVENTORY_PRODUCT_METRICS, 'product_metric_id, product_metric', array('product_metric_status' => 1, 'order_by' => 'product_metric'));
		$this->arrData['frmActionURL'] 			= base_url() . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'inventory_management/list_products', $this->arrData);
		$this->template->render();
	}
	
	public function save_vendor($vendorID)
	{	
		$arrWhere = array();
		$this->arrData['record'] = array();
		
		$vendorID = (int)$vendorID;
		if($vendorID) {				
			$arrWhere = array(
				'vendor_id' => $vendorID
				);
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('vendor', 'Vendor', 'trim|required|callback_validateFields[vendor]|min_length[3]|xss_clean');
		$this->form_validation->set_rules('vendorPerson1', 'Vendor Person 1', 'trim|required|callback_validateFields[vendorPerson1]|min_length[3]|xss_clean');
		$this->form_validation->set_rules('vendorPerson2', 'Vendor Person 2', 'trim|callback_validateFields[vendorPerson2]|min_length[3]|xss_clean');
		$this->form_validation->set_rules('vendorEmail', 'Vendor Email', 'trim|valid_email|xss_clean');
		$this->form_validation->set_rules('vendorAddress', 'Vendor Address', 'trim|xss_clean');
		$this->form_validation->set_rules('vendorContact1', 'Vendor Contact1', 'trim|required|xss_clean|callback_strValidate[vendorContact1]');
		$this->form_validation->set_rules('vendorContact2', 'Vendor Contact2', 'trim|xss_clean|callback_strValidate[vendorContact2]');
		$this->form_validation->set_rules('vendorNIC', 'Vendor NIC', 'trim|numeric|exact_length[13]|xss_clean');
		$this->form_validation->set_rules('vendorNTN', 'Vendor NTN', 'trim|numeric|exact_length[8]|xss_clean');
		$this->form_validation->set_rules('vendorOtherDetails', 'Vendor Details', 'trim|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true)
		{			
			if($this->input->post())
			{				
				$table = TABLE_INVENTORY_VENDORS;
				$column = 'vendor_id';
				$Where = array(
							'vendor_name' => $this->input->post("vendor")
							);
				$checkResult = $this->model_inventory->checkValues($table, $Where, $column, $vendorID);
				if($checkResult == 1)
				{		
					#	GST DOCUMENT UPLOAD
					$uploadConfig['upload_path'] 	= $this->arrData["gstFolder"];
					$uploadConfig['allowed_types'] 	= 'jpg|jpeg|png|bmp|doc|docs|pdf';
					$uploadConfig['max_size']		= '1024';
					$uploadConfig['max_filename']	= '100';
					$uploadConfig['encrypt_name']	= true;
		
					$this->load->library('upload');
					$this->upload->initialize($uploadConfig);
					
					$gstFileName = '';
					
					if(!$this->upload->do_upload('vendorGST')) {
						if(!empty($_FILES['vendorGST']['name'])) {
							$error = array('error' => $this->upload->display_errors());	
							$this->arrData['error_message'] = $error['error'];
						}					
					} else {				
						$dataUpload = $this->upload->data();
						$gstFileName = basename($dataUpload['file_name']);
						
						if($gstFileName != $this->input->post("gstFileName")) {
								unlink($this->arrData["gstFolder"] . $this->input->post("gstFileName")); # DELETE PREVIOUS RESUME FILE
							}
					}
					
					
					if($vendorID)
					{
						$status = $this->input->post("status");
						if($status == -1) {
							$status = 0;
						}
						
						$arrValues = array(
							'vendor_name' => ucwords($this->input->post("vendor")),
							'vendor_person_name1' => ucwords($this->input->post("vendorPerson1")),
							'vendor_person_name2' => ucwords($this->input->post("vendorPerson2")),
							'vendor_email' => $this->input->post("vendorEmail"),
							'vendor_address' => $this->input->post("vendorAddress"),
							'vendor_contact_number1' => $this->input->post("vendorContact1"),
							'vendor_contact_number2' => $this->input->post("vendorContact2"),
							'vendor_nic_number' => $this->input->post("vendorNIC"),
							'vendor_ntn_number' => $this->input->post("vendorNTN"),
							'vendor_other_details' => $this->input->post("vendorOtherDetails"),
							'vendor_status' => (int)$status,
							'modified_by' => (int)$this->userEmpNum,
							'modified_date' => date($this->arrData["dateTimeFormat"])
							);
					}
					else 
					{				
						$status = $this->input->post("status");
						if($status == -1) {
							$status = 0;
						}
						
						$arrValues = array(
								'vendor_name' => ucwords($this->input->post("vendor")),
								'vendor_person_name1' => ucwords($this->input->post("vendorPerson1")),
								'vendor_person_name2' => ucwords($this->input->post("vendorPerson2")),
								'vendor_email' => $this->input->post("vendorEmail"),
								'vendor_address' => $this->input->post("vendorAddress"),
								'vendor_contact_number1' => $this->input->post("vendorContact1"),
								'vendor_contact_number2' => $this->input->post("vendorContact2"),
								'vendor_nic_number' => $this->input->post("vendorNIC"),
								'vendor_ntn_number' => $this->input->post("vendorNTN"),
								'vendor_other_details' => $this->input->post("vendorOtherDetails"),
								'vendor_status' => (int)$status,
								'created_by' => (int)$this->userEmpNum,
								'created_date' => date($this->arrData["dateTimeFormat"])
								);
					}
					
					if(trim($gstFileName) != '') {
							$arrValues['vendor_gst_file_name'] = $gstFileName;
						}
					
					if($vendorID) {
					$result = $this->model_inventory->saveValues(TABLE_INVENTORY_VENDORS, $arrValues, $arrWhere);
					} else {
						$result = $this->model_inventory->saveValues(TABLE_INVENTORY_VENDORS, $arrValues);
					}
					
					if($result)
					{
						$this->session->set_flashdata('success_message', 'Vendor saved successfully');
						redirect(base_url() . $this->currentController . '/list_vendors');
						exit;
					} else {
						$this->arrData['error_message'] = 'Data not saved, try again';
					}
				}
				else
				{
					$this->arrData['error_message'] = 'Vendor you entering already exists.';
				}
			}
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}		
		
		# CODE FOR POPULATING CURRENT RECORD
		if($vendorID) {
			$this->arrData['record'] = $this->model_inventory->getValues(TABLE_INVENTORY_VENDORS, '*', $arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
				
		# TEMPLATE LOADING
		$this->template->write_view('content', 'inventory_management/save_vendor', $this->arrData);
		$this->template->render();
	}
	
	public function list_vendors($pageNum = 1)
	{
		if((int)$pageNum < 1) $pageNum = 1;
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id"))
		{
			if($this->arrData['canDelete'] == 1)
			{
				# Check this Product in Active Vendor Products
				$tableName = TABLE_INVENTORY_VENDORS_PRODUCTS;
				
				$Where = array(
					'vendor_id' => $this->input->post("record_id"),
					'vendor_product_status' => '1'
					);
				
				$resultModule = $this->model_inventory->checkValues($tableName, $Where);
				if($resultModule == 1)
				{
					$tblName = TABLE_INVENTORY_VENDORS;
					
					$arrDeleteWhere = array(
						'vendor_id' => $this->input->post("record_id")
						);
						
					$arrDeleteValues = array(
						'vendor_status' => 2,
						'deleted_by' => (int)$this->userEmpNum,
						'deleted_date' => date($this->arrData["dateTimeFormat"])
						);
					
					if(!$this->model_inventory->deleteValue($tblName,$arrDeleteValues,$arrDeleteWhere)) {
						echo "0"; exit;
					} else {
						echo "1"; exit;
					}
				}
				else { echo "2"; exit; }
			}
		}
				
		$arrWhere = array();
		
		if ($this->input->post()) 
		{
			if($this->input->post("vendor")) {				
				$arrWhere['vendor_name'] = $this->input->post("vendor");
				$this->arrData['vendor'] = $this->input->post("vendor");
			}
			if($this->input->post("vendorContactPerson")) {				
				$arrWhere['vendor_person_name'] = $this->input->post("vendorContactPerson");
				$this->arrData['vendorContactPerson'] = $this->input->post("vendorContactPerson");
			}
			if($this->input->post("vendorNIC")) {				
				$arrWhere['vendor_nic_number'] = $this->input->post("vendorNIC");
				$this->arrData['vendorNIC'] = $this->input->post("vendorNIC");
			}
			if($this->input->post("vendorNTN")) {				
				$arrWhere['vendor_ntn_number'] = $this->input->post("vendorNTN");
				$this->arrData['vendorNTN'] = $this->input->post("vendorNTN");
			}			
			if($this->input->post("status")) {
				$status = $this->input->post("status");
				if($status == -1) {
					$status = 0;
				}
				$arrWhere['vendor_status'] = $status;
				$this->arrData['status'] = $this->input->post("status");
			}
		}
		
		# CODE FOR APPLYING PAGING ON DATA AND DISPLAYING DATA
		$totalCount = $this->model_inventory->getTotalVendors($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = $totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_inventory->getVendors($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($totalCount / $this->limitRecords);
		$this->arrData['pageLinks']	= displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListVendors');
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData['frmActionURL'] 	= base_url() . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'inventory_management/list_vendors', $this->arrData);
		$this->template->render();
	}
	
	public function save_vendor_product($vendorProductID)
	{	
		$arrWhere = array();
		$this->arrData['record'] = array();
		
		$vendorProductID = (int)$vendorProductID;
		if($vendorProductID) {				
			$arrWhere = array(
				'vendor_product_id' => $vendorProductID
				);
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('product', 'Product', 'trim|required|xss_clean');
		$this->form_validation->set_rules('vendor', 'Vendor', 'trim|required|xss_clean');
		$this->form_validation->set_rules('vendorPriority', 'Vendor Priority', 'trim|required|xss_clean');
		$this->form_validation->set_rules('productUnitPrice', 'Product Unit Price', 'trim|required|numeric|min_length[1]|xss_clean');
		$this->form_validation->set_rules('comments', 'Comments', 'trim|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true)
		{			
			if($this->input->post())
			{				
				$table = TABLE_INVENTORY_VENDORS_PRODUCTS;
				$column = 'vendor_product_id';
				$Where = array(
							'product_id' => $this->input->post("product"),
							'vendor_id' => $this->input->post("vendor")
							);
				$checkResult = $this->model_inventory->checkValues($table, $Where, $column, $vendorProductID);
				
				if($checkResult == 1)
				{		
					if($vendorProductID)
					{
						$status = $this->input->post("status");
						if($status == -1) {
							$status = 0;
						}
						
						$arrValues = array(
							'product_id' => $this->input->post("product"),
							'vendor_id' => $this->input->post("vendor"),
							'vendor_priority' => $this->input->post("vendorPriority"),
							'product_unit_price' => $this->input->post("productUnitPrice"),
							'comments' => $this->input->post("comments"),
							'vendor_product_status' => (int)$status,
							'modified_by' => (int)$this->userEmpNum,
							'modified_date' => date($this->arrData["dateTimeFormat"])
							);
					}
					else 
					{				
						$status = $this->input->post("status");
						if($status == -1) {
							$status = 0;
						}
						
						$arrValues = array(
							'product_id' => $this->input->post("product"),
							'vendor_id' => $this->input->post("vendor"),
							'vendor_priority' => $this->input->post("vendorPriority"),
							'product_unit_price' => $this->input->post("productUnitPrice"),
							'comments' => $this->input->post("comments"),
							'vendor_product_status' => (int)$status,
							'created_by' => (int)$this->userEmpNum,
							'created_date' => date($this->arrData["dateTimeFormat"])
							);
					}
					
					if($vendorProductID) {
					$result = $this->model_inventory->saveValues(TABLE_INVENTORY_VENDORS_PRODUCTS, $arrValues, $arrWhere);
					} else {
						$result = $this->model_inventory->saveValues(TABLE_INVENTORY_VENDORS_PRODUCTS, $arrValues);
					}
					
					if($result)
					{
						$this->session->set_flashdata('success_message', 'Vendor Product saved successfully');
						redirect(base_url() . $this->currentController . '/list_vendors_products');
						exit;
					} else {
						$this->arrData['error_message'] = 'Data not saved, try again';
					}
				}
				else
				{
					$this->arrData['error_message'] = 'Vendor Product you entering already exists.';
				}
			}
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}		
		
		# CODE FOR POPULATING CURRENT RECORD
		if($vendorProductID) {
			$this->arrData['record'] = $this->model_inventory->getValues(TABLE_INVENTORY_VENDORS_PRODUCTS, '*', $arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData['arrProducts'] = $this->model_inventory->getProducts(array('ip.product_status' => 1)); 
		//getValues(TABLE_INVENTORY_PRODUCTS, 'product_id, product_name', array('product_status' => 1));
		$this->arrData['arrVendors'] = $this->model_inventory->getValues(TABLE_INVENTORY_VENDORS, 'vendor_id, vendor_name', array('vendor_status' => 1));
		$this->arrData['arrVendorPriorities'] = $this->config->item('vendor_priorities');
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'inventory_management/save_vendor_product', $this->arrData);
		$this->template->render();
	}
	
	public function list_vendors_products($pageNum = 1)
	{
		if((int)$pageNum < 1) $pageNu0m = 1;
		
		# CODE FOR DELETING RECORD
		
		if($this->input->post("record_id"))
		{
			if($this->arrData['canDelete'] == 1)
			{
				$tblName = TABLE_INVENTORY_VENDORS_PRODUCTS;
				
				$arrDeleteWhere = array(
					'vendor_product_id' => $this->input->post("record_id")
					);
					
				$arrDeleteValues = array(
					'vendor_product_status' => 2,
					'deleted_by' => (int)$this->userEmpNum,
					'deleted_date' => date($this->arrData["dateTimeFormat"])
					);
				
				if(!$this->model_inventory->deleteValue($tblName,$arrDeleteValues,$arrDeleteWhere)) {
					echo "0"; exit;
				} else {
					echo "1"; exit;
				}
			}
		}
		
		
		$arrWhere = array();
		
		if ($this->input->post()) {
			if($this->input->post("product")) {				
				$arrWhere['ivp.product_id'] = $this->input->post("product");
				$this->arrData['product'] = $this->input->post("product");
			}
			if($this->input->post("vendor")) {
				$arrWhere['ivp.vendor_id'] = $this->input->post("vendor");
				$this->arrData['vendor'] = $this->input->post("vendor");
			}
			if($this->input->post("status")) {
				$status = $this->input->post("status");
				if($status == -1) {
					$status = 0;
				}
				$arrWhere['ivp.vendor_product_status'] = $status;
				$this->arrData['status'] = $this->input->post("status");
			}
		}
		
		# CODE FOR APPLYING PAGING ON DATA AND DISPLAYING DATA
		$totalCount = $this->model_inventory->getTotalVendorsProducts($arrWhere);
		$totalCount = $totalCount[0]['total'];
		$this->arrData['totalRecordsCount'] = $totalCount;
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->model_inventory->getVendorsProducts($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($totalCount / $this->limitRecords);
		$this->arrData['pageLinks']	= displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListVendorsProducts');
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData['arrProducts']	= $this->model_inventory->getValues(TABLE_INVENTORY_PRODUCTS, 'product_id, product_name', array('product_status' => 1));
		$this->arrData['arrVendors'] 	= $this->model_inventory->getValues(TABLE_INVENTORY_VENDORS, 'vendor_id, vendor_name', array('vendor_status' => 1));
		$this->arrData['frmActionURL'] 	= base_url() . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'inventory_management/list_vendors_products', $this->arrData);
		$this->template->render();
	}
	
	public function save_purchases($purchaseID)
	{	
		$arrWhere = array();
		$this->arrData['record'] = array();
		
		$purchaseID = (int)$purchaseID;
		if($purchaseID) {				
			$arrWhere = array(
				'purchase_id' => $purchaseID
				);
		}
		
		#################################### FORM VALIDATION START ####################################		
		
		$this->form_validation->set_rules('product', 'Product', 'trim|required|xss_clean');
		$this->form_validation->set_rules('vendor', 'Vendor', 'trim|required|xss_clean');
		$this->form_validation->set_rules('productUnitPrice', 'Product Unit Price', 'trim|required|numeric|min_length[1]|xss_clean');
		$this->form_validation->set_rules('comments', 'Comments', 'trim|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true)
		{			
			if($this->input->post())
			{				
				$table = TABLE_INVENTORY_VENDORS_PRODUCTS;
				$column = 'vendor_product_id';
				$Where = array(
							'product_id' => $this->input->post("product"),
							'vendor_id' => $this->input->post("vendor")
							);
				$checkResult = $this->model_inventory->checkValues($table, $Where, $column, $vendorProductID);
				if($checkResult == 1)
				{		
					if($vendorProductID)
					{
						$status = $this->input->post("status");
						if($status == -1) {
							$status = 0;
						}
						
						$arrValues = array(
							'product_id' => $this->input->post("product"),
							'vendor_id' => $this->input->post("vendor"),
							'product_unit_price' => $this->input->post("productUnitPrice"),
							'comments' => $this->input->post("comments"),
							'vendor_product_status' => (int)$status,
							'modified_by' => (int)$this->userEmpNum,
							'modified_date' => date($this->arrData["dateTimeFormat"])
							);
					}
					else 
					{				
						$status = $this->input->post("status");
						if($status == -1) {
							$status = 0;
						}
						
						$arrValues = array(
							'product_id' => $this->input->post("product"),
							'vendor_id' => $this->input->post("vendor"),
							'product_unit_price' => $this->input->post("productUnitPrice"),
							'comments' => $this->input->post("comments"),
							'vendor_product_status' => (int)$status,
							'created_by' => (int)$this->userEmpNum,
							'created_date' => date($this->arrData["dateTimeFormat"])
							);
					}
				}
				else
				{
					$this->arrData['error_message'] = 'Vendor Product you entering already exists.';
				}
			}
			
			if(!$this->model_inventory->saveValues(TABLE_INVENTORY_VENDORS_PRODUCTS, $arrValues, $arrWhere)) {
				$this->arrData['error_message'] = 'Data not saved, try again';
			} else {
				$this->session->set_flashdata('success_message', 'Vendor Product saved successfully');
				redirect(base_url() . $this->currentController . '/list_vendor_products');
				exit;
			}
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}		
		
		# CODE FOR POPULATING CURRENT RECORD
		if($vendorProductID) {
			$this->arrData['record'] = $this->model_inventory->getValues(TABLE_INVENTORY_VENDORS_PRODUCTS, '*', $arrWhere);
			$this->arrData['record'] = $this->arrData['record'][0];
		}
		
		# CODE FOR POPULATING PAGE CONTENT
		$this->arrData['arrProducts'] = $this->model_inventory->getValues(TABLE_INVENTORY_PRODUCTS, 'product_id, product_name', array('product_status' => 1));
		$this->arrData['arrVendors'] = $this->model_inventory->getValues(TABLE_INVENTORY_VENDORS, 'vendor_id, vendor_name', array('vendor_status' => 1));
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'inventory_management/save_vendor_product', $this->arrData);
		$this->template->render();
	}
	
	public function strValidate($strValue, $strField) 
	{		
		if($strField == 'vendorContact1' || $strField == 'vendorContact2') { 
			
			if($strField == 'vendorContact1') {
				$strMsg = 'Vendor Contact Number 1 field must contain a valid number.';
			} else if($strField == 'vendorContact2') {
				$strMsg = 'Vendor Contact Number 2 field must contain a valid number.';
			}
			$this->form_validation->set_message('strValidate', $strMsg);
			return (preg_match("/^[0-9+-]*$/u", $strValue)) ? true : false;    
		} else if($strField == 'nic') {
			$this->form_validation->set_message('strValidate', 'Vendor NIC Number field must only contain numbers.');
			return (preg_match("/^[0-9]*$/u", $strValue)) ? true : false;
		}
		return false;
		
 	}
	
	public function validateFields($string,$value) 
	{
		if(preg_match("/((select|delete).+from|update.+set|(alter|truncate|drop).+table|<[a-z])/i", $string)==false)	//condition for sql injection
		{	
			if($value == 'vendor')
			{ 
				$this->form_validation->set_message('validateFields', 'Vendor field must only contain letters and numbers.');
				return (preg_match("/^[a-zA-Z0-9.-\/\s]*$/u", $string))?true:false;				
			}
			else if(($value == 'vendorPerson1') || ($value == 'vendorPerson2'))
			{ 
				$this->form_validation->set_message('validateFields', 'Vendor Person field must only contain letters and numbers.');
				return (preg_match("/^[a-zA-Z0-9.-\/\s]*$/u", $string))?true:false;				
			}
			else if($value == 'product')
			{ 
				$this->form_validation->set_message('validateFields', 'Product field must only contain letters and numbers.');
				return (preg_match("/^[a-zA-Z0-9-\/\s]*$/u", $string))?true:false;				
			}
		} else return false;
	}
    
}