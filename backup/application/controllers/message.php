<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Message extends CI_Controller {
	 
	public $arrData = array();
	public $currentController;
	public $imagePath;
	
	function __construct() {
		
		parent::__construct();
		
		$this->load->library('session');	
		$this->load->helper(array('url', 'common', 'html'));
		$this->arrData["baseURL"] = $this->config->item("base_url");
		$this->arrData["imagePath"] = $this->config->item("image_path");
		$this->imagePath = $this->config->item('image_path');
		$this->currentController = 'message';
		
		$this->template->set_template("message");
		$this->template->write_view('header', 'templates/header');
		$this->template->write_view('footer', 'templates/footer');
	}
	
	public function index()	{		
		// INDEX ACTION
	}
	
	public function access_denied()	{
		
		$this->arrData['error_message'] = 'ACCESS PROHIBITED - You are not allowed to enter on this page.';
		//	$this->arrData['error_message'][] = 'ACCESS PROHIBITED - You are not allowed to enter on this page.';
		
		# SET LOG
		# SET CONSTANT MESSAGE
		
		$this->template->write_view('content', 'messages/access_denied', $this->arrData);
		$this->template->render();

	}
	
	public function not_found()	{
		
		$this->arrData['error_message'] = 'ERROR 404 - Sorry, the requested page is not found.';
		//..	$this->arrData['error_message'][] = 'ERROR 404 - Sorry, the requested page is not found.';
		$this->template->write_view('content', 'messages/not_found', $this->arrData);
		$this->template->render();

	}
	
	public function down_for_maintenance()	{
		
		$this->arrData['error_message'] = '';
		//..	$this->arrData['error_message'][] = 'ERROR 404 - Sorry, the requested page is not found.';
		$this->template->write_view('content', 'messages/down_for_maintenance', $this->arrData);
		$this->template->render();

	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */