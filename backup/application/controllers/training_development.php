<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Training_Development extends Master_Controller {
		
	private $arrData = array();
	private $maxLinks;
	private $limitRecords;
	private $delimiter = '-';
	
	function __construct() {
		
		parent::__construct();
		
		$this->arrData["baseURL"]				= $this->baseURL;
		$this->arrData["imagePath"]				= $this->imagePath;
		$this->arrData["screensAllowed"]		= $this->screensAllowed;
		$this->arrData["currentController"] 	= $this->currentController;
		$this->arrData["dateTimeFormat"] 		= DATE_TIME_FORMAT;
		$this->arrData["dateFormat"] 			= DATE_FORMAT;
		$this->arrData["showDateFormat"] 		= SHOW_DATE_TIME_FORMAT;
		$this->maxLinks 						= MAX_PAGING_VISIBLE;
		$this->limitRecords 					= MAX_RECORDS_LISTING;
		$this->arrData["callsPath"]				= CALLS_PATH;
		$this->arrData["videosPath"]			= VIDEOS_PATH;
		$this->arrData['trainingDocs']			= TRAINING_DOCS;
		
		$currentActionArray = array();
		while(!empty($this->arrData["screensAllowed"]))
		{
			$temp = array_pop($this->arrData["screensAllowed"]);
			if($temp['module_name']== $this->currentAction){
				array_push($currentActionArray,$temp);
			}
		}
		$currentActionArray = $currentActionArray[0];
		$this->arrData['canWrite'] = $currentActionArray["can_write"];
		$this->arrData['canDelete'] = $currentActionArray["can_delete"];
		
		# SET LOG
		debugLog("Entered in " . current_url());
	}
	
	public function index($link="")
	{
		$moduleID = getValue($this->modulesAllowed, 'module_name', $this->currentController, 'module_id');
		$this->arrData['allowedSubModulesList'] = getValue($this->modulesAllowedForMenu, 'module_name', $this->currentController, 'sub_menu'); 
		
		$this->arrData['link'] = $link;
		$this->template->write_view('content', 'training_development/index', $this->arrData);
		$this->template->render();
	}
	
	function description ($descriptionID)
	{
		$this->template->write_view('content', 'training_development/description', $this->arrData);
		$this->template->render();
	}
	
	function visual_learning ()
	{
		$this->template->write_view('content', 'training_development/visual_learning', $this->arrData);
		$this->template->render();
	}
	
	function orientation($title)
	{
		$this->arrData['title'] = $title;
		$this->template->write_view('content', 'training_development/orientation', $this->arrData);
		$this->template->render();
	}
	
	function trainingcourse($courseID)
	{
		$this->arrData['courseID'] = $courseID;
		$this->template->write_view('content', 'training_development/trainingcourse', $this->arrData);
		$this->template->render();
	}
	
	function trainings($topicID)
	{
		$this->arrData['topicID'] = $topicID;
		$this->template->write_view('content', 'training_development/trainings', $this->arrData);
		$this->template->render();
	}
}

/* End of file user_management.php */
/* Location: ./application/controllers/user_management.php */