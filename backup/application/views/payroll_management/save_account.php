<?php
$empBank 				= (isset($_POST['empBank'])) 			? $_POST['empBank'] 			: $record['emp_salary_bank_id'];
$empBranch 				= (isset($_POST['empBranch'])) 			? $_POST['empBranch'] 			: $record['emp_salary_bank_branch'];
$empAccountNum 			= (isset($_POST['empAccountNum'])) 		? $_POST['empAccountNum'] 		: $record['emp_salary_bank_account_number'];
$empPayMode 			= (isset($_POST['empPayMode'])) 		? $_POST['empPayMode'] 			: $record['emp_pay_mode'];
$empPayrollExe 			= (isset($_POST['empPayrollExe'])) 		? $_POST['empPayrollExe'] 		: $record['emp_payroll_execution'];
?>
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
<tr>
<td>
<form name="frmAddAccount" id="frmAddAccount" method="post">
<div class="listPageMain">
	<div class="formMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
        <tr>
            <td class="formHeaderRow" colspan="2">Banking Details</td>
        </tr>
		<tr>
            <td class="formLabelContainer">Employee:</td>
            <td class="formTextBoxContainer"><?php echo $record['emp_full_name']; ?></td>
        </tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Pay Mode:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
                <select id="empPayMode" name="empPayMode" class="dropDown">
                  <option value="">Select Pay Mode</option>
                  <?php
                  if (count($arrPayModes)) {
                      for($ind = 0; $ind < count($arrPayModes); $ind++) {
                          $selected = '';
                          if($empPayMode == $arrPayModes[$ind]) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $arrPayModes[$ind]; ?>" <?php echo $selected; ?>><?php echo $arrPayModes[$ind]; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>
            </td>
        </tr>
		<tr>
            <td class="formLabelContainer">Bank Name:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
                <select id="empBank" name="empBank" class="dropDown">
                  <option value="">Select Bank</option>
                  <?php
                  if (count($arrBanks)) {
                      foreach($arrBanks as $arrBank) {
                          $selected = '';
                          if($empBank == $arrBank['bank_id']) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $arrBank['bank_id']; ?>" <?php echo $selected; ?>><?php echo $arrBank['bank_name']; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select>
            </td>
        </tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Branch Name:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empBranch" id="empBranch" class="textBox" value="<?php echo $empBranch; ?>">
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer">Account Number:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empAccountNum" id="empAccountNum" class="textBox" value="<?php echo $empAccountNum; ?>">
            </td>
      	</tr>
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Payroll Execution:</td>
            <td class="formTextBoxContainer">
        		<input type="checkbox" name="empPayrollExe" id="empPayrollExe" value="1" <?php if((int)$empPayrollExe) { ?>checked="checked"<?php } ?>>
            </td>
      	</tr>
		<tr>
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer">
      		<?php if($canWrite == YES) { ?>
      			<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save">&nbsp;
        		<input type="button" class="smallButton" value="Back" onclick="history.go(-1)">      
            <?php } ?>
    		</td>
        </tr>
  </table>
  </div>
  </div>
</form>
</td>
</tr>
</table>