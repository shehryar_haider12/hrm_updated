<?php 
if(trim($this->session->flashdata('success_message')) != '') {
	$success_message = trim($this->session->flashdata('success_message'));
}

if ($success_message != '' || $error_message != '' || $validation_error_message != '') { ?>
<div class="messageBox">
      <?php if($success_message != '') {?>
      <div class="successMessage"> <?php echo $success_message; ?> </div>
      <?php } ?>
      <?php if($error_message != '') {?>
      <div class="errorMessage"> <?php echo $error_message; ?> </div>
      <?php } ?>
      <?php if($validation_error_message != '') { ?>
      <div class="errorMessage"> <?php echo $validation_error_message; ?> </div>
      <?php } ?>
</div>
<?php } ?>
