<?php
if($leaveStatus == 0 && $leaveStatus != '') {
	$leaveStatus = -1;
}
$empSupervisor = 	isset($_POST['empSupervisor']) 	? 	$_POST['empSupervisor']		:	$empSupervisor;
?>
<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( "#dateFrom" ).datepicker( "setDate", "<?php echo $dateFrom; ?>" );
	$( "#dateTo" ).datepicker( "setDate", "<?php echo $dateTo; ?>" );
});
</script>
<form name="frmLeaveRequests" id="frmLeaveRequests" method="post" action="<?php echo $frmActionURL; ?>">
  <div class="searchBoxMain">
    <div class="searchHeader">Search Criteria</div>
    <div class="searchcontentmain">
      <div class="searchCol">
        <?php
		if(count($arrEmployees) > 0) {
		?>
        <div class="labelContainer">Employee:</div>
        <div class="textBoxContainer">
          <select name="empID" id="empID" class="dropDown">
              <option value="">All</option>
              <?php
                if (count($arrEmployees)) {
                    foreach($arrEmployees as $key => $arrEmp) {
                ?>
                    <optgroup label="<?php echo $key; ?>">
                        <?php for($i = 0; $i < count($arrEmp); $i++) { ?>					
                            <option value="<?php echo $arrEmp[$i]['emp_id']; ?>"><?php echo $arrEmp[$i]['emp_full_name']; ?></option>
                        <?php } ?>
                    </optgroup>
                <?php	}
                }
                ?>
          </select>
        </div>
		<?php
		}
		?>
        <div class="labelContainer">Date From:</div>
        <div class="textBoxContainer">
        	<input type="text" class="textBox datePicker" id="dateFrom" name="dateFrom" />
        </div>
        <div class="labelContainer">Date To:</div>
        <div class="textBoxContainer">
        	<input type="text" class="textBox datePicker" id="dateTo" name="dateTo" />
        </div>
      </div>
      <div class="searchCol">
        <?php //if(in_array($this->userRoleID, $forcedAccessRoles)) {
		if(isAdmin($this->userRoleID)) { ?>
        <div class="labelContainer">Reported To:</div>
        <div class="textBoxContainer">
          <select name="empSupervisor" id="empSupervisor" class="dropDown">
              <option value="">All</option>
              <?php
				if (count($arrSupervisors)) {
					foreach($arrSupervisors as $key => $arrSupervisor) {
				?>
					<optgroup label="<?php echo $key; ?>">
						<?php for($i=0;$i<count($arrSupervisor);$i++) { ?>					
							<option value="<?php echo $arrSupervisor[$i]['emp_id']; ?>"><?php echo $arrSupervisor[$i]['emp_full_name']; ?></option>
						<?php } ?>
					</optgroup>
				<?php	}
				}
				?>
          </select>
        </div>
        <?php } ?>
        <div class="labelContainer">Leave Category:</div>
        <div class="textBoxContainer">
      	<select name="leaveCategory" id="leaveCategory" class="dropDown">
            <option value="">All</option>
            <?php
			  if (count($arrCategories)) {
				  for($ind = 0; $ind < count($arrCategories); $ind++) {
			  ?>
				  <option value="<?php echo $arrCategories[$ind]['leave_category_id']; ?>"><?php echo $arrCategories[$ind]['leave_category']; ?></option>
			  <?php
				  }
			  }
			  ?>
        </select>
        </div>
        <div class="labelContainer">Leave Status:</div>
        <div class="textBoxContainer">
      	<select name="leaveStatus" id="leaveStatus" class="dropDown">
            <option value="">All</option>
            <option value="-1">Pending Approval</option>
            <option value="1">Approved</option>
            <option value="2">Rejected</option>
        </select>
        </div>
      </div>
      <div class="buttonContainer">
        <input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Search">
        <input class="searchButton" name="btnBack" id="btnBack" type="button" value="Back" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction; ?>';">
      </div>
    </div>
  </div>
  <script>
  	$('#empID').val('<?php echo $empID; ?>');
  	$('#empSupervisor').val('<?php echo $empSupervisor; ?>');
  	$('#leaveCategory').val('<?php echo $leaveCategory; ?>');
  	$('#leaveStatus').val('<?php echo $leaveStatus; ?>');
	
	function doConfirm(intType, intID) {
		var strType = '';
		if(intType == 1) {
			strType = 'Approve';
		} else if(intType == 2) {
			strType = 'Reject';
		}
		if(confirm(strType + ' Selected Record?')) {
			var strComments = encodeURIComponent($('#txtResponse' + intID).val());
			window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/1/'; ?>' + intType + '/' + intID + '/' + strComments;
		}
		return false;
	}
  </script>
</form>

<div class="centerElementsContainer">
	<div class="recordCountContainer">
		<?php echo "Total Records Count: " . $totalRecordsCount; ?>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<b>Legends:</b>
		&nbsp;&nbsp;&nbsp;<span style="background-color:#F9F084;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="mandatoryStar"> Pending</span>
    </div>
	
	<?php
	if($pageLinks) {
	?>
	<div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
	<?php 	}	?>
</div>

	<div class="listContentMain">
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
  <tr class="listHeader">
    <td class="listHeaderCol">Employee</td>
    <td class="listHeaderCol">Applied Date</td>
    <td class="listHeaderCol">From - To</td>
    <td class="listHeaderCol">Return Date</td>
    <td class="listHeaderCol">Total Days</td>
    <td class="listHeaderCol" style="max-width:200px;min-width:200px">Reason</td>
    <td class="listHeaderCol">Document</td>
    <!--<td class="listHeaderCol">Reported To</td>-->
    <td class="listHeaderCol">Processed By</td>
    <td class="listHeaderCol" style="max-width:200px;min-width:200px">Comments</td>
    <td class="listHeaderCol" align="center">Leave Form</td>
    <td class="listHeaderColLast" align="center">Action/Status</td>
  </tr>
  <?php
    for($ind = 0; $ind < count($arrRecords); $ind++) {
		
		if($arrRecords[$ind]['leave_status'] == 0) {
			$cssBtn = 'smallButtonUpdate';
			$trCSSClass = 'listContentAlternate" style="background-color:#F9F084"';
			$strStatus = 'Pending';
		} else if($arrRecords[$ind]['leave_status'] == 1) {
			$trCSSClass = 'listContentAlternate"';
			$strStatus = 'Approved';
		} else if($arrRecords[$ind]['leave_status'] == 2) {
			$trCSSClass = 'listContentAlternate"';
			$strStatus = 'Rejected';
		} else {
			$trCSSClass = 'listContentAlternate';
			$strStatus = '';
		}
	?>
  <tr class="<?php echo $trCSSClass; ?>" id="tr<?php echo $ind; ?>">
    <td class="listContentCol paddingTopBottom"><?php echo $arrRecords[$ind]['emp_full_name'] . '<br /><b>' . $arrTypes[$arrRecords[$ind]['leave_type']] . '<br />' . getValue($arrCategories, 'leave_category_id', $arrRecords[$ind]['leave_category'], 'leave_category') . '</b>'; ?></td>
    <td class="listContentCol"><?php echo readableDate($arrRecords[$ind]['created_date'], $showDateFormat); ?></td>
    <td class="listContentCol"><?php echo readableDate($arrRecords[$ind]['leave_from'], $showDateFormat) . ' -<br />' . readableDate($arrRecords[$ind]['leave_to'], $showDateFormat); ?></td>
    <td class="listContentCol"><?php echo readableDate($arrRecords[$ind]['leave_return'], $showDateFormat); ?></td>
    <td class="listContentCol"><?php echo (int)$arrRecords[$ind]['leave_days']; ?></td>
    <td class="listContentCol paddingTopBottom"><?php echo $arrRecords[$ind]['leave_reason']; ?></td>
    <td class="listContentCol"><?php echo ($arrRecords[$ind]['leave_doc'] != '') ? '<a href="' . $this->baseURL . '/' . $docFolderShow . $arrRecords[$ind]['leave_doc'] . '" style="color:#f00" target="_blank">View Document</a>' : ' - '; ?></td>
    <!--<td class="listContentCol"><?php //echo getEmployeeName($arrRecords[$ind]['emp_authority_id']); ?></td>-->
    <td class="listContentCol"><?php echo ($arrRecords[$ind]['processed_date'] != '') ? getEmployeeName($arrRecords[$ind]['processed_by']) . '<br />' . readableDate($arrRecords[$ind]['processed_date'], $showDateFormat) : ' - '; ?></td>
    <td class="listContentCol">
    	<?php if(!(int)$arrRecords[$ind]['leave_status']) { ?>
    	<textarea name="txtResponse<?php echo $arrRecords[$ind]['leave_id']; ?>" id="txtResponse<?php echo $arrRecords[$ind]['leave_id']; ?>" class="textArea" rows="3" style="width:185px"><?php echo $arrRecords[$ind]['leave_comments']; ?></textarea><br />
        Remaining &nbsp;&nbsp;&nbsp;
        Annual: <?php echo round($arrRecords[$ind]['emp_annual_leaves']); ?>&nbsp;&nbsp;&nbsp;
        Sick: <?php echo $arrRecords[$ind]['emp_sick_leaves']; ?>
        <?php } else {
			echo nl2br($arrRecords[$ind]['leave_comments']);
		}
		?>
    </td>
    <td class="listContentCol" align="center">
    <?php if((int)$arrRecords[$ind]['leave_status']) { ?>
      	<img title="Leave Form" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/display.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/leave_status/1/' . $arrRecords[$ind]['leave_id']; ?>';" />
    <?php } ?>
    </td>
    <td class="listContentColLast" align="center">
    <?php if($this->userRoleID == COMPANY_ADMIN_ROLE_ID) {
    		echo $strStatus;
    	  } else { ?>
    	<?php if($arrRecords[$ind]['leave_status'] == 0) { ?>
    	&nbsp;&nbsp;<img title="Approve" style="margin:-7px 0;cursor:pointer" width="25" src="<?php echo $this->imagePath . '/leave_approve.png';?>" onclick="doConfirm(1, <?php echo (int)$arrRecords[$ind]['leave_id']; ?>);">
    	&nbsp;&nbsp;<img title="Reject" style="margin:-7px 0;cursor:pointer" width="25" src="<?php echo $this->imagePath . '/delete.png';?>" onclick="doConfirm(2, <?php echo (int)$arrRecords[$ind]['leave_id']; ?>);">
        <?php } else {
					echo $strStatus;
					if($arrRecords[$ind]['leave_status'] == 1 && isAdmin($this->userRoleID)) {
		?>
        <br /><img title="Delete/Revert" style="margin:-7px 0;cursor:pointer" src="<?php echo $this->imagePath . '/remove.png';?>" onclick="deleteRecord('/<?php echo $this->currentController . '/' . $this->currentAction; ?>/', '<?php echo $arrRecords[$ind]['leave_id']; ?>');">
        <?php
					}
			  }
	}
		?>
    </td>
  </tr>
  <?php
	} 
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="12" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
</div>
<div style="clear:both">&nbsp;<div>
<div>
	<table cellpadding="0" cellspacing="0" border="0">
    	<tr>
        	<td width="35px"><img style="margin:5px 0;" width="30" src="<?php echo $this->imagePath . '/leave_approve.png';?>" /></td>
            <td width="110px">Approve</td>
            <td width="35px"><img style="margin:5px 0;" width="30" src="<?php echo $this->imagePath . '/delete.png';?>" /></td>
            <td width="165px">Reject</td>
        </tr>
    </table>
</div>