<?php
$currentSalary		= 	(isset($_POST['currentSalary'])) 		? 	$_POST['currentSalary'] 		: 	$record['current_salary'];
$expectedSalary 	= 	(isset($_POST['expectedSalary'])) 		? 	$_POST['expectedSalary'] 		: 	$record['expected_salary'];
$isWorkedBefore		=	(isset($_POST['isWorkedBefore'])) 		? 	$_POST['isWorkedBefore'] 		:	$record['is_worked_before'];
$previousPosition	=	(isset($_POST['previousPosition']))		?	$_POST['previousPosition']		:	$record['previous_position_id'];

$firstReferenceFullName 	= 	(isset($_POST['firstReferenceFullName'])) 	? 	$_POST['firstReferenceFullName'] 	: 	$record['first_reference_name'];
$firstReferenceEmail 		= 	(isset($_POST['firstReferenceEmail'])) 		? 	$_POST['firstReferenceEmail'] 		: 	$record['first_reference_email'];
$firstReferenceContactNo 	= 	(isset($_POST['firstReferenceContactNo'])) 	? 	$_POST['firstReferenceContactNo'] 	: 	$record['first_reference_contact_number'];
$firstReferenceAddress 		= 	(isset($_POST['firstReferenceAddress'])) 	? 	$_POST['firstReferenceAddress']	 	: 	$record['first_reference_address'];
$secondReferenceFullName 	= 	(isset($_POST['secondReferenceFullName'])) 	? 	$_POST['secondReferenceFullName'] 	: 	$record['second_reference_name'];
$secondReferenceEmail 		= 	(isset($_POST['secondReferenceEmail'])) 		? 	$_POST['secondReferenceEmail'] 		: 	$record['second_reference_email'];
$secondReferenceContactNo 	= 	(isset($_POST['secondReferenceContactNo'])) 	? 	$_POST['secondReferenceContactNo'] 	: 	$record['second_reference_contact_number'];
$secondReferenceAddress 		= 	(isset($_POST['secondReferenceAddress'])) 	? 	$_POST['secondReferenceAddress']	 	: 	$record['second_reference_address'];
?>
<script>
$(document).ready(function() {
	if($("#isWorkedBefore").val() == 1)
	chkVal(1);
});



function chkVal(strVal) {
	if(strVal == 1) {
		$('#divPosition').show();
	} else {
		$('#divPosition').hide();
	}
}
</script>

<div class="listPageMain">
	<form name="frmAddCandidate" id="frmAddCandidate" enctype="multipart/form-data" method="post">
		<div class="formMain">
			<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
				<tr>
					<td class="formHeaderRow" colspan="2">Application Form</td>
				</tr>
				<tr>
					<td class="formLabelContainer">Current Salary:</td>
					<td class="formTextBoxContainer">
						<input type="text" name="currentSalary" maxlength="7" id="currentSalary" class="textBox" value="<?php echo $currentSalary; ?>">
					</td>
				</tr>
				<tr class="formAlternateRow">
					<td class="formLabelContainer">Expected Salary:<span class="mandatoryStar"> *</span></td>
					<td class="formTextBoxContainer">
						<input type="text" name="expectedSalary" maxlength="7" id="expectedSalary" class="textBox" value="<?php echo $expectedSalary; ?>">
					</td>
				</tr>
				<tr>
					<td class="formLabelContainer">Have you worked with us before ?<span class="mandatoryStar"> *</span></td>
					<td class="formTextBoxContainer" colspan="2">
						<select id="isWorkedBefore" name="isWorkedBefore" class="dropDown" onchange="chkVal(this.value)">
							<option value="">Select Value</option>
							<option value="1" <?php if ($isWorkedBefore == 1) {echo 'selected="selected"';}?>>Yes</option>
							<option value="0" <?php if ($isWorkedBefore == 0) {echo 'selected="selected"';}?>>No</option>
						</select>
						<div style="clear:both"></div>
						<div id="divPosition" style="display:none; margin-top:5px">
							<select id="previousPosition" name="previousPosition" class="dropDown">
							  <option value="">Select Position</option>
							  <?php
							  if (count($jobPositions)) {
								  foreach($jobPositions as $jobPositionVal) {
									  $selected = '';
									  if($previousPosition == $jobPositionVal['job_title_id']) {
										  $selected = 'selected="selected"';
									  }
							  ?>
								  <option value="<?php echo $jobPositionVal['job_title_id']; ?>" <?php echo $selected; ?>><?php echo $jobPositionVal['job_title_name']; ?></option>
							  <?php
								  }
							  }
							  ?>
							</select> 
						</div>
					</td>
				</tr>
				<tr>
					<td class="formHeaderRow" colspan="2">References</td>
				</tr>
				<tr>
					<td colspan="4">
					<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
						<tr class="formAlternateRow">
							<td class="formLabelContainer center">Reference Person Name:<span class="mandatoryStar"> *</span></td>
							<td class="formLabelContainer center">Reference Person Email:<span class="mandatoryStar"> *</span></td>
							<td class="formLabelContainer center">Reference Person Contact:<span class="mandatoryStar"> *</span></td>
							<td class="formLabelContainer center">Reference Person Address:<span class="mandatoryStar"> *</span></td>
						</tr>
						<tr>
							
							<td class="formTextBoxContainer">
								<input type="text" name="firstReferenceFullName" maxlength="50" id="firstReferenceFullName" class="textBox" value="<?php echo $firstReferenceFullName; ?>">
							</td>
							
							<td class="formTextBoxContainer">
								<input type="text" name="firstReferenceEmail" maxlength="100" id="firstReferenceEmail" class="textBox" value="<?php echo $firstReferenceEmail; ?>">
							</td>
							
							<td class="formTextBoxContainer">
								<input type="text" name="firstReferenceContactNo" maxlength="14" id="firstReferenceContactNo" class="textBox" value="<?php echo $firstReferenceContactNo; ?>">
							</td>
							
							<td class="formTextBoxContainer">
								<textarea rows="5" cols="30" name="firstReferenceAddress" id="firstReferenceAddress" class="textAreaSub"><?php echo $firstReferenceAddress; ?></textarea>
							</td>
						</tr>
						<tr>
							<td class="formTextBoxContainer">
								<input type="text" name="secondReferenceFullName" maxlength="50" id="secondReferenceFullName" class="textBox" value="<?php echo $secondReferenceFullName; ?>">
							</td>
							
							<td class="formTextBoxContainer">
								<input type="text" name="secondReferenceEmail" maxlength="100" id="secondReferenceEmail" class="textBox" value="<?php echo $secondReferenceEmail; ?>">
							</td>
							
							<td class="formTextBoxContainer">
								<input type="text" name="secondReferenceContactNo" maxlength="14" id="secondReferenceContactNo" class="textBox" value="<?php echo $secondReferenceContactNo; ?>">
							</td>
							
							<td class="formTextBoxContainer">
								<textarea rows="5" cols="30" name="secondReferenceAddress" id="secondReferenceAddress" class="textAreaSub"><?php echo $secondReferenceAddress; ?></textarea>
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr class="formAlternateRow">
					<td class="formLabelContainer"></td>
					<td class="formTextBoxContainer">
						<input type="submit" class="smallButton" name="btnSave" value="Save">&nbsp;
						<input type="button" class="smallButton" value="Cancel" onclick="history.go(-1)">      
					</td>
				</tr>
			</table>
		</div>
	</form>
</div>