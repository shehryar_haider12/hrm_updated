<?php
$taskReportTo		=	(isset($_POST['taskReportTo']))	?	$_POST['taskReportTo']	:	$record['complain_report_to_id'];
$task				=	(isset($_POST['task'])) 		? 	$_POST['task'] 			:	$record['complain_text'];
$taskPriority		=	(isset($_POST['taskPriority'])) ? 	$_POST['taskPriority'] 	:	$record['complain_priority'];
?>

<?php if($canWrite == YES) { ?>
<form name="frmTasks" id="frmTasks" method="post" enctype="multipart/form-data">
  <div class="listPageMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
	<tr>
		<td class="formHeaderRow" colspan="2">Submit Task</td>
	</tr>		
	<tr class="formAlternateRow">
	  <td class="formLabelContainer">Report Task To:<span class="mandatoryStar"> *</span></td>
	  <td class="formTextBoxContainer">
		<select id="taskReportTo" name="taskReportTo" class="dropDown">
			<option value="">Select Report To Person</option>
			<?php
			if (count($arrReportToEmployees)) {
				foreach($arrReportToEmployees as $key => $arrReportToEmployee) {
			?>
				<optgroup label="<?php echo $key; ?>">
					<?php for($i=0;$i<count($arrReportToEmployee);$i++) { ?>
						<?php if($arrReportToEmployee[$i]['emp_id'] != $this->userEmpNum) { ?>				
							<option value="<?php echo $arrReportToEmployee[$i]['emp_id']; ?>"><?php echo $arrReportToEmployee[$i]['emp_full_name']; ?></option>
						<?php } ?>
					<?php } ?>
				</optgroup>
			<?php	}
			}
			?>
		</select>
	  </td>
	</tr>
	<tr>
		<td class="formLabelContainer">Description:<span class="mandatoryStar"> *</span></td>
		<td class="formTextBoxContainer">
			<textarea rows="5" cols="30" name="task" id="task" class="textArea"><?php echo $task; ?></textarea>
		</td>
	</tr>
	<tr class="formAlternateRow">
	  <td class="formLabelContainer">Priority:<span class="mandatoryStar"> *</span></td>
	  <td class="formTextBoxContainer">
		<select id="taskPriority" name="taskPriority" class="dropDown">
			<option value="">Select Priority</option>
			<?php if(count($taskPriorities)) {
            	foreach($taskPriorities as $key => $value) {
            ?>
            	<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
            <?php
            	}
            }
            ?>
		</select>
	  </td>
	</tr>
	<tr>
		<td class="formLabelContainer">Supportive Document (if any):</td>
		<td class="formTextBoxContainer">
			<input type="file" name="supportingDoc[]" id="supportingDoc" multiple="multiple" />
			<br /><br />
			[Add multiple files by using Ctrl function of keyboard]
		</td>
	</tr>
	<tr class="formAlternateRow">
        <td class="formLabelContainer"></td>
        <td class="formTextBoxContainer">
        	<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Submit">
        </td>
	</tr>
  </table>
  </div>
</form>
<?php } ?>

<script>
	$('#taskReportTo').val('<?php echo $taskReportTo; ?>');
	$('#taskPriority').val('<?php echo $taskPriority; ?>');
</script>

<?php if($canWrite == NO) { ?>
<script>$("#frmTasks :input").attr("disabled", true);</script>
<?php } ?>