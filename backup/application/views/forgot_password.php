<?php
$userName 			= 	$_POST['userName'];
$dateOfBirth 		= 	$_POST['userDOB'];
?>
<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo DATE_FORMAT; ?>" );
	$( ".datePicker" ).datepicker( "setDate", "<?php echo $dateOfBirth; ?>" );
	$( ".datePicker" ).datepicker( "option", "maxDate", '<?php echo date('Y-m-d', strtotime('18 years ago')); ?>' );
});
</script>
<form name="frmForgotPassword" id="frmForgotPassword" method="post">
	<div class="loginBoxMain" style="height:165px">	
        <div class="loginContent" style="margin:0">
        	<table border="0" cellspacing="0" cellpadding="0" style="width:100%">
                <tr>
                    <td class="formHeaderRow" colspan="2">Forgot Password</td>
                </tr>
                <tr>
                    <td class="formLabelContainer">User Name</td>
                    <td class="formTextBoxContainer">
                        <input type="text" name="userName" maxlength="30" id="userName" class="textBox" value="<?php echo $userName; ?>">
                    </td>
                </tr>
                <tr>
                    <td class="formLabelContainer">Date Of Birth</td>
                    <td class="formTextBoxContainer">
                        <input type="text" name="userDOB" maxlength="30" id="userDOB" class="textBox datePicker">
                    </td>
                </tr>
                <tr>
                    <td class="formLabelContainer"></td>
                    <td class="formTextBoxContainer">
                        <input type="submit" class="smallButton" name="btnSave" value="Submit">&nbsp;
                        <input type="button" class="smallButton" value="Back" onclick="history.go(-1)">      
                    </td>
                </tr>
          </table>
        </div>
    </div>
<?php echo form_close(); ?>