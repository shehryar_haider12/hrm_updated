<?php
$userRole	=	(isset($_POST['userRole']))	?	$_POST['userRole']	:	$roleID;
$module		=	(isset($_POST['module']))	?	$_POST['module'] 	:	$moduleID;
?>

<form name="frmAddUserPermission" id="frmAddUserPermission" method="post">
<div class="listPageMain">
	<div class="formMain">
		<table border="0" cellspacing="0" cellpadding="0" style="width:100%">
			<tr>
            	<?php if($roleID) { ?>
					<td class="formHeaderRow" colspan="2">Update Privileges of User Role</td>
                <?php } else { ?>
	                <td class="formHeaderRow" colspan="2">Add Privileges to User Role</td>
                <?php } ?>
			</tr>
			<tr>
				<td class="formLabelContainer">User Role:<span class="mandatoryStar"> *</span></td>
				<td class="formTextBoxContainer">
					<select name="userRole" id="userRole" class="dropDown" onChange="populateModuleScreens(this.value,$('#module').val());">
						<option value="">Select User Role</option>
						<?php
						if (count($userRoles)) {
							foreach($userRoles as $arrUserRole) {
						?>
							<option value="<?php echo $arrUserRole['user_role_id']; ?>" ><?php echo $arrUserRole['user_role_name']; ?></option>
						<?php
							}
						}
						?>
					</select>
					<?php if($editFlag == 1) { ?>
						<label id="user_role_name" name="user_role_name"></label>
					<?php }?>
				</td>
			</tr>
			<tr>
				<td class="formLabelContainer">Module:<span class="mandatoryStar"> *</span></td>
				<td class="formTextBoxContainer">
					<select name="module" id="module" class="dropDown" onChange="populateModuleScreens($('#userRole').val(),this.value);">
						<option value="">Select Main Module</option>
					<?php
					if (count($modules)) {
						foreach($modules as $arrModule) {
					?>
						<option value="<?php echo $arrModule['module_id']; ?>"><?php echo $arrModule['display_name']; ?></option>
					<?php
						}
					}
					?>
				</select>
				<?php if($editFlag == 1) { ?>
					<label id="module_name" name="module_name"></label>
				<?php }?>
				</td>
			</tr>
		</table>
	</div>
	
	<div class="formMainWide" id="screens" style="display:none">
		<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
		</table>
	</div>
	
	<table border="0" cellspacing="0" cellpadding="0" style="width:100%;display:none;" id="savePermission">
		<tr>
			<td class="formLabelContainer"></td>
			<td class="formButtonContainerWide" align="left">
				<input class="smallButton" name="addUserPermission" type="submit" value="Save">
				<?php if(strpos($_SERVER["REQUEST_URI"],$roleID.'/'.$moduleID.'/'.$subModuleID) != false) { ?>
					<input class="smallButton" type="button" value="Cancel" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/list_user_permission' ?>';">
				<?php } ?>
			</td>
		</tr>
	</table>
</div>
</form>

<script>
	$('#user_role_name').text($('#userRole option[value="<?php echo $userRole; ?>"]').text());
	$('#module_name').text($('#module option[value="<?php echo $module; ?>"]').text());

	<?php if($editFlag == 1) { ?> $('#userRole').hide(); $('#module').hide(); <?php } ?>

	$('#userRole').val('<?php echo $userRole; ?>');
	$('#module').val('<?php echo $module; ?>');

	<?php if($roleID && $moduleID) { ?>
		populateModuleParticularScreen('<?php echo $roleID; ?>', '<?php echo $moduleID; ?>', '<?php echo $subModuleID; ?>');
		$('#savePermission').show();
	<?php } ?>

	<?php if($canWrite == 0) { ?>
		$("#frmAddUserPermission :input").attr("disabled", true);
	<?php } ?>
</script>