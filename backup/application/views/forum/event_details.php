  <div class="listPageMain">
    <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
      <tr>
        <td class="formHeaderRow" colspan="2"><?php echo $arrRecord['event_title']; ?></td>
      </tr>
      <tr class="formAlternateRow">
        <td class="formLabelContainer">By:</td>
        <td class="formTextBoxContainer"><?php echo $arrRecord['company_name']; ?></td>
      </tr>
      <tr>
        <td class="formLabelContainer">Detail:</td>
        <td class="formTextBoxContainer"><?php echo ($arrRecord['event_description'] != '') ? nl2br($arrRecord['event_description']) : '-'; ?></textarea></td>
      </tr>
      <tr class="formAlternateRow">
        <td class="formLabelContainer">Date:</td>
        <td class="formTextBoxContainer"><?php echo date(SHOW_DATE_TIME_FORMAT, strtotime($arrRecord['event_date'])); ?></td>
      </tr>
      <tr>
        <td colspan="2" height="25px">&nbsp;</td>
      </tr>
    </table>
  </div>
  
<div class="centerElementsContainer">
	<div class="recordCountContainer">
		<?php echo "Total Images: " . count($arrRecordImages); ?>
    </div>
</div>

<table border="0" cellspacing="0" cellpadding="0" class="listTableMain dottedBorder">
  <tr class="listContent">
  <?php
    for($ind = 0; $ind < count($arrRecordImages); $ind++) {
		if($ind % 4 == 0) {
			echo '</tr>
			<tr class="listContent">';
		}
	?>
    <td class="listContentCol" width="25%" style="padding: 20px" align="center"><a class="grouped_elements" rel="group1" href="<?php echo $this->imagePath . '/events/' . $arrRecordImages[$ind]['image_name']; ?>"><img src="<?php echo $this->imagePath . '/events/' . $arrRecordImages[$ind]['image_name']; ?>" height="180"></a></td>
    <?php } ?>
  </tr>
  <?php
	if(!$ind) {
	?>
  <tr class="listContentAlternate">
    <td colspan="4" align="center" class="listContentCol">No Record Found</td>
  </tr>
  <?php
	}
	?>
</table>
<script type="text/javascript">
	$("a.grouped_elements").fancybox();
</script>