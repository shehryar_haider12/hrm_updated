<?php
$txtSortField 	= (isset($_POST['sort_field'])) 	? $_POST['sort_field'] 		: '';
$txtSortOrder 	= (isset($_POST['sort_order'])) 	? $_POST['sort_order'] 		: '';
?>
<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( "#dateFrom" ).datepicker( "setDate", "<?php echo $dateFrom; ?>" );
	$( "#dateTo" ).datepicker( "setDate", "<?php echo $dateTo; ?>" );
	
});
</script>
<form name="frmListEvents" id="frmListEvents" method="post" action="<?php echo $frmActionURL; ?>">
  <div class="searchBoxMain">
    <div class="searchHeader">Search Criteria</div>
    <div class="searchcontentmain">
      <div class="searchCol">
        <div class="labelContainer">Company:</div>
        <div class="textBoxContainer">
			<select id="companyID" name="companyID" class="dropDown">
                <option value="">All</option>
                <?php
                  for($ind = 0; $ind < count($arrCompanies); $ind++) {
                      $selected = '';
                      if($arrCompanies[$ind]['company_id'] == $companyID) {
                          $selected = 'selected="selected"';
                      }
                ?>
                 <option value="<?php echo $arrCompanies[$ind]['company_id']; ?>" <?php echo $selected; ?>><?php echo $arrCompanies[$ind]['company_name']; ?></option>
                <?php
                  }
                ?>
              </select>
        </div>
      </div>
      <div class="searchCol">
		<div class="labelContainer">Date From:</div>
        <div class="textBoxContainer">
          <input type="text" id="dateFrom" name="dateFrom" class="textBox datePicker" />
        </div>
      </div>        
      <div class="searchCol">
        <div class="labelContainer">Date To:</div>
        <div class="textBoxContainer">
          <input type="text" id="dateTo" name="dateTo" class="textBox datePicker" />
        </div>
      </div>
      <div class="buttonContainer">
        <input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Search">
      </div>
    </div>
  </div> 
  <script>
  	$('#companyID').val('<?php echo $companyID; ?>');
  </script>
</form>

<?php if($canWrite == YES) { ?>
<div class="centerButtonContainer">
	<input class="addButton" type="button" value="Add New Event" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/save_event' ?>';" />
</div>
<?php } ?>

<div class="centerElementsContainer">
	<div class="recordCountContainer">
		<?php echo "Total Records Count: " . $totalRecordsCount; ?>
    </div>
	
	<?php if($pageLinks) { ?>
	<div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
	<?php } ?>
</div>

	<div class="listContentMain">
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
  <tr class="listHeader">
    <td class="listHeaderCol">Event</td>
    <td class="listHeaderCol">Company</td>
    <td class="listHeaderCol">Date</td>
    <td class="listHeaderColLast">Action</td>
  </tr>
  <?php
    for($ind = 0; $ind < count($arrRecords); $ind++) {
		
		$cssWrkBtn = 'normal';
		$cssEduBtn = 'normal';
		
		if($ind % 2 == 0) {
			$trCSSClass = 'listContent';
		} else {
			$trCSSClass = 'listContentAlternate';
		}
	?>
  <tr class="<?php echo $trCSSClass; ?>">
    <td class="listContentCol"><?php echo $arrRecords[$ind]['event_title']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['company_name']; ?></td>
    <td class="listContentCol"><?php echo date(SHOW_DATE_TIME_FORMAT, strtotime($arrRecords[$ind]['event_date'])); ?></td>
    <td class="listContentColLast">
      <div class="empColButtonContainer">
      	<img title="View Event" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/display.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/event_details/' . $arrRecords[$ind]['event_id']; ?>';">
        <?php if($canWrite == YES) { ?>
      	<img title="View/Edit" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/view.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/save_event/' . $arrRecords[$ind]['event_id']; ?>';">
      	<?php } if($canDelete == YES) { ?>
      	<img title="Delete" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/delete.png';?>" onclick="deleteRecord('/<?php echo $this->currentController . '/' . $this->currentAction; ?>/', '<?php echo $arrRecords[$ind]['event_id']; ?>');">
      	<?php } ?>
	  </div>
	  </td>
  </tr>
  <?php
	} 
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="4" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
</div>