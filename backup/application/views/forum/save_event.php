<?php
$eventTitle		= (isset($_POST['eventTitle'])) 		? $_POST['eventTitle'] 			: $arrRecord['event_title'];
$eventDesc 		= (isset($_POST['eventDesc'])) 			? $_POST['eventDesc'] 			: $arrRecord['event_description'];
$eventDate	 	= (isset($_POST['eventDate'])) 			? $_POST['eventDate'] 			: $arrRecord['event_date'];
$companyID 		= (isset($_POST['companyID'])) 			? $_POST['companyID'] 			: $arrRecord['company_id'];
?>
<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( ".datePicker" ).datepicker( "option", "maxDate", 0 );
	$( "#eventDate" ).datepicker( "setDate", "<?php echo $eventDate; ?>" );
});
</script>
<?php if($canWrite == YES) { ?>

<form name="frmEvent" id="frmEvent" method="post" enctype="multipart/form-data">
  <div class="listPageMain">
    <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
      <tr>
        <td class="formHeaderRow" colspan="2">Add/Edit Event</td>
      </tr>
      <tr class="formAlternateRow">
        <td class="formLabelContainer">Event Title:<span class="mandatoryStar"> *</span></td>
        <td class="formTextBoxContainer"><input type="text" name="eventTitle" id="eventTitle" class="textBox" value="<?php echo $eventTitle; ?>"></td>
      </tr>
      <tr>
        <td class="formLabelContainer">Event Detail:</td>
        <td class="formTextBoxContainer"><textarea id="eventDesc" name="eventDesc" class="textArea" rows="6"><?php echo $eventDesc; ?></textarea></td>
      </tr>
      <tr class="formAlternateRow">
        <td class="formLabelContainer">Event Date:<span class="mandatoryStar"> *</span></td>
        <td class="formTextBoxContainer"><input type="text" name="eventDate" id="eventDate" class="textBox datePicker"></td>
      </tr>
      <tr>
        <td class="formLabelContainer">Company:<span class="mandatoryStar"> *</span></td>
        <td class="formTextBoxContainer"><select id="companyID" name="companyID" class="dropDown">
            <option value="">Select Company</option>
            <?php
              for($ind = 0; $ind < count($arrCompanies); $ind++) {
                  $selected = '';
                  if($arrCompanies[$ind]['company_id'] == $empCompany) {
                      $selected = 'selected="selected"';
                  }
            ?>
            <option value="<?php echo $arrCompanies[$ind]['company_id']; ?>" <?php echo $selected; ?>><?php echo $arrCompanies[$ind]['company_name']; ?></option>
            <?php
              }
            ?>
          </select></td>
      </tr>
      <tr class="formAlternateRow">
        <td class="formLabelContainer">Event Images:<span class="mandatoryStar"> *</span></td>
        <td class="formTextBoxContainer"><input type="file" name="eventImages[]" id="eventImages" multiple="multiple">
          <br />
          [Add multiple files by using Ctrl function of keyboard] </td>
      </tr>
      <tr>
        <td class="formLabelContainer"></td>
        <td class="formTextBoxContainer"><input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save"></td>
      </tr>
      <tr>
        <td colspan="2" height="25px">&nbsp;</td>
      </tr>
    </table>
  </div>
</form>
<script type="text/javascript">
	$('#companyID').val('<?php echo $companyID; ?>');
</script>
<?php } ?>

<div class="centerElementsContainer">
	<div class="recordCountContainer">
		<?php echo "Total Images: " . count($arrRecordImages); ?>
    </div>
</div>

<table border="0" cellspacing="0" cellpadding="0" class="listTableMain dottedBorder">
  <tr class="listHeader">
    <td class="listHeaderCol">Image</td>
    <td class="listHeaderCol">Added By</td>
    <td class="listHeaderCol">When</td>
      <?php if($canDelete == YES) { ?>
    <td class="listHeaderColLast">Action</td>
      <?php } ?>
  </tr>
  <?php
    for($ind = 0; $ind < count($arrRecordImages); $ind++) {
	?>
  <tr class="listContent">
    <td class="listContentCol"><img src="<?php echo $this->imagePath . '/events/' . $arrRecordImages[$ind]['image_name']; ?>" width="45"></td>
    <td class="listContentCol"><?php echo getEmployeeName($arrRecordImages[$ind]['created_by']); ?></td>
    <td class="listContentCol"><?php echo date('F j, Y, g:i A', strtotime($arrRecordImages[$ind]['created_date'])); ?></td>
    <?php if($canWrite == YES) { ?>
    <td class="listContentColLast"><div class="empColButtonContainer">
        <?php if($canDelete == YES) { ?>
        <img title="Delete" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/delete.png';?>" onclick="deleteRecord('/<?php echo $this->currentController . '/' . $this->currentAction; ?>/', '<?php echo $arrRecordImages[$ind]['image_id']; ?>');">
        <?php } ?>
      </div></td>
    <?php } ?>
  </tr>
  <?php
	}
	if(!$ind) {
	?>
  <tr class="listContentAlternate">
    <td colspan="6" align="center" class="listContentCol">No Record Found</td>
  </tr>
  <?php
	}
	?>
</table>