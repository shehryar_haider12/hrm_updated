<?php
$empCode 	= (isset($_POST['empCode'])) 	?	$_POST['empCode'] 	: '';
?>

<div class="topDescriptionContainer" style="min-height:100%">
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain dottedBorder">
    <?php if($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID) 
		{ $colspanValue = "4"; } else { $colspanValue = "3"; }
	?>
    <tr>
        <td class="formHeaderRow"><?php echo $arrRecord['poll_topic']; ?></td>
    </tr>
    <tr>
    	<td class="listContentCol articleText" style="padding:10px 5px">
        	<span class="bold">Created By:</span>
            <span class="paddingLeftFive"><?php echo $arrRecord['emp_full_name']; ?></span>
        </td>
   	</tr>
    <tr>
        <td class="listContentCol articleText" style="padding:10px 5px">
        	<b>Start Date:</b>
            <span class="paddingLeftFive"><?php echo readableDate($arrRecord['poll_start_date'], 'M j, Y'); ?></span>
        </td>
    </tr>
    <tr>
        <td class="listContentCol articleText" style="padding:10px 5px">
        	<span class="bold">End Date:</span>
            <span class="paddingLeftFive"><?php echo readableDate($arrRecord['poll_end_date'], 'M j, Y'); ?></span>
        </td>
    </tr>
</table>
</div>

<p>&nbsp;</p>
<form name="frmListContestEmployees" id="frmListContestEmployees" method="post" action="<?php echo $frmActionURL; ?>">
	<div class="searchBoxMain" style="width:100%">
		<div class="searchHeader" style="width:943px;">Search Criteria</div>
		<div class="searchcontentmain" style="width:100%;padding:0">            
			<div class="searchCol" style="padding-top:10px">
                <div class="labelContainer">Status:</div>
                <div class="textBoxContainer">
                   <select name="empCode" id="empCode" class="dropDown">
                      <option value="">Select Employee</option>
                      <?php for($ind = 0; $ind < count($arrEmployees); $ind++) { ?>
                      <option value="<?php echo $arrEmployees[$ind]['emp_id']; ?>"><?php echo $arrEmployees[$ind]['emp_code'] . ' - ' . $arrEmployees[$ind]['emp_full_name']; ?></option>
                      <?php } ?>
                  </select>
                </div>
			</div>
        
            <div class="formButtonContainerWide">
                <input type="submit" class="searchButton" name="btnSearchTasks" id="btnSearchTasks" value="Search">
            </div>
		</div>
		<script>
			$('#empCode').val('<?php echo $empCode; ?>');		
        </script>        
	</div>
</form>

<table border="0" cellspacing="0" cellpadding="0" class="listTableMain dottedBorder">
	<tr>
        <td class="formHeaderRow" colspan="<?php echo $colspanValue; ?>">Poll Result</td>
    </tr>
    <tr class="listHeader" align="center">
    	<td class="listHeaderCol" width="400px">Question</td>
        <td class="listHeaderCol" width="400px">Choices</td>
        <?php if(!$empCode) { ?>
            <td class="listHeaderCol" width="80px">Votes</td>
            <?php if($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID) { ?>
            <td class="listHeaderColLast" width="80px">Details</td>
            <?php } ?>
        <?php } ?>
    </tr>
    <?php for($i=0; $i < count($pollQuestionAnswers); $i++) { 
		$rowClass = ($i % 2 == 0) ? "" : "formAlternateRow" ;
	?>
    <tr class="<?php echo $rowClass; ?>">
    	<td class="listContentCol subHeadingLabel" style="padding:10px"><?php echo $pollQuestionAnswers[$i]['poll_question']; ?></td>
        <td class="listContentCol subHeadingLabel" style="padding:10px">
        	<table  border="0" cellspacing="0" cellpadding="0">
			<?php for($j=0; $j < count($pollQuestionAnswers[$i]['choices']); $j++) 
			{ 
				if($pollQuestionAnswers[$i]['poll_answer_type'] == 1) { $imageType = "radio-button.png"; }
				else if($pollQuestionAnswers[$i]['poll_answer_type'] == 2) { $imageType = "check-button.png"; }
			?>
                <tr>
                	<td><img src="<?php echo $imagePath.'/'.$imageType; ?>" /></td>
                    <td valign="top" class="paddingLeftFive paddingTopFive"><?php echo $pollQuestionAnswers[$i]['choices'][$j]['poll_answer']; ?></td>
                </tr>
            <?php } ?>
            </table>
        </td>
        <?php if(!$empCode) { ?>
            <td class="listContentCol subHeadingLabel" style="padding:10px" align="center">
                <table  border="0" cellspacing="0" cellpadding="0">
                <?php 
                $sumPollCount = 0;
                for($j=0; $j < count($pollQuestionAnswers[$i]['choices']); $j++) 
                {
					$sumPollCount = $sumPollCount + $pollQuestionAnswers[$i]['choices'][$j]['poll_count'];
                ?>
                    <tr>
                        <td valign="top" class="paddingLeftFive paddingTopFive"><?php echo $pollQuestionAnswers[$i]['choices'][$j]['poll_count']; ?></td>
                    </tr>
                <?php } ?>
                </table>
            </td>
            
			<?php if($this->userRoleID == WEB_ADMIN_ROLE_ID || $this->userEmpNum == GM_EMP_ID) { ?>
            <td class="listContentColLast subHeadingLabel" align="center">
                <table  border="0" cellspacing="0" cellpadding="0">
                <?php
                    for($j=0; $j < count($pollQuestionAnswers[$i]['choices']); $j++) 
                    {
                ?>
                    <tr>
                        <td valign="top" align="center" class="paddingTopFive">
                            <?php if($pollQuestionAnswers[$i]['choices'][$j]['poll_count'] > 0) { ?>
                                <span class="anchor normalLink" onclick="showPopup('<?php echo $this->baseURL . '/' . $this->currentController . '/voters_summary/' . $pollID . '/' . $pollQuestionAnswers[$i]['poll_question_id'] . '/' . $pollQuestionAnswers[$i]['choices'][$j]['poll_answer_id']; ?>', 650, 650)">View Voter(s)</span>
                            <?php } else { ?>
                                <span>-</span>
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
                </table>
            </td>
            <?php } ?>
        <?php } ?>
    </tr>
    <?php if(!$empCode) { ?>
    <tr>
    	<td colspan="2" class="listContentCol subHeadingLabel" style="padding:10px" align="right"><span class="bold">Total Votes</span></td>
        <td class="listContentCol subHeadingLabel" style="padding:10px" align="center"><span class="bold"><?php echo (int)$sumPollCount; ?></span></td>
        <?php if($colspanValue > 3) { ?>
        <td class="listContentColLast">&nbsp;</td>
        <?php } ?>
    </tr>
    <?php } ?>
    <?php
	}
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="4" align="center" class="listContentColLast">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>