<?php
$css_path = $this->config->item("css_path");
$style_css_path = $this->config->item("style_css_path");
$script_path = $this->config->item("script_path");
?>
<!DOCTYPE html>
<html>
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Naval Anchorage Club - HRMS</title>

	<link type="text/css" rel="stylesheet" href="<?php echo $style_css_path; ?>">
	<link type="text/css" rel="stylesheet" href="<?php echo $css_path; ?>reset.css">
    <script>var base_url = '<?php echo $this->baseURL; ?>';</script>
	<script type="text/javascript" src="<?php echo $script_path; ?>/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="<?php echo $script_path; ?>/common.js"></script>
	<style>
		@font-face {
		 font-family:"OSWALD-LIGHT";
		 src:url(<?php echo $this->baseURL; ?>/font/OSWALD/OSWALD-LIGHT.TTF) format("truetype");
		}
		@font-face {
		 font-family:"DIN";
		 src:url(<?php echo $this->baseURL; ?>/font/DIN/DIN.ttf) format("truetype");
		}
	</style>
  </head>
  <body>
	<div class="mainBody">
        <div class="popupMainDiv">
        	
            <!-- Header Start ---->
            <div class="headerTitle">
                <a href="<?php echo base_url();?>home">
                    <img src="<?php echo $this->imagePath; ?>/logo.png" alt="Naval Anchorage Club HRM" title="Naval Anchorage Club HRM">
                </a>
            </div>
            <!--- Header End ----->
            
            <!----- Message Box Start [It is required to move in elements later] ----->
			<?php 
			if(trim($this->session->flashdata('success_message')) != '')
				$success_message = trim($this->session->flashdata('success_message'));
			if ($success_message != '' || $error_message != '' || $validation_error_message != '') { ?>
				<div class="messageBox">
					<?php if($success_message != '') {?>
					<div class="successMessage">
						<?php echo $success_message; ?>
					</div>
					<?php } ?>

					<?php if($error_message != '') {?>
					<div class="errorMessage">
						<?php echo $error_message; ?>
					</div>
					<?php } ?>
					
					<?php if($validation_error_message != '') { ?>
					<div class="errorMessage">
						<?php echo $validation_error_message; ?>
					</div>
					<?php } ?>
				</div>
			<?php } ?>
			<!----- Message Box End  ----->
            
            <!--- Mid Section Start-------> 
            <div class="popupMidMain">
				<?php echo $content; ?>
			</div>
            <!--- Mid Section End ------->
            
            <!--- Footer Start -------->
            <div class="footerMain">
            	<div class="footerText">
                	Copyright <?php echo date('Y'); ?> Naval Anchorage Club. All Rights Reserved.
                </div>
            </div>
            <!--- Footer End ---------->
		</div>
	</div>
  </body>
</html>