<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
});
</script>
<div class="listPageMain">
	<form name="frmEmailConfig" id="frmEmailConfig" method="post">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
        <tr>
            <td class="formHeaderRow" colspan="4">System Settings</td>
        </tr>
    	<tr class="listHeader">
    		<td class="listHeaderCol" width="250px">Config Item</td>
    		<?php if($this->userRoleID == WEB_ADMIN_ROLE_ID) { ?>
			<td class="listHeaderCol" width="250px">Config Key</td>
            <?php	}	?>
			<td class="listHeaderCol">Config Value</td>
            <td class="listHeaderColLast" width="100px">Action</td>
        </tr>
                        
	<?php 
	for($ind = 0; $ind < count($arrConfigs); $ind++) { 	
		
		if($arrConfigs[$ind]['config_show_backend']) {
			
			if($ind % 2 == 0) {
				$cssClass = 'listContent';
			} else {
				$cssClass = 'listContentAlternate';
			}
	?>
	<tr class="<?php echo $cssClass; ?>">
    	<td class="listContentCol"><?php echo $arrConfigs[$ind]['config_title']; ?></td>
        <?php if($this->userRoleID == WEB_ADMIN_ROLE_ID) { ?>
		<td class="listContentCol"><?php echo $arrConfigs[$ind]['config_key']; ?></td>
		<?php	}	?>
		<?php if($configID == $arrConfigs[$ind]['config_id']) { ?>  
    	<td class="listContentCol"><?php echo buildControl($arrConfigs[$ind]['config_type'], $arrConfigs[$ind]['config_id'], $arrConfigs[$ind]['config_value']); ?></td>
    	<td class="listContentColLast">
        	<input type="submit" class="smallButton" value="Save" />
            <input type="hidden" name="type" id="type" value="<?php echo $arrConfigs[$ind]['config_type']; ?>" />			
        </td>
        <?php } else { ?>
    	<td class="listContentCol"><?php echo buildValue($arrConfigs[$ind]['config_type'], $arrConfigs[$ind]['config_id'], $arrConfigs[$ind]['config_value']); ?></td>
    	<td class="listContentColLast"><input type="button" value="Edit" class="smallButton" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction; ?>/<?php echo $arrConfigs[$ind]['config_id']; ?>';" /></td>
        <?php } ?>
    </tr>
    <?php 
		}
	} 
	?>
</table>
</form>
</div>