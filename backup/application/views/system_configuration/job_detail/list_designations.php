<?php
$path = '/'.$this->currentController.'/'.$this->currentAction;
$status = ($status != '') ? $status : $this->input->post('status');
$jobType = ($jobType != '') ? $jobType : $this->input->post('jobType');
?>

<form name="frmListDesignations" id="frmListDesignations" method="post" action="<?php echo $frmActionURL; ?>">
<div class="listPageMain">
	<div class="searchBoxMain">
    	<div class="searchHeader">Search Criteria</div>
        
        <div class="searchcontentmain">
			<div class="searchCol">
            	<div class="labelContainer">Designation Type:</div>
                <div class="textBoxContainer">					
                    <select id="jobType" name="jobType" class="dropDown">
                        <option value="">All</option>
                        <option value="1">Sales</option>
                        <option value="2">Non-Sales</option>
                    </select>
                </div>
				<div class="labelContainer">Status:</div>
				<div class="textBoxContainer">
					<?php echo statusCombo('status',$this->userRoleID, 'All', 'dropDown'); ?>
				</div>
			</div>
			
			<div class="formButtonContainerWide">
				<input type="submit" class="searchButton" name="btnSearchDesignation" id="btnSearchDesignation" value="Search">
			</div>
		</div>
	</div>
      
  <script>
  	$('#status').val('<?php echo $status; ?>');
	$('#jobType').val('<?php echo $jobType; ?>');
  </script>
</form>

	<?php if($canWrite == 1) { ?>
	<div class="centerButtonContainer">
		<input class="addButton" type="button" value="Add Designation" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/save_designation/' ?>';" />
	</div>
	<?php }	?>

	<div class="centerElementsContainer">
		<div class="recordCountContainer"><?php echo "Total Records Count: ".$totalRecordsCount; ?></div>
		<?php
        if($pageLinks) {
        ?>
            <div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
        <?php }	?>
	</div>

	<div class="listContentMain">
    <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
        <tr class="listHeader">
            <td class="listHeaderCol" style="width:150px">Designation Name</th>
            <td class="listHeaderCol" style="width:150px">Designation Type</th>
            <td class="listHeaderCol" style="width:150px">Status</th>
            <?php if(($canWrite == 1) || (($canDelete == 1) && ($arrRecords[$ind]['designation_status'] != STATUS_DELETED))) { ?>
            <td class="listHeaderColLast" style="width:150px">Action</th>
            <?php } ?>
        </tr>
        <?php
        for($ind = 0; $ind < count($arrRecords); $ind++) 
		{
			($arrRecords[$ind]['designation_status'] == STATUS_ACTIVE) ? $classListingData = "listContent" : $classListingData = "listContentAlternate" ;
        ?>
        <tr class="<?php echo $classListingData; ?>">
            <td class="listContentCol"><?php echo $arrRecords[$ind]['designation_name']; ?></td>
            <td class="listContentCol"><?php if ($arrRecords[$ind]['job_type'] == 1) echo "Sales"; else if ($arrRecords[$ind]['job_type'] == 2) echo "Non-Sales"; else echo "-"; ?></td>
            <td class="listContentCol"><?php if ($arrRecords[$ind]['designation_status'] == STATUS_ACTIVE) echo "Active"; else if ($arrRecords[$ind]['designation_status'] == STATUS_INACTIVE_VIEW) echo "InActive"; else if ($arrRecords[$ind]['designation_status'] == STATUS_DELETED) echo "Deleted"; ?></td>
            <?php if(($canWrite == 1) || (($canDelete == 1) && ($arrRecords[$ind]['designation_status'] != STATUS_DELETED))) { ?>
			<td class="listContentColLast">
            	<div class="colButtonContainer">
                <?php if($canWrite == 1) { ?>
                    <input class="smallButton" type="button" value="View/Edit" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/save_designation/' . $arrRecords[$ind]['designation_id']; ?>';" />
                <?php } ?>
                <?php if(($canDelete == 1) && ($arrRecords[$ind]['designation_status'] != STATUS_DELETED)){ ?>
                    <input class="smallButton" type="button" value="Delete" onclick="deleteRecord('<?php echo $path; ?>','<?php echo $arrRecords[$ind]['designation_id']; ?>');" />
                <?php } ?>
                </div>
            </td>
			<?php } ?>
        </tr>
        <?php
       }
       ?>
    </table>
    </div>
</div>