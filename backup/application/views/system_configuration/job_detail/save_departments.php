<?php
$departmentName	=	(isset($_POST['departmentName']))	?	$_POST['departmentName']	:	$record['department_name'];
if ($record['department_status'] == STATUS_INACTIVE_VIEW) { $recordStatus = -1;} else if ($record['department_status'] == STATUS_ACTIVE) {$recordStatus = 1;} else if ($record['department_status'] == STATUS_DELETED) {$recordStatus = 2;}
$status 		= 	(isset($_POST['status'])) 			?	$_POST['status']			:	$recordStatus;
?>

<form name="frmAddDepartment" id="frmAddDepartment" method="post">
<div class="listPageMain">
	<div class="formMain">
        <table border="0" cellspacing="0" cellpadding="0" style="width:100%">
            <tr>
            	<?php if($record['department_id']) { ?>
					<td class="formHeaderRow" colspan="2">Update Department</td>
                <?php } else { ?>
                	<td class="formHeaderRow" colspan="2">Add New Department</td>
                <?php } ?>
			</tr>
            <tr>
                <td class="formLabelContainer">Department Name:</td>
                <td class="formTextBoxContainer">
                    <input class="textBox" type="text" id="departmentName" name="departmentName" value="<?php echo $departmentName; ?>" maxlength="25" />
                </td>
            </tr>
            <tr class="formAlternateRow">
                <td class="formLabelContainer">Status:</td>
                <td class="formTextBoxContainer">
					<?php echo statusCombo('status',$this->userRoleID, 'Select Status', 'dropDown'); ?>
                </td>
            </tr>
            <tr>
                <td class="formLabelContainer"></td>
                <td class="formTextBoxContainer">
                    <input class="smallButton" name="addDepartment" type="submit" value="Save">
                    <?php if(strpos($_SERVER["REQUEST_URI"],$record['department_id']) != false) { ?>
                        <input class="smallButton" type="button" value="Cancel" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/list_department' ?>';">
                    <?php } ?>
                </td>
            </tr>
        </table>
	</div>    
</div>
</form>

<script>
	$('#status').val('<?php echo $status; ?>');
</script>