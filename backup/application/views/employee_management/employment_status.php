<?php
//$empID				=	(isset($_POST['empID']))				?	$_POST['empID']					:	$record['emp_code'];
//$empIPNum				=	(isset($_POST['empIPNum']))				?	$_POST['empIPNum']				:	$record['emp_ip_num'];
//$empSupervisor		=	(isset($_POST['empSupervisor']))		?	$_POST['empSupervisor']			:	$record['emp_authority_id'];
$empEmploymentStatus	=	(isset($_POST['empEmploymentStatus']))	?	$_POST['empEmploymentStatus']	:	$record['emp_employment_status'];
$empJoiningDate			=	(isset($_POST['empJoiningDate']))		?	$_POST['empJoiningDate']		:	$record['emp_joining_date'];
$empConfirmationDate	=	(isset($_POST['empConfirmationDate']))	?	$_POST['empConfirmationDate']	:	$record['emp_confirmation_date'];
$empEndDate				=	(isset($_POST['empEndDate']))			?	$_POST['empEndDate']			:	$record['emp_end_date'];
$empReason				=	(isset($_POST['empReason']))			?	$_POST['empReason']				:	$record['emp_end_reason'];
$empRemarks				=	(isset($_POST['empRemarks']))			?	$_POST['empRemarks']			:	$record['emp_end_remarks'];
//$empExpLetter			=	(isset($_POST['empExpLetter']))			?	$_POST['empExpLetter']			:	$record['emp_exp_letter'];
?>

<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( ".datePicker" ).datepicker( "option", "minDate", '1993-01-20' );
	$( "#empJoiningDate" ).datepicker( "setDate", "<?php echo $empJoiningDate; ?>" );
	<?php if($empConfirmationDate != '0000-00-00') { ?>
		$( "#empConfirmationDate" ).datepicker( "setDate", "<?php echo $empConfirmationDate; ?>" );
	<?php } ?>
	<?php if($empEndDate != '0000-00-00') { ?>
	$( "#empEndDate" ).datepicker( "setDate", "<?php echo $empEndDate; ?>" );
	<?php } ?>
	
});
</script>

<form name="frmAddEmploymentDetails" id="frmAddEmploymentDetails" enctype="multipart/form-data" method="post">
  <div class="employeeFormMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
        <tr>
            <td class="formHeaderRow" colspan="2">Employee's Exit</td>
        </tr>
        <?php /*
		<tr class="formAlternateRow">
            <td class="formLabelContainer">Employee Code:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empID" maxlength="4" id="empID" class="textBox" value="<?php echo $empID; ?>">
            </td>
      	</tr>
        <tr>
            <td class="formLabelContainer">IP Number:</td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empIPNum" maxlength="4" id="empIPNum" class="textBox" value="<?php echo $empIPNum; ?>">
            </td>
      	</tr>
        <tr class="formAlternateRow">
            <td class="formLabelContainer">Supervisor:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<select id="empSupervisor" name="empSupervisor" class="dropDown">
                  <option value="">Select Supervisor</option>
                  <?php
                  if (count($empSupervisors)) {
                      foreach($empSupervisors as $supervisorVal) {
                          $selected = '';
                          if($empSupervisor == $supervisorVal['emp_id']) {
                              $selected = 'selected="selected"';
                          }
                  ?>
                      <option value="<?php echo $supervisorVal['emp_id']; ?>" <?php echo $selected; ?>><?php echo $supervisorVal['emp_full_name']; ?></option>
                  <?php
                      }
                  }
                  ?>
              </select> 
            </td>
      	</tr>
		*/ ?>
        <tr>
            <td class="formLabelContainer">Employment Status:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<select id="empEmploymentStatus" name="empEmploymentStatus" class="dropDown" onchange="showResponse(this.value)">
                  <option value="">Select Employment Status</option>
                  <?php
				  $checkValue = 0;
				  if($record['emp_employment_status'] > 5) {
					  $checkValue = 5;
				  }
				  
                  if (count($empEmploymentStatuses)) {
                      foreach($empEmploymentStatuses as $employmentStatusVal) {
						  if($employmentStatusVal['employment_status_id'] > $checkValue) {
							  $selected = '';
							  if($empEmploymentStatus == $employmentStatusVal['employment_status_id']) {
								  $selected = 'selected="selected"';
							  }
							  
							  if($employmentStatusVal['employment_status_id'] < 6) {
								  $selected .= ' disabled="disabled"';
							  }
                  ?>
                      <option value="<?php echo $employmentStatusVal['employment_status_id']; ?>" <?php echo $selected; ?>><?php echo $employmentStatusVal['employment_status_name']; ?></option>
                  <?php
						  }
                      }
                  }
                  ?>
              </select>  
            </td>
      	</tr>
        <tr class="formAlternateRow" id="joiningDate">
            <td class="formLabelContainer">Joining Date:<span class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empJoiningDate" maxlength="30" id="empJoiningDate" class="textBox datePicker">
            </td>
      	</tr>
        <tr id="confirmationDate">
            <td class="formLabelContainer">Confirmation Date:<span id="confirmationDateStar" class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empConfirmationDate" maxlength="30" id="empConfirmationDate" class="textBox datePicker">
            </td>
      	</tr>
        <tr id="trEndDate" class="formAlternateRow" style="display:none;">
            <td class="formLabelContainer">End Date<span id="endDateStar" class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
        		<input type="text" name="empEndDate" maxlength="30" id="empEndDate" class="textBox datePicker">
            </td>
      	</tr>
        <tr id="trReason" style="display:none;">
            <td class="formLabelContainer">Reason For Leaving<span id="endReasonStar" class="mandatoryStar"> *</span></td>
            <td class="formTextBoxContainer">
                <textarea rows="5" cols="30" name="empReason" id="empReason" class="textArea"><?php echo $empReason; ?></textarea>
            </td>
      	</tr>
        <tr id="trRemarks" class="formAlternateRow" style="display:none;">
            <td class="formLabelContainer">Exit Interviewer Remarks</td>
            <td class="formTextBoxContainer">
                <textarea rows="5" cols="30" name="empRemarks" id="empRemarks" class="textArea"><?php echo $empRemarks; ?></textarea>
            </td>
      	</tr>
		<tr>      
            <td class="formLabelContainer"></td>
            <td class="formTextBoxContainer">
      			<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save">&nbsp;
        		<input type="button" class="smallButton" value="Back" onclick="history.go(-1)">      
    		</td>
        </tr>
  </table>
  </div>
</form>

<script>	
	function showResponse(value)
	{
		if((value == 6)||(value == 7)){
			$('#trEndDate').show();
			$('#trReason').show();
			$('#trRemarks').show();
			$('#trExpLetter').show();
		} else {
			$('#trEndDate').hide();
			$('#trReason').hide();
			$('#trRemarks').hide();
			$('#trExpLetter').hide();
		}
	}
	
	showResponse(<?php echo (int)$empEmploymentStatus; ?>);
</script>