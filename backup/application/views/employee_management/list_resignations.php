<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( "#dateFrom" ).datepicker( "setDate", "<?php echo $dateFrom; ?>" );
	$( "#dateTo" ).datepicker( "setDate", "<?php echo $dateTo; ?>" );
});
</script>
<form name="frmListResignations" id="frmListResignations" method="post" action="<?php echo $frmActionURL; ?>">
  <div class="searchBoxMain">
    <div class="searchHeader">Search Criteria</div>
    <div class="searchcontentmain">
      <div class="searchCol">
        <div class="labelContainer">Supervisor:</div>
        <div class="textBoxContainer">
        	<select id="empSupervisor" name="empSupervisor" class="dropDown">
                <option value="">All</option>
                <?php
                if (count($arrSupervisors)) {
                    foreach($arrSupervisors as $key => $arrSupervisor) {
						if(count($arrSupervisor)) {
                ?>
                    <optgroup label="<?php echo $key; ?>">
                        <?php for($i=0;$i<count($arrSupervisor);$i++) { ?>					
                            <option value="<?php echo $arrSupervisor[$i]['emp_id']; ?>"><?php echo $arrSupervisor[$i]['emp_code']." - ".$arrSupervisor[$i]['emp_full_name']; ?></option>
                        <?php } ?>
                    </optgroup>
                <?php	}
					}
                }
                ?>
            </select>
        </div>
        <div class="labelContainer">Date From:</div>
        <div class="textBoxContainer">
        	<input type="text" class="textBox datePicker" id="dateFrom" name="dateFrom" />
        </div>
        <div class="labelContainer">Date To:</div>
        <div class="textBoxContainer">
        	<input type="text" class="textBox datePicker" id="dateTo" name="dateTo" />
        </div>
      </div>     
      <div class="searchCol">
        <div class="labelContainer">Status:</div>
        <div class="textBoxContainer">
        	<select name="resStatus" id="resStatus" class="dropDown">
            	<option value="">Any</option>
				<?php
                if (count($arrResignationStepsDesc)) {
                    foreach($arrResignationStepsDesc as $arrResignationStepID => $arrResignationStep) {
                ?>
                <option value="<?php echo $arrResignationStepID; ?>"><?php echo 'Step ' . $arrResignationStepID . ': ' . $arrResignationStep; ?></option>
                <?php
                    }
                }
                ?>
            	<option value="<?php echo EXIT_MODULE_STEP_COMPLETED; ?>">Completed</option>
            	<option value="<?php echo EXIT_MODULE_STEP_CANCELED_RETAINED; ?>">Canceled By Employee</option>
            	<option value="<?php echo EXIT_MODULE_STEP_RETAINED_BY_MANAGER; ?>">Retained By Manager</option>
            	<option value="<?php echo EXIT_MODULE_STEP_RETAINED_BY_HR; ?>">Retained By HR</option>
            </select>
        </div>
      </div> 
      <div class="buttonContainer">
        <input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Search">
		<input class="searchButton" type="reset" value="Reset">
      </div>
    </div>  
  </div> 
  <script>
  	$('#empSupervisor').val('<?php echo $empSupervisor; ?>');
  	$('#resStatus').val('<?php echo $resStatus; ?>');
  	$('#dateFrom').val('<?php echo $dateFrom; ?>');
  	$('#dateTo').val('<?php echo $dateTo; ?>');
  </script>
</form>

<div class="centerElementsContainer">
	<div class="recordCountContainer">
		<?php echo "Total Records Count: " . $totalRecordsCount; ?>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
	
	<?php
	if($pageLinks) {
	?>
	<div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
	<?php 	}	?>
</div>

	<div class="listContentMain">
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
  <tr class="listHeader">
    <td class="listHeaderCol"></td>
    <td class="listHeaderCol">Employee Name</td>
    <td class="listHeaderCol">Gender</td>
    <td class="listHeaderCol">Supervisor</td>
    <td class="listHeaderCol">Date</td>
    <td class="listHeaderCol">Status</td>
    <td class="listHeaderColLast">Action</td>
  </tr>
  <?php
    for($ind = 0; $ind < count($arrRecords); $ind++) {
			
		if($ind % 2 == 0) {
			$trCSSClass = 'listContentAlternate';
		}  else {
			$trCSSClass = 'listContent';
		}
	?>
  <tr class="<?php echo $trCSSClass; ?>" id="tr<?php echo $ind; ?>">
    <td class="listContentCol" style="background-color:#FFF; padding:0px; width:60px">
    <?php
    if(!file_exists($pictureFolder . $arrRecords[$ind]['emp_photo_name']) || empty($arrRecords[$ind]['emp_photo_name'])) {
		$arrRecords[$ind]['emp_photo_name'] = 'no_image_' . strtolower($arrRecords[$ind]['emp_gender']) . '.jpg';
	}
	?>
    	<img src="<?php echo $this->baseURL . '/' . $pictureFolderShow . $arrRecords[$ind]['emp_photo_name']; ?>" width="60" height="60" />
    </td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_full_name']; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_gender']; ?></td>
    <td class="listContentCol"><?php echo getEmployeeName($arrRecords[$ind]['resignee_authority_id']); ?></td>
    <td class="listContentCol"><?php echo readableDate($arrRecords[$ind]['created_date'], $showDateFormat); ?></td>
    <td class="listContentCol">
		<?php        	
        	if($arrRecords[$ind]['resignation_step'] == EXIT_MODULE_STEP_CANCELED_RETAINED) {
				echo 'Canceled By Employee';
			} else if($arrRecords[$ind]['resignation_step'] == EXIT_MODULE_STEP_RETAINED_BY_MANAGER) {
				echo 'Retained By Manager';
			} else if($arrRecords[$ind]['resignation_step'] == EXIT_MODULE_STEP_RETAINED_BY_HR) {
				echo 'Retained By HR';
			} else if($arrRecords[$ind]['resignation_step'] == EXIT_MODULE_STEP_COMPLETED) {
				echo 'Completed';
			} else {
				echo 'Waiting for ' . $arrResignationStepsDesc[$arrRecords[$ind]['resignation_step']] . ' Response';
			}
		?>
    </td>
    <td class="listContentColLast">
      <div class="empColButtonContainer">
	  <?php if($canWrite == YES) { ?>
      	<img title="View/Edit" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/view.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/resignation/' . $arrRecords[$ind]['resignation_emp_id'] . '/' . $arrRecords[$ind]['resignation_id']; ?>';">
      <?php } if($canDelete == YES) { ?>
      	<img title="Delete" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/delete.png';?>" onclick="deleteRecord('/<?php echo $this->currentController . '/' . $this->currentAction; ?>/', '<?php echo $arrRecords[$ind]['resignation_id']; ?>');">
      <?php } ?>
	  </div>
	  </td>
  </tr>
  <?php
	} 
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="8" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
</div>
<div style="clear:both">&nbsp;<div>
<div>
	<table cellpadding="0" cellspacing="0" border="0">
    	<tr>
            <td width="35px"><img style="margin:5px 0;" width="30" src="<?php echo $this->imagePath . '/view.png';?>" /></td>
            <td width="70px">View / Edit</td>
            <td width="35px"><img style="margin:5px 0;" width="30" src="<?php echo $this->imagePath . '/delete.png';?>" /></td>
            <td width="65px">Delete</td>
        </tr>
    </table>
</div>

