<?php
if(count($arrEmployee)) {
?>
<div class="popupNotificationMain">
  <div class="popupNotificationBox">
    <div class="popupNotificationContent" style="height:100%">
      <b>Select Number To Call</b><br />
      <br />
      <a href="javascript:void(0)" class="normalLink" onclick="callEmployee('<?php echo (int)$arrEmployee['emp_id']; ?>', '<?php echo cleanNumber($arrEmployee['emp_mobile']); ?>');"><?php echo $arrEmployee['emp_mobile']; ?></a><br />
      <a href="javascript:void(0)" class="normalLink" onclick="callEmployee('<?php echo (int)$arrCandidate['emp_id']; ?>', '<?php echo cleanNumber($arrEmployee['emp_home_telephone']); ?>');"><?php echo $arrEmployee['emp_home_telephone']; ?></a><br />
      <br />
      <div id="msgContent" <?php if(!$showRemarksBox) { ?>style="display:none"<?php } ?>>
      	<b>Please wait, your phone will ring in a second !!</b><br />
        <span style="color:#F00">Note: Do not forget to submit remarks after the call. Remarks are mandatory.</span><br />
        <br />
        <form name="frmCallRemarks" method="post">
          <input type="hidden" name="callID" id="callID" value="<?php echo $callID; ?>" />
          <b>Remarks: <span class="mandatoryStar">*</span></b>
          <div class="clear"></div>
          <textarea class="textArea" name="callRemarks" id="callRemarks" style="width:250px; height:70px"><?php echo $callRemarks; ?></textarea>
          <div class="clear">&nbsp;</div>
          <input type="submit" name="btnSubmit" id="btnSubmit" class="smallButton" value="Submit" <?php if(!(int)$callID) { ?>disabled="disabled"<?php } ?> />
        </form>
      </div>
    </div>
  </div>
</div>
<?php } ?>