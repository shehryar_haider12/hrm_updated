<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('base_url'))
{
	function base_url()
	{
		$CI =& get_instance();
		return $CI->config->slash_item('base_url');
	}
}

function isLoggedIn() {
	
	if((int)$this->session->userdata('user_id')) {
		return true;
	}
	return false;
	
}

function debugLog($strActivity, $filename = "", $live = "true", $userID = 0, $moduleID = 0)
{
	if($live == "true")
	{
		$CI = &get_instance();	
		$CI->load->model('model_user_management', 'user', true);
		
		if($userID == 0) {
			$userID = $CI->userEmpNum;
		}
		if($moduleID == 0) {
			$moduleID = $CI->currModuleID;
			if((int)$moduleID == 0) {
				$moduleID = 1;
			}
		}
		
		$arrData = array(
						'emp_id' => $userID,
						'module_id' => $moduleID,
						'log_activity_txt' => $strActivity,
						'log_ip' => getIP(),
						'created_date' => date(DATE_TIME_FORMAT)
						);
						
		$CI->user->saveValues(TABLE_USER_LOGS, $arrData);
	
		/*
		set_time_limit(0);
		ini_set("error_log", "/var/log/dashboardmain.log");
		if($filename) {
			$msg = "[$filename] " . $msg;
		}
		//error_log($msg);
		*/
	}
	else
	{
		print("<br>[$msg]");
	}
}

function buildControl($strType, $strID, $strValue) {
	
	if($strType == 'textbox') {
		return '<input type="text" class="textBox" name="update_'.$strID.'" id="update_'.$strID.'" value="'.$strValue.'" />';
	} else if($strType == 'date') {
		return '<input type="text" class="textBox datePicker" name="update_'.$strID.'" id="datepicker" value="'.$strValue.'" />';
	} else if($strType == 'checkbox') {
		$checked = '';
		if($strValue) {
			$checked = ' checked="checked"';
		}
		return '<input type="checkbox" name="update_'.$strID.'" id="update_'.$strID.'" value="1"'.$checked.' />';
	} else if($strType == 'textarea') {
		return '<textarea class="textAreaSub" style="height:150px" name="update_'.$strID.'" id="update_'.$strID.'">'.$strValue.'</textarea>';
	} else if($strType == 'file') {
		return '<input type="file" name="update_'.$strID.'" id="update_'.$strID.'" />';
	}
	return '';
}

function buildValue($strType, $strID, $strValue) {
	if($strType == 'checkbox') {
		return ($strValue == 1) ? 'Yes' : 'No';
	} else if($strType == 'file') {
		return ($strValue == '') ? '' : '<img src="' . base_url() . 'images/' . $strValue.'" height="96px" />';
	} else {
		return $strValue;
	}
}

function buildGrid($arrData = array(), $colSkip = array(), $linkEdit = '', $colUnique = array(), $tblCssClass = '', $trCssClass = '', $tdCssClass = '', $thCssClass = '') {
	
	$strGrid = '';
	
	if(count($arrData) > 0) {		
		$strGrid .= '<table class="' . $tblCssClass . '" width="100%">';	
		$arrKeys = array_keys($arrData[0]);
		
		$strGrid .= '<tr class="' . $trCssClass . '">';	
		foreach($arrKeys as $colName) {
			if(!in_array($colName, $colSkip)) {
				$strGrid .= '<th align="left" class="' . $thCssClass . '">' . strFilter($colName) . '</th>';
			}
		}
		$strGrid .= '</tr>';
		
		for($ind = 0; $ind < count($arrData); $ind++) {		
		
			$strEditLink = '';
			if(!empty($linkEdit)) {
				if(is_array($colUnique)) {
					$strEditLink = 'onclick="window.location.href = \'' . base_url() . $linkEdit . strURL($colUnique, $arrData[$ind]) . '\'" style="cursor:pointer"';
				} else {
					$strEditLink = 'onclick="window.location.href = \'' . base_url() . $linkEdit . $arrData[$ind][$colUnique] . '\'" style="cursor:pointer"';
				}
			}
			
			$strGrid .= '<tr class="' . $trCssClass . '" ' . $strEditLink . '>';	
			
			for($jnd = 0; $jnd < count($arrData[$ind]); $jnd++) {
				if(!in_array($arrKeys[$jnd], $colSkip)) {
					$strGrid .= '<td class="' . $tdCssClass . '">' . $arrData[$ind][$arrKeys[$jnd]] . '</td>';				
				}
			}
			
			$strGrid .= '</tr>';	
		}
				
		$strGrid .= '</table>';
	} else {
		$strGrid = '<h2>No Record Found</h2>';
	}
	
	return $strGrid;
}

function strFilter($strColName) {
	return $strColName = ucwords(str_replace('_', ' ', $strColName));
}

function strURL($arrParameters, $arrData) {
	$strURL = '';
	foreach($arrParameters as $param) {
		foreach($arrData as $name => $val) {
			if($name == $param) {
				$strURL .= $arrData[$name] . '/';
			}
		}
	}
	return $strURL;
}

function displayLinks($number_of_pages, $max_page_links = 5, $current_page_number = 1, $parameters = '') {
	
	$display_links_string = '';

	// previous button - not displayed on first page
	if ($current_page_number > 1) $display_links_string .= '<a href="' . base_url() . str_replace('#', ($current_page_number - 1), $parameters) . '" class="paging">< Prev</a>&nbsp;&nbsp;';

	// check if number_of_pages > $max_page_links
	$cur_window_num = intval($current_page_number / $max_page_links);
	if ($current_page_number % $max_page_links) $cur_window_num++;

	$max_window_num = intval($number_of_pages / $max_page_links);
	if ($number_of_pages % $max_page_links) $max_window_num++;

	// previous window of pages
	if ($cur_window_num > 1) $display_links_string .= '<a href="' . base_url() . str_replace('#', (($cur_window_num - 1) * $max_page_links), $parameters) . '" class="paging">...</a>';

	// page nn button
	for ($jump_to_page = 1 + (($cur_window_num - 1) * $max_page_links); ($jump_to_page <= ($cur_window_num * $max_page_links)) && ($jump_to_page <= $number_of_pages); $jump_to_page++) {
	  if ($jump_to_page == $current_page_number) {
		$display_links_string .= '&nbsp;<strong class="paging">' . $jump_to_page . '</strong>&nbsp;';
	  } else {
		$display_links_string .= '&nbsp;<a href="' . base_url() . str_replace('#', $jump_to_page, $parameters) . '" class="paging">' . $jump_to_page . '</a>&nbsp;';
	  }
	}

	// next window of pages
	if ($cur_window_num < $max_window_num) $display_links_string .= '<a href="' . base_url() . str_replace('#', (($cur_window_num) * $max_page_links + 1), $parameters) . '" class="paging">...</a>&nbsp;';

	// next button
	if (($current_page_number < $number_of_pages) && ($number_of_pages != 1)) $display_links_string .= '&nbsp;<a href="' . base_url() . str_replace('#', ($current_page_number + 1), $parameters) . '" class="paging">Next ></a>&nbsp;';

	if ($display_links_string == '&nbsp;<strong class="paging">1</strong>&nbsp;') {
	  return false;
	} else {
	  return $display_links_string;
	}
}

function displayLinksFrm($number_of_pages, $max_page_links = 5, $current_page_number = 1, $parameters = '', $frmName = '') {
	
	$display_links_string = '';
	$strSubmit = 'href="#" onclick="document.'.$frmName.'.action = \'^\'; document.'.$frmName.'.submit(); return false;");"';
	
	// previous button - not displayed on first page
	if ($current_page_number > 1) $display_links_string .= '<a ' . str_replace('^', base_url() . str_replace('#', ($current_page_number - 1), $parameters), $strSubmit) . ' class="paging">< Prev</a>&nbsp;&nbsp;';

	// check if number_of_pages > $max_page_links
	$cur_window_num = intval($current_page_number / $max_page_links);
	if ($current_page_number % $max_page_links) $cur_window_num++;

	$max_window_num = intval($number_of_pages / $max_page_links);
	if ($number_of_pages % $max_page_links) $max_window_num++;

	// previous window of pages
	if ($cur_window_num > 1) $display_links_string .= '<a ' . str_replace('^', base_url() . str_replace('#', (($cur_window_num - 1) * $max_page_links), $parameters), $strSubmit) . ' class="paging">...</a>';

	// page nn button
	for ($jump_to_page = 1 + (($cur_window_num - 1) * $max_page_links); ($jump_to_page <= ($cur_window_num * $max_page_links)) && ($jump_to_page <= $number_of_pages); $jump_to_page++) {
	  if ($jump_to_page == $current_page_number) {
		$display_links_string .= '&nbsp;<strong class="paging">' . $jump_to_page . '</strong>&nbsp;';
	  } else {
		$display_links_string .= '&nbsp;<a ' . str_replace('^', base_url() . str_replace('#', $jump_to_page, $parameters), $strSubmit) . ' class="paging">' . $jump_to_page . '</a>&nbsp;';
	  }
	}

	// next window of pages
	if ($cur_window_num < $max_window_num) $display_links_string .= '<a ' . str_replace('^', base_url() . str_replace('#', (($cur_window_num) * $max_page_links + 1), $parameters), $strSubmit) . ' class="paging">...</a>&nbsp;';

	// next button
	if (($current_page_number < $number_of_pages) && ($number_of_pages != 1)) $display_links_string .= '&nbsp;<a ' . str_replace('^', base_url() . str_replace('#', ($current_page_number + 1), $parameters), $strSubmit) . ' class="paging">Next ></a>&nbsp;';

	if ($display_links_string == '&nbsp;<strong class="paging">1</strong>&nbsp;') {
	  return false;
	} else {
	  return $display_links_string;
	}
}

function dateDifference($dateFrom, $dateTo) {
	
	date_default_timezone_set('Asia/Dubai');
	$diffDate1 = new DateTime($dateFrom);
	$diffDate2 = new DateTime($dateTo);
	$diffDate = $diffDate2->diff($diffDate1);
	$arrDateDiff = (array)$diffDate;
	if($arrDateDiff['y']) {
		return $arrDateDiff['y'] . ' Year(s) ' . $arrDateDiff['m'] . ' Month(s)';
	} else if($arrDateDiff['m']) {
		return $arrDateDiff['m'] . ' Month(s) ' . $arrDateDiff['d'] . ' Day(s)';
	} else{
		return $arrDateDiff['d'] . ' Day(s)';
	}
	
}

function getValue($arrVals, $findCol, $valID, $colName) {
	
	for($ind = 0; $ind < count($arrVals); $ind++) {
		if($arrVals[$ind][$findCol] == $valID) {
			if(is_array($colName)) {
				$strReturn = '';
				for($jnd = 0; $jnd < count($colName); $jnd++) {
					$strReturn .= $arrVals[$ind][$colName[$jnd]] . ' ';
				}
				return $strReturn;
			} else {
				return $arrVals[$ind][$colName];
			}
		}
	}
	return '';
}

function getValueArray($arrVals, $findCol, $valID) {
	
	$arrReturn = array();
	for($ind = 0; $ind < count($arrVals); $ind++) {
		if($arrVals[$ind][$findCol] == $valID) {
			$arrReturn[] = $arrVals[$ind];
		}
	}
	return $arrReturn;
}

function getDBValue($tblName, $colList, $colReturn = null, $arrWhere) {
	
	$CI = &get_instance();
	$CI->load->model('model_system_configuration', 'configuration', true);
	
	$arrResult = $CI->configuration->getValues($tblName, $colList, $arrWhere);
	return $arrResult[0][$colReturn];
}

function strDateToStamp($strDate) {
	
	$strDate = str_replace('-', '/', $strDate);
	if(strpos($strDate, ' ') !== false) {
		$strDate = substr($strDate, 0, strpos($strDate, ' '));
	}
	return strtotime($strDate);
	
}

function readableDate($strDate, $showDateFormat) {
	
	if((!empty($strDate)) && ($strDate != '0000-00-00')) {
		return date($showDateFormat, strDateToStamp($strDate));
	} else { 
		return "-";
	}
	
}

function statusCombo($comboName, $roleID, $firstOption = '', $className = '') {
	
	$CI = &get_instance();
	$adminRoleIDs = $CI->load->config->item('forced_access_roles');
	$strCombo = '<select class="'.$className.'" name="'.$comboName.'" id="'.$comboName.'">
					<option value="">'.$firstOption.'</option>
					<option value="'.STATUS_ACTIVE.'">Active</option>
					<option value="'.STATUS_INACTIVE.'">InActive</option>
					';
	if(in_array($roleID, $adminRoleIDs)) {
		$strCombo .= '<option value="'.STATUS_DELETED.'">Deleted</option>';
	}
	$strCombo .= '</select>';
	return $strCombo;
	
}

function isAdmin($roleID) {
	
	$CI = &get_instance();
		
	if(in_array($roleID, $CI->arrRoleIDs)) {
		return true;
	}
	
	return false;
}

function diee($arr, $die = false) {
	
	echo '<pre>'; print_r($arr);
	if($die) {
		die('</pre>');
	} else {
		echo '</pre>';
	}
	
}
	
function randomString($length = 10) {
	$strCharacters = '0123456789abcdefghijklmnopqrstuvwxyz';
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $strCharacters[rand(0, strlen($strCharacters) - 1)];
	}
	return $randomString;
}

function getHTML($arrValues = array(), $strTemplate = 'default.html') {
	
	$CI = &get_instance();
	//$strHTML = file_get_contents($CI->config->item('email_templates_folder') . $strTemplate);
	$strHTML = file_get_contents(EMAIL_TEMPLATE_FOLDER . $strTemplate);
	
	foreach($arrValues as $key => $val) {
		$strHTML = str_replace($key, $val, $strHTML);
	}
	
	return $strHTML;
	
}


function alpha_dash_space($string, $value)
{
//	return ( ! preg_match("/^([-a-z_ ])+$/i", $str)) ? FALSE : TRUE;
//	return ( ! preg_match("/^[a-z0-9_ \-]+$/", $str)) ? FALSE : TRUE;
//	return ( ! preg_match("^[a-zA-Z0-9_ ]*$", $str)) ? FALSE : TRUE;
//	return ( ! preg_match("/^([a-z0-9 ])+$/i", $str)) ? FALSE : TRUE;
//	return ( ! preg_match("/^([-a-z0-9_-\s])+$/i", $str)) ? FALSE : TRUE;
	return ( ! preg_match("/^[a-zA-Z_\/\s]*$/u", $string)) ? FALSE : TRUE;
}

function getIssuesUnChecked($arrWhere = array())
{
	$CI = &get_instance();
	
	$CI->load->model('model_complain_management', 'complain', true);
	
	$result = $CI->complain->getIssuesUnChecked($arrWhere);
		
	return (int)$result[0]['total_complains'];	
}

function getTasksUnChecked($arrWhere = array())
{
	$CI = &get_instance();
	
	$CI->load->model('model_complain_management', 'complain', true);
	
	$result = $CI->complain->getTasksUnChecked($arrWhere);
		
	return (int)$result[0]['total_tasks'];	
}

function getReviewsUnChecked($empID)
{
	$CI = &get_instance();
	
	$CI->load->model('model_employee_management', 'employee', true);
	$CI->load->model('model_task_management', 'task', true);
	
	$arrData['strHierarchy'] = $CI->employee->getHierarchyWithMultipleAuthorities($empID);
	$arrEmployees = $CI->task->getTeamEmployeesID($empID);
	$arrSubordinates = array();
	
	for($ind = 0; $ind < count($arrEmployees); $ind++) {
		$arrSubordinates[] = $arrEmployees[$ind]['emp_id'];
	}
	
	if(count($arrSubordinates) <= 0) {
		return 0;
	}
	
	$waitingReview = 0;
	
	$arrWhere = array(
		'assessment_status_id' => STATUS_SUBMIT_TASKS_FOR_REVIEW,
		'emp_id in ' => '(' . implode(',', $arrSubordinates) . ')'
	);
		
	$arrTaskStatus = $CI->task->getTasks($arrWhere);
	$waitingReview = count($arrTaskStatus);
	
	return $waitingReview;	
}

function getLeavesUnchecked($arrWhere = array()) 
{		
	$CI = &get_instance();
	
	$CI->load->model('model_attendance_management', 'attendance', true);
	
	$result = $CI->attendance->getTotalLeavesForApproval($arrWhere);
		
	return (int)$result;	
}

function getActivityDocs($activityID) {
	$CI = &get_instance();	
	$CI->load->model('model_employee_management', 'employee', true);
	
	$result = $CI->employee->getActivityDocs($activityID);		
	return $result;	
}

function getActivityTopics($empJobCategoryID = 0, $strType = 'Reports') {
	
	$CI = &get_instance();
	$arrReports = $CI->config->item('reporting_topics');
	
	$arrResult = array();
	
	if((int)$empJobCategoryID) {
		foreach($arrReports as $catgID => $arrReport) {
			if($catgID == $empJobCategoryID) {
				foreach($arrReport[$strType] as $reportID => $reportTxt) {
					$arrResult[$reportID] = $reportTxt;
				}
			}
		}
	} else {
		$arrResult = $arrReports;
	}
	
	return $arrResult;	
}

function getDepartmentTasks($empJobCategoryID = 0) {
	
	$CI = &get_instance();
	$arrDepartmentTasks = $CI->config->item('pm_departmentTasks');
	
	$arrResult = array();
	
	foreach($arrDepartmentTasks as $catgID => $arrDepartmentTask) {
		if($catgID == $empJobCategoryID) {
			foreach($arrDepartmentTask as $field => $value) {
				$arrResult[$field] = $value;
			}
		}
	}
	
	return $arrResult;	
}

function getActivityThread($empID, $actID) {
	
	$CI = &get_instance();	
	$CI->load->model('model_task_management', 'task', true);
	
	$result = $CI->task->getActivityThread(array(
													'eal.activity_emp_id' => (int)$empID, 
													'eal.activity_id' => (int)$actID));
	return $result;
	
}

function getEmployeeName($empID = 0) {
	$CI = &get_instance();	
	$CI->load->model('model_employee_management', 'employee', true);
	
	$result = $CI->employee->getEmployeeDetail(array('e.emp_id' => $empID), false);		
	return trim($result['emp_full_name']);	
}

function getEmpSupervisors($empID = 0) {
	$CI = &get_instance();	
	$CI->load->model('model_employee_management', 'employee', true);
	
	$result = $CI->employee->getEmpSupervisors(array('emp_id' => $empID), false);
	$arrResult = array();
	
	for($ind = 0; $ind < count($result); $ind++) {
		$arrResult[] = $result[$ind]['supervisor_emp_id'];
	}
	
	return $arrResult;	
}

function getSupervisorName($empID = 0) {
	$CI = &get_instance();	
	$CI->load->model('model_employee_management', 'employee', true);
	$strName = '';
	
	$arrSupervisors = $CI->employee->getEmpSupervisorsDetails(array('es.emp_id' => $empID));
	for($ind = 0; $ind < count($arrSupervisors); $ind++) {
		if(!$ind) {
			$strName .= $arrSupervisors[$ind]['emp_full_name'];
		} else {
			$strName .= ' / ' . $arrSupervisors[$ind]['emp_full_name'];
		}
	}
	
	return $strName;	
}

function getSponsorName($sponsorID = 0) {
	$CI = &get_instance();	
	$CI->load->model('model_configuration_management', 'configuration', true);
	
	$arrSponsor = $CI->configuration->getSponsors(array('sponsor_id' => $sponsorID));
	return $arrSponsor[0]['sponsor_type'];	
}

function getAttendanceNotes($empID, $strDate) {
	$CI = &get_instance();	
	$CI->load->model('model_attendance_management', 'attendance', true);
	
	$result = $CI->attendance->getValues(TABLE_ATTENDANCE_NOTES, 'att_notes', array('emp_id' => $empID, 'att_date' => $strDate));
	return trim($result[0]['att_notes']);	
}

function getIfLeaveApplied($empID, $strDate) {
	$CI = &get_instance();	
	$CI->load->model('model_attendance_management', 'attendance', true);
	
	$CI->db->select(' l.*, lc.leave_category ');
	$CI->db->join(TABLE_ATTENDANCE_LEAVE_CATEGORIES . ' lc ', 'lc.leave_category_id = l.leave_category', 'left');
		
	$CI->db->where(" emp_id = " . (int)$empID . " AND (leave_from = '" . $strDate . "' OR leave_to = '" . $strDate . "' OR ('" . $strDate . "' BETWEEN leave_from AND leave_to))", null, false);
	$objResult = $CI->db->get(TABLE_ATTENDANCE_LEAVES . ' l ');
	$arrResult = $objResult->result_array();
	$objResult->free_result();
	
	return trim($arrResult[0]['leave_category']);	
}

function getIP() {
	
	if(!empty($_SERVER["HTTP_CF_CONNECTING_IP"])) {	# CHECK IP FROM CLOUDFLARE
		$strIP = $_SERVER["HTTP_CF_CONNECTING_IP"];
	} elseif ((!empty($_SERVER['HTTP_CLIENT_IP'])) && ($_SERVER['HTTP_CLIENT_IP'] != 'unknown')) {  # CHECK IP FROM SHARED INTERNET
		$strIP = $_SERVER['HTTP_CLIENT_IP'];
	} elseif ((!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) && ($_SERVER['HTTP_X_FORWARDED_FOR'] != 'unknown')) {  # CHECK IP PASS FROM PROXY
		$strIP = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$strIP = $_SERVER['REMOTE_ADDR'];
	}
	
	return $strIP;
}

function userInOffice() {
	
	$CI = &get_instance();
	
	if(in_array(getIP(), $CI->config->item('office_ips'))) {
		return true;
	}
	return false;
}

function calculateAge($strDOB) {
	$strDOB = explode("-", $strDOB);
	$strAge = (date("md", date("U", mktime(0, 0, 0, $strDOB[1], $strDOB[2], $strDOB[0]))) > date("md")
	? ((date("Y") - $strDOB[0]) - 1)
	: (date("Y") - $strDOB[0]));
	return (int)$strAge;
}

function wavDuration($fileName) {
	
	if(strpos($fileName, '.wav')) {
		
		$fp = fopen($fileName, 'r'); 
		if (fread($fp,4) == "RIFF") {
			 
			fseek($fp, 20); 
			$rawheader = fread($fp, 16); 
			$header = unpack('vtype/vchannels/Vsamplerate/Vbytespersec/valignment/vbits',$rawheader); 
			$pos = ftell($fp); 
			while (fread($fp,4) != "data" && !feof($fp)) { 
			$pos++; 
			fseek($fp,$pos); 
			} 
			$rawheader = fread($fp, 4); 
			$data = unpack('Vdatasize',$rawheader); 
			$sec = $data[datasize]/$header[bytespersec]; 
			$minutes = intval(($sec / 60) % 60); 
			$seconds = intval($sec % 60);
			
			$strDuration = '';
			if((int)$minutes) {
				$strDuration .= $minutes . ' Min(s) ';
			}
			if((int)$seconds) {
				$strDuration .= $seconds . ' Sec(s) ';
			}
			
			return $strDuration; 
			
	  	}  else {
			return 0;
		}
	} else {
		return 0;
	}
}  

function cleanNumber($strNum) {
	$strNum = substr(str_replace(array('+', '-'), '', filter_var($strNum, FILTER_SANITIZE_NUMBER_INT)), -10);
	$firstDigit = substr($strNum, 0, 1);
	if($firstDigit == '3' && strlen($strNum) == 10) {
		return '0' . $strNum;
	} else {
		$strNum = substr($strNum, -8);
	}
	return $strNum;
}  

function cleanNumberForSMS($strNum) {
	$strNum = substr(str_replace(array('+', '-'), '', filter_var($strNum, FILTER_SANITIZE_NUMBER_INT)), -10);
	$firstDigit = substr($strNum, 0, 1);
	if($firstDigit == '3' && strlen($strNum) == 10) {
		return '92' . $strNum;
	}
	
	return $strNum;
}

function replace_underscore_with_space($string) {
    $string = str_replace("_", " ", $string);
    return $string;
}

function replace_space_with_dash($string) {
    $string = str_replace(" ", "-", $string);
    return $string;
}
?>
