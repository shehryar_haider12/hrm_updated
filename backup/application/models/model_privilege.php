<?php
class Model_Privilege extends Model_Master {
	
	private $colIdentifier 				= 'user_permission_id';
	
	function __construct() {
		 parent::__construct();	
	}
		
	function getTopPrivileges($roleID = 0) 
	{
		$this->db->select('up.sub_module_id as module_id, m.module_name, m.display_name');
		$this->db->from(TABLE_MODULE . ' m ');
		$this->db->join(TABLE_USER_PERMISSIONS . ' up ', 'up.sub_module_id = m.module_id');
		$this->db->where('up.user_role_id', $roleID);
		$this->db->where('up.can_read', '1');
		$this->db->where('up.status', '1');	
		$this->db->where('up.module_id <= ', $this->homeModuleID, true);	
		$this->db->where('m.module_skip_display', '0');
		$this->db->order_by('module_sort_order', 'ASC');
		$results = $this->db->get();
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;		
	}
	
	function getPrivilege($roleID = 0, $moduleID='', $skipDisplay = false) 
	{		
		$this->db->distinct();
		$this->db->select('up.module_id, m.module_name,m.display_name,up.can_write,up.can_delete');
		$this->db->from(TABLE_USER_PERMISSIONS . ' up ');
		$this->db->join(TABLE_MODULE . ' m ', 'up.sub_module_id = m.module_id');
		$this->db->where('up.user_role_id', $roleID);
		if($moduleID != '')
			$this->db->where('up.module_id', $moduleID);
		$this->db->where('up.can_read', '1');
		$this->db->where('up.status', '1');
		if($skipDisplay) {			
			$this->db->where('m.module_skip_display', '0');
		}
		$this->db->order_by('module_sort_order', 'ASC');
		$results = $this->db->get();
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;		
	}
	
	function getScreenLayout($screenName = 'welcome', $controllerId = '', $tableName = TABLE_MODULE) 
	{	
		$this->db->select(' l.layout_name as layout ');
		$this->db->join(TABLE_LAYOUT . ' l ', 'm.layout = l.layout_id', 'left');
		$this->db->where('m.module_name', $screenName);
		if(!empty($controllerId))
			$this->db->where('m.module_parent_id', $controllerId);
		$results = $this->db->get($tableName . ' m ');
		$arrResult = $results->result_array();
		$results->free_result();
		return $arrResult[0]['layout'];		
	}
		
	function getSpecialPrivileges($empID = null, $deptID = null, $roleID = null, $getTop = true, $moduleID = 0) {
		
		$this->db->distinct();
		$this->db->select(' m.module_name, m.module_id, sp.emp_id, sp.job_category_id, sp.user_role_id, sp.assigned_role_id, m.display_name ');
		
		if($getTop) {					
			$this->db->join(TABLE_MODULE . ' m ', 'm.module_id = sp.module_id', 'left');
			$this->db->where('m.module_parent_id', 1);			
		} else if((int)$moduleID) {
			$this->db->join(TABLE_MODULE . ' m ', 'm.module_id = sp.sub_module_id', 'left');
			$this->db->where('m.module_parent_id', (int)$moduleID);			
		}
		
		$this->db->where('(sp.emp_id = ' . (int)$empID . ' OR sp.job_category_id = ' . (int)$deptID . ' OR sp.user_role_id = ' . (int)$roleID . ')');
		$this->db->where('m.module_skip_display', '0');
		$this->db->order_by('module_sort_order', 'ASC');
		
		$objResult = $this->db->get(TABLE_USER_SPECIAL_PERMISSIONS . ' sp ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
		
	function moduleScreenExists($strModule = '') {
		
		if(!empty($strModule)) {
			
			$this->db->select(' count(*) as total_records ');
			$this->db->where('module_name', $strModule);
			
			$objResult = $this->db->get(TABLE_MODULE);
			$arrResult = $objResult->result_array();
			$objResult->free_result();
			return ((int)$arrResult[0]['total_records'] > 0) ? true : false;
		}
		return false;
	}
}
?>