<?php
class Model_Quality_Control extends Model_Master {
	
	public $strHierarchy;
	
	function __construct() {
		 parent::__construct();	
	}
	
	function deleteValue($tblName, $arrDeleteValues = array(), $colStatus = '', $valStatus = 0) {
		
		if(count($arrDeleteValues)) {
			
			$this->db->where($arrDeleteValues);
			
			$arrValues = array(
								'deleted_by' => $this->userEmpNum,
								'deleted_date' => date(DATE_TIME_FORMAT)
								);
								
			if(!empty($colStatus) && strlen($colStatus) > 7 && is_numeric($valStatus)) {
				$arrValues[$colStatus] = $valStatus;
			}
			
			$this->db->update($tblName, $arrValues, $arrDeleteValues);
			
			return ($this->db->affected_rows() > 0);			
		}
	}
	
	function getReports($arrWhere = array(), $rowsLimit = '', $rowsOffset = '')
	{	
		$this->db->select(' qr.*, CONCAT(e1.emp_first_name, \' \', e1.emp_last_name) AS uploaded_by,e1.emp_ip_num AS uploaded_by_emp_ip_num ', FALSE);
		$this->db->join(TABLE_EMPLOYEE . ' e1 ', 'e1.emp_id = qr.uploaded_by', 'left');
				
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('uploaded_date', DESC);
		
		$results = $this->db->get(TABLE_QC_REPORTS . ' qr ');
		$arrResult = $results->result_array();
		$results->free_result();
						
		return $arrResult;
	}
	
	function getTotalReports($arrWhere = array())
	{	
		$this->db->select(' count(*) as total ');
		$this->db->join(TABLE_EMPLOYEE . ' e1 ', 'e1.emp_id = qr.uploaded_by', 'left');
				
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
				
		$results = $this->db->get(TABLE_QC_REPORTS . ' qr ');
		$arrResult = $results->result_array();
		$results->free_result();
						
		return $arrResult;
	}
	
	function getCDRReportDetails($arrWhere = array(), $rowsLimit = '', $rowsOffset = '')
	{	
		$this->db->select(' qr.qc_report_type_desc, e.emp_ip_num, qcr.* ', FALSE);
		$this->db->join(TABLE_QC_REPORTS . ' qr ', 'qr.report_id = qcr.report_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = qcr.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if($arrWhere['qcr.lm_code'] != '') {
			$this->db->where('(qcr.lm_code = ' .$arrWhere['qcr.lm_code']. ' OR qcr.sm_code = ' .$arrWhere['qcr.lm_code'].')');
			unset($arrWhere['qcr.lm_code']);
		}
		
		if($arrWhere['e.emp_name'] != '') {
			$this->db->where('(e.emp_first_name like \'%' . $arrWhere['e.emp_name'] . '%\' or e.emp_last_name like \'%' . $arrWhere['e.emp_name'] . '%\')'); 
			unset($arrWhere['e.emp_name']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('qr.qc_report_type_desc', DESC);
		$this->db->order_by('qcr.cdr_report_id', DESC);
		
		$results = $this->db->get(TABLE_QC_CDR_REPORTS . ' qcr ');
		$arrResult = $results->result_array();
		$results->free_result();
						
		return $arrResult;
	}
	
	
	function getTotalCDRReportDetails($arrWhere = array())
	{	
		$this->db->select(' count(*) as total ');
		$this->db->join(TABLE_QC_REPORTS . ' qr ', 'qr.report_id = qcr.report_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = qcr.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if($arrWhere['qcr.lm_code'] != '') {
			$this->db->where('(qcr.lm_code = ' .$arrWhere['qcr.lm_code']. ' OR qcr.sm_code = ' .$arrWhere['qcr.lm_code'].')');
			unset($arrWhere['qcr.lm_code']);
		}
		
		if($arrWhere['e.emp_name'] != '') {
			$this->db->where('(e.emp_first_name like \'%' . $arrWhere['e.emp_name'] . '%\' or e.emp_last_name like \'%' . $arrWhere['e.emp_name'] . '%\')'); 
			unset($arrWhere['e.emp_name']);
		}
			
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		$this->db->order_by('qr.qc_report_type_desc', DESC);
		$this->db->order_by('qcr.cdr_report_id', DESC);
				
		$results = $this->db->get(TABLE_QC_CDR_REPORTS . ' qcr ');
		$arrResult = $results->result_array();
		$results->free_result();
						
		return $arrResult;
	}
	
	function getCallingReportDetails($arrWhere = array(), $rowsLimit = '', $rowsOffset = '')
	{	
		$this->db->select(' qr.qc_report_type_desc, e.emp_ip_num, qcar.* ', FALSE);
		$this->db->join(TABLE_QC_REPORTS . ' qr ', 'qr.report_id = qcar.report_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = qcar.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if($arrWhere['qcar.lm_code'] != '') {
			$this->db->where('(qcar.lm_code = ' .$arrWhere['qcar.lm_code']. ' OR qcar.sm_code = ' .$arrWhere['qcar.lm_code'].')');
			unset($arrWhere['qcar.lm_code']);
		}
		
		if($arrWhere['e.emp_name'] != '') {
			$this->db->where('(e.emp_first_name like \'%' . $arrWhere['e.emp_name'] . '%\' or e.emp_last_name like \'%' . $arrWhere['e.emp_name'] . '%\')'); 
			unset($arrWhere['e.emp_name']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('qr.qc_report_type_desc', DESC);
		
		$results = $this->db->get(TABLE_QC_CALLING_REPORTS . ' qcar ');
		$arrResult = $results->result_array();
		$results->free_result();
						
		return $arrResult;
	}
	
	
	function getTotalCallingReportDetails($arrWhere = array())
	{	
		$this->db->select(' count(*) as total ');
		$this->db->join(TABLE_QC_REPORTS . ' qr ', 'qr.report_id = qcar.report_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = qcar.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if($arrWhere['qcar.lm_code'] != '') {
			$this->db->where('(qcar.lm_code = ' .$arrWhere['qcar.lm_code']. ' OR qcar.sm_code = ' .$arrWhere['qcar.lm_code'].')');
			unset($arrWhere['qcar.lm_code']);
		}
		
		if($arrWhere['e.emp_name'] != '') {
			$this->db->where('(e.emp_first_name like \'%' . $arrWhere['e.emp_name'] . '%\' or e.emp_last_name like \'%' . $arrWhere['e.emp_name'] . '%\')'); 
			unset($arrWhere['e.emp_name']);
		}
			
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		$this->db->order_by('qr.qc_report_type_desc', DESC);
		
		$results = $this->db->get(TABLE_QC_CALLING_REPORTS . ' qcar ');
		$arrResult = $results->result_array();
		$results->free_result();
						
		return $arrResult;
	}
	
	function getAddressVerificationReportDetails($arrWhere = array(), $rowsLimit = '', $rowsOffset = '')
	{	
		$this->db->select(' qr.qc_report_type_desc, e.emp_ip_num, qavr.* ', FALSE);
		$this->db->join(TABLE_QC_REPORTS . ' qr ', 'qr.report_id = qavr.report_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = qavr.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if($arrWhere['qavr.lm_code'] != '') {
			$this->db->where('(qavr.lm_code = ' .$arrWhere['qavr.lm_code']. ' OR qavr.sm_code = ' .$arrWhere['qavr.lm_code'].')');
			unset($arrWhere['qavr.lm_code']);
		}
		
		if($arrWhere['e.emp_name'] != '') {
			$this->db->where('(e.emp_first_name like \'%' . $arrWhere['e.emp_name'] . '%\' or e.emp_last_name like \'%' . $arrWhere['e.emp_name'] . '%\')'); 
			unset($arrWhere['e.emp_name']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('qr.qc_report_type_desc', DESC);
		
		$results = $this->db->get(TABLE_QC_ADDRESS_VERIFICATION_REPORTS . ' qavr ');
		$arrResult = $results->result_array();
		$results->free_result();
						
		return $arrResult;
	}
	
	
	function getTotalAddressVerificationReportDetails($arrWhere = array())
	{	
		$this->db->select(' count(*) as total ');
		$this->db->join(TABLE_QC_REPORTS . ' qr ', 'qr.report_id = qavr.report_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = qavr.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if($arrWhere['qavr.lm_code'] != '') {
			$this->db->where('(qavr.lm_code = ' .$arrWhere['qavr.lm_code']. ' OR qavr.sm_code = ' .$arrWhere['qavr.lm_code'].')');
			unset($arrWhere['qavr.lm_code']);
		}
		
		if($arrWhere['e.emp_name'] != '') {
			$this->db->where('(e.emp_first_name like \'%' . $arrWhere['e.emp_name'] . '%\' or e.emp_last_name like \'%' . $arrWhere['e.emp_name'] . '%\')'); 
			unset($arrWhere['e.emp_name']);
		}
			
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		$this->db->order_by('qr.qc_report_type_desc', DESC);
				
		$results = $this->db->get(TABLE_QC_ADDRESS_VERIFICATION_REPORTS . ' qavr ');
		$arrResult = $results->result_array();
		$results->free_result();
						
		return $arrResult;
	}
	
	function getConsigneeVerificationReportDetails($arrWhere = array(), $rowsLimit = '', $rowsOffset = '')
	{	
		$this->db->select(' qr.qc_report_type_desc, e.emp_ip_num, qcvr.* ', FALSE);
		$this->db->join(TABLE_QC_REPORTS . ' qr ', 'qr.report_id = qcvr.report_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = qcvr.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if($arrWhere['qcvr.lm_code'] != '') {
			$this->db->where('(qcvr.lm_code = ' .$arrWhere['qcvr.lm_code']. ' OR qcvr.sm_code = ' .$arrWhere['qcvr.lm_code'].')');
			unset($arrWhere['qcvr.lm_code']);
		}
		
		if($arrWhere['e.emp_name'] != '') {
			$this->db->where('(e.emp_first_name like \'%' . $arrWhere['e.emp_name'] . '%\' or e.emp_last_name like \'%' . $arrWhere['e.emp_name'] . '%\')'); 
			unset($arrWhere['e.emp_name']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('qr.qc_report_type_desc', DESC);
		
		$results = $this->db->get(TABLE_QC_CONSIGNEE_VERIFICATION_REPORTS . ' qcvr ');
		$arrResult = $results->result_array();
		$results->free_result();
						
		return $arrResult;
	}
	
	
	function getTotalConsigneeVerificationReportDetails($arrWhere = array())
	{	
		$this->db->select(' count(*) as total ');
		$this->db->join(TABLE_QC_REPORTS . ' qr ', 'qr.report_id = qcvr.report_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = qcvr.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if($arrWhere['qcvr.lm_code'] != '') {
			$this->db->where('(qcvr.lm_code = ' .$arrWhere['qcvr.lm_code']. ' OR qcvr.sm_code = ' .$arrWhere['qcvr.lm_code'].')');
			unset($arrWhere['qcvr.lm_code']);
		}
		
		if($arrWhere['e.emp_name'] != '') {
			$this->db->where('(e.emp_first_name like \'%' . $arrWhere['e.emp_name'] . '%\' or e.emp_last_name like \'%' . $arrWhere['e.emp_name'] . '%\')'); 
			unset($arrWhere['e.emp_name']);
		}
			
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		$this->db->order_by('qr.qc_report_type_desc', DESC);
				
		$results = $this->db->get(TABLE_QC_CONSIGNEE_VERIFICATION_REPORTS . ' qcvr ');
		$arrResult = $results->result_array();
		$results->free_result();
						
		return $arrResult;
	}
	
	function getReserveVerificationReportDetails($arrWhere = array(), $rowsLimit = '', $rowsOffset = '')
	{	
		$this->db->select(' qr.qc_report_type_desc, e.emp_ip_num, qrvr.* ', FALSE);
		$this->db->join(TABLE_QC_REPORTS . ' qr ', 'qr.report_id = qrvr.report_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = qrvr.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if($arrWhere['qrvr.lm_code'] != '') {
			$this->db->where('(qrvr.lm_code = ' .$arrWhere['qrvr.lm_code']. ' OR qrvr.sm_code = ' .$arrWhere['qrvr.lm_code'].')');
			unset($arrWhere['qrvr.lm_code']);
		}
		
		if($arrWhere['e.emp_name'] != '') {
			$this->db->where('(e.emp_first_name like \'%' . $arrWhere['e.emp_name'] . '%\' or e.emp_last_name like \'%' . $arrWhere['e.emp_name'] . '%\')'); 
			unset($arrWhere['e.emp_name']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('qr.qc_report_type_desc', DESC);
		
		$results = $this->db->get(TABLE_QC_RESERVE_VERIFICATION_REPORTS . ' qrvr ');
		$arrResult = $results->result_array();
		$results->free_result();
						
		return $arrResult;
	}
	
	
	function getTotalReserveVerificationReportDetails($arrWhere = array())
	{	
		$this->db->select(' count(*) as total ');
		$this->db->join(TABLE_QC_REPORTS . ' qr ', 'qr.report_id = qrvr.report_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = qrvr.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if($arrWhere['qrvr.lm_code'] != '') {
			$this->db->where('(qrvr.lm_code = ' .$arrWhere['qrvr.lm_code']. ' OR qrvr.sm_code = ' .$arrWhere['qrvr.lm_code'].')');
			unset($arrWhere['qrvr.lm_code']);
		}
		
		if($arrWhere['e.emp_name'] != '') {
			$this->db->where('(e.emp_first_name like \'%' . $arrWhere['e.emp_name'] . '%\' or e.emp_last_name like \'%' . $arrWhere['e.emp_name'] . '%\')'); 
			unset($arrWhere['e.emp_name']);
		}
			
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		$this->db->order_by('qr.qc_report_type_desc', DESC);
				
		$results = $this->db->get(TABLE_QC_RESERVE_VERIFICATION_REPORTS . ' qrvr ');
		$arrResult = $results->result_array();
		$results->free_result();
						
		return $arrResult;
	}
	
	function getSalesStatsReportDetails($arrWhere = array(), $rowsLimit = '', $rowsOffset = '')
	{	
		$this->db->select(' qssm.*, qr.qc_report_type_id, qr.qc_report_type_desc', FALSE);
		$this->db->join(TABLE_QC_REPORTS . ' qr ', 'qr.report_id = qssm.report_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}	
		
		$results = $this->db->get(TABLE_QC_SALES_STATS_MONTHLY . ' qssm ');
		$arrResult = $results->result_array();
		$results->free_result();
						
		return $arrResult;
	}
	
	function getCallMonitoringReportDetails($arrWhere = array(), $rowsLimit = '', $rowsOffset = '')
	{	
		$this->db->select(' qr.qc_report_type_id, qr.qc_report_type_desc, e.emp_ip_num, qcmr.*', FALSE);
		$this->db->join(TABLE_QC_REPORTS . ' qr ', 'qr.report_id = qcmr.report_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = qcmr.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if($arrWhere['qcmr.lm_code'] != '') {
			$this->db->where('(qcmr.lm_code = ' .$arrWhere['qcmr.lm_code']. ' OR qcmr.sm_code = ' .$arrWhere['qcmr.lm_code'].')');
			unset($arrWhere['qcmr.lm_code']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('qr.qc_report_type_desc', DESC);
		
		$results = $this->db->get(TABLE_QC_CALL_MONITORING_REPORTS . ' qcmr ');
		$arrResult = $results->result_array();
		$results->free_result();
						
		return $arrResult;
	}
	
	function getTotalCallMonitoringReportDetails($arrWhere = array())
	{	
		$this->db->select(' count(*) as total ');
		$this->db->join(TABLE_QC_REPORTS . ' qr ', 'qr.report_id = qcmr.report_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = qcmr.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if($arrWhere['qcmr.lm_code'] != '') {
			$this->db->where('(qcmr.lm_code = ' .$arrWhere['qcmr.lm_code']. ' OR qcmr.sm_code = ' .$arrWhere['qcmr.lm_code'].')');
			unset($arrWhere['qcmr.lm_code']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		$this->db->order_by('qr.qc_report_type_desc', DESC);
		
		$results = $this->db->get(TABLE_QC_CALL_MONITORING_REPORTS . ' qcmr ');
		$arrResult = $results->result_array();
		$results->free_result();
						
		return $arrResult;
	}
	
	function getPmtReportDetails($arrWhere = array(), $rowsLimit = '', $rowsOffset = '')
	{	
		$this->db->select(' qr.qc_report_type_desc, e.emp_ip_num, qpr.* ', FALSE);
		$this->db->join(TABLE_QC_REPORTS . ' qr ', 'qr.report_id = qpr.report_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = qpr.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if($arrWhere['qpr.lm_code'] != '') {
			$this->db->where('(qpr.lm_code = ' .$arrWhere['qpr.lm_code']. ' OR qpr.sm_code = ' .$arrWhere['qpr.lm_code'].')');
			unset($arrWhere['qpr.lm_code']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('qr.qc_report_type_desc', DESC);
		
		$results = $this->db->get(TABLE_QC_PMT_REPORTS . ' qpr ');
		$arrResult = $results->result_array();
		$results->free_result();
						
		return $arrResult;
	}
	
	function getTotalPmtReportDetails($arrWhere = array())
	{	
		$this->db->select(' count(*) as total ');
		$this->db->join(TABLE_QC_REPORTS . ' qr ', 'qr.report_id = qpr.report_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = qpr.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if($arrWhere['qpr.lm_code'] != '') {
			$this->db->where('(qpr.lm_code = ' .$arrWhere['qpr.lm_code']. ' OR qpr.sm_code = ' .$arrWhere['qpr.lm_code'].')');
			unset($arrWhere['qpr.lm_code']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		$this->db->order_by('qr.qc_report_type_desc', DESC);
		
		$results = $this->db->get(TABLE_QC_PMT_REPORTS . ' qpr ');
		$arrResult = $results->result_array();
		$results->free_result();
						
		return $arrResult;
	}
	
	function getIsmbuPmtReportDetails($arrWhere = array(), $rowsLimit = '', $rowsOffset = '')
	{	
		$this->db->select(' qr.qc_report_type_desc, e.emp_ip_num, qipr.* ', FALSE);
		$this->db->join(TABLE_QC_REPORTS . ' qr ', 'qr.report_id = qipr.report_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = qipr.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('qr.qc_report_type_desc', DESC);
		
		$results = $this->db->get(TABLE_QC_ISMBU_PMT_REPORTS . ' qipr ');
		$arrResult = $results->result_array();
		$results->free_result();
						
		return $arrResult;
	}
	
	function getTotalIsmbuPmtReportDetails($arrWhere = array())
	{	
		$this->db->select(' count(*) as total ');
		$this->db->join(TABLE_QC_REPORTS . ' qr ', 'qr.report_id = qipr.report_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = qipr.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		$this->db->order_by('qr.qc_report_type_desc', DESC);
		
		$results = $this->db->get(TABLE_QC_ISMBU_PMT_REPORTS . ' qipr ');
		$arrResult = $results->result_array();
		$results->free_result();
						
		return $arrResult;
	}
	
	function getOffenses($arrWhere = array(), $arrCheck = array(), $rowsLimit = '', $rowsOffset = '')
	{	
		$this->db->select('DISTINCT qo.*, e.emp_full_name AS empName, e.emp_ip_num AS empIP, e.emp_photo_name, e.emp_gender ', FALSE);
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = qo.emp_id', 'left');
		if($arrCheck['qop.emp_id']) {
			$this->db->join(TABLE_QC_OFFENSES_PERSON . ' qop ', 'qop.offense_id = qo.offense_id', 'left');
		}
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
				$this->db->where($arrWhere);
		}
		
		if($arrCheck['qop.emp_id']) {
			$this->db->or_where('qop.emp_id', $arrCheck['qop.emp_id']);
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('qo.offense_status', ASC);
		$this->db->order_by('qo.offense_id', DESC);
		
		$results = $this->db->get(TABLE_QC_OFFENSES . ' qo ');
		$arrResult = $results->result_array();
		$results->free_result();
						
		return $arrResult;
	}
	
	function getTotalOffenses($arrWhere = array(), $arrCheck = array())
	{	
		$this->db->select(' count(*) as total ');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = qo.emp_id', 'left');
		if($arrCheck['qop.emp_id']) {
			$this->db->join(TABLE_QC_OFFENSES_PERSON . ' qop ', 'qop.offense_id = qo.offense_id', 'left');
		}
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
				$this->db->where($arrWhere);
		}
		
		if($arrCheck['qop.emp_id']) {
			$this->db->or_where('qop.emp_id', $arrCheck['qop.emp_id']);
		}
		
		$this->db->order_by('qo.offense_status', ASC);
		
		$results = $this->db->get(TABLE_QC_OFFENSES . ' qo ');
		$arrResult = $results->result_array();
		$results->free_result();
						
		return $arrResult;
	}
	
	function getOffensePersonDetails($arrWhere = array())
	{	
		$this->db->select(' e.emp_full_name AS concernedPersonName, e.emp_ip_num AS concernedPersonIP ', FALSE);
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = qop.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		$results = $this->db->get(TABLE_QC_OFFENSES_PERSON . ' qop ');
		$arrResult = $results->result_array();
		$results->free_result();
						
		return $arrResult;
	}
	
	function getOffenseDetails($arrWhere = array())
	{	
		$this->db->select(' qod.*, e.emp_full_name, e.emp_ip_num, e.emp_photo_name, e.emp_gender ', FALSE);
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = qod.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);		
		}
		
		$results = $this->db->get(TABLE_QC_OFFENSES_DETAILS . ' qod ');
		$arrResult = $results->result_array();
		$results->free_result();
						
		return $arrResult;
	}
}
?>