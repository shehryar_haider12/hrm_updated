<?php
class Model_Inventory_Management extends Model_Master {
	
	function __construct() {
		 parent::__construct();	
	}
	
	function getTotalProductTypes($arrWhere = array())
	{		
		$this->db->select(' count(*) as total ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$objResult = $this->db->get(TABLE_INVENTORY_PRODUCT_TYPES);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getProductTypes($arrWhere = array()) 
	{
		$this->db->select(' * ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);
		}
				
		$objResult = $this->db->get(TABLE_INVENTORY_PRODUCT_TYPES);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getProductFrequency($arrWhere = array()) 
	{
		$this->db->select(' * ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);
		}
				
		$objResult = $this->db->get(TABLE_INVENTORY_PRODUCT_FREQUENCY);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getProductMetrics($arrWhere = array()) 
	{
		$this->db->select(' * ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);
		}
				
		$objResult = $this->db->get(TABLE_INVENTORY_PRODUCT_METRICS);
		$arrResult = $objResult->result_array();
		
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getProducts($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') 
	{
		$this->db->select(' ip.product_id,ip.product_name,ip.product_reorder_quantity,ip.product_status,ipt.product_type,ipf.product_frequency,ipm.product_metric ');
		
		if($arrWhere['ip.product_name'] != '') {
			$this->db->where('(ip.product_name like \'%' . $arrWhere['ip.product_name'] . '%\')'); 
			unset($arrWhere['ip.product_name']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		if(!isset($_POST['sort_field'])) {
			$this->db->order_by('ip.product_status', 'ASC');
			$this->db->order_by('ip.product_id', 'ASC');
		} else {
			$sortColumn = $_POST['sort_field'];
			$sortOrder = $_POST['sort_order'];
			
			if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
				$this->db->order_by('ip.' . $sortColumn, $sortOrder);
			}
		}
		
		$this->db->join(TABLE_INVENTORY_PRODUCT_TYPES . ' ipt ', 'ipt.product_type_id = ip.product_type_id', 'left');
		$this->db->join(TABLE_INVENTORY_PRODUCT_FREQUENCY . ' ipf ', 'ipf.product_frequency_id = ip.product_frequency_id', 'left');
		$this->db->join(TABLE_INVENTORY_PRODUCT_METRICS . ' ipm ', 'ipm.product_metric_id = ip.product_metric_id', 'left');
		$results = $this->db->get(TABLE_INVENTORY_PRODUCTS . ' ip ');
		$arrResult = $results->result_array();
		$results->free_result();
				
		return $arrResult;
	}	
	
	function getTotalProducts($arrWhere = array())
	{		
		$this->db->select(' count(*) as total ');
		
		if($arrWhere['ip.product_name'] != '') {
			$this->db->where('(ip.product_name like \'%' . $arrWhere['ip.product_name'] . '%\')'); 
			unset($arrWhere['ip.product_name']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$this->db->join(TABLE_INVENTORY_PRODUCT_TYPES . ' ipt ', 'ipt.product_type_id = ip.product_type_id', 'left');
		$this->db->join(TABLE_INVENTORY_PRODUCT_FREQUENCY . ' ipf ', 'ipf.product_frequency_id = ip.product_frequency_id', 'left');
		$this->db->join(TABLE_INVENTORY_PRODUCT_METRICS . ' ipm ', 'ipm.product_metric_id = ip.product_metric_id', 'left');
		$results = $this->db->get(TABLE_INVENTORY_PRODUCTS . ' ip ');
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;
	}
	
	function getVendors($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') 
	{
		$this->db->select(' * ');
		
		if($arrWhere['vendor_name'] != '') {
			$this->db->where('(vendor_name like \'%' . $arrWhere['vendor_name'] . '%\')'); 
			unset($arrWhere['vendor_name']);
		}
		
		if($arrWhere['vendor_person_name'] != '') {
			$this->db->where('(vendor_person_name1 like \'%' . $arrWhere['vendor_person_name'] . '%\' or vendor_person_name2 like \'%' . $arrWhere['vendor_person_name'] . '%\')'); 
			unset($arrWhere['vendor_person_name']);
		}
		
		if($arrWhere['vendor_nic_number'] != '') {
			$this->db->where('(vendor_nic_number like \'%' . $arrWhere['vendor_nic_number'] . '%\')'); 
			unset($arrWhere['vendor_nic_number']);
		}
		
		if($arrWhere['vendor_ntn_number'] != '') {
			$this->db->where('(vendor_ntn_number like \'%' . $arrWhere['vendor_ntn_number'] . '%\')'); 
			unset($arrWhere['vendor_ntn_number']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		
		$results = $this->db->get(TABLE_INVENTORY_VENDORS);
		$arrResult = $results->result_array();
		$results->free_result();
				
		return $arrResult;
	}
	
	function getTotalVendors($arrWhere = array())
	{		
		$this->db->select(' count(*) as total ');
		
		if($arrWhere['vendor_name'] != '') {
			$this->db->where('(vendor_name like \'%' . $arrWhere['vendor_name'] . '%\')'); 
			unset($arrWhere['vendor_name']);
		}
		
		if($arrWhere['vendor_person_name'] != '') {
			$this->db->where('(vendor_person_name1 like \'%' . $arrWhere['vendor_person_name'] . '%\' or vendor_person_name2 like \'%' . $arrWhere['vendor_person_name'] . '%\')'); 
			unset($arrWhere['vendor_person_name']);
		}
		
		if($arrWhere['vendor_ntn_number'] != '') {
			$this->db->where('(vendor_ntn_number like \'%' . $arrWhere['vendor_ntn_number'] . '%\')'); 
			unset($arrWhere['vendor_ntn_number']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$results = $this->db->get(TABLE_INVENTORY_VENDORS);
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;
	}
	
	function getTotalVendorsProducts($arrWhere = array())
	{		
		$this->db->select(' count(*) as total ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$this->db->join(TABLE_INVENTORY_VENDORS . ' iv ', 'iv.vendor_id = ivp.vendor_id', 'left');
		$this->db->join(TABLE_INVENTORY_PRODUCTS . ' ip ', 'ip.product_id = ivp.product_id', 'left');
		$results = $this->db->get(TABLE_INVENTORY_VENDORS_PRODUCTS . ' ivp ');
		$arrResult = $results->result_array();
		$results->free_result();
		
		return $arrResult;
	}
	
	function getVendorsProducts($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') 
	{
		$this->db->select(' ivp.vendor_product_id, ivp.product_unit_price, ivp.comments, ivp.vendor_product_status, ivp.vendor_priority, iv.vendor_name, ip.product_name ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->join(TABLE_INVENTORY_VENDORS . ' iv ', 'iv.vendor_id = ivp.vendor_id', 'left');
		$this->db->join(TABLE_INVENTORY_PRODUCTS . ' ip ', 'ip.product_id = ivp.product_id', 'left');
		$results = $this->db->get(TABLE_INVENTORY_VENDORS_PRODUCTS . ' ivp ');
		$arrResult = $results->result_array();
		$results->free_result();
				
		return $arrResult;
	}
	
}
?>