<?php
class Model_Task_Management extends Model_Master {
	
	public $strHierarchy;
	
	function __construct() {
		 parent::__construct();	
	}
	
	function getTimeStamps($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db->distinct();
		$this->db->select(' ea.created_date ');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}		
		
		if(!isset($_POST['sort_field'])) {
			$this->db->order_by('ea.created_date', 'DESC');
		} else {
			$sortColumn = $_POST['sort_field'];
			$sortOrder = $_POST['sort_order'];
			
			if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
				$this->db->order_by($sortColumn, $sortOrder);
			}
		}
		
		$objResult = $this->db->get(TABLE_TASK_EMPLOYEE_ACTIVITIES . ' ea ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getActivities($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db->select(' ea.*, e.emp_full_name, e.emp_ip_num ');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'ea.emp_id = e.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}		
		
		if(!isset($_POST['sort_field'])) {
			$this->db->order_by('ea.created_date', 'DESC');
		} else {
			$sortColumn = $_POST['sort_field'];
			$sortOrder = $_POST['sort_order'];
			
			if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
				$this->db->order_by($sortColumn, $sortOrder);
			}
		}
		
		$objResult = $this->db->get(TABLE_TASK_EMPLOYEE_ACTIVITIES . ' ea ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getActivitiesCount($arrWhere = array()) {
		
		$this->db->select(' count(*) as total_count ');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'ea.emp_id = e.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$objResult = $this->db->get(TABLE_TASK_EMPLOYEE_ACTIVITIES . ' ea ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return (int)$arrResult[0]['total_count'];
	}
	
	function getActivityDocs($activityID) {
				
		$this->db->where('activity_id', $activityID);
		
		$objResult = $this->db->get(TABLE_TASK_EMPLOYEE_ACTIVITIES_DOCS);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTasks($arrWhere = array()) {
		
		$this->db->select(' * ');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
				
		$this->db->order_by('assessment_id', 'ASC');
		
		$objResult = $this->db->get(TABLE_TASK_EMPLOYEE_PERFORMANCE);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getKPIS($arrWhere = array()) {
		
		$this->db->select(' * ');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
				
		$this->db->order_by('review_id', 'ASC');
		
		$objResult = $this->db->get(TABLE_TASK_EMPLOYEE_PERFORMANCE_KPIS);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getMonthTasksStatus($arrWhere = array()) {
		
		$this->db->select(' pe_status ');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
				
		$objResult = $this->db->get(TABLE_TASK_EMPLOYEE_PERFORMANCE_STATUS);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult[0];
	}
	
	function getTaskSummary($arrWhere = array())
	{
		$this->db->select(' * ');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
				
		$objResult = $this->db->get(TABLE_TASK_EMPLOYEE_PERFORMANCE_SUMMARY);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult[0];
	}
	
	function getTaskStatus($arrWhere = array()) {
		
		$this->db->select(' * ');
				
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$this->db->order_by('id');
				
		$objResult = $this->db->get(TABLE_TASK_EMPLOYEE_PERFORMANCE_STATUS);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTaskDetails($arrWhere = array())
	{
		$this->db->select(' ep.pe_id, ep.emp_id, ep.month, ep.year, eps.total_weighted_score ');
		$this->db->join(TABLE_TASK_EMPLOYEE_PERFORMANCE_SUMMARY . ' eps ', 'eps.pe_id = ep.pe_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
				
		$objResult = $this->db->get(TABLE_TASK_EMPLOYEE_PERFORMANCE . ' ep ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult[0];
	}
	
	function getTaskID($empID, $arrWhere = array())
	{
		$this->db->distinct();
		$this->db->select(' ep.pe_id, eps.pe_status ');
		$this->db->join(TABLE_TASK_EMPLOYEE_PERFORMANCE_STATUS . ' eps ', 'eps.pe_id = ep.pe_id', 'left');
		
		if($arrWhere['department'])
		{
			$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = ep.emp_id', 'left');
		}
		
		$this->db->where('ep.emp_id in ', '('.$empID.')', false);
		
		if($arrWhere['department'])
		{
			$this->db->where('e.emp_job_category_id =', $arrWhere['department']);
		}
		
		if($arrWhere['supervisor'])
		{
			$this->db->where('es.supervisor_emp_id =', $arrWhere['supervisor']);
			$this->db->where('e.emp_id =', $arrWhere['supervisor']);
		}
		
		if($arrWhere['year']) {		
			$this->db->where('ep.year =', $arrWhere['year']);
		}
		
		if($arrWhere['month']) {		
			$this->db->where('ep.month =', $arrWhere['month']);
		}
		
		if($arrWhere['fromYear']) {
			$this->db->where('ep.year >=', $arrWhere['fromYear']);
		}
		
		if($arrWhere['fromMonth']) {
			$this->db->where('ep.month >=', $arrWhere['fromMonth']);
		}
		
		if($arrWhere['toYear']) {		
			$this->db->where('ep.year <=', $arrWhere['toYear']);
		}
		
		if($arrWhere['toMonth']) {		
			$this->db->where('ep.month <=', $arrWhere['toMonth']);
		}
		
		$this->db->order_by('ep.year');
		$this->db->order_by('ep.month');
		$this->db->order_by('ep.emp_id');
		
		$objResult = $this->db->get(TABLE_TASK_EMPLOYEE_PERFORMANCE . ' ep ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTeamEmployeesID($empID) {
		$this->db->select(' e.emp_id ');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.emp_id = e.emp_id', 'left');
		
		$this->db->where('es.supervisor_emp_id', $empID);		
		$this->db->where('e.emp_status', '1');
		$this->db->where('e.emp_code > ','0');
			
		$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
				
		return $arrResult;
	}
	
	function getActivityThread($arrWhere = array())	{
		
		$this->db->select(' eal.*, e.emp_full_name AS response_by, e.emp_id, e.emp_gender, e.emp_work_email, e.emp_photo_name ', FALSE);
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = eal.thread_txt_by', 'left');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
				
		$objResult = $this->db->get(TABLE_TASK_EMPLOYEE_ACTIVITY_THREAD . ' eal ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTotalEmployeesForPerformanceSubmission($arrWhere = array(), $staffFlag=0)	{
		$this->db->select(' e.emp_id, e.emp_job_category_id, jc.job_category_name, e.emp_code, e.emp_full_name, e.emp_ip_num, e.emp_work_email, e.emp_authority_id ');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.emp_id = e.emp_id', 'left');
		$this->db->join(TABLE_JOB_CATEGORY . ' jc ', 'jc.job_category_id = e.emp_job_category_id', 'left');
		
		if($staffFlag)
		{
			$this->db->join(TABLE_USER . ' u ', 'u.employee_id = e.emp_id', 'left');
		}
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$this->db->where('e.emp_status', STATUS_ACTIVE);
		$this->db->where('e.emp_employment_status < ','6');
		$this->db->where('e.emp_id > ','3');
		
		$this->db->order_by('e.emp_id', 'ASC');
		$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;		
	}
	
	function getTotalEmployeesSubmittedPerformance($arrWhere = array(), $staffFlag=0)	{
		$this->db->distinct();
		$this->db->select(' e.emp_id, e.emp_job_category_id, e.emp_authority_id, jc.job_category_name, e.emp_code, e.emp_full_name, e.emp_ip_num, e.emp_work_email, ep.pe_id, eps.pe_status ');
		
		$this->db->join(TABLE_JOB_CATEGORY . ' jc ', 'jc.job_category_id = e.emp_job_category_id', 'left');
		$this->db->join(TABLE_TASK_EMPLOYEE_PERFORMANCE . ' ep ', 'ep.emp_id = e.emp_id', 'left');
		$this->db->join(TABLE_TASK_EMPLOYEE_PERFORMANCE_STATUS . ' eps ', 'eps.pe_id = ep.pe_id', 'left');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.emp_id = e.emp_id', 'left');
		
		if($staffFlag)
		{
			$this->db->join(TABLE_USER . ' u ', 'u.employee_id = e.emp_id', 'left');
		}
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$this->db->where('e.emp_status', STATUS_ACTIVE);
		$this->db->where('e.emp_employment_status < ','6');
		$this->db->where('e.emp_id > ','3');
		
		$this->db->order_by('e.emp_id', 'ASC');
		$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;		
	}
	
	function getProbationAssessment($arrWhere = array(), $staffFlag=0)	{
		
		$this->db->distinct();
		$this->db->select(' pa.*, e.emp_id, e.emp_full_name, e.emp_authority_id, e.emp_designation, e.emp_code ');		
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = pa.emp_id', 'left');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.emp_id = e.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$this->db->order_by('pa.created_date', 'DESC');
		
		$objResult = $this->db->get(TABLE_PROBATION_ASSESSMENT . ' pa ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;		
	}
	
	function getTotalProbationAssessment($arrWhere = array(), $staffFlag=0)	{
		
		$this->db->distinct();
		$this->db->select(' count(*) as total_count ');
			
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = pa.emp_id', 'left');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.emp_id = e.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$this->db->order_by('pa.created_date', 'DESC');
		
		$objResult = $this->db->get(TABLE_PROBATION_ASSESSMENT . ' pa ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return (int)$arrResult[0]['total_count'];
	}
}
?>